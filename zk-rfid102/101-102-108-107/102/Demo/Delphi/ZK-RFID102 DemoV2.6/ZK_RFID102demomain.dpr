program ZK_RFID102demomain;



uses
  Forms,
  fZK_RFID102demomain in 'fZK_RFID102demomain.pas' {frmZK_RFID102demomain},
  ZK_RFID102_DLL_Head in 'ZK_RFID102_DLL_Head.pas',
  ZK_RFID102_Head in 'ZK_RFID102_Head.pas',
  fProgress in 'fProgress.pas' {frmProgress},
  locatedlg in 'locatedlg.pas' {locateForm},
  ChangeDlg in 'ChangeDlg.pas' {ChangeIPdlg},
  Setdlg in 'Setdlg.pas' {fSetdlg},
  NhSetDlg in 'NhSetDlg.pas' {fNhSetDlg},
  PhSetDlg in 'PhSetDlg.pas' {fPhSetDlg};

{$R *.res}

begin
  Application.Initialize;
  Application.CreateForm(TfrmZK_RFID102demomain, frmZK_RFID102demomain);
  Application.CreateForm(TfrmProgress, frmProgress);
  Application.CreateForm(TlocateForm, locateForm);
  Application.CreateForm(TChangeIPdlg, ChangeIPdlg);
  Application.CreateForm(TfSetdlg, fSetdlg);
  Application.CreateForm(TfNhSetDlg, fNhSetDlg);
  Application.CreateForm(TfPhSetDlg, fPhSetDlg);
  Application.Run;
end.
