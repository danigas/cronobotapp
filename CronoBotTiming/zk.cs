﻿using System;
using System.Diagnostics;
using System.IO.Ports;

namespace Readers_UHF
{
    public class Zk_Reader
    {
        public int readerAddr = 0x00;
        Tools tools = new Tools();

        /**************************************    OPEN COM PORT     ***********************************/
        //public static int OpenComPort(int Port, ref byte ComAddr, byte Baud, ref int PortHandle);
        public bool ConnectAntenna(SerialPort port, byte ComAddr)
        {
            bool portOpen = false;
            try
            {
                int powerDbm = 0xff, scantime = 0xff;
                if (GetReaderInformation(ComAddr, ref powerDbm, ref scantime, port))
                {
                    portOpen = true;
                }
            }
            catch (Exception ex)
            {
                Debug.Print(ex.Message);
            }

            return portOpen;
        }

        /**************************************            ***********************************/
        // GET READER INFORMATION
        // int GetReaderInformation(ref byte ConAddr, byte[] VersionInfo, ref byte ReaderType, byte[] TrType, ref byte dmaxfre, ref byte dminfre, ref byte powerdBm, ref byte ScanTime, int PortHandle);
        public bool GetReaderInformation(byte devAddr, ref int powerdBm, ref int scanTime, SerialPort port)
        {
            bool getInfo = false;

            byte[] command = { 0x04, devAddr, 0x21, 0x00, 0x00 }; // 0x21 es comando get reader info
            ushort crc = tools.crc16_MCRF4XX(command, 3);
            command[3] = (byte)(crc & 0x00ff);
            command[4] = (byte)(crc >> 8);

            try
            {
                if (port.BytesToRead > 0)
                {
                    string flushPort = port.ReadExisting();
                }

                port.Write(command, 0, command.Length);

                tools.WaitByte(port);

                byte[] bytesRead = new byte[30];
                int justRead = port.Read(bytesRead, 0, port.BytesToRead);
                if (justRead >= 14)
                {
                    if (bytesRead[2] == 0x21 && bytesRead[1] == devAddr && bytesRead[3] == 0x00 && tools.Check_CRC16_MCRF4XX(bytesRead, 14))
                    {
                        //devAddr = bytesRead[1];
                        powerdBm = ((int)bytesRead[10]) * 100 / 30;
                        scanTime = (int)bytesRead[11];
                        getInfo = true;
                    }
                }
            }
            catch { }

            return getInfo;
        }

        /*************************************    SET POWER     **************************/
        //StaticClassReaderB.SetPowerDbm(ref antAdrr, (byte)valorAconvertir2, portIdx))
        public bool SetPower(byte devAdrr, int power, SerialPort port)
        {
            bool powerSet = false;
            byte[] command = { 0x05, devAdrr, 0x2f, (byte)(power * 30 / 100), 0x00, 0x00 };
            ushort crc = tools.crc16_MCRF4XX(command, 4);
            command[4] = (byte)(crc & 0x00ff);
            command[5] = (byte)(crc >> 8);
            try
            {
                if (port.BytesToRead > 0)
                {
                    string flushPort = port.ReadExisting();
                }

                port.Write(command, 0, command.Length);
                tools.WaitByte(port);

                byte[] bytesRead = new byte[30];
                int justRead = port.Read(bytesRead, 0, port.BytesToRead);
                if (justRead >= 6 && bytesRead[1] == devAdrr && tools.Check_CRC16_MCRF4XX(bytesRead, 6))
                {
                    powerSet = true;
                }
            }
            catch { }
            return powerSet;
        }

        /*********************** GET WORK MODE PARAMS *******************/
        //int GetWorkModeParameter(ref byte ConAddr, byte[] Parameter, int PortHandle);
        // workmode = 0: answer (lee tags por comando)
        // workmode = 1: active (lee tags constantemente)
        public bool GetWorkMode(byte devAddr, ref byte workMode, ref byte readMode, ref byte filterTime, SerialPort port)
        {
            bool getParams = false;
            byte[] command = { 0x04, devAddr, 0x36, 0x00, 0x00 };
            ushort crc = tools.crc16_MCRF4XX(command, 3);
            command[3] = (byte)(crc & 0x00ff);
            command[4] = (byte)(crc >> 8);
            try
            {
                port.ReadExisting();

                port.Write(command, 0, command.Length);
                tools.WaitByte(port);

                byte[] bytesRead = new byte[30];
                int justRead = port.Read(bytesRead, 0, port.BytesToRead);
                if (justRead >= 18 && bytesRead[1] == devAddr && tools.Check_CRC16_MCRF4XX(bytesRead, 18))
                {
                    workMode = bytesRead[8];
                    readMode = bytesRead[10];
                    filterTime = bytesRead[13];
                    getParams = true;
                }
                else
                {
                    Debug.Print("GET WORK MODE. Read bytes 18 but:" + justRead.ToString() + " CRC: " + tools.Check_CRC16_MCRF4XX(bytesRead, 18).ToString());
                }
            }
            catch { }
            return getParams;
        }

        /*********************** SET WORK MODE PARAMS *******************/
        //int SetWorkMode(ref byte ConAddr, byte[] Parameter, int PortHandle);
        public bool SetWorkMode(byte devAddr, byte workmode, byte readMode, byte filterTime, SerialPort port)
        {
            bool setParams = false;

            port.ReadExisting();
 
            // SET PARAMS
            //byte[] parameters1 = { Parameter[4], 2, readMode, 0, 1, filterTime};
            byte[] setParam = new byte[6];
            setParam[0] = workmode; // solo altero este parametro, los demas siguen iguales.
            setParam[1] = 2;
            setParam[2] = readMode; //debe ser 4(single tag) o 5(multitag)
            setParam[3] = 0;
            setParam[4] = 1;
            setParam[5] = filterTime; // entre 0 y 255
            byte[] header = { 0x0A, devAddr, 0x35 };
            byte[] command = new byte[11];
            Array.ConstrainedCopy(header, 0, command, 0, header.Length);
            Array.ConstrainedCopy(setParam, 0, command, 3, setParam.Length);
            ushort crc2 = tools.crc16_MCRF4XX(command, 9);
            command[9] = (byte)(crc2 & 0x00ff);
            command[10] = (byte)(crc2 >> 8);
            try
            {
                port.Write(command, 0, command.Length);
                tools.WaitByte(port);

                byte[] bytesRead = new byte[20];
                int just_read = port.Read(bytesRead, 0, port.BytesToRead);
                if (6 == just_read && bytesRead[1] == devAddr && tools.Check_CRC16_MCRF4XX(bytesRead, 6))
                {
                    // check status byte
                    setParams = bytesRead[3] == 0x00 ? true : false;
                }
                else
                {
                    Debug.Print("SET WORK MODE. Read bytes 6 but:" + just_read.ToString() + " CRC: " + tools.Check_CRC16_MCRF4XX(bytesRead, 6).ToString());
                }
            }
            catch { }
            return setParams;
        }

        /*********************** READ SCAN MODE DATA *******************/
        //int ReadActiveModeData(byte[] ModeData, ref int Datalength, int PortHandle);
        public int ReadActiveModeData(ref string[] EPCs, ref string[] devAddr, SerialPort port)
        {
            byte[] readBuff = new byte[1800];
            int readBuffIdx = 0;
            int validDataIdx = 0;

            int nbOfEPCs = new int(); //hace de indice

            while (port.IsOpen && port.BytesToRead > 0)
            {
                //byte[] scanModeData = new byte[1800];
                //si hay datos (bytes) NO (comprobado) van a ser siempre multiplos de 18 (tamaño de lectura de tag)                       
                int nbOfBytesRead = port.Read(readBuff, readBuffIdx, port.BytesToRead);
                tools.DelayBlock(20);// delay/bloqueo del thread sin salir del mismo

                if (nbOfBytesRead > 0)
                {
                   // Array.ConstrainedCopy(readBuff, 0, readBuff, readBuffIdx, nbOfBytesRead);
                    readBuffIdx += nbOfBytesRead;
                    Debug.WriteLine("pack read: " + nbOfBytesRead.ToString());
                    while (readBuffIdx - validDataIdx >= 18)
                    {
                        //check crc
                        byte[] fullMsg = new byte[18];
                        Array.ConstrainedCopy(readBuff, validDataIdx, fullMsg, 0, fullMsg.Length);
                        // usar check crc!
                        if (tools.Check_CRC16_MCRF4XX(fullMsg, fullMsg.Length))
                        {
                            // EPC son 12 bytes 
                            byte[] epc = new byte[12];
                            Array.ConstrainedCopy(fullMsg, 4, epc, 0, epc.Length);
                            Array.Reverse(epc);
                            devAddr[nbOfEPCs] = fullMsg[1].ToString(); // relacion con variable devAddr??
                            EPCs[nbOfEPCs++] = BitConverter.ToUInt64(epc, 0).ToString("X");

                            // cumple CRC! incremento el indice de comienzo de data valida
                            // todo regio!
                            validDataIdx += 18;

                            //solo 20 epcs
                            /*if(readBuffIdx == validDataIdx)
                            {
                                readBuffIdx = 0;
                                validDataIdx = 0;
                                readBuff = new byte[readBuff.Length];                                
                            }
                            /*
                            if (nbOfEPCs == 20)
                            {
                                break;
                            }
                            */
                        }
                        else
                        {
                            //si no comple CRC, moverme un byte mas adelante, esperar y recomenzar
                            validDataIdx++;
                            tools.DelayBlock(20);
                            Debug.WriteLine("resync and wait");
                        }
                    }
                    if (readBuffIdx - validDataIdx < 18 && readBuffIdx - validDataIdx != 0)
                    {
                        tools.DelayBlock(20);
                        Debug.WriteLine("wait for more bytes to complete");
                    }
                }
            }
 
            return nbOfEPCs;
        }


        /*********************** READ ANSWER MODE DATA *******************/
        // int Inventory_G2(ref byte ConAddr, byte AdrTID, byte LenTID, byte TIDFlag, byte[] EPClenandEPC, ref int Totallen, ref int CardNum, int PortHandle);
        public string AnswerModeData(byte devAddr, SerialPort port)
        {
            //Int64 retval = 0;
            string retVal = "";
            byte[] command = { 0x04, devAddr, 0x01, 0x00, 0x00 };
            ushort crc = tools.crc16_MCRF4XX(command, 3);
            command[3] = (byte)(crc & 0x00ff);
            command[4] = (byte)(crc >> 8);
            try
            {
                port.ReadExisting();

                port.Write(command, 0, command.Length);
                tools.WaitByte(port);

                byte[] bytesRead = new byte[200];
                int just_read = port.Read(bytesRead, 0, port.BytesToRead);
                if (6 == just_read && bytesRead[1] == devAddr && tools.Check_CRC16_MCRF4XX(bytesRead, 6))
                {
                    // sin tags en el campo
                    // check status byte  
                    if (bytesRead[3] == 0xfb)
                    {
                        //retval = -1;
                        retVal = "";
                    }
                }
                else if (20 == just_read && bytesRead[1] == devAddr && tools.Check_CRC16_MCRF4XX(bytesRead, 20))
                {
                    // tag en el campo! decode
                    // si bien EPC son 12 bytes
                    byte[] epc = new byte[12];
                    Array.ConstrainedCopy(bytesRead, 6, epc, 0, epc.Length);
                    retVal = tools.ByteArrayToHexString(epc);
                }
            }
            catch { }

            return retVal;
        }

        /*********************** WRITE CARD *******************/
        // public static int WriteCard_G2(ref byte ConAddr, byte[] EPC, byte Mem, byte WordPtr, byte Writedatalen, byte[] Writedata, byte[] Password, byte maskadr, byte maskLen, byte maskFlag, int WrittenDataNum, byte EPClength, ref int errorcode, int PortHandle);
        public bool WriteCard_G2(byte devAddr, string EPC_original, string EPC_new, SerialPort port)
        {
            bool writeOk = false;
            byte[] epcOriginal = tools.HexStringToByteArray(EPC_original);
            byte[] epcNew = tools.HexStringToByteArray(EPC_new);

            byte[] command = { 0x26, devAddr, 0x03, 0x07, 0x06, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0x01, 0x01, 0x30, 0x00, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0, 0, 0, 0, 0xff, 0xff };
            Array.ConstrainedCopy(epcOriginal, 0, command, 5, epcOriginal.Length);
            Array.ConstrainedCopy(epcNew, 0, command, 21, epcNew.Length);
            ushort crc = tools.crc16_MCRF4XX(command, (uint)command.Length - 2);
            command[command.Length - 2] = (byte)(crc & 0x00ff);
            command[command.Length - 1] = (byte)(crc >> 8);

            try
            {
                port.ReadExisting();
                port.Write(command, 0, command.Length);
                tools.WaitByte(port);

                byte[] bytesRead = new byte[20];
                int just_read = port.Read(bytesRead, 0, port.BytesToRead);
                if (6 == just_read && tools.Check_CRC16_MCRF4XX(bytesRead, 6))
                {
                    // check status byte
                    writeOk = bytesRead[3] == 0x00 ? true : false;
                }
            }
            catch { }

            return writeOk;
        }

        public bool StopAntenna(SerialPort serialPortx, byte antAdrr)
        {
            bool stopreadingOk = false;
            byte workmode = 0xff, readMode = 0xff, filterTime = 0xff;

            if (GetWorkMode(antAdrr, ref workmode, ref readMode, ref filterTime, serialPortx))
            {
                workmode = 0;// set answer Mode
                if (SetWorkMode(antAdrr, workmode, readMode, filterTime, serialPortx))
                {
                    stopreadingOk = true;
                }
            }

            return stopreadingOk;
        }

        public bool StartAntenna(SerialPort serialPortx, byte antAdrr)
        {
            bool startReadingOk = false;
            byte workmode = 0xff, readMode = 0xff, filterTime = 0xff;

            if (GetWorkMode(antAdrr, ref workmode, ref readMode, ref filterTime, serialPortx))
            {
                workmode = 1; // scan/response mode (start reading)
                if (SetWorkMode(antAdrr, workmode, readMode, filterTime, serialPortx)) // set Response Working Mode
                {
                    startReadingOk = true;
                }
            }
  
            return startReadingOk;
        }
    }
}
