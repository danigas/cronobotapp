﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;

namespace Readers_UHF
{
    class Tools
    {
        public void DelayBlock(int delayMs)
        {
            // delay/bloqueo del thread sin salir del mismo
            Stopwatch i_Watch = new Stopwatch();
            i_Watch.Start();
            while (i_Watch.ElapsedMilliseconds < delayMs) ;
            i_Watch.Stop();
        }

        public void WaitByte(SerialPort serialPort)
        {
            Stopwatch i_Watch = new Stopwatch();
            int MAX_DELAY = 250;
            i_Watch.Start();
            while (serialPort.BytesToRead == 0 && i_Watch.ElapsedMilliseconds < MAX_DELAY) ;
            DelayBlock(30);
            i_Watch.Stop();
            Debug.Print("wait byte ms:" + i_Watch.ElapsedMilliseconds.ToString());
        }

        // funciones choreadas de algun lado en internet
        // calculo CRC-16/MCRF4XX
        public UInt16 crc16_MCRF4XX(byte[] pData, UInt32 Len)
        {
            UInt16 Crc = 0xffff;

            for (UInt32 i = 0; i < Len; i++)
            {
                Crc = Utils_CRC16_MCRF4XX(Crc, pData[i]);
            }
            return (Crc);
        }

        private UInt16 Utils_CRC16_MCRF4XX(UInt16 Crc, byte Byte)
        {
            //make byte 16 bit format
            UInt16 TempByte = (UInt16)Byte;

            for (byte i = 0; i < 8; i++)
            {
                if ((Crc & 0x0001) == (TempByte & 0x0001))
                {
                    //right shift crc
                    Crc >>= 1;
                    //right shift data
                    TempByte >>= 1;
                }
                else
                {
                    Crc >>= 1;
                    TempByte >>= 1;
                    Crc = (ushort)(Crc ^ 0x8408);
                }
            }

            return Crc;
        }

        public ushort crc16_BUYPASS(byte[] data, int idx,int len)
        {
            ushort crc = 0x0000;
            for (int i = idx; i < idx + len; i++)
            {
                crc ^= (ushort)(data[i] << 8);
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x8000) > 0)
                        crc = (ushort)((crc << 1) ^ 0x8005);
                    else
                        crc <<= 1;
                }
            }
            //return crc.ToString("X4");
            return crc;
        }

        // chequeo CRC. Toma todos los elementos, entiende que los dos ultimos son el CRC
        public bool Check_CRC16_MCRF4XX(byte[] buffer, int len)
        {
            bool crcOK = false;

            if (len > 2 && crc16_MCRF4XX(buffer, (uint)len - 2) == (UInt16)((buffer[len - 1] << 8) + buffer[len - 2]))
            {
                crcOK = true;
            }

            return crcOK;
        }

        // int len: includes crc bytes
        public bool Check_CRC16_BUYPASS(byte[] buffer, int idx, int len)
        {
            ushort crcRead = (ushort)((buffer[len - 1] << 8) + buffer[len]);
            ushort crcCalc = crc16_BUYPASS(buffer, idx, len - 2);
            return crcRead == crcCalc;
        }

        public byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        public string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();

        }

        public byte[] EscapeChars(byte[] byteArray, ref int escapedChars)
        {
            byte[] startByte = new byte[50000];
            int startByteIdx = 0;
            byte[] finalArray = { 0 };
            bool starter = false;

            if (byteArray[0] == 0x55)
            {
                starter = true;
                for (int i = 0; i < byteArray.Length; i++)
                {
                    if (byteArray[i] == 0x56 && byteArray[i + 1] == 0x56)
                    {
                        // habemus un 0x55
                        startByte[startByteIdx++] = 0x55;
                        i++;
                        escapedChars++;
                    }
                    else if (byteArray[i] == 0x56 && byteArray[i + 1] == 0x57)
                    {
                        // habemus un 0x56
                        startByte[startByteIdx++] = 0x56;
                        i++;
                        escapedChars++;
                    }
                    else if (starter == true && i != 0 && byteArray[i] == 0x55)
                    {
                        // comienza otro string EPC
                        break;
                    }
                    else
                    {
                        startByte[startByteIdx++] = byteArray[i];
                    }
                }
                finalArray = new byte[startByteIdx];
                Array.ConstrainedCopy(startByte, 0, finalArray, 0, startByteIdx);
            }
            return finalArray;
        }
    }
}
