﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.IO;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using ExcelLibrary.SpreadSheet;
using System.Management;
using System.Threading;
using System.Globalization;
using System.Data.Sql;
using System.Configuration;
using System.Diagnostics;
using Readers_UHF;
using System.Security.Cryptography;


namespace CronoBotTiming
{
    public partial class main : Form
    {
        enum AntType
        {
            UNKNOWN,
            ZK,
            INNOTECH,
            KAELCO,
            YANPODO_USB
        }

        // variables globales
        string supportFilesPath = "";
        string gConnectionString;
        string gServerName;

        string gFileNameIngestor = "";
        List<string> gColNames = new List<string>();

        //TimeSpan gVueltaMinima = new TimeSpan(0, 12, 0); // tiempo minimo de vuelta. Default: 15 min 

        // variables control antenas
        volatile bool[] gAntActive = { false, false, false, false };
        byte[] gAntAdrr = { 1, 2, 3, 4 };

        SerialPort[] gPortsAvailable = new SerialPort[4];
        SerialPort[] gPortsConnected = new SerialPort[4];
        bool[] gAntEsWireless = { false, false, false, false };
        AntType[] gAntType = { AntType.UNKNOWN, AntType.UNKNOWN, AntType.UNKNOWN, AntType.UNKNOWN }; //ZK = 1, INNO = 2, Kaelco = 3, YopondoUSB = 4

        // variables text y labels formulario
        Label[] gComLabel = new Label[4];
        Label[] gConnectLabel1 = new Label[4];
        Label[] gConnectLabel2 = new Label[4];
        TextBox[] gLeerPotenciaText = new TextBox[4];

        bool[] gAntConnected = { false, false, false, false };
        SerialPort port1 = new SerialPort();
        SerialPort port2 = new SerialPort();
        SerialPort port3 = new SerialPort();
        SerialPort port4 = new SerialPort();
        public Action[] methods = new Action[4];

        //variables globales eventos en RAM
        List<string> evId = new List<string>(10);
        List<string> evName = new List<string>(10);
        List<string> evType = new List<string>(10);
        List<string> evStatus = new List<string>(10);
        List<string> evStart = new List<string>(10);
        List<string> evKey = new List<string>(10);
        List<string> evPrint = new List<string>(10);
        List<TimeSpan> evVuelta = new List<TimeSpan>(10);
        List<string> gCatNombres = new List<string>(200);
        List<DateTime> gCatLargadas = new List<DateTime>(200);

        // variables viewer
        string[] gCats = new string[30];
        int gCatsCount = 0;
        int gCatActual = 0;
        string gEvName = "";

        //variables Lapeo Automatico
        string[] catLapeo = new string[30];
        string[] catFinish = new string[30];
        int[] vueltaActual = new int[30];
        TimeSpan[] tiempoVuelta = new TimeSpan[30];
        DateTime[] ultimoPaso = new DateTime[30];
        DateTime[] horaLapeo = new DateTime[30];
        int catCountLapeo = 0;
        int catCountFinish = 0;

        // variables chip leidos
        static int gBuffLen = 20;
        string[] gNumerosLeidos = new string[gBuffLen];
        DateTime[] gHorasDePaso = new DateTime[gBuffLen];
        int[] gPuestoDeControl = new int[gBuffLen];

        string[] gNumType = new string[gBuffLen]; // puede ser numero chip team_id
        volatile int gReadIdx = 0;
        volatile int gAddIdx = 0;
        volatile int gAntennaActivityIdx = 0;

        // variables update datagrid "eventos en curso" mediante timer2
        bool gUpdateDataGrid = false;
        bool gUpdateClasificacion = false;
        string gCategoria = "";
        string gEvento = "";

        Thread TriggerAntenna;
        Thread AddChip;
        volatile bool gThreadActive = false;

        Zk_Reader ZkReader = new Zk_Reader();
        Innovatech InnoReader = new Innovatech();
        UsbReader UsbReader = new UsbReader();

        public main()
        {
            InitializeComponent();

            // Serial Ports
            gPortsAvailable[0] = port1;
            gPortsAvailable[1] = port2;
            gPortsAvailable[2] = port3;
            gPortsAvailable[3] = port4;

            // labels
            gComLabel[0] = label29;
            gComLabel[1] = label49;
            gComLabel[2] = label62;
            gComLabel[3] = label66;

            gConnectLabel1[0] = label20;
            gConnectLabel1[1] = label47;
            gConnectLabel1[2] = label60;
            gConnectLabel1[3] = label64;

            gConnectLabel2[0] = label53;
            gConnectLabel2[1] = label72;
            gConnectLabel2[2] = label73;
            gConnectLabel2[3] = label44;

            gLeerPotenciaText[0] = textBox17;
            gLeerPotenciaText[1] = textBox25;
            gLeerPotenciaText[2] = textBox26;
            gLeerPotenciaText[3] = textBox27;

            //methods[0] = ClickBotonStop1;

            // create support files directory/folder
            string appFileName = Environment.GetCommandLineArgs()[0];
            string directory = Path.GetDirectoryName(appFileName);
            int mitadPath = directory.Length / 2;
            label89.Text= directory.Substring(0,mitadPath) +"\n"+ directory.Substring(mitadPath);

            supportFilesPath = directory + "\\Support_Files";
            System.IO.Directory.CreateDirectory(supportFilesPath);

            // finicializaciones data base
            // chequear server activo
            comboBox45.Text = ConfigurationManager.AppSettings["server"];

            // boton "connect server"
            button75_Click(null, null);

            // tip de largada
            checkBox3.Checked = ConfigurationManager.AppSettings["largadaInd"] == "True" ? true : false;

            dateTimePicker1.Format = DateTimePickerFormat.Custom;
            dateTimePicker1.CustomFormat = "dd/MM/yyyy";

            dateTimePicker2.Format = DateTimePickerFormat.Custom;
            dateTimePicker2.CustomFormat = "dd/MM/yyyy HH:mm:ss";

            dateTimePicker3.Format = DateTimePickerFormat.Custom;
            dateTimePicker3.CustomFormat = "dd/MM/yyyy";

            dateTimePicker4.Format = DateTimePickerFormat.Custom;
            dateTimePicker4.CustomFormat = "dd/MM/yyyy HH:mm:ss";

            dateTimePicker5.Format = DateTimePickerFormat.Custom;
            dateTimePicker5.CustomFormat = "HH:mm:ss dd/MM/yyyy";

            dateTimePicker6.Format = DateTimePickerFormat.Custom;
            dateTimePicker6.CustomFormat = "HH:mm:ss";

            dateTimePicker7.Format = DateTimePickerFormat.Custom;
            dateTimePicker7.CustomFormat = "dd/MM/yyyy HH:mm:ss";

            dateTimePicker8.Format = DateTimePickerFormat.Custom;
            dateTimePicker8.CustomFormat = "HH:mm:ss";

            dateTimePicker9.Format = DateTimePickerFormat.Custom;
            dateTimePicker9.CustomFormat = "dd/MM/yyyy";

            // iniciar timer que muestra el cronometro de carrera
            timer1.Enabled = true;
            timer1.Start();

            //button37.Enabled = false;
            //button38.Enabled = false;
            //textBox21.Enabled = false;

            // GSM/EEPROM Buttons
            button84.Enabled = false;
            button113.Enabled = false;
            button114.Enabled = false;
            button115.Enabled = false;
            button116.Enabled = false;
            button86.Enabled = false;
            numericUpDown6.Enabled = false;
            button83.Enabled = false;
            button88.Enabled = false;
            textBox33.Enabled = false;

            label80.Text = "Actual: ?";

            //timer lapeo
            //timer5.Enabled = true;
            //button110.Text = "Stop Lapeo";
            //checkBox2.CheckState = CheckState.Checked;

            // threads!
            TriggerAntenna = new Thread(Thread1);

            AddChip = new Thread(Thread2);
            AddChip.Start(); // este thread debe correr continuamente independiente de las antenas

            //UpdateClasificacion = new Thread(Thread3);
            //UpdateClasificacion.Start();

            timer2.Start(); // timer para updatear datagrid finishers

            // auto conectar antenas
            //AutoConnectAntenas();
            AutoConnectAntenas();

            try
            {
                textBox30.AppendText(File.ReadAllText(supportFilesPath + "\\timestamps.txt"));
            }
            catch {}
            
            // generar la tabla 'resultados'
            foreach(string eventName in evName)
            {
                comboBox30.Text = eventName;
                comboBox29.Text = "General";
                button36_Click_2(null, null);
            }

            tabControl1.TabPages.Remove(tab1_Listado_General);
            //tabControl1.TabPages.Remove(campeonatos);
            //tabControl1.TabPages.Remove(tabPage1);
            //tabControl1.TabPages.Remove(tabPage5);

            if(UsbReader.ConnectAntenna())
            {
                groupBox10.Enabled = true;
                label125.Text = "USB READER";
            }

            toggleView(false);

#if (DEBUG)
            //checkLicencia();
#else
            checkLicencia();
#endif
        }

        private void displayTable(String tableName, DataGridView dataGridView, bool autoSizeColumns)
        {
            SqlConnection connection = new SqlConnection(gConnectionString);
            if (sqlServerConnect(connection))
            {
                SqlCommand cmd = connection.CreateCommand();

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [" + tableName + "]";
                try
                {
                    cmd.ExecuteNonQuery();
                    DataTable dta = new DataTable();
                    SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                    dataadapt.Fill(dta);
                    dataGridView.DataSource = dta;

                    if (autoSizeColumns)
                    {
                        dataGridView.AutoResizeColumns();
                        dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                    }

                    // mostrar numero de fila
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        row.HeaderCell.Value = (row.Index + 1).ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connection.Close();
            }
        }

        private void poblarComboBoxColumna(string tabla, string columna, ComboBox comboBox, string eventName)
        {
            // poblar comboBox con nombres extraidos de "columna" de la tabla "tabla" 
            SqlConnection connection = new SqlConnection(gConnectionString);
            if (sqlServerConnect(connection))
            {
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;

                if (eventName == null)
                {
                    cmd.CommandText = "SELECT DISTINCT " + columna + " FROM " + tabla + " ";
                }
                else
                {
                    string eventId = buscarIdEventoPorNombre(eventName);
                    cmd.CommandText = " SELECT DISTINCT " + columna + " FROM " + tabla + " WHERE id = '" + eventId + "' ";
                    comboBox.Items.Add("");
                }

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    comboBox.Items.Add(reader[columna].ToString());
                }

                connection.Close();
            }
        }

        private void mostrarTablaChildEvento(string eventNameStr, string tableNameStr, DataGridView dataGridView, bool autoSizeColumns)
        {
            if (eventNameStr != "")
            {
                // Buscar nombre evento cuyo nombre es eventNameString
                string eventIdString = buscarIdEventoPorNombre(eventNameStr);

                // Mostrar la sub tabla "tableNameStr" para el evento cuyo nombre es "eventNameStr"
                SqlConnection connection = new SqlConnection(gConnectionString);
                if (sqlServerConnect(connection))
                {
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [" + tableNameStr + "] WHERE id = '" + eventIdString + "'";

                    try
                    {
                        cmd.ExecuteNonQuery();
                        DataTable dta = new DataTable();
                        SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                        dataadapt.Fill(dta);
                        dataGridView.DataSource = dta;

                        if (autoSizeColumns)
                        {
                            dataGridView.AutoResizeColumns();
                            dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                        }

                        // mostrar numero de fila
                        foreach (DataGridViewRow row in dataGridView.Rows)
                        {
                            row.HeaderCell.Value = (row.Index + 1).ToString();
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    connection.Close();
                }
            }
        }

        private void mostrarTablaInscriptos()
        {
            string eventIdString = buscarIdEventoPorNombre(comboBox9.Text);

            SqlConnection connection = new SqlConnection(gConnectionString);
            if (sqlServerConnect(connection))
            {
                SqlCommand cmd1 = connection.CreateCommand();
                cmd1.CommandType = CommandType.Text;
                cmd1.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.team_id,finishers.categoria,corredores.sexo,corredores.ciudad,corredores.dni FROM corredores, finishers WHERE corredores.dni = finishers.dni AND finishers.id = '" + eventIdString + "'";

                try
                {
                    cmd1.ExecuteNonQuery();
                    DataTable dta = new DataTable();
                    SqlDataAdapter dataadapt = new SqlDataAdapter(cmd1);
                    dataadapt.Fill(dta);
                    dataGridView3.DataSource = dta;
                    dataGridView3.AutoResizeColumns();
                    dataGridView3.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                    // mostrar numero de fila
                    foreach (DataGridViewRow row in dataGridView3.Rows)
                    {
                        row.HeaderCell.Value = (row.Index + 1).ToString();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connection.Close();
            }
        }

        private string buscarIdEventoPorNombre(string nombreEvento)
        {
            if (nombreEvento != "")
            {
                try
                {
                    return evId[evName.IndexOf(nombreEvento)].ToString();
                }
                catch
                {
                }
            }

            return "";
        }

        private void calcularCategoria(string idEvento, DateTime fechaDeNac, string sexo, ref string[] catArray)
        {
            //string[] catArray = new string[10];
            int i = 0;
            // edad segun la clase o año de nacimiento
            int edad = DateTime.Now.Year - fechaDeNac.Year;

            if (checkBox17.CheckState == CheckState.Checked)
            {
                // categorias por edad actual
                if(fechaDeNac.Date > DateTime.Now.AddYears(-edad))
                {
                    edad--;
                }
            }

            if (edad > 0) // fechaDeNac.Year pude ser 2019. Eso pasa cuando no se tiene la f de nac del participante, se pone la fecha actual (hoy). Por ende la edad podria ser 0.
            {
                SqlConnection connection = new SqlConnection(gConnectionString);
                sqlServerConnect(connection);
                SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT categoria,inicio,fin FROM [categorias] WHERE id = '" + idEvento + "' AND sexo = '" + sexo + "' ORDER BY inicio ASC, categoria ASC";
                SqlDataReader tablaCategorias;

                try
                {
                    //comboBox5.Items.Clear();
                    tablaCategorias = cmd.ExecuteReader();
                   
                    while (tablaCategorias.Read())
                    {
                        int edadInicio = (int)tablaCategorias["inicio"];
                        int edadFin = (int)tablaCategorias["fin"];

                        if ((edad >= edadInicio) && (edad <= edadFin))
                        {
                            catArray[i++] = (string)tablaCategorias["categoria"];             
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connection.Close();
            }
        }

        private bool chequearValorExisteEnColumna(string tabla, string nombreColumna, string valorAbuscar)
        {
            bool returnVal = false;

            if (valorAbuscar == "")
            {
                return returnVal;
            }

            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            sqlServerConnect(connection);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [" + tabla + "] WHERE " + nombreColumna + " = '" + valorAbuscar + "'";
            SqlDataReader readerTabla;

            try
            {
                readerTabla = cmd.ExecuteReader();

                if (readerTabla.Read())
                {
                    //MessageBox.Show(nombreColumna + ": " + valorAbuscar +" ya existe en este evento / duplicado");
                    returnVal = true;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
            return returnVal;
        }


        private bool agregarCorredor(string nombre, string dni, string email, DateTime fDeNac, string sexo, string ciudad, string team)
        {
            bool returnVal = true;

            SqlConnection connection = new SqlConnection(gConnectionString);
            sqlServerConnect(connection);
            SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO [corredores] VALUES ('" + nombre + "','" + fDeNac.ToString("MM-dd-yyyy") + "','" + dni + "','" + email + "','" + sexo + "','" + ciudad + "', '" + team + "', '', '','','')";

            try
            {
                cmd.ExecuteNonQuery();
                MessageBox.Show("Se agrego corredor a la base de datos principal");
            }
            catch (SqlException)
            {
                MessageBox.Show("DNI ya existe en base de datos principal");
                returnVal = false;
            }

            connection.Close();

            return returnVal;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if ((textBox11.Text != "")
                && (comboBox3.Text != "") && (textBox10.Text != "")
                && (dateTimePicker1.Value.Date.ToString() != ""))
            {
                if (agregarCorredor(textBox11.Text, textBox10.Text, textBox9.Text, dateTimePicker1.Value.Date, comboBox3.Text, textBox2.Text, textBox15.Text))
                {
                    textBox2.Text = "";
                    textBox10.Text = "";
                    textBox9.Text = "";
                    textBox11.Text = "";
                    comboBox3.Text = "";
                    textBox15.Text = "";
                    dateTimePicker1.Value = DateTime.Now;

                    displayTable("corredores", dataGridView_Corredores, false);
                }
            }
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("ESTA A PUNTO DE HACER CAGADAS", "Ojo, va a borrar info importante" + comboBox6.Text, MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                DialogResult dialogResult1 = MessageBox.Show("Seguro???", "Ojo, va a borrar info importante" + comboBox6.Text, MessageBoxButtons.YesNo);

                if (dialogResult1 == DialogResult.Yes)
                {                    
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    sqlServerConnect(connection);
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM [corredores]";
                    cmd.ExecuteNonQuery();
                    connection.Close();
                    displayTable("corredores", dataGridView_Corredores, false);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                SqlConnection connection = new SqlConnection(gConnectionString);
                if (sqlServerConnect(connection))
                {
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [corredores] WHERE dni = '" + textBox1.Text + "'";
                    cmd.ExecuteNonQuery();

                    DataTable dta = new DataTable();
                    SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                    dataadapt.Fill(dta);
                    dataGridView_Corredores.DataSource = dta;
                    connection.Close();
                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            sqlServerConnect(connection);
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT * FROM [Corredores] ORDER BY team_id ASC";
            try
            {
                cmd.ExecuteNonQuery();
                DataTable dta = new DataTable();
                SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                dataadapt.Fill(dta);
                dataGridView_Corredores.DataSource = dta;

                dataGridView_Corredores.AutoResizeColumns();
                dataGridView_Corredores.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                // mostrar numero de fila
                foreach (DataGridViewRow row in dataGridView_Corredores.Rows)
                {
                    row.HeaderCell.Value = (row.Index + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                DialogResult dialogResult = MessageBox.Show("", "Borrar Registro " + textBox1.Text + "?", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    SqlConnection connection = new SqlConnection(gConnectionString);
                    sqlServerConnect(connection);
                    SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM [corredores] WHERE dni = '" + textBox1.Text + "'";
                    cmd.ExecuteNonQuery();
                    displayTable("corredores", dataGridView_Corredores, false);
                    connection.Close();
                }
            }
        }

        // Funcion actualizar registro DB principal
        private void button6_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "UPDATE [corredores] SET nombre ='" + textBox11.Text + "', nacimiento = '" + dateTimePicker1.Value.Date.ToString("dd-MM-yyyy") + "', dni = '" + textBox10.Text + "', email = '" + textBox9.Text + "', sexo = '" + comboBox3.Text + "',ciudad = '" + textBox2.Text + "' WHERE dni = '" + textBox1.Text + "'";
                sqlServerConnect(connection);

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connection.Close();
                displayTable("corredores", dataGridView_Corredores, false);
            }
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker2_ValueChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label7_Click(object sender, EventArgs e)
        {

        }

        private void tabPage2_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click_1(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            if ((comboBox43.Text != "") && (dateTimePicker2.Value.Date.ToString() != "") && (comboBox31.Text != ""))
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO[eventos] VALUES('" + comboBox43.Text + "', '" + dateTimePicker2.Value.ToString("MM-dd-yyyy HH:mm:ss") + "', '" + textBox4.Text + "', 'a venir', '" + comboBox31.Text + "', '" + textBox39.Text + "', '" + dateTimePicker8.Value.ToString("HH:mm:ss") + "', '" + textBox41.Text + "')";

                try
                {
                    sqlServerConnect(connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();

                    //agregar items a los comboBox que almacenan eventos
                    comboBox2.Items.Add(comboBox43.Text);
                    comboBox6.Items.Add(comboBox43.Text);
                    comboBox9.Items.Add(comboBox43.Text);
                    comboBox34.Items.Add(comboBox43.Text);
                    comboBox43.Items.Add(comboBox43.Text);

                    comboBox43.Text = "";
                    textBox4.Text = "";
                    comboBox31.Text = "";
                    dateTimePicker2.Value = DateTime.Now;
                }
                catch (SqlException ex)
                {
                    MessageBox.Show("Fallo al crear Evento: " + ex.Message);
                    // Resetear el indice de las filas
                    // solo la tabla eventos tiene al menos 1 record
                    cmd.CommandText = "DECLARE @MaxID INT; SELECT @MaxID = MAX(id) FROM [eventos]; DBCC CHECKIDENT (eventos, RESEED, @MaxID)";
                    try
                    {
                        sqlServerConnect(connection);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                    catch (SqlException exep)
                    {
                        MessageBox.Show("Segunda excepcion: " + exep.Message);
                        cmd.CommandText = "DBCC CHECKIDENT(eventos, RESEED, 0)";
                        cmd.ExecuteNonQuery();
                    }
                }

                displayTable("eventos", dataGridView1, true);
                LlenarEventosRam();
            }
        }

        private void button9_Click(object sender, EventArgs e)
        {
            if ((numericUpDown1.Value >= 0) && (numericUpDown2.Value > 0)
                && (comboBox1.Text != "") && (comboBox2.Text != "")
                && (numericUpDown2.Value >= numericUpDown1.Value))
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;

                // Buscar el id del evento señalado en comboBox2
                // el id evento es primary key para las categorias
                string eventId = buscarIdEventoPorNombre(comboBox2.Text);
                //Nombre de la categoria
                string nombreCategoria = "";

                if (eventId != "")
                {
                    string sexo_string = (comboBox1.Text == "masculino")? "MASC" : "FEM";

                    // si textBox19 tiene un valor => override calculo de nombre de categoria
                    if (textBox19.Text != "")
                    {
                        nombreCategoria = textBox19.Text;
                    }
                    else
                    {
                        if (numericUpDown2.Value >= 100)
                        {
                            nombreCategoria = numericUpDown1.Value.ToString() + "_AÑOS_Y_MAS_" + sexo_string + "_" + evKey[evId.IndexOf(eventId)];
                        }
                        else
                        {
                            // crear string con el nombre de la categoria a agregar
                            string inicioCat = numericUpDown1.Value.ToString() + "_A_";

                            if (numericUpDown1.Value == 0)
                            {
                                inicioCat = "HASTA_";
                            }

                            nombreCategoria = inicioCat + numericUpDown2.Value.ToString() + "_AÑOS_" + sexo_string + "_" + evKey[evId.IndexOf(eventId)];
                        }
                    }

                    //Buscar categoria con el mismo nombre (no pueden tener nombres repetidos ni en eventos diferentes
                    cmd.CommandText = "SELECT * FROM categorias WHERE categoria = '" + nombreCategoria + "'";
                    sqlServerConnect(connection);
                    SqlDataReader nombreReader = cmd.ExecuteReader();
                    Boolean categoriaExiste = false;
                    if (nombreReader.Read())
                    {
                        categoriaExiste = true;
                    }
                    connection.Close();

                    if (!categoriaExiste)
                    {
                        if (numericUpDown3.Value > 0)
                        {
                            sqlServerConnect(connection);
                            //agregar record en tabla "categoria"
                            cmd.CommandText = "INSERT INTO[categorias] VALUES('" + eventId + "','" + nombreCategoria + "', '" + numericUpDown1.Value.ToString() + "', '" + numericUpDown2.Value.ToString() + "', '" + comboBox1.Text + "', '" + numericUpDown3.Value.ToString() + "','')";

                            try
                            {
                                cmd.ExecuteNonQuery();
                                textBox19.Text = "";
                                numericUpDown3.Value = 1;
                            }
                            catch (Exception ex)
                            {
                                //MessageBox.Show(ex.Message);
                                cmd.CommandText = "INSERT INTO[categorias] VALUES('" + eventId + "','" + nombreCategoria + "', '" + numericUpDown1.Value.ToString() + "', '" + numericUpDown2.Value.ToString() + "', '" + comboBox1.Text + "', '" + numericUpDown3.Value.ToString() + "','','')";

                                try
                                {
                                    cmd.ExecuteNonQuery();
                                    textBox19.Text = "";
                                    numericUpDown3.Value = 1;
                                }
                                catch (Exception exi)
                                {
                                    MessageBox.Show(exi.Message);
                                }
                            }

                            connection.Close();

                            // refrescar y mostrar la tabla categorias para el evento sobre el cual se esta agregando la categoria
                            mostrarTablaChildEvento(comboBox2.Text, "categorias", dataGridView2, true);
                        }
                    }
                    else
                    {
                        MessageBox.Show("Ya existe una categoria con ese nombre");
                    }
                }
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("ESTA A PUNTO DE HACER CAGADAS", "Ojo, va a borrar info importante" + comboBox6.Text, MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                string eventId = buscarIdEventoPorNombre(comboBox34.Text);
                if (eventId != "")
                {
                    cmd.CommandText = "DELETE FROM [eventos] where id = '" + eventId + "'";
                    sqlServerConnect(connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    connection.Close();

                    cmd.CommandText = "SELECT id FROM [eventos]";
                    sqlServerConnect(connection);
                    SqlDataReader reader = cmd.ExecuteReader();
                    bool tableEmpty = true;

                    if (reader.Read())
                    {
                        tableEmpty = false;
                    }
                    connection.Close();

                    if (tableEmpty)
                    {
                        // Resetear el indice de las filas en caso de haber borrado todos los eventos existentes
                        sqlServerConnect(connection);
                        cmd.CommandText = "DBCC CHECKIDENT (eventos, RESEED, 0)";
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                    LlenarEventosRam();
                    displayTable("eventos", dataGridView1, true);
                }
            }
        }

        private void button11_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("ESTA A PUNTO DE HACER CAGADAS", "Ojo, va a borrar info importante" + comboBox6.Text, MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string eventId = buscarIdEventoPorNombre(comboBox34.Text);
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "DELETE FROM [categorias] WHERE id = '" + eventId + "'";
                sqlServerConnect(connection);
                cmd.ExecuteNonQuery();
                connection.Close();
                displayTable("categorias", dataGridView2, true);
            }
        }

        private void label9_Click(object sender, EventArgs e)
        {

        }

        private void label18_Click(object sender, EventArgs e)
        {

        }

        private void textBox13_TextChanged(object sender, EventArgs e)
        {
            //autogenerar chip textBox8.Text = textBox13.Text;
            if (checkBox19.CheckState == CheckState.Checked)
            {
                string texto = "";

                foreach (char digito in textBox13.Text)
                {
                    //if (digito >= '0' && digito <= '9')
                    {
                        texto += digito;
                    }
                }

                textBox8.Text = texto;
            }

            //autogenerar team ID
            if (checkBox1.CheckState == CheckState.Checked)
            {
                string texto = "";

                foreach (char digito in textBox13.Text)
                {
                    if (digito >= '0' && digito <= '9')
                    {
                        texto += digito;
                    }
                }

                textBox28.Text = texto;
            }
        }

        private void textBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            if ((comboBox44.Text != "")
                && (textBox7.Text != "") /*&& (textBox8.Text != "")*/
                && (dateTimePicker3.Text != "") && (comboBox4.Text != "")
                && (textBox13.Text != "") && (comboBox9.Text != ""))
            {
                // Conseguir id evento donde el participante sera inscripto
                string idEvento = buscarIdEventoPorNombre(comboBox9.Text);

                // si comboBox5 tiene un valor, forzar ese valor como categoria. 
                // Si comboBox5 = "", calcular categoria automaticamente.
                string categoria;

                if (comboBox5.Text != "")
                {
                    // TODO: chequear validez de la categoria
                    categoria = comboBox5.Text;
                }
                else // calcular la categoria.
                {
                    DateTime nacimiento = dateTimePicker3.Value;
                    string[] catArray = new string[10];
                    calcularCategoria(idEvento, nacimiento, comboBox4.Text, ref catArray);
                    categoria = catArray[0];
                }

                if (categoria != "")
                {
                    // antes de insertar, se deberia chequear que dni OR numero OR chip
                    // no existan ya para el evento seleccionado
                    if (!chequearValorExisteEnColumna("finishers", "dni", comboBox44.Text))
                    {
                        if (!chequearValorExisteEnColumna("finishers", "numero", textBox13.Text))
                        {
                            if (!chequearValorExisteEnColumna("finishers", "chip", textBox8.Text))
                            {
                                try
                                {
                                    SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                                    sqlServerConnect(connection);
                                    cmd.CommandType = CommandType.Text;
                                    // Leer datos de categoria
                                    cmd.CommandText = "SELECT vueltas, largada FROM [categorias] WHERE id = '" + idEvento + "' AND categoria = '" + categoria + "'";
                                    SqlDataReader reader = cmd.ExecuteReader();
                                    if (reader.Read())
                                    {
                                        string vueltas = reader["vueltas"].ToString();
                                        string largada = DateTime.Parse(reader["largada"].ToString()).ToString("MM-dd-yyy HH:mm:ss");

                                        connection.Close();
                                        string estado = "7-to start";

                                        // si la categoria ya largo, el año de largada ya esta seteado ( != 1900)
                                        if (largada != "" && DateTime.Parse(largada).Year > 1900) // igualdad sucede cuando se importa la tabla de categorias desde db_categorias
                                        {
                                            estado = "2-running";
                                        }

                                        DialogResult dialogResult = MessageBox.Show("Evento: " + comboBox9.Text + "\r\nNombre: " + textBox7.Text + "\r\nNumero: " + textBox13.Text + "\r\nTeam_ID: " + textBox28.Text + "\r\nChip: " + textBox8.Text + "\r\nCategoria: " + categoria, "Agregar corredor?", MessageBoxButtons.YesNo);

                                        if (dialogResult == DialogResult.Yes)
                                        {
                                            cmd.CommandText = "INSERT INTO[finishers] (id,numero,categoria,dni,tiempo,estado,lap,team_id,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,chip,presente) " +
                                                                                              "VALUES('" + idEvento + "', '" + textBox13.Text + "', '" + categoria + "'," +
                                                                                              "'" + comboBox44.Text + "','23:59:59', '" + estado + "', '0', '" + textBox28.Text + "'," +
                                                                                              " '', '','', '', '', '', '', '', '', '', '', '" + textBox8.Text + "','False');";
                                            sqlServerConnect(connection);
                                            cmd.ExecuteNonQuery();

                                            connection.Close();
                                            // insertar en base de datos principal
                                            try
                                            {
                                                cmd.CommandText = "INSERT INTO [corredores] (nombre,sexo,dni,ciudad,nacimiento,campo,team) VALUES ('" + textBox7.Text + "', '" + comboBox4.Text.ToLower() + "','" + comboBox44.Text + "', '" + textBox16.Text + "', '" + dateTimePicker3.Value.ToString("MM-dd-yyyy") + "','" + textBox32.Text + "','" + textBox36.Text + "')";
                                                sqlServerConnect(connection);
                                                cmd.ExecuteNonQuery();
                                            }
                                            catch /*(Exception ex)*/
                                            {
                                                connection.Close();
                                                // Actualizar registro con DNI
                                                cmd.CommandText = "UPDATE [corredores] SET nombre = '" + textBox7.Text.ToLower() + "', sexo = '" + comboBox4.Text.ToLower() + "', ciudad = '" + textBox16.Text + "', nacimiento = '" + dateTimePicker3.Value.ToString("MM-dd-yyyy") + "', campo = '" + textBox32.Text + "', team = '" + textBox36.Text + "' WHERE dni = '" + comboBox44.Text + "'";
                                                sqlServerConnect(connection);
                                                cmd.ExecuteNonQuery();
                                            }

                                            comboBox44.Text = "";
                                            comboBox44.Items.Clear();
                                            textBox7.Text = "";

                                            dateTimePicker3.Value = DateTime.Now;
                                            comboBox4.Text = "";
                                            textBox32.Text = "";
                                            textBox36.Text = "";

                                            textBox16.Text = "";
                                            textBox8.Text = "";

                                            label93.Text = "ultimo numero: " + textBox13.Text + " (+)";

                                            //if (textBox13.Text.Contains("B") || checkBox14.CheckState == CheckState.Unchecked)
                                            {
                                                textBox8.Text = "";
                                                textBox28.Text = "";
                                                textBox13.Text = "";
                                                comboBox5.Text = "";
                                            }
                                        }
                                    }
                                    connection.Close();
                                }
                                catch (Exception ex)
                                {
                                    MessageBox.Show(ex.Message);
                                }
                            }
                            else
                            {
                                MessageBox.Show("El CHIP asignado ya esta en uso por otro corredor");
                            }
                        }
                        else
                        {
                            MessageBox.Show("El NUMERO asignado ya esta en uso por otro corredor");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Corredor duplicado, ya existe en algun evento");
                    }
                    comboBox9_TextChanged(null, null);
                    comboBox5.Text = categoria;
                }
                else
                {
                    // no se encontro categoria, agregar al log
                    addToLog(idEvento, "categoria", textBox13.Text, "sin cat");
                }
            }
        }

        private void comboBox2_TextChanged(object sender, EventArgs e)
        {
            if (comboBox2.Text != "")
            {
                mostrarTablaChildEvento(comboBox2.Text, "categorias", dataGridView2, true);
                comboBox25.Items.Clear();
                poblarComboBoxColumna("categorias", "categoria", comboBox25, comboBox2.Text);
            }
        }

        private void button14_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("ESTA A PUNTO DE HACER CAGADAS", "Ojo, va a borrar info importante" + comboBox6.Text, MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                string eventId = buscarIdEventoPorNombre(comboBox34.Text);
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "DELETE FROM [finishers] WHERE id = '" + eventId + "'";
                sqlServerConnect(connection);
                cmd.ExecuteNonQuery();
                connection.Close();
                displayTable("finishers", dataGridView3, true);
            }
        }

        private void textBox10_TextChanged(object sender, EventArgs e)
        {

        }

        private void button15_Click(object sender, EventArgs e)
        {
            // TODO este boton no se deberia usar en eventos con largada undividual
            CheckedListBox.CheckedIndexCollection checkedIndex = checkedListBox1.CheckedIndices;
            CheckedListBox.CheckedItemCollection checkedCats = checkedListBox1.CheckedItems;

            if ((checkedCats.Count > 0) && (comboBox6.Text != "") && !(checkedCats[0].ToString() == "Todas" && checkedCats.Count == 1))
            {
                string cats = "";
                foreach (string categoria in checkedCats)
                {
                    if (categoria != "Todas")
                    {
                        cats += categoria + '\n';
                    }
                }

                DialogResult dialogResult = MessageBox.Show("Categoria(s): " + "\n\n" + cats + "\nContinuar?", "Esta por Iniciar Evento: " + comboBox6.Text, MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    DateTime fechaNow = DateTime.Now;
                    // Conseguir id evento 
                    string idEvento = buscarIdEventoPorNombre(comboBox6.Text);
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    string eventName = comboBox6.Text;

                    if (idEvento != "")
                    {
                        // check si hay alguna categoria ya ha largado
                        // esto para evitar volver a largar categorias que estan en curso y arruinar el crono =(
                        string[] categoriasYaIniciadas = new string[50];
                        int indx_cat = 0;
                        string catsYaLargadas = "";
                        bool reiniciarCats = true;
                        sqlServerConnect(connection);
                        cmd.CommandText = "SELECT categoria,largada FROM [categorias] WHERE id = '" + idEvento + "'";
                        SqlDataReader largadasReader = cmd.ExecuteReader();
                        while (largadasReader.Read())
                        {
                            if (largadasReader["largada"].ToString() != "1/1/1900 12:00:00 AM" && checkedCats.Contains(largadasReader["categoria"].ToString()))
                            {
                                categoriasYaIniciadas[indx_cat++] = largadasReader["categoria"].ToString();
                                catsYaLargadas += largadasReader["categoria"].ToString() + '\n';
                            }
                        }
                        connection.Close();

                        if (indx_cat > 0)
                        {
                            DialogResult dialRes = MessageBox.Show("Hay categorias que ya largaron: \n\n" + catsYaLargadas + "\nVolver a largar?","ATENCION", MessageBoxButtons.YesNo);
                            if (dialRes == DialogResult.No)
                            {
                                //quitar categorias de la lista
                                for (int i = 0; i < indx_cat; i++)
                                {
                                    cats = cats.Replace(categoriasYaIniciadas[i] + "\n", "");                                    
                                }
                                reiniciarCats = false;
                            }
                        }

                        // chequear si el evento esta en curso
                        sqlServerConnect(connection);
                        cmd.CommandText = "SELECT estado FROM [eventos] WHERE id = '" + idEvento + "'";
                        SqlDataReader estadoReader = cmd.ExecuteReader();

                        string estadoEvento = "";
                        estadoReader.Read();
                        estadoEvento = estadoReader[0].ToString();
                        connection.Close();

                        // Si evento no comenzo, Editar hora de largada del evento con la fecha y hora actual.
                        if ((estadoEvento != "en curso" || checkedCats.Contains("Todas")) && reiniciarCats == true)
                        {
                            cmd.CommandText = "UPDATE [eventos] SET estado = 'en curso', fecha = '" + fechaNow.ToString("MM-dd-yyyy HH:mm:ss") + "' WHERE id = '" + idEvento + "'";

                            sqlServerConnect(connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                            LlenarEventosRam();
                            displayTable("eventos", dataGridView1, true);

                            if (!comboBox7.Items.Contains(eventName))
                            {
                                comboBox7.Items.Add(eventName);
                            }
                            if (!comboBox30.Items.Contains(eventName))
                            {
                                comboBox30.Items.Add(eventName);
                            }

                            int index = evId.IndexOf(idEvento);
                            evStatus[index] = "en curso";
                        }

                        cmd.CommandText = "";

                        try
                        {
                            for (int i = 0; i < checkedCats.Count; i++)
                            {
                                if((reiniciarCats == true || !categoriasYaIniciadas.Contains(checkedCats[i].ToString())) && checkedCats[i].ToString() != "Todas")
                                {
                                    cmd.CommandText += "UPDATE [finishers] SET lap = 0, estado = '2-running', tiempo = '23:59:59' WHERE id = '" + idEvento + "' AND categoria = '" + checkedCats[i].ToString() + "';";
                                    cmd.CommandText += "UPDATE [finishers] SET v1 = '',v2 = '',v3 = '',v4 = '',v5 = '',v6 = '',v7 = '',v8 = '',v9 = '',v10 = '' WHERE id = '" + idEvento + "' AND categoria = '" + checkedCats[i].ToString() + "';"; // TODO resetear v0
                                    cmd.CommandText += "UPDATE [categorias] SET largada = '" + DateTime.Now.ToString("MM-dd-yyyy HH:mm:ss") + "' WHERE id = '" + idEvento + "' AND categoria = '" + checkedCats[i].ToString() + "';";
                                }
                            }

                            if (cmd.CommandText != "")
                            {
                                sqlServerConnect(connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();

                                MessageBox.Show("Evento " + comboBox6.Text + "\n\n" + cats + "\r\n" + "Iniciado con exito!");
                                load_cats();
                                //timer5.Start();

                                //cargar archivo de texto "Manual" para el evento
                                if (!File.Exists(supportFilesPath + "\\timestamps.txt"))
                                {
                                    //File.Delete(supportFilesPath + "\\" + idEvento + ".txt");
                                    FileStream newFile = File.Create(supportFilesPath + "\\timestamps.txt");
                                    newFile.Close();
                                }

                                // agregar timestamp de comienzo de evento
                                string timestampStart = "--------------------\r\n" + fechaNow.ToString("HH:mm:ss") + ", INICIA: " + idEvento + "\r\n--------------------\r\n";
                                File.AppendAllText(supportFilesPath + "\\timestamps.txt", timestampStart);

                                textBox30.Text = "";
                                string textToAppend = File.ReadAllText(supportFilesPath + "\\timestamps.txt");
                                textBox30.AppendText(textToAppend);

                                comboBox7.Text = eventName;
                                comboBox7_TextChanged(null, null);
                                if (checkedCats.Count > 1)
                                {
                                    comboBox8.Text = "General";
                                }
                                else
                                {
                                    comboBox8.Text = checkedCats[0].ToString();
                                }
                                comboBox8_TextChanged(null, null);
                            }
                            foreach (int idx in checkedIndex)
                            {
                                checkedListBox1.SetItemCheckState(idx, CheckState.Unchecked);
                            }
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }
            }
        }

        private void button16_Click(object sender, EventArgs e)
        {
            CheckedListBox.CheckedIndexCollection checkedIndex = checkedListBox1.CheckedIndices;
            CheckedListBox.CheckedItemCollection checkedCats = checkedListBox1.CheckedItems;

            if ((comboBox6.Text != "") && (checkedCats.Count > 0))
            {
                DialogResult dialogResult = new DialogResult();
                string eventId = buscarIdEventoPorNombre(comboBox6.Text);
                // conseguir tabla segun el tipo de evento
                string cats = "";

                foreach (string categoria in checkedCats)
                {
                    cats += categoria + ", ";
                }

                dialogResult = MessageBox.Show("Finalizar la(s) categoria(s): " + cats, "", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    //dialogResult = MessageBox.Show("Marcar Corredores en carrera como LAPPED?" + "\n" + "Si elige NO, se marcaran como DNF", "", MessageBoxButtons.YesNo);

                    string estatus = "3-lapped";
                    //if (dialogResult == DialogResult.No)
                    {
                        estatus = "4-dnf";
                    }
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    foreach (string categoria in checkedCats)
                    {
                        cmd.CommandText += "UPDATE [finishers] SET estado = '" + estatus + "' WHERE categoria = '" + categoria + "' AND id = '" + eventId + "' AND estado = '2-running';";
                        cmd.CommandText += "UPDATE [finishers] SET estado = '6-dns' WHERE categoria = '" + categoria + "' AND id = '" + eventId + "' AND estado = '7-to start';";
                    }

                    sqlServerConnect(connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch { }

                    connection.Close();

                    foreach (int idx in checkedIndex)
                    {
                        checkedListBox1.SetItemCheckState(idx, CheckState.Unchecked);
                    }

                    button31_Click(null, null);
                    comboBox6_TextChanged(null, null);
                }
            }
        }

        // presionar enter al ingresar numero en forma manual
        private void textBox14_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // clasificar por numero de corredor
                if (textBox14.Text != "")
                {
                    DateTime pasada = DateTime.Now;

                    string[] numero = new string[60];

                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
           
                    if (cbLargada.Checked)
                    {
                        // Decodificar el string en textBox14.Text.
                        // Formatos string textBox14.Text:
                        // numero1.numero2.numero3.
                        // team_id1.team_id2.team_id3
                        string[] porLargar = textBox14.Text.Split('.');

                        for(int i = 0; i < porLargar.Length; i++)
                        {
                            gNumerosLeidos[gReadIdx] = porLargar[i];                           
                            gNumType[gReadIdx] = checkBox2.CheckState == CheckState.Checked? "team_id" : "numero"; //checkear checkBox Team id. Checkeado es team_id
                            gHorasDePaso[gReadIdx] = pasada;
                            gPuestoDeControl[gReadIdx] = -1; // es largada individual

                            gReadIdx++;

                            if (gReadIdx >= gBuffLen)
                            {
                                gReadIdx = 0;
                            }
                        }
                    }
                    else // no es largada individual es paso de corredor por meta
                    {
                        //checkear checkBox Team id. Checkeado es team_id
                        gNumType[gReadIdx] = (checkBox2.CheckState == CheckState.Checked) ? "team_id" : "numero";

                        gHorasDePaso[gReadIdx] = pasada;
                        gPuestoDeControl[gReadIdx] = 0; //numero leido en llegada
                        gNumerosLeidos[gReadIdx] = textBox14.Text;

                        gReadIdx++;

                        if (gReadIdx >= gBuffLen)
                        {
                            gReadIdx = 0;
                        }
                    }
                }

                textBox14.Text = "";
            }
        }

        private void comboBox9_TextChanged(object sender, EventArgs e)
        {
            /*
            if (comboBox9.Text != "")
            {
                mostrarTablaInscriptos();

                // vaciar items de comboBox5 y re poblar
                comboBox5.Items.Clear();
                poblarComboBoxColumna("categorias", "categoria", comboBox5, comboBox9.Text);
            }
            */
        }

        private void label26_Click(object sender, EventArgs e)
        {

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            // chequear que eventos estan en curso
            if ((comboBox7.Text != "") && (comboBox8.Text != ""))
            {
                string idEvento = buscarIdEventoPorNombre(comboBox7.Text);

                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;

                try
                {
                    if ((comboBox8.Text == "General") || (comboBox8.Text == "Masculino") || (comboBox8.Text == "Femenino"))
                    {
                        DateTime fechaLargada = Convert.ToDateTime(evStart[evId.IndexOf(idEvento)]);
                        label27.Text = showTimeString(DateTime.Now.Subtract(fechaLargada));
                    }
                    else
                    {
                        // leer hora de largada segun categoria
                        DateTime horaLargada = gCatLargadas[gCatNombres.IndexOf(idEvento + "-" + comboBox8.Text)];
                        label27.Text = showTimeString(DateTime.Now.Subtract(horaLargada));
                    }
                }
                catch
                {
                    label27.Text = "TIEMPO";
                }
            }
            else
            {
                label27.Text = "TIEMPO";
            }

            label51.Text = DateTime.Now.ToString("HH:mm:ss dd/MM/yyyy");

            // lapeos
            if (timer5.Enabled == true)
            {

                dataGridView10.Rows.Clear();
                int idxT = 0;
                for (; idxT < catCountLapeo; idxT++)
                {
                    DataGridViewRow row = (DataGridViewRow)dataGridView10.Rows[0].Clone();
                    row.Cells[0].Value = catLapeo[idxT];

                    if ((horaLapeo[idxT] - DateTime.Now).TotalSeconds <= 0)
                    {
                        row.Cells[1].Value = "Lapeo";
                        row.DefaultCellStyle.BackColor = Color.Green;
                    }
                    else
                    {
                        row.Cells[1].Value = showTimeString(horaLapeo[idxT] - DateTime.Now);
                        row.DefaultCellStyle.BackColor = Color.White;
                    }
                    dataGridView10.Rows.Add(row);
                }

                // mostrar categorias finalizadas
                for (idxT = 0; idxT < catCountFinish; idxT++)
                {
                    DataGridViewRow row = (DataGridViewRow)dataGridView10.Rows[0].Clone();
                    row.Cells[0].Value = catFinish[idxT];
                    row.Cells[1].Value = "Finished";
                    dataGridView10.Rows.Add(row);
                    row.DefaultCellStyle.BackColor = Color.Red;
                }
            }
        }

        private void tabPage3_Click(object sender, EventArgs e)
        {

        }

        private void poblarComboBoxEventosEnCurso(ComboBox comboBox)
        {
            comboBox.Items.Clear();
            comboBox.Items.Add("");

            // poblar comboBox con nombres extraidos de "columna" de la tabla "tabla" 
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT nombre FROM [eventos] WHERE estado = 'en curso'";
            if (sqlServerConnect(connection))
            {
                SqlDataReader eventosActivosReader = cmd.ExecuteReader();

                while (eventosActivosReader.Read())
                {
                    comboBox.Items.Add(eventosActivosReader[0].ToString());
                }

                connection.Close();
            }
        }

        private void comboBox7_TextChanged(object sender, EventArgs e)
        {

        }

        private string showTimeString(TimeSpan timeToShow)
        {
            String stringToReturn = "";

            if (timeToShow.Hours < 10)
            {
                stringToReturn += "0" + timeToShow.Hours.ToString();
            }
            else
            {
                stringToReturn += timeToShow.Hours.ToString();
            }

            stringToReturn += ":";

            if (timeToShow.Minutes < 10)
            {
                stringToReturn += "0" + timeToShow.Minutes.ToString();
            }
            else
            {
                stringToReturn += timeToShow.Minutes.ToString();
            }

            stringToReturn += ":";

            if (timeToShow.Seconds < 10)
            {
                stringToReturn += "0" + timeToShow.Seconds.ToString();
            }
            else
            {
                stringToReturn += timeToShow.Seconds.ToString();
            }

            return stringToReturn;
        }


        private bool OpenSerialPort(string portName, ref int antType, SerialPort serialPortx, ref byte antAdrr, ref bool esWireless)
        {
            bool portOpen = false;
            if (portName != "")
            {
                // primero probar con antena Zk
                int port = Convert.ToInt16(portName.Substring(3));

                // resetear adrr siempre antes de abrir puerto por si cambiaron de posicion las antenas
                //antAdrr = 0xFF;
                if (ZkReader.ConnectAntenna(serialPortx, antAdrr))
                {
                    portOpen = true;
                    antType = 2;
                }
                else //  caso contrario, abrir puerto convencionalmente
                {
                    if (serialPortx.IsOpen)
                    {
                        serialPortx.Close();
                    }
                    try
                    {
                        serialPortx.PortName = portName;
                        serialPortx.BaudRate = 9600;
                        serialPortx.ReadTimeout = 1000;
                        serialPortx.WriteTimeout = 1000;
                        serialPortx.Open();
                        portOpen = true;
                        antType = 1;
                    }
                    catch (Exception ex)
                    {
                        //MessageBox.Show("No se pudo abrir puerto " + portName + ". " + ex.Message);
                    }
                }
            }
            else
            {
                MessageBox.Show("Seleccione un Puerto Serie");
            }

            return portOpen;
        }

        private byte checksumCalculator(byte[] buffer, int nbOfBytes)
        {
            int suma = 0;

            for (int i = 0; i < nbOfBytes; i++)
            {
                suma += buffer[i];
            }

            suma = 255 - (byte)suma + 1;
            return (byte)suma;
        }

        private UInt64 readTagID(int idx)
        {
            UInt64 retVal = 0;            

            string[] Epc = new string[1000];
            string[] devAdd = new string[1000];
            DateTime dateTimeNow = DateTime.Now;
            int nb_Of_Epc = 0;
            switch (gAntType[idx])
            {
                case AntType.ZK:
                    nb_Of_Epc = ZkReader.ReadActiveModeData(ref Epc, ref devAdd, gPortsConnected[idx]);
                    break;
                case AntType.INNOTECH:
                    nb_Of_Epc = InnoReader.ReadActiveModeData(ref Epc, ref devAdd, gPortsConnected[idx]);
                    break;
                case AntType.KAELCO:
                case AntType.UNKNOWN:
                default:
                    break;
            }
                     
            for (int i=0; i < nb_Of_Epc; i++)
            {
                // agragar chip!!!
                gNumType[gReadIdx] = "antena " + devAdd[i];
                gNumerosLeidos[gReadIdx] = Epc[i];
                gHorasDePaso[gReadIdx] = dateTimeNow;

                //incremento indice de chips leidos
                gReadIdx++;

                if (gReadIdx >= gBuffLen)
                {
                    gReadIdx = 0;
                }
            }
            return retVal;
        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void button17_Click(object sender, EventArgs e)
        {
            textBox8.Text = "";

            try
            {
                UsbReader.ConnectAntenna();
                string usbread = UsbReader.AnswerModeData();
                if (usbread != "")
                {
                    textBox8.Text = Convert.ToInt64(usbread).ToString();
                }
                else
                {
                    textBox8.Text = "";
                }
                UsbReader.StopAntenna();
                //textBox8.Text = Convert.ToInt16(ZkReader.AnswerModeData(gAntAdrr[0], gPortsConnected[0])).ToString();
            }
            catch { }
        }

        private void button22_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM [resultados]";
            sqlServerConnect(connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            displayTable("resultados", dataGridView4, false);
        }

        private void label28_Click(object sender, EventArgs e)
        {

        }

        private void comboBox8_TextChanged(object sender, EventArgs e)
        {

        }

        private void generateSheetFormat(Worksheet worksheet, int lapNumber, int row)
        {
            worksheet.Cells[row, 0] = new Cell("Pos");
            worksheet.Cells[row, 1] = new Cell("Numero");
            worksheet.Cells[row, 2] = new Cell("Nombre");
            worksheet.Cells[row, 3] = new Cell("Categoria");
            worksheet.Cells[row, 4] = new Cell("Ciudad");
            worksheet.Cells[row, 5] = new Cell("Tiempo");

            if (lapNumber > 1)
            {
                for (int i = 1; i <= lapNumber; i++)
                {
                    string vuelta = "lap " + i.ToString();
                    ushort col = (ushort)(5 + i);
                    worksheet.Cells[row, col] = new Cell(vuelta);
                    worksheet.Cells.ColumnWidth[col] = 2100;
                }
            }

            worksheet.Cells.ColumnWidth[0] = 1500;
            worksheet.Cells.ColumnWidth[2] = 7000;
            worksheet.Cells.ColumnWidth[1] = 3000;
            worksheet.Cells.ColumnWidth[3] = 6000;
            worksheet.Cells.ColumnWidth[4] = 5000;
            worksheet.Cells.ColumnWidth[5] = 2100;
        }

        private void button26_Click(object sender, EventArgs e)
        {
            if (comboBox9.Text != "")
            {
                // buscar id eveno segun el nombre
                string eventId = buscarIdEventoPorNombre(comboBox9.Text);
                if (eventId != "")
                {
                    // levantar los datos del evento 
                    string idEvento = buscarIdEventoPorNombre(comboBox9.Text);
                    DateTime fechaEvento = DateTime.Parse(evStart[evId.IndexOf(idEvento)]);
                    string modalidad = evKey[evId.IndexOf(idEvento)];
                    string nombreEventoPrint = evPrint[evId.IndexOf(idEvento)];

                    string nombreEvento = "[" + fechaEvento.Year.ToString() + "-" + fechaEvento.Month.ToString().PadLeft(2,'0') + "-" + fechaEvento.Day.ToString().PadLeft(2, '0') + "] " + nombreEventoPrint;

                    // carpeta destino es fija
                    string fileNameAndLoc = "";
                    string list_dir = "C:\\cronobot\\" + nombreEvento + "\\start list\\";
                    System.IO.Directory.CreateDirectory(list_dir);
                    //fileNameAndLoc = list_dir + comboBox9.Text + " " + DateTime.Now.Ticks.ToString() + ".xls";
                    fileNameAndLoc = list_dir + comboBox9.Text + ".xls";

                    //nombre del excel a generar
                    Workbook workbook = new Workbook();
                    Worksheet worksheet = new Worksheet("Participantes");


                    // Seleccionar los records de tabla finishers segun el evento
                    SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT corredores.nacimiento, corredores.nombre, corredores.dni, corredores.sexo, corredores.ciudad, corredores.campo, finishers.categoria, finishers.team_id, finishers.numero, finishers.chip FROM corredores, finishers WHERE corredores.dni = finishers.dni AND finishers.id = '" + eventId + "' ORDER BY  finishers.categoria ASC";

                    sqlServerConnect(connection);
                    SqlDataReader inscriptosReader = cmd.ExecuteReader();

                    // contador de finishers
                    // poblar filas
                    string lastCat = "";
                    int filaCounter = 0; // fila 0 es el header

                    worksheet.Cells[filaCounter, 0] = new Cell("Categoria");
                    worksheet.Cells[filaCounter, 1] = new Cell("team id");
                    worksheet.Cells[filaCounter, 2] = new Cell("Nombre");
                    worksheet.Cells[filaCounter, 3] = new Cell("DNI");
                    worksheet.Cells[filaCounter, 4] = new Cell("Sexo");
                    worksheet.Cells[filaCounter, 5] = new Cell("Nacimiento");
                    worksheet.Cells[filaCounter, 6] = new Cell("Ciudad");
                    worksheet.Cells[filaCounter, 7] = new Cell("Campo");
                    worksheet.Cells[filaCounter, 8] = new Cell("numero");
                    worksheet.Cells[filaCounter, 9] = new Cell("chip");
                    filaCounter++;

                    while (inscriptosReader.Read())
                    {
                        // categoria
                        worksheet.Cells[filaCounter, 0] = new Cell(inscriptosReader["categoria"].ToString());
                        // numero
                        int numero = new int();

                        try
                        {
                            numero = Convert.ToInt16(inscriptosReader["team_id"].ToString());
                            worksheet.Cells[filaCounter, 1] = new Cell(numero);
                        }
                        catch (Exception ex)
                        {
                            string numeroStr = inscriptosReader["team_id"].ToString();
                            worksheet.Cells[filaCounter, 1] = new Cell(numeroStr);
                        }
                        // el resto
                        worksheet.Cells[filaCounter, 2] = new Cell(inscriptosReader["nombre"].ToString());
                        worksheet.Cells[filaCounter, 3] = new Cell(inscriptosReader["dni"].ToString());
                        worksheet.Cells[filaCounter, 4] = new Cell(inscriptosReader["sexo"].ToString());
                        worksheet.Cells[filaCounter, 5] = new Cell(Convert.ToDateTime(inscriptosReader["nacimiento"]), "dd/MM/yyyy");
                        worksheet.Cells[filaCounter, 6] = new Cell(inscriptosReader["ciudad"].ToString());
                        worksheet.Cells[filaCounter, 7] = new Cell(inscriptosReader["campo"].ToString());
                        worksheet.Cells[filaCounter, 7] = new Cell(inscriptosReader["campo"].ToString());
                        worksheet.Cells[filaCounter, 8] = new Cell(inscriptosReader["numero"].ToString());
                        worksheet.Cells[filaCounter, 9] = new Cell(inscriptosReader["chip"].ToString());
                        filaCounter++;
                        lastCat = inscriptosReader["categoria"].ToString();
                    }

                    worksheet.Cells.ColumnWidth[0] = 7000;
                    worksheet.Cells.ColumnWidth[1] = 3000;
                    worksheet.Cells.ColumnWidth[2] = 7000;

                    connection.Close();

                    workbook.Worksheets.Add(worksheet);

                    try
                    {
                        workbook.Save(fileNameAndLoc);
                    }
                    catch (IOException)
                    {
                        MessageBox.Show("Cierre el archivo e intente nuevamente");
                    }
                }
            }
        }

        private void comboBox10_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            Workbook workbook = new Workbook();

            if (cbImportEvent.Text != "")
            {
                string eventId = "";

                // conseguir tabla segun el tipo de evento
                workbook = Workbook.Load(gFileNameIngestor);
                // conseguir indice para cada nombre de columna
                int teamIdIdx = gColNames.IndexOf(comboBox12.Text);
                int nombreIdx = gColNames.IndexOf(comboBox13.Text);
                int apellidoIdx = gColNames.IndexOf(comboBox48.Text);
                int dniIdx = gColNames.IndexOf(comboBox15.Text);
                int nacIdx = gColNames.IndexOf(comboBox17.Text);
                int sexoIdx = gColNames.IndexOf(comboBox19.Text);
                int localidadIdx = gColNames.IndexOf(comboBox16.Text);
                int emailIdx = gColNames.IndexOf(comboBox14.Text);
                int teamIdx = gColNames.IndexOf(comboBox18.Text);
                int numeroIdx = gColNames.IndexOf(comboBox21.Text);
                int catIdx = gColNames.IndexOf(comboBox22.Text);
                int campoIdx = gColNames.IndexOf(comboBox35.Text);
                int confirmadoIdx = gColNames.IndexOf(comboBox46.Text);
                int idEventoIdx = gColNames.IndexOf(comboBox47.Text);
                int chipIdx = gColNames.IndexOf(comboBox49.Text);

                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;

                int i = 1; //primer fila de datos esta despues del encabezado
                int k = 0; // cantidad de corredores importados con exito
                while (workbook.Worksheets[0].Cells[i, 0].ToString() != "") // chequear que la linea no este vacia
                {
                    string confirmado = workbook.Worksheets[0].Cells[i, confirmadoIdx].ToString();
                    eventId = workbook.Worksheets[0].Cells[i, idEventoIdx].ToString();

                    if ((evId.Contains(eventId) && (confirmado == "1" || confirmado == "si"))|| cbImportEvent.SelectedIndex == 0)
                    {
                        string nombre = workbook.Worksheets[0].Cells[i, nombreIdx].ToString().ToLower();
                        string apellido = workbook.Worksheets[0].Cells[i, apellidoIdx].ToString().ToLower();

                        if (apellido != "" && apellido != null)
                        {
                            nombre = apellido + ", " + nombre;
                        }

                        nombre = nombre.Replace(" ,", ",");

                        string dni = workbook.Worksheets[0].Cells[i, dniIdx].ToString();
                        if (dni.Contains("."))
                        {
                            dni = dni.Replace(".", "");
                        }
                        if (dni.Contains(" "))
                        {
                            dni = dni.Replace(" ", "");
                        }
                        string sexo_crudo = workbook.Worksheets[0].Cells[i, sexoIdx].ToString().ToLower();
                        string sexo = DetectarSexo(sexo_crudo);
                        string campo = workbook.Worksheets[0].Cells[i, campoIdx].ToString();

                        DateTime nacimiento = new DateTime();
                        string nac = workbook.Worksheets[0].Cells[i, nacIdx].Value.ToString();
                        if (nac != "")
                        {
                            nacimiento = detectarFormatoFecha(nac);
                        }

                        string categoria = workbook.Worksheets[0].Cells[i, catIdx].ToString();

                        if (categoria != "" && !gCatNombres.Contains(eventId + "-" + categoria)) // checkar y asignar categoria
                        {
                            categoria = ""; //mala categoria, reset valor!
                        }

                        if (categoria == "" && comboBox17.Text != "") //si no se provee la categoria pero si la f de nac, calcular categoria
                        {                            
                            if (nac != "")
                            {
                                string[] catArray = new string[10];
                                calcularCategoria(eventId, nacimiento, sexo, ref catArray);
                                categoria = catArray[0];
                            }
                        }

                        string localidad = workbook.Worksheets[0].Cells[i, localidadIdx].ToString();
                        string runningTeam = workbook.Worksheets[0].Cells[i, teamIdx].ToString(); // cortar a 20 caracteres
                        string email = workbook.Worksheets[0].Cells[i, emailIdx].ToString();
                        string numero = workbook.Worksheets[0].Cells[i, numeroIdx].ToString();
                        string teamId = workbook.Worksheets[0].Cells[i, teamIdIdx].ToString();
                        string chip = workbook.Worksheets[0].Cells[i, chipIdx].ToString();

                        if (nombre != "" && sexo != "" && dni != "" && nacimiento.ToString() != "" && (numero != "" && teamId != "" && categoria != "" && categoria != null || cbImportEvent.SelectedIndex == 0))
                        {
                            if (cbImportEvent.SelectedIndex == 0) // agregar a "Listado General"
                            {
                                cmd.CommandText = "INSERT INTO [corredores] (nombre, sexo, dni, ciudad, campo, email, team, nacimiento) VALUES ('" + nombre + "','" + sexo + "','" + dni + "', '" + localidad + "','" + campo + "','" + email + "','" + runningTeam + "','" + nacimiento.ToString("yyyy-MM-dd HH:mm:ss") + "');";
                            }
                            else if (!chequearValorExisteEnColumna("finishers", "dni", dni))  // agregar finishers a eventos en DB
                            {
                                if (!chequearValorExisteEnColumna("finishers", "numero", numero))
                                {
                                    if (!chequearValorExisteEnColumna("finishers", "chip", chip))
                                    {
                                        // se deberia tambien intentar agregar/modificar la base de datos "corredores"
                                        cmd.CommandText = "INSERT INTO [finishers] (id, numero, categoria, dni, team_id, estado, lap,v0,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,chip,presente) VALUES ('" + eventId + "','" + numero + "','" + categoria + "','" + dni + "','" + teamId + "', '7-to start', '0','','','','','','','','','','','','" + chip + "','False');";
                                        cmd.CommandText += "INSERT INTO [corredores] (nombre, sexo, dni, ciudad, campo, email, team, nacimiento) VALUES ('" + nombre + "','" + sexo + "','" + dni + "', '" + localidad + "','" + campo + "','" + email + "','" + runningTeam + "','" + nacimiento.ToString("yyyy-MM-dd HH:mm:ss") + "');";
                                    }
                                    else
                                    {
                                        MessageBox.Show("No se pudo agregar el corredor:\nnombre: " + nombre + "\ndni: " + dni + "\n\nCHIP repetido: " + chip, "Error de carga! evento: " + evName[evId.IndexOf(eventId)]);
                                        cmd.CommandText = "";
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("No se pudo agregar el corredor:\nnombre: " + nombre + "\ndni: " + dni + "\n\nNUMERO repetido: " + numero, "Error de carga! evento: " + evName[evId.IndexOf(eventId)]);
                                    cmd.CommandText = "";
                                }
                            }
                            else
                            {
                                MessageBox.Show("No se pudo agregar el corredor:\nnombre: " + nombre + "\ndni: " + dni + "\n\nDNI repetido", "Error de carga! evento: " + evName[evId.IndexOf(eventId)]);
                                cmd.CommandText = "";
                            }

                            sqlServerConnect(connection);
                            try
                            {
                                if (cmd.CommandText != "")
                                {
                                    cmd.ExecuteNonQuery();
                                    k++;
                                }
                            }
                            catch (Exception ex)
                            {
                                //MessageBox.Show(ex.Message);
                                if (ex.Message.Contains("duplicate key") && ex.Message.Contains("Corredores")) // 
                                {
                                    try
                                    {
                                        cmd.CommandText = "UPDATE [corredores] SET nombre = '" + nombre + "', ciudad = '" + localidad + "', nacimiento = '" + nacimiento.ToString("yyyy-MM-dd HH:mm:ss") + "', sexo = '" + sexo + "', team = '" + runningTeam + "', campo = '" + campo + "', email = '" + email + "' WHERE dni = '" + dni + "'";
                                        cmd.ExecuteNonQuery();
                                        k++;
                                    }
                                    catch
                                    {
                                        //MessageBox.Show("dni repetido: " + dni.ToString() + "\r\nEvento: " + evName[evId.IndexOf(eventId)]);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("error inesperado: insert finisher/insert corredor: " + ex.Message);
                                }
                            }
                            cmd.CommandText = "";
                            connection.Close();
                        }
                        else
                        {
                            MessageBox.Show("Participante con DNI: " + dni + " Error en sus datos personales" + "\r\nEvento: " + evName[evId.IndexOf(eventId)]);
                        }
                    }

                    i++;
                }

                MessageBox.Show("Tabla Importada con Exito!\n\rNumero de importados:" + k.ToString());
                mostrarTablaChildEvento(cbImportEvent.Text, "finishers", dataGridView3, true);
                button5_Click(sender, e);
            }
        }

        private void button27_Click(object sender, EventArgs e)
        {
            Workbook workbook = new Workbook();

            comboBox12.Items.Clear();
            comboBox13.Items.Clear();
            comboBox14.Items.Clear();
            comboBox15.Items.Clear();
            comboBox16.Items.Clear();
            comboBox17.Items.Clear();
            comboBox18.Items.Clear();
            comboBox19.Items.Clear();
            comboBox21.Items.Clear();
            comboBox22.Items.Clear();
            comboBox35.Items.Clear();
            comboBox46.Items.Clear();
            comboBox47.Items.Clear();
            comboBox48.Items.Clear();
            comboBox49.Items.Clear();
            gColNames = new List<string>();

            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            if (result == DialogResult.OK) // Test result.
            {
                gFileNameIngestor = openFileDialog1.FileName;
                try
                {
                    workbook = Workbook.Load(gFileNameIngestor);

                    // levantar nombres de columnas
                    int col = 0;
                    while (workbook.Worksheets[0].Cells[0, col].ToString() != "")
                    {
                        comboBox12.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox13.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox14.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox15.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox16.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox17.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox18.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox19.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox21.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox22.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox35.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox46.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox47.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox48.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        comboBox49.Items.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        gColNames.Add(workbook.Worksheets[0].Cells[0, col].ToString());
                        col++;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    gFileNameIngestor = "";
                }
            }
        }

        private void label32_Click(object sender, EventArgs e)
        {

        }

        private void groupBox5_Enter(object sender, EventArgs e)
        {

        }

        private byte leerParametroSimple(byte paramAddr)
        {
            byte returnByte = 0xFF;
            byte[] readMode = { 0xA0, 0x04, 0x61, 0x00, paramAddr, 0x00 };
            int comandSize = 6;
            int answSize = 7;
            byte[] respuesta = new byte[answSize];
            readMode[5] = checksumCalculator(readMode, 5);

            try
            {
                serialPort1.Write(readMode, 0, comandSize);

                for (int i = 0; i < answSize; i++)
                {
                    respuesta[i] = (byte)serialPort1.ReadByte();
                }

                byte checksum = checksumCalculator(respuesta, 6);

                if (checksum == respuesta[6])
                {
                    returnByte = respuesta[5];
                }
                else
                {

                }
            }
            catch (Exception)
            {

            }

            return returnByte;
        }

        private void timer2_Tick(object sender, EventArgs e)
        {
            // timer para updatear datagrid "eventos en curso"
            if (gUpdateDataGrid)
            {
                gUpdateDataGrid = false;
                comboBox7.Text = gEvento;
                if (comboBox8.Text == gCategoria)
                {
                    calcularClasificacion(gEvento, gCategoria, "finishers", dataGridView4);
                }
                else
                {
                    comboBox8.Text = gCategoria;                
                }
                
                string evenId = evId[evName.IndexOf(gEvento)];
                textBox30.Text = "";
                string textToAppend = File.ReadAllText(supportFilesPath + "\\timestamps.txt");
                textBox30.AppendText(textToAppend);
            }
            
            // mostar actividad antenas
            while (gAntennaActivityIdx != gReadIdx)
            {
                if (checkBox6.Checked)
                {
                    textBox38.AppendText(gHorasDePaso[gAntennaActivityIdx].ToString("HH:mm:ss.fff") + "; " + gNumerosLeidos[gAntennaActivityIdx] + "; " + gNumType[gAntennaActivityIdx] + "\r\n");
                }
                
                // TODO: DEBE TENER SU PROPIO CHECKBOX
                if (false) //mostrar Info corredores
                {
                    SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;

                    // buscar chip en la tabla de finishers
                    sqlServerConnect(connection);
                    cmd.CommandText = "SELECT finishers.id, finishers.numero, finishers.chip, finishers.team_id, finishers.categoria, corredores.nacimiento, corredores.ciudad, corredores.campo, corredores.sexo, corredores.nombre, corredores.dni, corredores.team FROM finishers, corredores WHERE  finishers.dni = corredores.dni AND finishers.chip = '" + gNumerosLeidos[gAntennaActivityIdx] + "'";
                    SqlDataReader chipInscipto = cmd.ExecuteReader();

                    if (chipInscipto.Read())
                    {
                        // datos corredor
                        labelNombre.Text = chipInscipto["nombre"].ToString();
                        labelGenero.Text = chipInscipto["sexo"].ToString();
                        string eventId = chipInscipto["id"].ToString();
                        string teamId = chipInscipto["team_id"].ToString();
                        labelNumero.Text = chipInscipto["numero"].ToString();
                        labelCategoria.Text = chipInscipto["categoria"].ToString();
                        labelEvento.Text = evName[evId.IndexOf(eventId)];
                    }

                    connection.Close();
                }

                // TOMA DE ASISTENCIA:
                if(checkBox12.Checked && gNumType[gAntennaActivityIdx].Contains("antena"))
                {
                    SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;

                    // buscar chip en la tabla de finishers
                    sqlServerConnect(connection);
                    cmd.CommandText = "UPDATE [finishers] SET presente = '1' WHERE chip = '" + gNumerosLeidos[gAntennaActivityIdx] + "'";
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }

                gAntennaActivityIdx++;

                if (gAntennaActivityIdx == gBuffLen)
                {
                    gAntennaActivityIdx = 0;
                }
            }
        }

        private void Thread3()
        {
            while (true)
            {
                // timer para updatear datagrid "eventos en curso"
                if (gUpdateClasificacion)
                {
                    calcularClasificacion(gEvento, gCategoria, "finishers", dataGridView4);
                    gUpdateClasificacion = false;
                }
            }
        }

        private void Thread1()
        {
            while (true)
            {
                // LEER TAG Y AGREGAR A CLASIFICACION
                if (gAntActive[0] && gAntConnected[0] && gThreadActive)
                {
                    readTagID(0);
                }
                if (gAntActive[1] && gAntConnected[1] && gThreadActive)
                {
                    readTagID(1);
                }
                if (gAntActive[2] && gAntConnected[2] && gThreadActive)
                {
                    readTagID(2);
                }
                if (gAntActive[3] && gAntConnected[3] && gThreadActive)
                {
                    readTagID(3);
                }
            }
        }

        private void Thread2()
        {
            SqlConnection connectionDbMain = new SqlConnection(gConnectionString);

            while (true)
            {
                while (gAddIdx != gReadIdx)
                {
                    switch(gNumType[gAddIdx])
                    {
                        case "team_id":
                            string[] numerosFromTeamId = new string[20];
                            int count = 0;

                            //buscar numeros que correspondan al team id
                            try
                            {
                                SqlCommand cmd = connectionDbMain.CreateCommand();
                                cmd.CommandText = "SELECT numero FROM[finishers] WHERE team_id = '" + gNumerosLeidos[gAddIdx] + "'";
                                connectionDbMain.Open();
                                SqlDataReader reader = cmd.ExecuteReader();

                                while (reader.Read())
                                {
                                    numerosFromTeamId[count++] = reader[0].ToString();
                                }
                                connectionDbMain.Close();

                                for (int j = 0; j < count; j++)
                                {
                                    agregarNumeroClasificacion(numerosFromTeamId[j], gHorasDePaso[gAddIdx], gPuestoDeControl[gAddIdx], gNumType[gAddIdx]);
                                }

                            }
                            catch (Exception exep)
                            {
                                //MessageBox.Show(exep.Message); // todo comentar esta linea
                                connectionDbMain.Close();
                            }
                            break;
                        case "numero":
                            agregarNumeroClasificacion(gNumerosLeidos[gAddIdx], gHorasDePaso[gAddIdx], gPuestoDeControl[gAddIdx], gNumType[gAddIdx]);
                            break;
                        case "chip":
                        case "antena 0":
                        case "antena 1":
                        case "antena 2":
                        case "antena 3":
                        case "antena 4":
                            if (checkBox2.CheckState == CheckState.Unchecked) // no covertir chip a team_id
                            {
                                try
                                {
                                    //buscar numero que corresponde con el chip
                                    string numeroFromChip = "";
                                    SqlCommand cmd = connectionDbMain.CreateCommand();
                                    cmd.CommandText = "SELECT numero FROM [finishers] WHERE chip = '" + gNumerosLeidos[gAddIdx] + "'";
                                    connectionDbMain.Open();
                                    SqlDataReader reader = cmd.ExecuteReader();

                                    if (reader.Read())
                                    {
                                        numeroFromChip = reader["numero"].ToString();
                                    }
                                    connectionDbMain.Close();

                                    agregarNumeroClasificacion(numeroFromChip, gHorasDePaso[gAddIdx], gPuestoDeControl[gAddIdx], gNumType[gAddIdx]);
                                }
                                catch (Exception exep)
                                {
                                    MessageBox.Show(exep.Message); // todo comentar esta linea
                                    connectionDbMain.Close();
                                }
                            }
                            else // convertir chip a team_id
                            {
                                //puede darse el caso que se lean chips y la competencia sea por equipos 
                                // y se quiera tomar la pasada de todo el equipo cada vez, ejemplos: 
                                // -Dua/tria en relevo
                                // -duplas con distinto chip pero que se quiere evitar perder la lectura de uno de ellos par ano afectar su clasificacion
                                try
                                {
                                    //buscar team_id que corresponde con el chip
                                    string teamIdFromChip = "";
                                    SqlCommand cmd = connectionDbMain.CreateCommand();
                                    cmd.CommandText = "SELECT team_id FROM[finishers] WHERE chip = '" + gNumerosLeidos[gAddIdx] + "'";
                                    connectionDbMain.Open();
                                    SqlDataReader readerTeamId = cmd.ExecuteReader();

                                    while (readerTeamId.Read())
                                    {
                                        teamIdFromChip = readerTeamId[0].ToString();
                                    }
                                    connectionDbMain.Close();

                                    //buscar numeros que correspondan al team id
                                    string[] numerosFromTeam_Id = new string[20];
                                    int cuenta = 0;
                                    cmd = connectionDbMain.CreateCommand();
                                    cmd.CommandText = "SELECT numero FROM[finishers] WHERE team_id = '" + teamIdFromChip + "'";
                                    connectionDbMain.Open();
                                    SqlDataReader readerNumeros = cmd.ExecuteReader();

                                    while (readerNumeros.Read())
                                    {
                                        numerosFromTeam_Id[cuenta++] = readerNumeros[0].ToString();
                                    }
                                    connectionDbMain.Close();

                                    for (int j = 0; j < cuenta; j++)
                                    {
                                        agregarNumeroClasificacion(numerosFromTeam_Id[j], gHorasDePaso[gAddIdx], gPuestoDeControl[gAddIdx], gNumType[gAddIdx]);
                                    }

                                }
                                catch (Exception exep)
                                {
                                    MessageBox.Show(exep.Message); // todo comentar esta linea
                                    connectionDbMain.Close();
                                }
                            }
                            break;
                        default:
                            break;
                            
                    }

                    gAddIdx++;

                    if(gAddIdx >= gBuffLen)
                    {
                        gAddIdx = 0;
                    }
                }
            }
        }

        private void label25_Click(object sender, EventArgs e)
        {

        }

        private void comboBox9_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox9.Text != "")
            {
                mostrarTablaInscriptos();

                // vaciar items de comboBox5 y re poblar
                comboBox5.Items.Clear();
                poblarComboBoxColumna("categorias", "categoria", comboBox5, comboBox9.Text);
            }
        }

        private void sendSimpleCmd(byte command, SerialPort serialPort)
        {
            byte[] sendCmd = { 0xA0, 0x02, command, 0x00 };
            sendCmd[3] = checksumCalculator(sendCmd, 3);

            try
            {
                serialPort.Write(sendCmd, 0, 4);
            }
            catch (Exception)
            {

            }
        }

        private byte getSimpleAnswer(UInt16 lenght, SerialPort serialPort)
        {
            byte retByte = 0xFF;
            byte[] respuesta = new byte[lenght];

            for (int i = 0; i < lenght; i++)
            {
                respuesta[i] = (byte)serialPort.ReadByte();
            }

            byte checksum = checksumCalculator(respuesta, lenght - 1);

            if (checksum == respuesta[lenght - 1])
            {
                retByte = respuesta[lenght - 2];
            }

            return retByte;
        }

        private void button18_Click_2(object sender, EventArgs e)
        {
            getPorts(comboBox10);
        }

        private void button23_Click_1(object sender, EventArgs e)
        {
            /*
            bool isWireless = false;
            if(connectAntenna(gPortsAvailable[0], comboBox10.Text, gComLabel[0], gAntAdrr[0]))
            {
                gPortsConnected[0] = gPortsAvailable[0];
                gAntEsWireless[0] = isWireless;
            }
            */
        }

        private void button24_Click_1(object sender, EventArgs e)
        {
            desconectarAntena(0);
            CheckThreads();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            leerPotencia(0);
        }

        private void button21_Click_1(object sender, EventArgs e)
        {
            ClickBotonStop(0);
        }

        private void ClickBotonStop(int idx)
        {
            if (gAntConnected[idx])
            {
                bool prevThreadStaus = gThreadActive;
                gThreadActive = false;

                gAntActive[idx] = false;
                bool stopAnt = false;
                switch(gAntType[idx])
                {
                    case AntType.ZK:
                        stopAnt = ZkReader.StopAntenna(gPortsConnected[idx], gAntAdrr[idx]);
                        break;
                    case AntType.INNOTECH:
                        stopAnt = InnoReader.StopAntenna(gPortsConnected[idx], gAntAdrr[idx]);
                        break;
                    case AntType.UNKNOWN:
                    default:
                        break;
                }
                if (stopAnt)
                {
                    Thread.Sleep(20);
                    gConnectLabel1[idx].Text = "Desactivada";
                    gConnectLabel1[idx].ForeColor = Color.FromArgb(255, 10, 10);

                    gConnectLabel2[idx].Text = "Descativada";
                    gConnectLabel2[idx].ForeColor = Color.FromArgb(255, 10, 10);

                    // TODO esto va a salir cuando la edicion de tags sea solo por READER USB
                    if (idx == 0) // antena 1!
                    {
                        groupBox10.Enabled = true;
                        //button37.Enabled = true;
                        //button38.Enabled = true;
                        //textBox21.Enabled = true;
                    }
        
                    gAntActive[idx] = false;
                    gThreadActive = false;
                    CheckThreads();
                }
                else
                {
                    gAntActive[idx] = true;
                }

                gThreadActive = prevThreadStaus;
            }      
        }

        private void button28_Click_1(object sender, EventArgs e)
        {
            ClickBotonStart(0);
        }

        private void ClickBotonStart(int idx)
        {
            if (gAntConnected[idx])
            {
                bool prevThreadStatus = gThreadActive;
                gThreadActive = false;
                bool startAnt = false;
                switch (gAntType[idx])
                {
                    case AntType.ZK:
                        startAnt = ZkReader.StartAntenna(gPortsConnected[idx], gAntAdrr[idx]);
                        break;
                    case AntType.INNOTECH:
                        startAnt = InnoReader.StartAntenna(gPortsConnected[idx], gAntAdrr[idx]);
                        break;
                    case AntType.UNKNOWN:
                    default:
                        break;
                }
                if (startAnt)
                {
                    Thread.Sleep(20);
                    gConnectLabel1[idx].Text = "Antena Activada";
                    gConnectLabel1[idx].ForeColor = Color.FromArgb(10, 255, 10);
                    gConnectLabel2[idx].Text = "Activada";
                    gConnectLabel2[idx].ForeColor = Color.FromArgb(10, 255, 10);
                    gAntActive[idx] = true;
                    CheckThreads();

                    // TODO: ESTO TIENE QUE SALIR
                    if (idx == 0) // antena 1!
                    {
                        groupBox10.Enabled = false;
                        //button37.Enabled = false;
                        //button38.Enabled = false;
                        //textBox21.Enabled = false;
                    }

                    gThreadActive = true;
                }
                else
                {
                    gThreadActive = prevThreadStatus;
                }
            }
        }

        private void button29_Click_1(object sender, EventArgs e)
        {
            ResetAntenna(0);
        }

        private bool setSimpleParam(byte param, byte value, SerialPort serialport)
        {
            bool retval = false;
            byte[] cmdStr = { 0xA0, 0x05, 0x60, 0x00, param, value, 0x00 };
            cmdStr[6] = checksumCalculator(cmdStr, 6);

            try
            {
                serialport.Write(cmdStr, 0, 7);

                if (0xFF != getSimpleAnswer(5, serialport))
                {
                    retval = true;
                }
            }
            catch
            {

            }

            return retval;
        }

        private bool stopReadingAntennaKaelco(SerialPort serialport)
        {
            bool retval = false;

            try
            {
                sendSimpleCmd(0xFE, serialport); // stop read

                if (getSimpleAnswer(6, serialport) == 0x88)
                {
                    sendSimpleCmd(0x50, serialport); // stop working

                    if (getSimpleAnswer(5, serialport) == 0x00)
                    {
                        retval = true;
                    }
                }

            }
            catch (Exception)
            {

            }

            return retval;
        }

        private bool startReadingAntennaKaelco(SerialPort serialport)
        {
            bool retval = false;

            try
            {
                sendSimpleCmd(0x65, serialport); // reposition reader

                if (getSimpleAnswer(5, serialport) == 0x00)
                {
                    retval = true;
                }
            }
            catch (Exception exp)
            {
                MessageBox.Show(exp.Message);
            }

            return retval;
        }

        private void textBox18_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox18_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void textBox18_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //timer1.Stop();
                if (textBox18.Text != "")
                {
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.categoria,corredores.sexo,finishers.tiempo,finishers.estado, finishers.lap, categorias.vueltas FROM finishers JOIN corredores ON corredores.dni = finishers.dni JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.numero = '" + textBox18.Text + "'";

                    try
                    {
                        sqlServerConnect(connection);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            textBox23.Text = reader["lap"].ToString();
                            connection.Close();
                            sqlServerConnect(connection);
                            cmd.ExecuteNonQuery();
                            DataTable dta = new DataTable();
                            SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                            dataadapt.Fill(dta);
                            dataGridView4.DataSource = dta;  
                        }
                        else
                        {
                            MessageBox.Show("El corredor no esta en la lista de largada");
                        }

                        connection.Close();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                //timer1.Start();
            }
        }

        private void button30_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "" && textBox23.Text != "")
            {
                string eventId = "";
                string numero = "";
                string tiempo = "";
                string largada = "";
                string largadaInd = "";
                string laps = "";
                string lap = "";
                string vueltaAmodificar = "";
                DateTime nuevoPaso = new DateTime();
                int vueltaAmodificarNro = 0;
                string vueltaAmodificarStr = "";
                string estado = "";
                bool todoOk = true;
                numero = textBox18.Text;
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT categorias.vueltas, categorias.largada, finishers.id, finishers.lap, finishers.estado, finishers.v0, finishers.v1, finishers.v2, finishers.v3, finishers.v4, finishers.v5, finishers.v6, finishers.v7, finishers.v8, finishers.v9, finishers.v10, finishers.tiempo FROM finishers JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.numero = '" + numero + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    tiempo = reader["tiempo"].ToString();
                    largada = reader["largada"].ToString();
                    laps = reader["vueltas"].ToString();
                    lap = reader["lap"].ToString();
                    estado = reader["estado"].ToString();
                    eventId = reader["id"].ToString();
                    largadaInd = reader["v0"].ToString();

                    // modificar tiempo de paso de alguna vuelta
                    // debe ser una vuelta cumplida!
                    // se puede modificar la hora de largada si textBox23.Text = "0".
                    // TODO: Aun no se puede modificar largadas individuales!!!
                    vueltaAmodificarNro = textBox23.Text.All(char.IsDigit)? Convert.ToInt16(textBox23.Text):0;
                    vueltaAmodificarStr = vueltaAmodificarNro.ToString();
                    if ((vueltaAmodificarNro > 0 || checkBox3.Checked) && vueltaAmodificarNro <= Convert.ToInt16(lap) && (estado != "7-to start"))
                    { 
                        vueltaAmodificar = reader[5 + vueltaAmodificarNro].ToString();
                        nuevoPaso = dateTimePicker4.Value;
                        //el tiempo se modifica en dos casos: modificando hr largada o modificando ultimo paso
                        if (vueltaAmodificarNro == 0) // modificar largada solo en caso de largadas individuales
                        {
                            tiempo = (DateTime.Parse(reader[5 + Convert.ToInt16(lap)].ToString()) - nuevoPaso).ToString();
                        }
                        else if (vueltaAmodificarNro == Convert.ToInt16(laps) && checkBox3.Checked) //  si se modifica el ultimo paso (largada individual)
                        {
                            tiempo = (nuevoPaso - DateTime.Parse(largadaInd)).ToString();
                        }
                        else if (vueltaAmodificarNro == Convert.ToInt16(laps) && !checkBox3.Checked) //  si se modifica el ultimo paso (largada masiva)
                        {
                            tiempo = (nuevoPaso - DateTime.Parse(largada)).ToString();
                        }
                    }
                    else
                    {
                        todoOk = false;
                    }
                }

                connection.Close();

                if (todoOk)
                {
                    DialogResult dialogResult = MessageBox.Show("Modificar hora de paso por llegada (vuelta " + vueltaAmodificarStr + ") del corredor nro " + numero + " ?", "Editor de Resultado", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        // modificar el paso de algun registro (vuelta)
                        dialogResult = MessageBox.Show("Nueva hora de paso por llegada (vuelta " + vueltaAmodificarStr + ") para el corredor nro " + numero + " es " + nuevoPaso.ToString("dd/MM/yyyy HH:mm:ss") + ". Guardar?", "Editor de Resultado", MessageBoxButtons.YesNo);

                        if (dialogResult == DialogResult.Yes)
                        {
                            // TODO: Aun no se puede modificar largadas individuales!!!
                            /* 
                            if (vueltaAmodificarNro == 0)
                            {
                                cmd.CommandText = "UPDATE [finishers] SET largada = '" + nuevoPaso.ToString() + "', tiempo = '" + tiempo + "' WHERE numero = '" + numero + "' AND id = '" + eventId + "'";
                            }
                            else
                            */
                            {
                                cmd.CommandText = "UPDATE [finishers] SET v" + vueltaAmodificarNro.ToString() + " = '" + nuevoPaso.ToString("yyyy-MM-dd HH:mm:ss") + "', tiempo = '" + tiempo + "' WHERE numero = '" + numero + "' AND id = '" + eventId + "'";
                            }

                            sqlServerConnect(connection);
                            try
                            {
                                cmd.ExecuteNonQuery();
                                //add to log
                                addToLog("", "modif tiempo", numero, "");
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                            connection.Close();
                        }
                    }
                }
                else
                {
                    MessageBox.Show("Datos incoherentes");
                }
            }
        }

        private void label44_Click(object sender, EventArgs e)
        {

        }

        private void button32_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "")
            {
                cambiarEstado(textBox18.Text, "4-dnf");
                calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
            }
        }

        private void button31_Click(object sender, EventArgs e)
        {
            calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
        }

        private void button33_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "")
            {
                cambiarEstado(textBox18.Text, "1-finish");
                calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
            }
        }

        private void addToLog(string id, string source, string numero, string obs)
        {
            DateTime time = DateTime.Now;
            SqlConnection connectionDbMain = new SqlConnection(gConnectionString);
            SqlCommand cmd = connectionDbMain.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "INSERT INTO [log] VALUES ('" + time.ToString("MM-dd-yyyy HH:mm:ss") + "','" + id + "','" + source + "','" + numero + "','" + obs + "')";
            connectionDbMain.Open();
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            connectionDbMain.Close();

            //displayTable("log", dataGridView5, true);
        }

        private void button34_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM [log]";
            sqlServerConnect(connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            displayTable("log", dataGridView5, false);
        }

        private void comboBox8_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button35_Click(object sender, EventArgs e)
        {
            resultViewer resultViewer = new resultViewer();
            resultViewer.Show();
        }

        private string selectLapNumber(string eventId, string categoria, string tabla)
        {
            string returnStr = "";
            int vueltas = 0;
            // Asignar numero de vueltas segun la categorias
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            switch (categoria)
            {
                case "General":
                case "Masculino":
                case "Femenino":
                    cmd.CommandText = "SELECT MAX(vueltas) FROM [categorias] WHERE id = '" + eventId + "'";
                    break;
                default:
                    cmd.CommandText = "SELECT vueltas FROM [categorias] WHERE id = '" + eventId + "' AND categoria = '" + categoria + "'";
                    break;
            }

            if (sqlServerConnect(connection))
            {
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    if (reader[0].ToString() != "")
                    {
                        vueltas = Convert.ToInt16(reader[0].ToString());

                        for (int i = 1; i <= vueltas; i++)
                        {
                            returnStr = returnStr + tabla +".v" + i.ToString() + ",";
                        }
                    }
                }

                connection.Close();
            }

            // si la categoria tiene una sola vuelta
            // no mostrarla, solo se vera el tiempo total
            if (vueltas == 1)
            {
                returnStr = "";
            }
           
            return returnStr;
        }

        private void agregarNumeroClasificacion(string numero, DateTime horaDePasoActual, int puestoDeControl, string source)
        {
            // si se detecto el chip inscripto en algun evento...
            string idEvento = "";
            if (numero != "")
            {
                SqlConnection connectionDbMain = new SqlConnection(gConnectionString);
                SqlCommand cmd = connectionDbMain.CreateCommand();
                cmd.CommandType = CommandType.Text;

                // verificar a que evento pertenece el chip detectado
                connectionDbMain.Open();

                cmd.CommandText = "SELECT id FROM [finishers] WHERE numero = '" + numero + "'";
                SqlDataReader inscriptoReader;
                try
                {
                    inscriptoReader = cmd.ExecuteReader();
                    if (inscriptoReader.Read())
                    {
                        idEvento = inscriptoReader["id"].ToString();
                    }
                }
                catch { }
              
                connectionDbMain.Close();

                if (idEvento != "")
                {
                    int index = evId.IndexOf(idEvento);
                    string estadoEvento = evStatus[index];
                    string tipoEvento = evType[index];
                    string nombreEvento = evName[index];

                    int currentLap = 0; // es la vuelta que se esta completando
                    int totalLaps = 0;
                    string estado = "";
                    string categoria = "";
                    string vueltaStr = "";
                    TimeSpan vueltaMinima = evVuelta[index];
                    DateTime[] vueltas = new DateTime[11]; // incluye la largada!!!
                    int vtaNb = 0;

                    // buscar que vuelta es la que completa el corredor
                    cmd.CommandText = "SELECT * FROM finishers JOIN categorias ON categorias.categoria = finishers.categoria AND finishers.numero = '" + numero + "'";
                    connectionDbMain.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        currentLap = Convert.ToInt16(reader["lap"].ToString());
                        totalLaps = Convert.ToInt16(reader["vueltas"].ToString());
                        estado = reader["estado"].ToString();
                        categoria = reader["categoria"].ToString();
                        vueltas[vtaNb++] = checkBox3.Checked ? reader.GetDateTime(reader.GetOrdinal("v0")) : reader.GetDateTime(reader.GetOrdinal("largada")); // vueltas[0] es la largada
                        for (; vtaNb < totalLaps; vtaNb++)
                        {
                            vueltas[vtaNb] = reader.GetDateTime(reader.GetOrdinal("v"+ vtaNb.ToString()));
                        }
                    }

                    connectionDbMain.Close();
                    TimeSpan tiempoTotal = horaDePasoActual - vueltas[0]; // tiempo total es siempre el paso actual registrado menos la hora de largada

                    if ((estadoEvento == "en curso") && (estado == "2-running") && puestoDeControl != -1)
                    {
                        switch (tipoEvento)
                        {
                            case "vueltas":
                                // calcular tiempo de vuelta

                                TimeSpan vuelta = horaDePasoActual - vueltas[currentLap];

                                // comparar contra el tiempo minimo de vuelta para no agregar multiples
                                // lecturas de chip
                                //if (vuelta > gVueltaMinima) // hasta aqui tengo el id del evento
                                if (vuelta > vueltaMinima) // hasta aqui tengo el id del evento
                                {
                                    currentLap++;
                                    vueltaStr = "v" + currentLap.ToString();

                                    if (currentLap == totalLaps)
                                    {
                                        estado = "1-finish";
                                    }
                                    // Lapeo Automatico
                                    else if (checkBox11.CheckState == CheckState.Checked)
                                    {
                                        // ver si la categoria esta para lapear
                                        int indiceCat = Array.IndexOf(catLapeo, categoria);
                                        if (indiceCat != -1)
                                        {
                                            if ((horaDePasoActual >= horaLapeo[indiceCat] && currentLap == vueltaActual[indiceCat]) || (currentLap < vueltaActual[indiceCat]))
                                            {
                                                estado = "3-lapped";
                                            }
                                        }
                                        // ver si la categoria ya finalizo
                                        else if (Array.IndexOf(catFinish, categoria) != -1)
                                        {
                                            estado = "3-lapped";
                                        }
                                    }
                                }
                                break;
                            case "raid":
                                if (puestoDeControl > 0) // es paso por un puesto de control
                                {
                                    // si vueltas[puestoDeControl] es distinto de (1/1/1900)
                                    // entoces incrementar currentlap
                                    if (vueltas[puestoDeControl].Year == 1900)
                                    {
                                        vueltaStr = 'v' + puestoDeControl.ToString();
                                        currentLap++;
                                    }
                                }
                                else if (puestoDeControl == 0)// es paso por meta
                                {
                                    vueltaStr = 'v' + totalLaps.ToString();
                                    estado = "1-finish";
                                    currentLap++;
                                    // NOTA: si current lap es distinto de total laps al final,
                                    // quiere decir que se salteo algun pc
                                }
                                break;
                        }
                    }
                    else if (puestoDeControl == -1 && (estado == "7-to start")) // TODO caso largada individual
                    {
                        vueltaStr = "v0"; // TODO es largada sino "v0"
                        tiempoTotal = new TimeSpan(23, 59, 59);
                        currentLap = 0;
                        estado = "2-running";

                        if (estadoEvento == "a venir")
                        {
                            cmd.CommandText = "UPDATE [eventos] SET estado = 'en curso' WHERE id = '" + idEvento + "'";
                            connectionDbMain.Open();
                            try
                            {
                                cmd.ExecuteNonQuery();
                                LlenarEventosRam();
                            }
                            catch { }
                            connectionDbMain.Close();
                        }

                        //TODO chequear hora de largada en tabla "categorias" y ponerle hr de largada
                    }

                    if (vueltaStr != "")
                    {
                        cmd.CommandText = "UPDATE [finishers] SET " + vueltaStr + " = '" + horaDePasoActual.ToString("MM-dd-yyyy HH:mm:ss.fffffff") + "', tiempo = '" + tiempoTotal.ToString() + "', lap = '" + currentLap + "', estado = '" + estado + "' WHERE numero = '" + numero + "'";
                        connectionDbMain.Open();
                        try
                        {
                            cmd.ExecuteNonQuery();
                            addToLog(idEvento, source, numero, "estadistica lectura");
                            gEvento = nombreEvento;
                            gCategoria = categoria;
                            string appendTexto = numero + ", " + horaDePasoActual.ToString("HH:mm:ss") + ", " + currentLap.ToString() + "/" + totalLaps.ToString() + "\r\n";
                            gUpdateDataGrid = true;

                            try
                            {
                                File.AppendAllText(supportFilesPath + "\\timestamps.txt", appendTexto); // usar un solo archivo log
                            }
                            catch { }                            
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message + "\n\r" + cmd.CommandText);
                        }

                        connectionDbMain.Close();
                    }
                }
            }
            else
            {               
                // agregar al log de eventos
                //addToLog(idEvento, "clasificacion", numero, "chip o num no registrado");
            }
        }

        private void textBox5_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void label22_Click(object sender, EventArgs e)
        {

        }

        private void comboBox6_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox21_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void label48_Click(object sender, EventArgs e)
        {

        }

        private void comboBox6_TextChanged(object sender, EventArgs e)
        {
            if (comboBox6.Text != "")
            {
                string eventId = buscarIdEventoPorNombre(comboBox6.Text);
                checkedListBox1.Items.Clear();
                checkedListBox1.Items.Add("Todas");
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT categoria FROM [categorias] WHERE id = '" + eventId + "' ORDER BY sexo DESC, inicio ASC";
                sqlServerConnect(connection);
                SqlDataReader catReader = cmd.ExecuteReader();

                while (catReader.Read())
                {
                    checkedListBox1.Items.Add(catReader["categoria"].ToString());
                }

                connection.Close();
            }
        }

        private void label50_Click(object sender, EventArgs e)
        {

        }

        private void textBox19_TextChanged(object sender, EventArgs e)
        {

        }

        private void main_Load(object sender, EventArgs e)
        {
            //MessageBox.Show("llamando main load ()");
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button36_Click(object sender, EventArgs e)
        {

        }

        private void button39_Click(object sender, EventArgs e)
        {
        }

        private void button37_Click(object sender, EventArgs e)
        {
            // TODO necesita full integracion segun el tipo de lector
            textBox20.Text = UsbReader.AnswerModeData();
            if(textBox20.Text == "")
            {
                textBox20.Text = ZkReader.AnswerModeData(gAntAdrr[0], gPortsConnected[0]);  
            }
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }

        private string ByteArrayToHexString(byte[] data)
        {
            StringBuilder sb = new StringBuilder(data.Length * 3);
            foreach (byte b in data)
                sb.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
            return sb.ToString().ToUpper();

        }

        private void button38_Click(object sender, EventArgs e)
        {
            if (textBox21.Text != "" && textBox20.Text != "")
            {
                // TODO necesita full integracion
                string EPC_new = textBox21.Text.PadLeft(24, '0');
                string EPC_or = textBox20.Text;
                if (UsbReader.WriteCard_G2(EPC_new) || ZkReader.WriteCard_G2(gAntAdrr[0], EPC_or, EPC_new, gPortsConnected[0]))
                {
                    // MessageBox.Show("Error! No se cambio el EPC!");
                    textBox20.Text = EPC_new;
                }
            }
        }

        private void comboBox5_SelectedIndexChanged(object sender, EventArgs e)
        {
            // mostrar en datagridview3 los participantes de la categoria
            // cuyo texto aparece en el combobox
            string eventId = buscarIdEventoPorNombre(comboBox9.Text);
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;

            sqlServerConnect(connection);
            if (comboBox5.Text == "")
            {
                cmd.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.team_id,finishers.chip,finishers.categoria,corredores.sexo,corredores.ciudad,corredores.dni FROM corredores, finishers WHERE corredores.dni = finishers.dni AND id = '" + eventId + "'";
            }
            else
            {
                cmd.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.team_id,finishers.chip,finishers.categoria,corredores.sexo,corredores.ciudad,corredores.dni FROM corredores, finishers WHERE corredores.dni = finishers.dni AND finishers.categoria = '" + comboBox5.Text + "' ORDER BY finishers.numero DESC";
            }
            cmd.ExecuteNonQuery();
            DataTable dta = new DataTable();
            SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
            dataadapt.Fill(dta);
            dataGridView3.DataSource = dta;

            // mostrar numero de fila
            foreach (DataGridViewRow row in dataGridView3.Rows)
            {
                row.HeaderCell.Value = (row.Index + 1).ToString();
            }
            connection.Close();
        }

        private void button13_Click_1(object sender, EventArgs e)
        {
            if(actualizadDatosCorredor(comboBox9.Text, comboBox44.Text, textBox7.Text, dateTimePicker3.Value, comboBox4.Text, textBox16.Text,
                textBox36.Text, textBox32.Text, textBox13.Text, textBox28.Text, textBox8.Text, comboBox5.Text))
            {
                comboBox44.Text = "";
                comboBox44.Items.Clear();
                textBox7.Text = "";

                dateTimePicker3.Value = DateTime.Now;
                comboBox4.Text = "";
                textBox32.Text = "";
                textBox36.Text = "";

                textBox16.Text = "";
                label93.Text = "ultimo numero:\n\r" + textBox13.Text + " (actualizar)";

                textBox8.Text = "";
                textBox28.Text = "";
                textBox13.Text = "";
                //comboBox5.Text = "";

                //comboBox9_TextChanged(null, null);
                //comboBox5.Text = categoria;
                comboBox5_SelectedIndexChanged(null, null);
            }

            //actualizar compañero
            if (actualizadDatosCorredor(comboBox9.Text, textBox44.Text, textBox47.Text, 
                dateTimePicker9.Value, 
                comboBox52.Text, textBox45.Text, textBox42.Text, textBox43.Text, textBox46.Text, textBox49.Text, textBox48.Text, comboBox50.Text))
            {
                SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                sqlServerConnect(connection);
                cmd.CommandText = "SELECT corredores.nombre, finishers.numero, finishers.team_id, finishers.chip, finishers.categoria, corredores.sexo, corredores.dni  FROM finishers, corredores WHERE  finishers.dni = corredores.dni AND finishers.team_id = '" + textBox49.Text + "' ORDER BY finishers.numero ASC";
                cmd.ExecuteNonQuery();
                DataTable dta = new DataTable();
                SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                dataadapt.Fill(dta);
                dataGridView3.DataSource = dta;
                connection.Close();

                textBox44.Text = "";
                textBox47.Text = "";

                dateTimePicker9.Value = DateTime.Now;
                comboBox52.Text = "";
                textBox45.Text = "";
                textBox42.Text = "";

                textBox43.Text = "";
                label93.Text += "\n\r" + textBox46.Text + " (actualizar)";

                textBox46.Text = "";
                textBox49.Text = "";
                textBox48.Text = "";
            }
            /*
            if ((comboBox44.Text != "")
                    && (textBox7.Text != "")
                    && (dateTimePicker3.Text != "") && (comboBox4.Text != "")
                    && (textBox13.Text != "") && (comboBox9.Text != ""))
            {
                SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                string categoria = "";

                // Conseguir id evento donde el participante sera inscripto
                string idEvento = buscarIdEventoPorNombre(comboBox9.Text);

                // chequear si el corredor esta en la lista de inscriptos
                cmd.CommandText = "SELECT dni FROM [finishers] WHERE dni = '" + comboBox44.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader existeReader = cmd.ExecuteReader();
                bool existeCorredor = existeReader.Read();
                connection.Close();

                if (existeCorredor)
                {
                    // si comboBox5 tiene un valor, forzar ese valor como categoria. chequear categoria!
                    // Si comboBox5 = "", calcular categoria automaticamente.

                    if (comboBox5.Text != "" && gCatNombres.Contains(idEvento + "-" + comboBox5.Text))
                    {
                        categoria = comboBox5.Text;
                    }
                    else // calcular la categoria.
                    {
                        DateTime nacimiento = dateTimePicker3.Value;
                        string[] catArray = new string[10];
                        calcularCategoria(idEvento, nacimiento, comboBox4.Text, ref catArray);
                        categoria = catArray[0];
                    }

                    if (categoria != "")
                    {
                        // antes de actualizar registro, se deberia chequear numero OR chip
                        // no existan ya para el evento seleccionado
                        bool editar = true;
                        bool numYaExiste = chequearValorExisteEnColumna("finishers", "numero", textBox13.Text);
                        bool chipYaExiste = chequearValorExisteEnColumna("finishers", "chip", textBox8.Text);
                        if (numYaExiste)
                        {
                            // numero pertenece al corredor editando?
                            cmd.CommandText = "SELECT dni FROM [finishers] WHERE numero = '" + textBox13.Text + "'";
                            sqlServerConnect(connection);
                            SqlDataReader dniReader = cmd.ExecuteReader();
                            string dniLeido = "";
                            if (dniReader.Read())
                            {
                                dniLeido = dniReader["dni"].ToString();

                                if (dniLeido != comboBox44.Text)
                                {
                                    // otro corredor tiene este numero
                                    // No editar!
                                    editar = false;
                                    MessageBox.Show("El NUMERO ya esta en uso por otro corredor!");
                                }
                            }
                            connection.Close();
                        }

                        if (chipYaExiste)
                        {
                            // numero pertenece al corredor editando?
                            cmd.CommandText = "SELECT dni FROM [finishers] WHERE chip = '" + textBox8.Text + "'";
                            sqlServerConnect(connection);
                            SqlDataReader dniReader = cmd.ExecuteReader();
                            string dniLeido = "";
                            if (dniReader.Read())
                            {
                                dniLeido = dniReader["dni"].ToString();

                                if (dniLeido != comboBox44.Text)
                                {
                                    // otro corredor tiene este chip
                                    // No editar!
                                    editar = false;
                                    MessageBox.Show("El CHIP ya esta en uso por otro corredor!");
                                }
                            }
                            connection.Close();
                        }

                        if (editar)
                        {
                            try
                            {
                                DialogResult dialogResult = MessageBox.Show(textBox7.Text + "\r\n" + "Numero: " + textBox13.Text + "\r\n" + "team_id: " + textBox28.Text + "\r\n" + "chip: " + textBox8.Text + "\r\n" + "Categoria: " + categoria, "Editar corredor?", MessageBoxButtons.YesNo);

                                if (dialogResult == DialogResult.Yes)
                                {
                                    cmd.CommandText = "UPDATE [finishers] SET numero ='" + textBox13.Text + "', team_id ='" + textBox28.Text + "', categoria ='" + categoria + "', chip='" + textBox8.Text + "', id='" + idEvento + "' WHERE dni = '" + comboBox44.Text + "'";
                                    sqlServerConnect(connection);
                                    cmd.ExecuteNonQuery();

                                    connection.Close();
                                    // insertar en base de datos principal
                                    try
                                    {
                                        cmd.CommandText = "UPDATE [corredores] SET nombre = '" + textBox7.Text.ToLower() + "', sexo = '" + comboBox4.Text.ToLower() + "', ciudad = '" + textBox16.Text + "', nacimiento = '" + dateTimePicker3.Value.ToString("MM-dd-yyyy") + "', campo = '" + textBox32.Text + "', team = '" + textBox36.Text + "' WHERE dni = '" + comboBox44.Text + "'";
                                        sqlServerConnect(connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                    }
                                      
                                    comboBox44.Text = "";
                                    comboBox44.Items.Clear();
                                    textBox7.Text = "";

                                    dateTimePicker3.Value = DateTime.Now;
                                    comboBox4.Text = "";
                                    textBox32.Text = "";
                                    textBox36.Text = "";

                                    textBox16.Text = "";
                                    label93.Text = "ultimo numero: " + textBox13.Text + " (actualizar)";

                                    //if (textBox13.Text.Contains("B") || checkBox14.CheckState == CheckState.Unchecked)
                                    {
                                        textBox8.Text = "";
                                        textBox28.Text = "";
                                        textBox13.Text = "";
                                        comboBox5.Text = "";
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                        comboBox9_TextChanged(null, null);
                        comboBox5.Text = categoria;
                    }
                    else
                    {
                        // no se encontro categoria, agregar al log
                        addToLog(idEvento, "categoria", textBox13.Text, "sin cat");
                    }
                }
                else
                {
                    MessageBox.Show("Corredor no existe entre los inscriptos");
                }
            }
            */
        }

        private bool actualizadDatosCorredor(string evento, string dni, string nombre, DateTime fechadenac, string sexo, string ciudad, string team, string campo, string numero, string teamid, string chip, string categoria)
        {
            bool retVal = false;

            if ((evento != "")
                && (nombre != "") /*&& (textBox8.Text != "")*/
                && (fechadenac.ToString() != "") && (dni != "")
                && (numero != "") && (teamid != ""))
            {
                SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                string cat = "";

                // Conseguir id evento donde el participante sera inscripto
                string idEvento = buscarIdEventoPorNombre(evento);

                // chequear si el corredor esta en la lista de inscriptos
                cmd.CommandText = "SELECT dni FROM [finishers] WHERE dni = '" + dni + "'";
                sqlServerConnect(connection);
                SqlDataReader existeReader = cmd.ExecuteReader();
                bool existeCorredor = existeReader.Read();
                connection.Close();

                if (existeCorredor)
                {
                    // si comboBox5 tiene un valor, forzar ese valor como categoria. chequear categoria!
                    // Si comboBox5 = "", calcular categoria automaticamente.

                    if (categoria != "" && gCatNombres.Contains(idEvento + "-" + categoria))
                    {
                        cat = categoria;
                    }
                    else // calcular la categoria.
                    {
                        //DateTime nacimiento = dateTimePicker3.Value;
                        string[] catArray = new string[10];
                        calcularCategoria(idEvento, fechadenac, sexo, ref catArray);
                        cat = catArray[0];
                    }

                    if (cat != "")
                    {
                        // antes de actualizar registro, se deberia chequear numero OR chip
                        // no existan ya para el evento seleccionado
                        bool editar = true;
                        bool numYaExiste = chequearValorExisteEnColumna("finishers", "numero", numero);
                        bool chipYaExiste = chequearValorExisteEnColumna("finishers", "chip", chip);
                        if (numYaExiste)
                        {
                            // numero pertenece al corredor editando?
                            cmd.CommandText = "SELECT dni FROM [finishers] WHERE numero = '" + numero + "'";
                            sqlServerConnect(connection);
                            SqlDataReader dniReader = cmd.ExecuteReader();
                            string dniLeido = "";
                            if (dniReader.Read())
                            {
                                dniLeido = dniReader["dni"].ToString();

                                if (dniLeido != dni)
                                {
                                    // otro corredor tiene este numero
                                    // No editar!
                                    editar = false;
                                    MessageBox.Show("El NUMERO ya esta en uso por otro corredor!");
                                }
                            }
                            connection.Close();
                        }

                        if (chipYaExiste)
                        {
                            // numero pertenece al corredor editando?
                            cmd.CommandText = "SELECT dni FROM [finishers] WHERE chip = '" + chip + "'";
                            sqlServerConnect(connection);
                            SqlDataReader dniReader = cmd.ExecuteReader();
                            string dniLeido = "";
                            if (dniReader.Read())
                            {
                                dniLeido = dniReader["dni"].ToString();

                                if (dniLeido != dni)
                                {
                                    // otro corredor tiene este chip
                                    // No editar!
                                    editar = false;
                                    MessageBox.Show("El CHIP ya esta en uso por otro corredor!");
                                }
                            }
                            connection.Close();
                        }

                        if (editar)
                        {
                            try
                            {
                                DialogResult dialogResult = MessageBox.Show(nombre + "\r\n" + "Numero: " + numero + "\r\n" + "team_id: " + teamid + "\r\n" + "chip: " + chip + "\r\n" + "Categoria: " + cat, "Editar corredor?", MessageBoxButtons.YesNo);

                                if (dialogResult == DialogResult.Yes)
                                {
                                    cmd.CommandText = "UPDATE [finishers] SET numero ='" + numero + "', team_id ='" + teamid + "', categoria ='" + cat + "', chip='" + chip + "', id='" + idEvento + "' WHERE dni = '" + dni + "'";
                                    sqlServerConnect(connection);
                                    cmd.ExecuteNonQuery();

                                    connection.Close();
                                    // insertar en base de datos principal
                                    try
                                    {
                                        cmd.CommandText = "UPDATE [corredores] SET nombre = '" + nombre.ToLower() + "', sexo = '" + sexo.ToLower() + "', ciudad = '" + ciudad + "', nacimiento = '" + fechadenac.ToString("MM-dd-yyyy") + "', campo = '" + campo + "', team = '" + team + "' WHERE dni = '" + dni + "'";
                                        sqlServerConnect(connection);
                                        cmd.ExecuteNonQuery();
                                        connection.Close();
                                    }
                                    catch (Exception ex)
                                    {
                                        MessageBox.Show(ex.Message);
                                    }

                                    retVal = true;
                                }
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                        //comboBox9_TextChanged(null, null);
                        //comboBox5.Text = categoria;
                    }
                    else
                    {
                        // no se encontro categoria, agregar al log
                        MessageBox.Show("No se pudo calcular la categoria");
                        addToLog(idEvento, "categoria", textBox13.Text, "sin cat");
                    }
                }
                else
                {
                    MessageBox.Show("Corredor no existe entre los inscriptos");
                }
            }

            return retVal;
        }

        private void groupBox6_Enter(object sender, EventArgs e)
        {

        }

        private void label58_Click(object sender, EventArgs e)
        {

        }

        private void comboBox22_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void textBox8_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox8.Text != "")
                {
                    string dniStr = "";
                    string nombreStr = "";
                    DateTime nacimiento = new DateTime();
                    string campo = "";
                    string sexoStr = "";
                    string teamId = "";
                    string numero = "";
                    string chip = "";
                    string categoria = "";
                    string ciudad = "";
                    string runningteam = "";
                    string eventId = "";

                    SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;

                    // buscar numero en la tabla de finishers
                    sqlServerConnect(connection);
                    cmd.CommandText = "SELECT finishers.id, finishers.numero, finishers.chip, finishers.team_id, finishers.categoria, corredores.nacimiento, corredores.ciudad, corredores.campo, corredores.sexo, corredores.nombre, corredores.dni, corredores.team FROM finishers, corredores WHERE  finishers.dni = corredores.dni AND finishers.chip = '" + textBox8.Text + "'";
                    SqlDataReader chipInscipto = cmd.ExecuteReader();

                    if (chipInscipto.Read())
                    {
                        // datos corredor
                        nombreStr = chipInscipto["nombre"].ToString();
                        campo = chipInscipto["campo"].ToString();
                        ciudad = chipInscipto["ciudad"].ToString();
                        dniStr = chipInscipto["dni"].ToString();
                        sexoStr = chipInscipto["sexo"].ToString();
                        runningteam = chipInscipto["team"].ToString();
                        nacimiento = Convert.ToDateTime(chipInscipto["nacimiento"].ToString());
                        // datos corredor en carrera
                        eventId = chipInscipto["id"].ToString();
                        teamId = chipInscipto["team_id"].ToString();
                        numero = chipInscipto["numero"].ToString();
                        chip = chipInscipto["chip"].ToString();
                        categoria = chipInscipto["categoria"].ToString();

                        comboBox9.Text = evName[evId.IndexOf(eventId)];
                        textBox7.Text = nombreStr;
                        dateTimePicker3.Value = nacimiento;
                        comboBox4.Text = sexoStr;
                        comboBox5.Text = categoria;
                        comboBox44.Text = dniStr;
                        textBox13.Text = numero;
                        textBox28.Text = teamId;
                        textBox32.Text = campo;
                        textBox16.Text = ciudad;
                        textBox36.Text = runningteam;
                    }

                    connection.Close();

                    if (chip != "")
                    {
                        sqlServerConnect(connection);
                        cmd.CommandText = "SELECT corredores.nombre, finishers.numero, finishers.team_id, finishers.chip, finishers.categoria, corredores.sexo, corredores.dni  FROM finishers, corredores WHERE  finishers.dni = corredores.dni AND finishers.categoria = '" + categoria + "' ORDER BY finishers.numero ASC";
                        cmd.ExecuteNonQuery();
                        DataTable dta = new DataTable();
                        SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                        dataadapt.Fill(dta);
                        dataGridView3.DataSource = dta;
                        connection.Close();
                    }
                }
            }
        }

        private void textBox13_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox13.Text != "")
                {
                    comboBox44.Text = "";
                    string dniStr = "";
                    string nombreStr = "";
                    DateTime nacimiento = new DateTime();
                    string campo = "";
                    string sexoStr = "";
                    string teamId = "";
                    string numero = "";
                    string chip = "";
                    string categoria = "";
                    string ciudad = "";
                    string runningteam = "";
                    string eventId = "";

                    SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;

                    // buscar numero en la tabla de finishers
                    sqlServerConnect(connection);
                    cmd.CommandText = "SELECT finishers.id, finishers.numero, finishers.chip, finishers.team_id, finishers.categoria, corredores.nacimiento, corredores.ciudad, corredores.campo, corredores.sexo, corredores.nombre, corredores.dni, corredores.team FROM finishers, corredores WHERE  finishers.dni = corredores.dni AND finishers.numero = '" + textBox13.Text + "'";
                    SqlDataReader numeroInscipto = cmd.ExecuteReader();

                    if (numeroInscipto.Read())
                    {
                        // datos corredor
                        nombreStr = numeroInscipto["nombre"].ToString();
                        campo = numeroInscipto["campo"].ToString();
                        ciudad = numeroInscipto["ciudad"].ToString();
                        dniStr = numeroInscipto["dni"].ToString();
                        sexoStr = numeroInscipto["sexo"].ToString();
                        runningteam = numeroInscipto["team"].ToString();
                        nacimiento = Convert.ToDateTime(numeroInscipto["nacimiento"].ToString());
                        // datos corredor en carrera
                        eventId = numeroInscipto["id"].ToString();
                        teamId = numeroInscipto["team_id"].ToString();
                        numero = numeroInscipto["numero"].ToString();
                        chip = numeroInscipto["chip"].ToString();
                        categoria = numeroInscipto["categoria"].ToString();

                        comboBox9.Text = evName[evId.IndexOf(eventId)];
                        textBox7.Text = nombreStr;
                        dateTimePicker3.Value = nacimiento;
                        comboBox4.Text = sexoStr;
                        comboBox5.Text = categoria;
                        comboBox44.Text = dniStr;
                        textBox8.Text = chip;
                        textBox28.Text = teamId;
                        textBox32.Text = campo;
                        textBox16.Text = ciudad;
                        textBox36.Text = runningteam;
                    }

                    connection.Close();

                    if (numero != "")
                    {
                        sqlServerConnect(connection);
                        cmd.CommandText = "SELECT corredores.nombre, finishers.numero, finishers.team_id, finishers.chip, finishers.categoria, corredores.sexo, corredores.dni  FROM finishers, corredores WHERE  finishers.dni = corredores.dni AND finishers.categoria = '" + categoria + "' ORDER BY finishers.numero ASC";
                        cmd.ExecuteNonQuery();
                        DataTable dta = new DataTable();
                        SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                        dataadapt.Fill(dta);
                        dataGridView3.DataSource = dta;
                        connection.Close();
                    }
                }
                if (comboBox44.Text != "")
                {
                    poblarDatosCorredor();
                }
            }
        }

        private void dataGridView4_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button42_Click(object sender, EventArgs e)
        {
            CheckedListBox.CheckedIndexCollection checkedIndex = checkedListBox1.CheckedIndices;
            CheckedListBox.CheckedItemCollection checkedCats = checkedListBox1.CheckedItems;

            if ((comboBox6.Text != "") && (checkedCats.Count > 0))
            {
                DialogResult dialogResult = new DialogResult();
                string eventId = buscarIdEventoPorNombre(comboBox6.Text);
                string cats = "";

                foreach (string categoria in checkedCats)
                {
                    cats += categoria + ", ";
                }

                dialogResult = MessageBox.Show("Modificar la hora de largada de la(s) categoria(s): " + cats + " la nueva hora es: " + dateTimePicker5.Value.ToString(), "Editar hora de largada", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    foreach (string categoria in checkedCats)
                    {
                        cmd.CommandText = "UPDATE [categorias] SET largada = '" + dateTimePicker5.Value.ToString("MM-dd-yyyy HH:mm:ss") + "' WHERE categoria = '" + categoria + "' AND id = '" + eventId + "';";

                        sqlServerConnect(connection);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }

                    if (checkedCats.Contains("Todas"))
                    {
                        cmd.CommandText += "UPDATE [eventos] SET fecha = '" + dateTimePicker5.Value.ToString("MM-dd-yyyy HH:mm:ss") + "' WHERE id = '" + eventId + "'";
                        sqlServerConnect(connection);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }

                    foreach (int idx in checkedIndex)
                    {
                        checkedListBox1.SetItemCheckState(idx, CheckState.Unchecked);
                    }

                    RecalcularTiempoDeCarrera(comboBox6.Text);
                    comboBox7.Text = comboBox6.Text;
                    comboBox7_TextChanged(null, null);
                    LlenarEventosRam(); //en caso haya modificado la hora del evento
                    load_cats();
                }
            }
        }

        private void button43_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "")
            {
                cambiarEstado(textBox18.Text, "2-running");
                calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
            }
        }

        private void checkedListBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (checkedListBox1.SelectedIndex == 0) // "Todas" seleccionado
            {
                CheckState checkState = checkedListBox1.GetItemCheckState(0);
                int itemsCount = checkedListBox1.Items.Count;
                for (int idx = 0; idx < itemsCount; idx++)
                {
                    checkedListBox1.SetItemCheckState(idx, checkState);
                }
            }
            else
            {
                checkedListBox1.SetItemCheckState(0, CheckState.Unchecked);
            }
        }

        private void button45_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "")
            {
                cambiarEstado(textBox18.Text, "6-dns");
                calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
            }
        }

        private void button47_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "" && textBox23.Text != "")
            {         
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [finishers] WHERE numero = '" + textBox18.Text + "'";

                try
                {
                    sqlServerConnect(connection);
                    SqlDataReader reader = cmd.ExecuteReader();

                    if (reader.Read())
                    {
                        DialogResult dialogResult = MessageBox.Show("Corredor Numero: " + textBox18.Text + ". Modificar Vueltas?", "Editor de Resultado", MessageBoxButtons.YesNo);

                        if (dialogResult == DialogResult.Yes)
                        {
                            cmd.CommandText = "UPDATE [finishers] SET lap = '" + textBox23.Text + "'  WHERE numero = '" + textBox18.Text + "'";
                            connection.Close();
                            sqlServerConnect(connection);
                            try
                            {
                                cmd.ExecuteNonQuery();

                                //add to log
                                addToLog("", "modif vueltas", textBox18.Text, "a 1 vuelta");
                            }
                            catch (Exception ex)
                            {
                                MessageBox.Show(ex.Message);
                            }
                        }
                    }
                    else
                    {
                        MessageBox.Show("El corredor no esta en la lista de largada");
                    }

                    connection.Close();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button46_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "" && comboBox23.Text != "")
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                try
                {
                    SqlDataReader reader;
                    string newCat = comboBox23.Text;
                    string newLargada = "";

                    cmd.CommandText = "SELECT largada FROM [categorias] WHERE categoria = '" + newCat + "'";

                    sqlServerConnect(connection);
                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        newLargada = DateTime.Parse(reader["largada"].ToString()).ToString("MM-dd-yyyy HH:mm:ss");
                    }
                    connection.Close();

                    DialogResult dialogResult = MessageBox.Show("Corredor Numero: " + textBox18.Text + ". Modificar Categoria?", "Editor de Resultado", MessageBoxButtons.YesNo);

                    if (dialogResult == DialogResult.Yes)
                    {
                        cmd.CommandText = "UPDATE [finishers] SET categoria = '" + newCat + "' WHERE numero = '" + textBox18.Text + "'";
                        sqlServerConnect(connection);
                        try
                        {
                            cmd.ExecuteNonQuery();
                            //add to log
                            addToLog("", "modif vueltas", textBox18.Text, " ");
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button48_Click(object sender, EventArgs e)
        {
            if (comboBox24.Text != "")
            {
                DialogResult dialogResult = MessageBox.Show("", "Importar Set de catecorias: " + comboBox24.Text + "?", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    string eventId = buscarIdEventoPorNombre(comboBox2.Text);
                    if (eventId != "")
                    {
                        SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                        cmd.CommandType = CommandType.Text;
                        DateTime startTime = new DateTime(1900,01,01,00,00,00);
                        cmd.CommandText = "INSERT INTO [categorias] (categoria,inicio,fin,sexo,id) SELECT categoria,inicio,fin,sexo,nombre FROM [db_categorias] WHERE nombre = '" + comboBox24.Text + "'; UPDATE [categorias] SET id = '" + eventId + "', largada = '" + startTime.ToString() + "', vueltas = 1 WHERE id = '" + comboBox24.Text + "'";
                        sqlServerConnect(connection);
                        try
                        {
                            cmd.ExecuteNonQuery();
                            poblarComboBoxColumna("db_categorias", "nombre", comboBox24, null);
                            poblarComboBoxColumna("db_categorias", "nombre", comboBox38, null); // campeonatos
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                        connection.Close();
                    }
                }
            }
        }

        private void button49_Click(object sender, EventArgs e)
        {
            if (textBox24.Text != "" && comboBox2.Text != "")
            {
                string nombreSet = textBox24.Text;
                string eventId = buscarIdEventoPorNombre(comboBox2.Text);

                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "INSERT INTO [db_categorias] (categoria,inicio,fin,sexo,nombre) SELECT categoria,inicio,fin,sexo,id FROM [categorias] WHERE id = '" + eventId + "'; UPDATE [db_categorias] SET nombre = '" + nombreSet + "' WHERE nombre = '" + eventId + "'";
                sqlServerConnect(connection);
                try
                {
                    //chequear que el nombre del set no exista aun
                    if (!comboBox24.Items.Contains(nombreSet))
                    {
                        cmd.ExecuteNonQuery();
                        MessageBox.Show("Set de categorias guardado con exito");
                        comboBox24.Items.Add(nombreSet);
                    }
                    else
                    {
                        MessageBox.Show("Nombre del Set ya existe, modificar");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();

            }
        }

        private void comboBox25_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (comboBox25.Text != "")
                {
                    string eventId = buscarIdEventoPorNombre(comboBox2.Text);
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [categorias] WHERE id = '" + eventId + "' AND categoria = '" + comboBox25.Text + "'";
                    sqlServerConnect(connection);
                    try
                    {
                        SqlDataReader reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            textBox19.Text = reader["categoria"].ToString();
                            numericUpDown1.Value = Convert.ToInt16(reader["inicio"].ToString());
                            numericUpDown2.Value = Convert.ToInt16(reader["fin"].ToString());
                            comboBox1.Text = reader["sexo"].ToString();
                            if (reader["vueltas"].ToString() == "")
                            {
                                numericUpDown3.Value = 1;
                            }
                            else
                            {
                                numericUpDown3.Value = Convert.ToInt16(reader["vueltas"].ToString());
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                    connection.Close();
                }
            }
        }

        private void button50_Click(object sender, EventArgs e)
        {
            if ((numericUpDown1.Value >= 0) && (numericUpDown2.Value > 0)
            && (comboBox1.Text != "") && (comboBox2.Text != "")
            && (numericUpDown2.Value >= numericUpDown1.Value))
            {
                // Buscar el id del evento señalado en comboBox2
                // el id evento es primary key para las categorias
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT id FROM [eventos] WHERE nombre = '" + comboBox2.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader eventId = cmd.ExecuteReader();
                eventId.Read();
                string eventIdString = eventId["id"].ToString();
                connection.Close();

                string nombreCategoria = textBox19.Text;
                sqlServerConnect(connection);
                //updatear record en tabla "categoria"
                cmd.CommandText = "UPDATE [categorias] SET inicio = '" + numericUpDown1.Value.ToString() + "',fin = '" + numericUpDown2.Value.ToString() + "',sexo = '" + comboBox1.Text + "', vueltas = '" + numericUpDown3.Value.ToString() + "'  WHERE id = '" + eventIdString + "' AND categoria = '" + nombreCategoria + "';";
                //cmd.CommandText += "UPDATE [finishers] SET laps = '" + numericUpDown3.Value.ToString() + "'  WHERE id = '" + eventIdString + "' AND categoria = '" + nombreCategoria + "';";

                try
                {
                    cmd.ExecuteNonQuery();
                    textBox19.Text = "";
                    numericUpDown1.Value = 0;
                    numericUpDown2.Value = 0;
                    numericUpDown3.Value = 1;
                    comboBox1.Text = "";
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connection.Close();

                // refrescar y mostrar la tabla categorias para el evento sobre el cual se esta agregando la categoria
                mostrarTablaChildEvento(comboBox2.Text, "categorias", dataGridView2, true);
            }
        }

        private void button51_Click(object sender, EventArgs e)
        {
            if (textBox19.Text != "")
            {
                DialogResult dialogResult = MessageBox.Show(textBox19.Text, "Borrar categoria?", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM [categorias] WHERE categoria = '" + textBox19.Text + "' AND id = '" + buscarIdEventoPorNombre(comboBox2.Text) + "'";
                    sqlServerConnect(connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                        textBox19.Text = "";
                        numericUpDown1.Value = 0;
                        numericUpDown2.Value = 0;
                        numericUpDown3.Value = 1;
                        comboBox1.Text = "";
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    connection.Close();
                    // refrescar y mostrar la tabla categorias para el evento sobre el cual se esta agregando la categoria
                    mostrarTablaChildEvento(comboBox2.Text, "categorias", dataGridView2, true);
                    comboBox2_TextChanged(null, null);
                }
            }
        }

        private void comboBox10_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void getPorts(ComboBox comboBox)
        {
            comboBox.Items.Clear();
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\CIMV2", "SELECT * FROM Win32_PnPEntity WHERE Name LIKE '%Port (COM%'");
            //ManagementObjectSearcher searcher = new ManagementObjectSearcher("root\\WMI","SELECT * FROM Win32_SerialPort");
            string[] portNames = new string[20];
            try
            {
                foreach (ManagementObject queryObj in searcher.Get())
                {
                    string caption = queryObj["Caption"].ToString();
                    int idx_of_name = caption.IndexOf("(") + 1;
                    comboBox.Items.Add(caption.Substring(idx_of_name, caption.Length - idx_of_name - 1));
                }
            }
            catch { }

            //string[] ports = SerialPort.GetPortNames();
        }

        private void desconectarAntena(int idx)
        {
            try
            {
                if (!gAntEsWireless[idx])
                {
                    gPortsConnected[idx].Close(); 
                }
                else //si es wireless, hay que ver si es la unica conectada para cerrar el puerto
                {
                    bool otraWireless = false;
                    for (int x = 0; x < 4; x++)
                    {
                        if(gAntEsWireless[x] && x != idx)
                        {
                            otraWireless = true;
                            break;
                        }
                    }

                    if(!otraWireless)
                    {
                        gPortsConnected[idx].Close();
                    }
                }

                gPortsConnected[idx] = null;
                gAntActive[idx] = false;
                gAntConnected[idx] = false;
                //gAntEsWireless[idx] = false;
                gComLabel[idx].Text = "Port Closed";
                gConnectLabel1[idx].Text = "Desconectado";
                gConnectLabel1[idx].ForeColor = Color.FromArgb(255, 10, 10);
                gConnectLabel2[idx].Text = "Desconectado";
                gConnectLabel2[idx].ForeColor = Color.FromArgb(255, 10, 10);

            }
            catch { }
        }

        private void ResetAntenna(int idx)
        {
            if (gAntConnected[idx])
            {
                bool prevThreadStatus = gThreadActive;
                gThreadActive = false;
                bool resetAnt = false;
                switch (gAntType[idx])
                {
                    case AntType.ZK:
                        byte readMode = 5, workmode = 1, filterTime = 5;
                        if (ZkReader.SetWorkMode(gAntAdrr[idx], workmode, readMode, filterTime, gPortsConnected[idx])) // set "answer" Working Mode, single tag, 5 seg filtering.
                        {
                            if (ZkReader.SetPower(gAntAdrr[idx], 50, gPortsConnected[idx])) // Power al 50% (30)
                            {
                                resetAnt = true;
                            }
                        }
                        break;
                    case AntType.INNOTECH:
                        break;
                    case AntType.KAELCO:
                    case AntType.YANPODO_USB:
                    case AntType.UNKNOWN:
                    default:
                        break;
                }

                if (resetAnt) // Power al 50% (30)
                {
                    MessageBox.Show("Antena Reestablecida");
                    gConnectLabel1[idx].Text = "Antena Activada";
                    gConnectLabel1[idx].ForeColor = Color.FromArgb(10, 255, 10);
                    gConnectLabel2[idx].Text = "Activada";
                    gConnectLabel2[idx].ForeColor = Color.FromArgb(10, 255, 10);
                    gThreadActive = true;
                    gAntActive[idx] = true;
                    leerPotencia(idx);
                    CheckThreads();
                }
                else
                {
                    gThreadActive = prevThreadStatus;
                }
            }
        }

        private void button53_Click(object sender, EventArgs e)
        {
            getPorts(comboBox26);
        }

        private void button61_Click(object sender, EventArgs e)
        {
            getPorts(comboBox27);
        }

        private void button69_Click(object sender, EventArgs e)
        {
            getPorts(comboBox28);
        }

        private void button55_Click(object sender, EventArgs e)
        {
            //connectAntenna(ref gAnt2_Type, serialPort2, comboBox26.Text, label49, ref gAnt2Adr);
        }

        private void button63_Click(object sender, EventArgs e)
        {
            //connectAntenna(ref gAnt3_Type, serialPort3, comboBox27.Text, label62, ref gAnt3Adr);
        }

        private void button71_Click(object sender, EventArgs e)
        { 
            
        }

        private void button57_Click(object sender, EventArgs e)
        {
            desconectarAntena(1);
            CheckThreads();
        }

        private void button65_Click(object sender, EventArgs e)
        {
            desconectarAntena(2);
            CheckThreads();
        }

        private void button73_Click(object sender, EventArgs e)
        {
            desconectarAntena(3);
            CheckThreads();
        }

        private void button60_Click(object sender, EventArgs e)
        {
            leerPotencia(1);
        }

        private void button68_Click(object sender, EventArgs e)
        {
            leerPotencia(2);
        }

        private void button76_Click(object sender, EventArgs e)
        {
            leerPotencia(3);
        }

        private bool ajustarPotencia(int idx, int value)
        {
            bool potenciaOk = false;
            gLeerPotenciaText[idx].Text = "";
            if (gAntConnected[idx] == true)
            {
                bool prevThreadStatus = gThreadActive;
                gThreadActive = false;
                if (value <= 100)
                {
                    switch (gAntType[idx])
                    {
                        case AntType.ZK:
                            if (ZkReader.SetPower(gAntAdrr[idx], Convert.ToInt16(value), gPortsConnected[idx]))
                            {
                                gLeerPotenciaText[idx].Text = value.ToString();
                                potenciaOk = true;
                            }
                            break;
                        case AntType.INNOTECH:
                            potenciaOk = InnoReader.StopAntenna(gPortsConnected[idx], gAntAdrr[idx]);
                            potenciaOk &= InnoReader.SetPower(gAntAdrr[idx], Convert.ToInt16(value), gPortsConnected[idx]);
                            potenciaOk &= (!gAntActive[idx] || InnoReader.StartAntenna(gPortsConnected[idx], gAntAdrr[idx]));
                            gLeerPotenciaText[idx].Text = potenciaOk? value.ToString():"";
                            break;
                        case AntType.UNKNOWN:
                        default:
                            break;
                    }
                }

                gThreadActive = prevThreadStatus;
            }

            return potenciaOk;
        }

        private void leerPotencia(int idx)
        {
            gLeerPotenciaText[idx].Text = "";
            if (gAntConnected[idx] == true)
            {
                bool prevThreadStatus = gThreadActive;
                gThreadActive = false;
                int ScanTime = 0;
                int powerDb = 0xff;
                try
                {
                    switch (gAntType[idx])
                    {
                        case AntType.ZK:
                            if (ZkReader.GetReaderInformation(gAntAdrr[idx], ref powerDb, ref ScanTime, gPortsConnected[idx]))
                            {
                                gLeerPotenciaText[idx].Text = powerDb.ToString();
                            }
                            break;
                        case AntType.INNOTECH:
                            if (InnoReader.GetReaderInformation(gAntAdrr[idx], ref powerDb, ref ScanTime, gPortsConnected[idx]))
                            {
                                gLeerPotenciaText[idx].Text = powerDb.ToString();
                            }
                            break;
                        case AntType.UNKNOWN:
                        default:
                            break;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                gThreadActive = prevThreadStatus;
            }
        }

        private bool connectAntenna(SerialPort serialport, Label labelCom, byte antAdrr)
        {
            bool retval = false;

            if (ZkReader.ConnectAntenna(serialport, antAdrr))
            {
                gAntType[antAdrr - 1] = AntType.ZK;
                retval = true;
                labelCom.Text = "CONECTADO ZK_ANT" + antAdrr.ToString("X").PadLeft(2, '0') + " " + serialport.PortName;
            }
            else if(InnoReader.ConnectAntenna(serialport, antAdrr))
            {
                gAntType[antAdrr - 1] = AntType.INNOTECH;
                retval = true;
                labelCom.Text = "CONECTADO INNO_ANT" + antAdrr.ToString("X").PadLeft(2, '0') + " " + serialport.PortName;
            }
            else
            {
                gAntType[antAdrr - 1] = AntType.UNKNOWN;
                labelCom.Text = "No conectado";
            }

            return retval;
        }

        private void AutoConnectAntenas()
        {
            gThreadActive = false;
            getPorts(comboBox10);
            List<string> already_open_commports = new List<string>(20);
            int portIdx = 0;
            string prevCom = "";
            Stopwatch i_Watch = new Stopwatch();
            i_Watch.Start();
            foreach (string comport in comboBox10.Items)
            {
                if (!already_open_commports.Contains(comport))
                {
                    for (int antIdx = 0; antIdx < 4; antIdx++)
                    {
                        //chequear aqui si los comport son wireles o no
                        if (OpenSerialPort(gPortsAvailable[portIdx], comport))
                        {

                            if (gPortsConnected[antIdx] == null)
                            {
                                gPortsConnected[antIdx] = gPortsAvailable[portIdx];
                            }

                            if (comport == prevCom && gAntEsWireless[antIdx - 1] == true)
                            {
                                gAntEsWireless[antIdx] = true;
                            }
                            else if (comport == prevCom && gAntEsWireless[antIdx - 1] == false)
                            {
                                gAntEsWireless[antIdx] = false;
                            }
                            else
                            {
                                gAntEsWireless[antIdx] = IsWireless(gPortsAvailable[portIdx]);
                                prevCom = comport;
                            }

                            if (!gAntConnected[antIdx] && connectAntenna(gPortsConnected[antIdx], gComLabel[antIdx], gAntAdrr[antIdx]))
                            {
                                if (!already_open_commports.Contains(comport))
                                {
                                    already_open_commports.Add(comport);
                                }

                                //gPortsConnected[antIdx] = gPortsAvailable[portIdx];
                                gAntConnected[antIdx] = true;
                                ClickBotonStop(antIdx);
                                leerPotencia(antIdx);

                                // es wireless?
                                // si: aumentar address y mantener comport
                                // no: aumentar address y aumentar comport
                                if (!gAntEsWireless[antIdx])
                                {
                                    break;
                                }
                            }
                            else if (!gAntConnected[antIdx]) //si no hay otra antena usando el puerto
                            {
                                //gAntEsWireless[antIdx] = false;
                                desconectarAntena(antIdx);
                            }
                        }
                    }

                    portIdx++;
                } 
            }

            i_Watch.Stop();
            Debug.Print("Autoconnect ms:" + i_Watch.ElapsedMilliseconds.ToString());

            CheckThreads();
        }

        private bool OpenSerialPort(SerialPort port, string portName)
        {
            bool portOpen = false;

            if (port.IsOpen)
            {
                portOpen = true ;
            }
            else //si el puerto no estaba ya abierto
            {
                try
                {
                    port.PortName = portName;
                    port.BaudRate = 9600;
                    port.ReadTimeout = 1200;
                    port.WriteTimeout = 1200;
                    port.Open();
                    portOpen = true;
                }
                catch (Exception ex)
                {
                    //MessageBox.Show(ex.Message);
                }
            }

            return portOpen;
        }

        private bool IsWireless(SerialPort port)
        {
            bool esWireless = false;

            if (port.IsOpen) //si el puerto no estaba ya abierto
            {
                port.ReadExisting();
                byte[] comando1 = { 0x2b, 0x2b, 0x2b }; //"+++"
                port.Write(comando1, 0, 3);
                byte[] resp = new byte[5];
                Thread.Sleep(200);
                int bytesread = port.Read(resp, 0, port.BytesToRead);
                if (bytesread == 3 && resp[0] == 0x4f) // "OK"
                {
                    byte[] comando2 = { 0x41, 0x54, 0x43, 0x4e, 0x0d }; //"ATCN\r"
                    port.Write(comando2, 0, 5);
                    Thread.Sleep(250);
                    bytesread = port.Read(resp, 0, port.BytesToRead);
                    if (bytesread == 3 && resp[0] == 0x4f) // "OK"
                    {
                        Debug.Print(port.PortName + " es wireless");
                        esWireless = true;
                    }
                }
            }

            return esWireless;
        }

        private void button66_Click(object sender, EventArgs e)
        {
            ClickBotonStop(2);
        }

        private void button58_Click(object sender, EventArgs e)
        {
            ClickBotonStop(1);
        }

        private void button74_Click(object sender, EventArgs e)
        {
            //ClickBotonStop(3);
            if (gAntConnected[3])
            {
                bool prevThreadStaus = gThreadActive;
                gThreadActive = false;

                gAntActive[3] = false;
                if (InnoReader.StopAntenna(gPortsConnected[3], gAntAdrr[3]))
                {
                    Thread.Sleep(20);
                    gConnectLabel1[3].Text = "Desactivada";
                    gConnectLabel1[3].ForeColor = Color.FromArgb(255, 10, 10);

                    gConnectLabel2[3].Text = "Descativada";
                    gConnectLabel2[3].ForeColor = Color.FromArgb(255, 10, 10);

                    gAntActive[3] = false;
                    gThreadActive = false;
                    CheckThreads();
                }
                else
                {
                    gAntActive[3] = true;
                }

                gThreadActive = prevThreadStaus;
            }
        }

        private void button56_Click(object sender, EventArgs e)
        {
            ClickBotonStart(1);
        }

        private void button64_Click(object sender, EventArgs e)
        {
            ClickBotonStart(2);
        }

        private void button72_Click(object sender, EventArgs e)
        {
            ClickBotonStart(3);
        }

        private void button54_Click(object sender, EventArgs e)
        {
            ResetAntenna(1);
        }

        private void button62_Click(object sender, EventArgs e)
        {
            ResetAntenna(2);
        }

        private void button70_Click(object sender, EventArgs e)
        {
            ResetAntenna(3);
        }

        private void label20_TextChanged(object sender, EventArgs e)
        {
            label53.Text = label20.Text;
            label53.ForeColor = label20.ForeColor;
        }

        private void label47_TextChanged(object sender, EventArgs e)
        {
            label72.Text = label47.Text;
            label72.ForeColor = label47.ForeColor;
        }

        private void label60_TextChanged(object sender, EventArgs e)
        {
            label73.Text = label60.Text;
            label73.ForeColor = label60.ForeColor;
        }

        private void label64_TextChanged(object sender, EventArgs e)
        {
            label74.Text = label64.Text;
            label74.ForeColor = label64.ForeColor;
        }

        private void label20_TextChanged_1(object sender, EventArgs e)
        {

        }

        private void label29_TextChanged(object sender, EventArgs e)
        {

        }

        private void button36_Click_1(object sender, EventArgs e)
        {

        }

        // Filtrar la clasificacion por evento y por categoria
        private void calcularClasificacion(string eventName, string categoria, string tableName, DataGridView dataGridView)
        {
            if ((categoria != "") && (eventName != ""))
            {
                // Buscar nombre evento cuyo nombre es eventNameString
                string eventIdString = buscarIdEventoPorNombre(eventName);

                SqlConnection connectionDbMain = new SqlConnection(gConnectionString);
                SqlCommand cmd = connectionDbMain.CreateCommand();
                cmd.CommandType = CommandType.Text;

                // Mostrar la sub tabla "finishers" para el evento cuyo nombre es "comboBox7.Text"
                // y categoria "comboBox8.Text"
                string vueltas = selectLapNumber(eventIdString, categoria, tableName);
                string generales = ",resultados.gral,resultados.gral_sex,resultados.cat";

                if (tableName == "finishers")
                {
                    switch (categoria)
                    {
                        case "General":
                            cmd.CommandText = "SELECT corredores.nombre, finishers.team_id, categorias.categoria, finishers.tiempo, finishers.lap, categorias.vueltas, finishers.estado, categorias.largada, corredores.ciudad FROM finishers JOIN corredores ON finishers.dni = corredores.dni JOIN categorias ON categorias.categoria = finishers.categoria AND finishers.id = '" + eventIdString + "' AND NOT finishers.estado = '7-to start' ORDER BY  finishers.estado ASC, finishers.lap DESC,finishers.tiempo ASC";
                            break;
                        case "Masculino":
                        case "Femenino":
                            cmd.CommandText = "SELECT corredores.nombre,finishers.team_id,finishers.categoria,finishers.tiempo,finishers.lap, categorias.vueltas,finishers.estado, corredores.ciudad FROM finishers JOIN corredores ON corredores.dni = finishers.dni JOIN categorias ON categorias.sexo = corredores.sexo AND categorias.categoria = finishers.categoria WHERE finishers.id = '" + eventIdString + "' AND corredores.sexo = '" + categoria.ToLower() + "' AND NOT finishers.estado = '7-to start' ORDER BY finishers.estado ASC, finishers.lap DESC, finishers.tiempo ASC";
                            break;
                        default:
                            cmd.CommandText = "SELECT corredores.nombre,finishers.team_id,finishers.categoria," + vueltas + tableName + ".tiempo,finishers.lap, categorias.vueltas,finishers.estado, corredores.ciudad FROM finishers JOIN corredores ON corredores.dni =  finishers.dni JOIN categorias ON categorias.categoria =  finishers.categoria WHERE finishers.id = '" + eventIdString + "' AND finishers.categoria = '" + categoria + "' ORDER BY finishers.estado ASC, finishers.lap DESC ,finishers.tiempo ASC";
                            break;
                    }
                }
                else
                {
                    switch (categoria)
                    {
                        case "General":
                            cmd.CommandText = "SELECT nombre, team_id, categoria, lap, laps, tiempo, estado FROM resultados WHERE id = '" + eventIdString + "' AND NOT estado = '7-to start' ORDER BY estado ASC, lap DESC, tiempo ASC";
                            break;
                        case "Masculino":
                        case "Femenino":
                        case "Equipo_Masculino":
                        case "Equipo_Femenino":
                        case "Equipo_Mixto":
                            cmd.CommandText = "SELECT nombre,team_id,categoria,lap,laps,tiempo,estado " + generales + " FROM resultados WHERE resultados.id = '" + eventIdString + "' AND sexo = '" + categoria.ToLower() + "' AND NOT estado = '7-to start' ORDER BY estado ASC, lap DESC, tiempo ASC";
                            break;
                        default:
                            cmd.CommandText = "SELECT nombre,team_id,categoria," + vueltas + " tiempo,lap,laps,estado " + generales + ", ciudad FROM resultados WHERE id = '" + eventIdString + "' AND categoria = '" + categoria + "' ORDER BY estado ASC, lap DESC, tiempo ASC";
                            break;
                    }
                }

                connectionDbMain.Open();

                try
                {
                    cmd.ExecuteNonQuery();
                    DataTable dta = new DataTable();
                    SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                    dataadapt.Fill(dta);
                    dataGridView.DataSource = dta;

                    // mostrar numero de fila
                    foreach (DataGridViewRow row in dataGridView.Rows)
                    {
                        row.HeaderCell.Value = (row.Index + 1).ToString();
                    }

                    //Change cell font
                    foreach (DataGridViewColumn c in dataGridView.Columns)
                    {
                        c.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
                    }

                    dataGridView.AutoResizeColumns();
                    dataGridView.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                connectionDbMain.Close(); ;
            }
        }

        private void comboBox30_TextChanged(object sender, EventArgs e)
        {
            // resultados
            comboBox29.Items.Clear();
            comboBox29.Items.Add("General");
            comboBox29.Items.Add("Masculino");
            comboBox29.Items.Add("Femenino");
            comboBox29.Items.Add("Equipo_Masculino");
            comboBox29.Items.Add("Equipo_Femenino");
            comboBox29.Items.Add("Equipo_Mixto");
            poblarComboBoxColumna("categorias", "categoria", comboBox29, comboBox30.Text);
        }

        private void comboBox29_TextChanged(object sender, EventArgs e)
        {
            calcularClasificacion(comboBox30.Text, comboBox29.Text, "resultados", dataGridView6);
        }

        private void CalcularTiempos(string eventName)
        {
            // Buscar nombre evento cuyo nombre es eventNameString
            string eventId = buscarIdEventoPorNombre(eventName);

            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "DELETE FROM [resultados] WHERE id = '" + eventId + "'";
            sqlServerConnect(connection);
            cmd.ExecuteNonQuery();
            connection.Close();

            //variales para llenar la nueva tabla 
            int arraySize = 700; // TODO: menor tamaño para eventos mas chicos.
            string[] numero = new string[arraySize];
            string[] nombre = new string[arraySize];
            TimeSpan[] tiempo = new TimeSpan[arraySize];
            string[] estado = new string[arraySize];
            string[] teamId = new string[arraySize];
            string[] campo = new string[arraySize];
            string[] categoria = new string[arraySize];
            string[] sexo = new string[arraySize];
            int[] lap = new int[arraySize];
            int[] laps = new int[arraySize];
            string[] ciudad = new string[arraySize];
            //string[] dnis = new string[arraySize];
            TimeSpan[] v1 = new TimeSpan[arraySize];
            TimeSpan[] v2 = new TimeSpan[arraySize];
            TimeSpan[] v3 = new TimeSpan[arraySize];
            TimeSpan[] v4 = new TimeSpan[arraySize];
            TimeSpan[] v5 = new TimeSpan[arraySize];
            TimeSpan[] v6 = new TimeSpan[arraySize];
            TimeSpan[] v7 = new TimeSpan[arraySize];
            TimeSpan[] v8 = new TimeSpan[arraySize];
            TimeSpan[] v9 = new TimeSpan[arraySize];
            TimeSpan[] v10 = new TimeSpan[arraySize];
            DateTime[] largada = new DateTime[arraySize]; //TODO ojo de donde sale la hr de largada
            DateTime[] p1 = new DateTime[arraySize];
            DateTime[] p2 = new DateTime[arraySize];
            DateTime[] p3 = new DateTime[arraySize];
            DateTime[] p4 = new DateTime[arraySize];
            DateTime[] p5 = new DateTime[arraySize];
            DateTime[] p6 = new DateTime[arraySize];
            DateTime[] p7 = new DateTime[arraySize];
            DateTime[] p8 = new DateTime[arraySize];
            DateTime[] p9 = new DateTime[arraySize];
            DateTime[] p10 = new DateTime[arraySize];

            int idx = 0;

            string presente = "";

            if(checkBox14.Checked)
            {
                presente = " AND finishers.presente = 'True'";
            }

            cmd.CommandText = "SELECT * FROM finishers JOIN corredores ON corredores.dni = finishers.dni JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.id = '" + eventId + "' AND NOT estado = '7-to start'"+ presente +" ORDER BY finishers.team_id ASC";
            sqlServerConnect(connection);
            SqlDataReader reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                nombre[idx] = reader["nombre"].ToString();
                numero[idx] = reader["numero"].ToString();
                teamId[idx] = reader["team_id"].ToString();
                campo[idx] = reader["campo"].ToString();
                categoria[idx] = reader["categoria"].ToString();
                //tiempo[idx] = TimeSpan.Parse(reader["tiempo"].ToString());
                estado[idx] = reader["estado"].ToString();
                sexo[idx] = reader["sexo"].ToString();
                ciudad[idx] = reader["ciudad"].ToString();
                //dnis[idx] = reader["dni"].ToString();
                lap[idx] = Convert.ToInt16(reader["lap"].ToString());
                laps[idx] = Convert.ToInt16(reader["vueltas"].ToString());
                //largada[idx] = DateTime.Parse(reader["largada"].ToString()); // TODO: elegir fuente de info de hr de largada: categorias.largada o finishers.v0
                //largada[idx] = reader.GetDateTime(reader.GetOrdinal("largada")); // gets date time with milliseconds
                //Evento con largada individual?
                try
                {
                    largada[idx] = checkBox3.Checked == true ? reader.GetDateTime(reader.GetOrdinal("v0")) : reader.GetDateTime(reader.GetOrdinal("largada")); // gets date time with milliseconds

                    p1[idx] = (DateTime.Parse(reader["v1"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v1")) : largada[idx];  // TODO ojo largada
                    p2[idx] = (DateTime.Parse(reader["v2"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v2")) : p1[idx];
                    p3[idx] = (DateTime.Parse(reader["v3"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v3")) : p2[idx];
                    p4[idx] = (DateTime.Parse(reader["v4"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v4")) : p3[idx];
                    p5[idx] = (DateTime.Parse(reader["v5"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v5")) : p4[idx];
                    p6[idx] = (DateTime.Parse(reader["v6"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v6")) : p5[idx];
                    p7[idx] = (DateTime.Parse(reader["v7"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v7")) : p6[idx];
                    p8[idx] = (DateTime.Parse(reader["v8"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v8")) : p7[idx];
                    p9[idx] = (DateTime.Parse(reader["v9"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v9")) : p8[idx];
                    p10[idx] = (DateTime.Parse(reader["v10"].ToString()).Year != 1900) ? reader.GetDateTime(reader.GetOrdinal("v10")) : p9[idx];

                    v1[idx] = ((p1[idx] > largada[idx])) ? p1[idx] - largada[idx] : new TimeSpan(); // TODO ojo largada
                    v2[idx] = ((p2[idx] > p1[idx])) ? p2[idx] - p1[idx] : new TimeSpan();
                    v3[idx] = ((p3[idx] > p2[idx])) ? p3[idx] - p2[idx] : new TimeSpan();
                    v4[idx] = ((p4[idx] > p3[idx])) ? p4[idx] - p3[idx] : new TimeSpan();
                    v5[idx] = ((p5[idx] > p4[idx])) ? p5[idx] - p4[idx] : new TimeSpan();
                    v6[idx] = ((p6[idx] > p5[idx])) ? p6[idx] - p5[idx] : new TimeSpan();
                    v7[idx] = ((p7[idx] > p6[idx])) ? p7[idx] - p6[idx] : new TimeSpan();
                    v8[idx] = ((p8[idx] > p7[idx])) ? p8[idx] - p7[idx] : new TimeSpan();
                    v9[idx] = ((p9[idx] > p8[idx])) ? p9[idx] - p8[idx] : new TimeSpan();
                    v10[idx] = ((p10[idx] > p9[idx])) ? p10[idx] - p9[idx] : new TimeSpan();
                    tiempo[idx] = v1[idx] + v2[idx] + v3[idx] + v4[idx] + v5[idx] + v6[idx] + v7[idx] + v8[idx] + v9[idx] + v10[idx];
                    //idx++;
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.Message);
                }
                idx++;
            }
            connection.Close();

            int i = 0;
            while (i < idx)
            {
                // calcular el rango donde los team_id coinciden
                int idxMax = 0;
                string teamMates = teamId[i];
                while (teamId[i + idxMax] == teamMates)
                {
                    idxMax++;
                }

                // armar campos
                string nombreT = "";
                string numeroT = "";
                string ciudadT = "";
                for (int j = i; j < (i + idxMax); j++)
                {
                    // nombre, numero y ciudad
                    if (j == (i + idxMax - 1))
                    {
                        nombreT += nombre[j];
                        if (idxMax > 1 && campo[j - 1] != "" && campo[j - 1] != "0")
                        {
                            //nombreT += " (" + campo[j - 1] + ")";
                        }
                        numeroT += numero[j];
                        ciudadT += ciudad[j];
                    }
                    else
                    {
                        nombreT += nombre[j] + "/";
                        numeroT += numero[j] + "/";
                        ciudadT += ciudad[j] + "/";
                    }
                }

                // estado
                string[] estadoCopy = new string[idxMax];
                string estadoT = "1-finish";
                Array.ConstrainedCopy(estado, i, estadoCopy, 0, idxMax);
                if (estadoCopy.Contains("6-dns"))
                {
                    estadoT = "6-dns";
                }
                else if (estadoCopy.Contains("4-dnf"))
                {
                    estadoT = "4-dnf";
                }
                else if (estadoCopy.Contains("2-running"))
                {
                    estadoT = "2-running";
                }
                else if (estadoCopy.Contains("5-dsq"))
                {
                    estadoT = "5-dsq";
                }
                else if (estadoCopy.Contains("3-lapped"))
                {
                    estadoT = "3-lapped";
                }

                //tiempo    
                TimeSpan[] tiempoCopy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(tiempo, i, tiempoCopy, 0, idxMax);
                TimeSpan tiempoT = tiempoCopy.Max();

                //v1
                TimeSpan[] v1Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v1, i, v1Copy, 0, idxMax);
                TimeSpan v1T = v1Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v1Copy.Max();

                //v2
                TimeSpan[] v2Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v2, i, v2Copy, 0, idxMax);
                TimeSpan v2T = v2Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v2Copy.Max();
                //v3
                TimeSpan[] v3Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v3, i, v3Copy, 0, idxMax);
                TimeSpan v3T = v3Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v3Copy.Max();
                //v4
                TimeSpan[] v4Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v4, i, v4Copy, 0, idxMax);
                TimeSpan v4T = v4Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v4Copy.Max();
                //v5
                TimeSpan[] v5Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v5, i, v5Copy, 0, idxMax);
                TimeSpan v5T = v5Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v5Copy.Max();
                //6
                TimeSpan[] v6Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v6, i, v6Copy, 0, idxMax);
                TimeSpan v6T = v6Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v6Copy.Max();
                //v7
                TimeSpan[] v7Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v7, i, v7Copy, 0, idxMax);
                TimeSpan v7T = v7Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v7Copy.Max();
                //v7
                TimeSpan[] v8Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v8, i, v8Copy, 0, idxMax);
                TimeSpan v8T = v8Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v8Copy.Max();
                //v8
                TimeSpan[] v9Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v9, i, v9Copy, 0, idxMax);
                TimeSpan v9T = v9Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v9Copy.Max();
                //v7
                TimeSpan[] v10Copy = new TimeSpan[idxMax];
                Array.ConstrainedCopy(v10, i, v10Copy, 0, idxMax);
                TimeSpan v10T = v10Copy.Min() == TimeSpan.Parse("00:00:00") ? TimeSpan.Parse("00:00:00") : v10Copy.Max();

                // sexo
                // masculino, femenino, equipo_masculino, equipo_femenino y equipo_mixto
                string[] sexoCopy = new string[idxMax];
                string sexoT = "masculino";
                Array.ConstrainedCopy(sexo, i, sexoCopy, 0, idxMax);
                if (sexoCopy.Contains("masculino") && sexoCopy.Contains("femenino"))
                {
                    sexoT = "equipo_mixto";
                }
                else if (sexoCopy.Contains("femenino") && idxMax > 1)
                {
                    sexoT = "equipo_femenino";
                }
                else if (sexoCopy.Contains("femenino") && idxMax == 1)
                {
                    sexoT = "femenino";
                }
                else if (idxMax > 1)
                {
                    sexoT = "equipo_masculino";
                }

                // lap
                int[] lapCopy = new int[idxMax];
                Array.ConstrainedCopy(lap, i, lapCopy, 0, idxMax);
                int lapT = lapCopy.Min();

                // campo


                cmd.CommandText = "INSERT INTO [resultados] (id , nombre, numero, team_id, categoria, tiempo, estado, sexo, lap, laps,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10, largada, ciudad, campo) " +
                    "VALUES ('" + eventId + "','" + nombreT + "','" + numeroT + "','" + teamId[i] + "', '" + categoria[i] + "', '" + tiempoT.ToString().Substring(0, (tiempoT.ToString().Length >= 12) ? 12 : tiempoT.ToString().Length) + "', '" + estadoT + "', '" + sexoT + "', '" + lapT + "', '" + laps[i] + "','" + v1T + "','" + v2T + "','" + v3T + "','" + v4T + "','" + v5T + "','" + v6T + "','" + v7T + "','" + v8T + "','" + v9T + "','" + v10T + "', '" + largada[i].ToString("MM-dd-yyyy HH:mm:ss") + "', '" + ciudadT + "', '" + campo[i] + "')";

                sqlServerConnect(connection);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
                connection.Close();

                i += idxMax;
            }
        }

        private void button36_Click_2(object sender, EventArgs e)
        {
            CalcularTiempos(comboBox30.Text);
            Calcular_Pos_General_y_Categoria(comboBox30.Text);
            calcularClasificacion(comboBox30.Text, comboBox29.Text, "resultados", dataGridView6);
        }

        private void button39_Click_1(object sender, EventArgs e)
        {
            comboBox29.Text = "General";
            button36_Click_2(null, null);
            imprimirResultados(comboBox30.Text);
        }

        private void imprimirResultados(string nombreEvento)
        {
            if (nombreEvento != "")
            {
                // levantar los datos del evento 
                string idEvento = buscarIdEventoPorNombre(nombreEvento);
                DateTime fechaEvento = DateTime.Parse(evStart[evId.IndexOf(idEvento)]);
                string modalidad = evKey[evId.IndexOf(idEvento)];
                string nombreEventoPrint = evPrint[evId.IndexOf(idEvento)];

                nombreEventoPrint = "[" + fechaEvento.Year.ToString() + "-" + fechaEvento.Month.ToString().PadLeft(2, '0') + "-" + fechaEvento.Day.ToString().PadLeft(2, '0') + "] " + nombreEventoPrint;

                // Levantar categorias del evento
                SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT categoria,vueltas FROM [categorias] WHERE id = '" + idEvento + "'";
                string[] catArray = new string[50];
                int[] lapArray = new int[50];
                sqlServerConnect(connection);
                SqlDataReader catReader = cmd.ExecuteReader();

                // contador de categorias => mismo numero de worksheets
                int catCount = 0;

                //Primero Agregar categorias: General, General Hombres y General Mujeres
                catArray[catCount++] = "General";
                catArray[catCount++] = "General_Hombres";
                catArray[catCount++] = "General_Mujeres";
                catArray[catCount++] = "General_Equipo_Hombres";
                catArray[catCount++] = "General_Eqiupo_Mujeres";
                catArray[catCount++] = "General_Equipo_Mixto";

                while (catReader.Read())
                {
                    catArray[catCount] = catReader[0].ToString();
                    if (catReader[1].ToString() != "")
                    {
                        lapArray[catCount] = Convert.ToInt16(catReader[1].ToString());
                    }
                    catCount++;
                }

                connection.Close();

                // carpeta destino es fija
                string result_dir = "C:\\cronobot\\" + nombreEventoPrint + "\\resultados\\";
                System.IO.Directory.CreateDirectory(result_dir);
                string pathArchivo = result_dir + nombreEvento + " " + DateTime.Now.Ticks.ToString();
                Workbook workbook = new Workbook();
                Worksheet worksheet = new Worksheet("Resultados");

                // crear csv
                StreamWriter csv_file = new StreamWriter(new FileStream(pathArchivo + ".csv", FileMode.Create), Encoding.Unicode);
                //header
                csv_file.Write(lineForCsv("pechera", "pos cat", "gral abs", "gral sexo", "nombre", "localidad", "categoria", "tiempo", "vueltas", "vuelta 1", "vuelta 2", "vuelta 3", "vuelta 4", "vuelta 5", "vuelta 6", "vuelta 7", "evento","modalidad", "dni", "sexo"));

                // loop para generear los resultados: gral, gral H, gral M, gral Eq H, gral E Muj, gral E Mix, todas; 
                int i = 0;
                if (checkBox4.CheckState == CheckState.Unchecked)
                {
                    i = 6; // no se imprimen generales
                }

                if (checkBox13.CheckState == CheckState.Unchecked)
                {
                    catCount = 6; // no se imprimes categorias
                }

                if (checkBox4.CheckState == CheckState.Checked || checkBox13.CheckState == CheckState.Checked)
                {
                    int filaCounter = 0;
                    for (; i < catCount; i++)
                    {
                        switch (i)
                        {
                            case 0: // clasificacion general
                                cmd.CommandText = "SELECT TOP " + numericUpDown7.Value.ToString() + " nombre,team_id,categoria,sexo,tiempo,estado,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,ciudad,lap,laps,campo,gral,gral_sex,cat FROM [resultados] WHERE id = '" + idEvento + "' AND NOT estado = '7-to start' ORDER BY gral ASC";
                                break;
                            case 1: // clasificacion general masculina
                                cmd.CommandText = "SELECT TOP " + numericUpDown7.Value.ToString() + " nombre,team_id,categoria,sexo,tiempo,estado,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,ciudad,lap,laps,campo,gral,gral_sex,cat FROM [resultados] WHERE id = '" + idEvento + "' AND sexo = 'masculino' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                                break;
                            case 2: // clasificacion general femenina
                                    //sheetName = catArray[i];
                                    //worksheet = new Worksheet(sheetName);
                                cmd.CommandText = "SELECT TOP " + numericUpDown7.Value.ToString() + " nombre,team_id,categoria,sexo,tiempo,estado,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,ciudad,lap,laps,campo,gral,gral_sex,cat FROM [resultados] WHERE id = '" + idEvento + "' AND sexo = 'femenino' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                                break;
                            case 3: // clasificacion general equipo masculino
                                cmd.CommandText = "SELECT TOP " + numericUpDown7.Value.ToString() + " nombre,team_id,categoria,sexo,tiempo,estado,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,ciudad,lap,laps,campo,gral,gral_sex,cat FROM [resultados] WHERE id = '" + idEvento + "' AND sexo = 'equipo_masculino' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                                break;
                            case 4: // clasificacion general equipo femenino
                                cmd.CommandText = "SELECT TOP " + numericUpDown7.Value.ToString() + " nombre,team_id,categoria,sexo,tiempo,estado,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,ciudad,lap,laps,campo,gral,gral_sex,cat FROM [resultados] WHERE id = '" + idEvento + "' AND sexo = 'equipo_femenino' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                                break;
                            case 5: // clasificacion general equipo mixto
                                cmd.CommandText = "SELECT TOP " + numericUpDown7.Value.ToString() + " nombre,team_id,categoria,sexo,tiempo,estado,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,ciudad,lap,laps,campo,gral,gral_sex,cat FROM [resultados] WHERE id = '" + idEvento + "' AND sexo = 'equipo_mixto' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                                break;
                            default: // resto categorias
                                cmd.CommandText = "SELECT TOP " + numericUpDown7.Value.ToString() + " nombre,team_id,categoria,sexo,tiempo,estado,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,laps,lap,ciudad,campo,gral,gral_sex,cat FROM [resultados] WHERE id = '" + idEvento + "' AND NOT estado = '7-to start' AND categoria = '" + catArray[i] + "' ORDER BY cat ASC";
                                break;
                        }

                        sqlServerConnect(connection);
                        SqlDataReader generalReader = cmd.ExecuteReader();

                        int posicion = 1;
                        string prevCat = "";
                        while (generalReader.Read())
                        {
                            // variables para csv
                            string tiempoCarrera = "";
                            string pos = "";
                            string posCat = generalReader["cat"].ToString();
                            string nombre = generalReader["nombre"].ToString();
                            string numero = generalReader["team_id"].ToString();
                            string ciudad = generalReader["ciudad"].ToString();
                            string posGral = generalReader["gral"].ToString();
                            string posGralSex = generalReader["gral_sex"].ToString();
                            string sexo = generalReader["sexo"].ToString();

                            if ((prevCat != generalReader["categoria"].ToString()) && (i > 5)) // detectar cambio de categoria (imprimiendo sheet "categorias")
                            {
                                filaCounter += 2; // dos filas vacias entre categoarias
                                worksheet.Cells[filaCounter - 1, 0] = new Cell(generalReader["categoria"].ToString());
                                generateSheetFormat(worksheet, Convert.ToInt16(generalReader["laps"].ToString()), filaCounter);
                                filaCounter++;
                                posicion = 1; // reiniciar posiciones
                            }
                            else if ((i <= 5) && (prevCat != catArray[i])) // imprimiendo generales
                            {
                                filaCounter += 2; // dos filas vacias entre categoarias
                                worksheet.Cells[filaCounter - 1, 0] = new Cell(catArray[i]);
                                generateSheetFormat(worksheet, 0, filaCounter);
                                filaCounter++;
                                posicion = 1; // reiniciar posiciones
                            }

                            if (generalReader["estado"].ToString() == "1-finish" || generalReader["estado"].ToString() == "3-lapped")
                            {
                                worksheet.Cells[filaCounter, 0] = new Cell(posicion.ToString());
                                pos = posicion.ToString();
                                posicion++;
                            }
                            else if (generalReader["estado"].ToString() != "1-finish")
                            {
                                pos = generalReader["estado"].ToString().Substring(2);
                                posCat = pos;
                                posGral = pos;
                                posGralSex = pos;
                                worksheet.Cells[filaCounter, 0] = new Cell(pos);
                                posicion++;
                            }

                            if (generalReader["estado"].ToString() == "1-finish")
                            {
                                tiempoCarrera = generalReader["tiempo"].ToString().Substring(0, 8);
                                // por las dudas:
                                if (tiempoCarrera == "23:59:59")
                                {
                                    tiempoCarrera = "--:--:--";
                                }
                            }
                            else if (generalReader["estado"].ToString() == "3-lapped")
                            {
                                tiempoCarrera = (Convert.ToInt16(generalReader["lap"].ToString()) - Convert.ToInt16(generalReader["laps"].ToString())).ToString() + "LAP";
                            }
                            else
                            {
                                tiempoCarrera = "N/A";
                            }

                            worksheet.Cells[filaCounter, 3] = new Cell(generalReader["categoria"].ToString());

                            worksheet.Cells[filaCounter, 2] = new Cell(nombre);
                            worksheet.Cells[filaCounter, 1] = new Cell(numero);
                            worksheet.Cells[filaCounter, 4] = new Cell(ciudad);
                            worksheet.Cells[filaCounter, 5] = new Cell(tiempoCarrera);

                            int vueltas = 0;
                            string vueltaStr = "";
                            string[] lap = new string[10]; 
                            if (i > 5) // imprimiendo categorias
                            {
                                vueltas = Convert.ToInt16(generalReader["laps"].ToString());
                                vueltaStr = generalReader["lap"].ToString();
                            }

                            if (vueltas > 1)
                            {
                                for (int j = 0; j < vueltas; j++)
                                {
                                    if (generalReader[6 + j].ToString() != "" && generalReader[6 + j].ToString() != "00:00:00")
                                    {
                                        lap[j] = generalReader[6 + j].ToString().Substring(0, 8);
                                        worksheet.Cells[filaCounter, 6 + j] = new Cell(lap[j]);
                                    }
                                    else
                                    {
                                        lap[j] = "--:--:--";
                                        worksheet.Cells[filaCounter, 6 + j] = new Cell(lap[j]);
                                    }
                                }
                            }

                            filaCounter++;
                            if (i <= 5) // generales
                            {
                                prevCat = catArray[i];
                                string catGral = "?";
                                switch (i)
                                {
                                    case 0:
                                        catGral = "GRAL_ABS";
                                        break;
                                    case 1:
                                        catGral = "GRAL_MASC";
                                        break;
                                    case 2:
                                        catGral = "GRAL_FEM";
                                        break;
                                    case 3:
                                        catGral = "GRAL_TEAM_MASC";
                                        break;
                                    case 4:
                                        catGral = "GRAL_TEAM_FEM";
                                        break;
                                    case 5:
                                        catGral = "GRAL_TEAM_MIX";
                                        break;
                                }

                                //csv_file.Write(lineForCsv(numero, posCat, posGral, posGralSex, nombre, ciudad, catGral, tiempoCarrera, vueltaStr, lap[0], lap[1], lap[2], lap[3], lap[4], lap[5], lap[6], nombreEventoPrint, modalidad));
                            }
                            else
                            {
                                //necesito el dni
                                SqlConnection connection2 = new SqlConnection(gConnectionString); SqlCommand cmd2 = connection2.CreateCommand();
                                cmd2.CommandType = CommandType.Text;
                                cmd2.CommandText = "SELECT finishers.dni FROM finishers WHERE numero = '" + numero + "'";
                                sqlServerConnect(connection2);
                                SqlDataReader dniReader = cmd2.ExecuteReader();
                                string dni = "";
                                if(dniReader.Read())
                                {
                                    dni = dniReader["dni"].ToString();
                                    //MessageBox.Show(dni);
                                }
                                connection2.Close();
                                prevCat = generalReader["categoria"].ToString();
                                csv_file.Write(lineForCsv(numero, pos, posGral, posGralSex, nombre, ciudad, catArray[i], tiempoCarrera, vueltaStr, lap[0], lap[1], lap[2], lap[3], lap[4],lap[5],lap[6], nombreEventoPrint, modalidad, dni, sexo));
                            }
                        }

                        connection.Close();
                    }

                    try
                    {
                        workbook.Worksheets.Add(worksheet);
                        workbook.Save(pathArchivo + ".xls");
                        csv_file.Close();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
        }

        //private string lineForCsv(string numero, string posCat, string posGralAbs, string posGralsex, string nombre, string localidad, string categoria, string tiempo, string vueltas, string v1, string v2, string v3, string v4, string v5, string v6, string v7, string evento, string modalidad, string dni)
        private string lineForCsv(string numero, string posCat, string posGralAbs, string posGralsex, string nombre, string localidad, string categoria, string tiempo, string vueltas, string v1, string v2, string v3, string v4, string v5, string v6, string v7, string evento, string modalidad, string dni, string sexo)
        {
            //dni, posicion, nombre cate
            return dni + ";" + posCat + ";" + nombre + ";" + categoria + ";" + numero + ";" +  posGralAbs + ";" + posGralsex + ";" +  localidad + ";" + tiempo + ";" + vueltas + ";" + v1 + ";" + v2 + ";" + v3 + ";" + v4 + ";" + v5 + ";" + v6 + ";" + v7 + ";" + evento + ";" + modalidad + ";" + sexo + ";\n";
        }

        private void label13_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button41_Click(object sender, EventArgs e)
        {

        }

        private void button40_Click(object sender, EventArgs e)
        {
            string nombre = "";
            string numero = "";
            SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;

            if (comboBox44.Text != "")
            {
                // borrar el registro seleccionado por dni
                cmd.CommandText = "SELECT corredores.nombre, finishers.numero FROM corredores, finishers WHERE corredores.dni = finishers.dni AND finishers.dni = '" + comboBox44.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    nombre = reader["nombre"].ToString();
                    numero = reader["numero"].ToString();
                }
                connection.Close();
            }
            else if (textBox13.Text != "")
            {
                // borrar el registro seleccionado por numero
                cmd.CommandText = "SELECT corredores.nombre, finishers.numero FROM corredores, finishers WHERE corredores.dni = finishers.dni AND finishers.numero = '" + textBox13.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    nombre = reader["nombre"].ToString();
                    numero = reader["numero"].ToString();
                }
                connection.Close();
            }

            if (nombre != "" && numero != "")
            {
                DialogResult dialogResult = MessageBox.Show("Nombre: " + nombre + '\n' + "Numero:  " + numero + "?", "Borrar Participante", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    cmd.CommandText = "DELETE FROM [finishers] WHERE numero = '" + numero + "'";
                    sqlServerConnect(connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                    label93.Text = "ultimo numero: " + textBox13.Text + " (borrar)";
                    comboBox44.Text = "";
                    textBox7.Text = "";
                    textBox8.Text = "";
                    textBox13.Text = "";
                    dateTimePicker3.Value = DateTime.Now;
                    comboBox4.Text = "";
                    comboBox5.Text = "";
                    textBox16.Text = "";
                    textBox28.Text = "";
                }
            }
        }

        private void readGsm()
        {
            // Leer los mensajes nuevos
            // contar la cantidad de sms
            try
            {
                string dummy = "";
                int msgCount = 0;
                string cellNb = "";
                // list unread sms
                serialPort6.Write("AT+CMGL=\"REC UNREAD\"\r\n");
                System.Threading.Thread.Sleep(200);
                while (serialPort6.BytesToRead > 0)
                {
                    dummy = serialPort6.ReadLine();

                    if ((dummy.Length > 7) && (dummy.Substring(0, 7) == "+CMGL: "))
                    {
                        // hay un nuevo mensaje
                        // decodificar el numero de mensaje y el numero de telefono
                        msgCount = Convert.ToInt16(dummy.Substring(7, (dummy.IndexOf(',') == 8 ? 1 : 2)));
                        cellNb = dummy.Substring(dummy.IndexOf('+', 1), 13); //salteo a poscion 0 que tiene le primer +
                    }
                    else if (dummy[0] == '@')
                    {
                        // Es un registro de corredore
                        DecodeSms(dummy, cellNb);
                    }
                    else if (!dummy.Contains("OK") && !dummy.Contains("AT"))
                    {
                        // Mostrar solo el texto del sms
                        textBox31.AppendText(dummy + '\n');
                    }
                }
            }
            catch{}
        }

        private void button77_Click(object sender, EventArgs e)
        {
            getPorts(comboBox20);
        }

        private void button79_Click(object sender, EventArgs e)
        {
            string dummy = "";

            try
            {
                serialPort5.PortName = comboBox20.Text;
                serialPort5.BaudRate = 9600;
                serialPort5.ReadTimeout = 1000;
                serialPort5.WriteTimeout = 1000;
                serialPort5.Open();

                // Chequear que FW tiene el modulo:
                serialPort5.Write("!S!!!!!!!!!!!!!!#");  // QUERY STATUS
                System.Threading.Thread.Sleep(3000);
                if (serialPort5.BytesToRead > 0)
                {
                    dummy = serialPort5.ReadExisting();

                    if(!dummy.Contains("at24c32 not detected"))
                    {
                        // activar botones eeprom
                        //textBox6.Text += "EEPROM FW !" + "\r\n";
                        button113.Enabled = true;
                        button114.Enabled = true;
                        button115.Enabled = true;
                        button116.Enabled = true;
                        button86.Enabled = true;
                        numericUpDown6.Enabled = true;
                        button83.Enabled = true;
                        button88.Enabled = true;  
                    }
                    if(!dummy.Contains("SIM800 not detected"))
                    {
                        // Entrar modo AT
                        serialPort5.Write("!A!!!!!!!!!!!!!!#");
                        System.Threading.Thread.Sleep(100);
                        if (serialPort5.BytesToRead > 0)
                        {
                            dummy = serialPort5.ReadExisting();
                        }

                        if (dummy.Contains("AT OK"))
                        {
                            if(dummy.Contains("Exit"))
                            {
                                //estaba en modo AT y salio. Volver a entrar a ese modo
                                serialPort5.Write("!A!!!!!!!!!!!!!!#");
                                System.Threading.Thread.Sleep(100);
                                if (serialPort5.BytesToRead > 0)
                                {
                                    dummy = serialPort5.ReadExisting();
                                }
                            }

                            dummy = "";
                            // test modo AT
                            serialPort5.Write("AT" + "\r\n");
                            System.Threading.Thread.Sleep(100);
                            if (serialPort5.BytesToRead > 0)
                            {
                                dummy = serialPort5.ReadExisting();
                            }

                            if (dummy.Contains("OK"))
                            {
                                dummy = "";
                                // configurar modo texto
                                serialPort5.Write("AT+CMGF=1" + "\r\n");
                                System.Threading.Thread.Sleep(100);
                                if (serialPort5.BytesToRead > 0)
                                {
                                    dummy = serialPort5.ReadExisting();
                                }

                                if (dummy.Contains("OK"))
                                {
                                    dummy = "";
                                    // configurar que hacer con sms entrante
                                    serialPort5.Write("AT+CNMI=0,1,0,0,0" + "\r\n");
                                    System.Threading.Thread.Sleep(100);
                                    while (serialPort5.BytesToRead > 0)
                                    {
                                        dummy = serialPort5.ReadExisting();
                                    }

                                    if (dummy.Contains("OK"))
                                    {
                                        // Salir modo AT
                                        serialPort5.Write("!A!!!!!!!!!!!!!!#");
                                        System.Threading.Thread.Sleep(100);
                                        while (serialPort5.BytesToRead > 0)
                                        {
                                            dummy = serialPort5.ReadExisting();
                                        }

                                        if(dummy.Contains("Exit AT OK"))
                                        {
                                            textBox6.Text += "Modulo Lector Remoto Conectado" + "\r\n";
                                            button84.Enabled = true;
                                            button83.Enabled = true;
                                            button88.Enabled = true;
                                            textBox33.Enabled = true;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Modulo Sin Conexion: " + ex.Message);
                serialPort5.Close();
            }
        }

        private void button81_Click(object sender, EventArgs e)
        {
            try
            {
                textBox6.Text += serialPort5.ReadExisting();
                serialPort5.Close();

                // GSM Buttons
                button84.Enabled = false;
                button113.Enabled = false;
                button114.Enabled = false;
                button115.Enabled = false;
                button116.Enabled = false;
                button86.Enabled = false;
                numericUpDown6.Enabled = false;
                button83.Enabled = false;
                button88.Enabled = false;
                textBox33.Enabled = false;
            }
            catch{}
        }

        private void button78_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "")
            {
                cambiarEstado(textBox18.Text, "5-dsq");
                calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
            }
        }

        private void cambiarEstado(string numero, string nuevoEstado)
        {
            //no se puede pasar a estado "running" por este medio. Solo atraves de funcion step back / step forward!
            //no se puede pasar a estado "to-start" por este medio. Solo atraves de funcion step back / step forward!
            if (textBox18.Text != "" && (nuevoEstado == "1-finish" || nuevoEstado == "4-dnf" || nuevoEstado == "5-dsq" || nuevoEstado == "6-dns"))
            {
                bool corredorExiste = false;
                string tiempo = "";
                int laps = 0;
                int totalLaps = 0;
                string viejoEstado = "";
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT finishers.estado, finishers.tiempo, finishers.lap, categorias.vueltas FROM finishers JOIN categorias on finishers.categoria = categorias.categoria WHERE numero = '" + numero + "'";

                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    tiempo = reader["tiempo"].ToString();
                    totalLaps = Convert.ToInt16(reader["vueltas"].ToString());
                    laps = Convert.ToInt16(reader["lap"].ToString());
                    viejoEstado = reader["estado"].ToString();
                    corredorExiste = true;
                }

                connection.Close();

                if (corredorExiste)
                {
                    DialogResult dialogResult = MessageBox.Show("Tiempo del corredor " + numero + " es " + tiempo + ". Marcar como " + nuevoEstado + "?", "Editor de Resultado", MessageBoxButtons.YesNo);
                    string mensajeError = "";

                    if (dialogResult == DialogResult.Yes)
                    {
                        cmd.CommandText = "UPDATE [finishers] SET estado = '" + nuevoEstado + "'  WHERE numero = '" + numero + "'";

                        sqlServerConnect(connection);
                        try
                        {
                            cmd.ExecuteNonQuery();
                        }
                        catch (Exception ex)
                        {
                            mensajeError = ex.Message;
                        }
                        connection.Close();

                        if (mensajeError != "")
                        {
                            //add to log
                            MessageBox.Show(mensajeError);
                            addToLog("", "modif estado", numero, "a " + nuevoEstado);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("El corredor no esta en la lista de largada");
                }
            }
        }

        private void dataGridView2_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
        }

        private void comboBox32_TextChanged(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "";

            switch (comboBox32.Text)
            {
                case "2-running":
                case "1-finish":
                case "6-dns":
                case "4-dnf":
                case "7-to start":
                case "5-dsq":
                case "3-lapped":
                    //cmd.CommandText = "SELECT nombre,numero,categoria,tiempo,lap,laps,estado,largada FROM [finishers] WHERE id = '" + buscarIdEventoPorNombre(comboBox7.Text) + "' AND estado = '" + comboBox32.Text + "' ORDER BY categoria DESC, lap DESC, tiempo ASC";
                    cmd.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.categoria,finishers.tiempo,finishers.lap,categorias.vueltas,finishers.estado, finishers.tiempo FROM finishers JOIN corredores ON corredores.dni = finishers.dni JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.id = '" + buscarIdEventoPorNombre(comboBox7.Text) + "' AND finishers.estado = '" + comboBox32.Text + "' ORDER BY finishers.categoria DESC, finishers.lap DESC, finishers.tiempo ASC";

                    break;
                case "8-absent":
                    cmd.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.categoria,finishers.tiempo,finishers.lap,categorias.vueltas,finishers.estado, finishers.tiempo FROM finishers JOIN corredores ON corredores.dni = finishers.dni JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.id = '" + buscarIdEventoPorNombre(comboBox7.Text) + "' AND finishers.presente = 'False' ORDER BY finishers.categoria DESC, finishers.lap DESC, finishers.tiempo ASC;";
                    //cmd.CommandText += "UPDATE [finishers] SET presente = '1'";
                    break;
                default:
                    calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
                    break;
            }

            sqlServerConnect(connection);

            try
            {
                cmd.ExecuteNonQuery();
                DataTable dta = new DataTable();
                SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                dataadapt.Fill(dta);
                dataGridView4.DataSource = dta;

                // mostrar numero de fila
                foreach (DataGridViewRow row in dataGridView4.Rows)
                {
                    row.HeaderCell.Value = (row.Index + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }

        private void RecalcularTiempoDeCarrera(string nombreEvento)
        {
            string idEvento = buscarIdEventoPorNombre(nombreEvento);
            TimeSpan[] tiempoDeCarrera = new TimeSpan[2500];
            string[] numeroCorredor = new string[2500];
            int indice = 0;
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            DateTime[] vueltas = new DateTime[11]; // incluye la largada!!!

            cmd.CommandText = "SELECT lap,laps,numero,largada,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10 FROM [finishers] WHERE id = '" + idEvento + "' ORDER BY numero ASC";
            sqlServerConnect(connection);
            try
            {
                SqlDataReader reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    numeroCorredor[indice] = reader["numero"].ToString();
                    tiempoDeCarrera[indice] = TimeSpan.Parse("23:59:59");
                    DateTime largadaa = DateTime.Parse(reader["largada"].ToString());
                    int lapp = Convert.ToInt16(reader["lap"].ToString()); // numero de vuelas cumplidas hace de indice para conseguir el datetime del ultimo paso registrado 
                    if (DateTime.Parse(reader[lapp + 3].ToString()) > DateTime.Parse(reader["largada"].ToString()))
                    {
                        tiempoDeCarrera[indice] = DateTime.Parse(reader[lapp + 3].ToString()) - DateTime.Parse(reader["largada"].ToString());
                    }
                    indice++;
                }
            }
            catch { }

            connection.Close();

            sqlServerConnect(connection);
            for (int counter = 0; counter < indice; counter++)
            {
                cmd.CommandText = "UPDATE [finishers] SET tiempo = '" + tiempoDeCarrera[counter].ToString() + "' WHERE numero = '" + numeroCorredor[counter] + "' AND id = '" + idEvento + "' AND NOT estado = '7-to start'";
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch { }
            }
            connection.Close();
        }

        private void Calcular_Pos_General_y_Categoria(string eventName)
        {
            if (eventName != "")
            {
                // Buscar nombre evento cuyo nombre es eventNameString
                string eventIdString = buscarIdEventoPorNombre(eventName);

                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                string columna = "";

                // Levantar categorias del evento
                cmd.CommandText = "SELECT categoria FROM [categorias] WHERE id = '" + eventIdString + "'";
                string[] catArray = new string[50];
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();
                int catCount = 0;

                //Primero Agregar categorias: General, General Hombres y General Mujeres
                //if (checkBox4.CheckState == CheckState.Checked)
                {
                    catArray[catCount++] = "general";
                    catArray[catCount++] = "masculino";
                    catArray[catCount++] = "femenino";
                    catArray[catCount++] = "equipo_masculino";
                    catArray[catCount++] = "equipo_femenino";
                    catArray[catCount++] = "equipo_mixto";
                }

                while (reader.Read())
                {
                    catArray[catCount++] = reader["categoria"].ToString();
                }

                connection.Close();

                // loop para ordenar categorias y generales
                for (int j = 0; j < catCount; j++)
                {
                    switch (catArray[j])
                    {
                        case "general":
                            cmd.CommandText = "SELECT team_id FROM [resultados] WHERE id = '" + eventIdString + "' AND NOT estado = '7-to start' ORDER BY estado ASC, lap DESC,tiempo ASC,laps DESC";
                            columna = "gral";
                            break;
                        case "masculino":
                        case "femenino":
                        case "equipo_mixto":
                        case "equipo_masculino":
                        case "equipo_femenino":
                            cmd.CommandText = "SELECT team_id FROM [resultados] WHERE id = '" + eventIdString + "' AND sexo = '" + catArray[j] + "' AND NOT estado = '7-to start' ORDER BY estado ASC, lap DESC, tiempo ASC, laps DESC";
                            columna = "gral_sex";
                            break;
                        default:
                            cmd.CommandText = "SELECT team_id FROM [resultados] WHERE id = '" + eventIdString + "' AND categoria = '" + catArray[j] + "' ORDER BY estado ASC, lap DESC, tiempo ASC";
                            columna = "cat";
                            break;
                    }

                    int[] team_id = new int[2500];
                    int orden = 0;
                    // leer numeros segun el orden del comando SELECT
                    sqlServerConnect(connection);
                    reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        team_id[orden++] = Convert.ToInt32(reader["team_id"].ToString());
                    }
                    connection.Close();

                    //ordenar
                    for (int i = 0; i < orden; i++)
                    {
                        cmd.CommandText = "UPDATE [resultados] SET " + columna + " = '" + (i + 1) + "' WHERE id =  '" + eventIdString + "' AND team_id = '" + team_id[i] + "'";
                        sqlServerConnect(connection);
                        cmd.ExecuteNonQuery();
                        connection.Close();
                    }
                }
            }
        }

        private void dataGridView6_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        void DecodeSms(string smsStr, string cellNo)
        {
            // Decodificar numero de corredor
            string epcStr = smsStr.Substring(1, 5);

            //quitr los ceros precedentes
            while (epcStr[0] == '0')
            {
                epcStr = epcStr.Substring(epcStr.IndexOf('0') + 1);
            }

            // Decodificar Tiempo
            int fecha = Convert.ToInt16(smsStr[6] - 33);
            int hora = Convert.ToInt16(smsStr[7] - 33);
            int minuto = Convert.ToInt16(smsStr[8] - 33);
            int segundo = Convert.ToInt16(smsStr[9] - 33);

            DateTime horaDePaso = new DateTime(DateTime.Now.Year, DateTime.Now.Month, fecha, hora, minuto, segundo);
            int puestoDeControl = 0;

            switch (cellNo)
            {
                case "+543815526959":
                    puestoDeControl = 1;
                    break;
                default:
                    puestoDeControl = 1;
                    break;
            }

            textBox31.AppendText("PC" + puestoDeControl.ToString() + ", #" + epcStr + ",  Hr:" + horaDePaso.ToString("HH:mm:ss") + " (" + smsStr + ") " + "\r\n");
            //agregarNumeroClasificacion(epcStr, horaDePaso, puestoDeControl);
        }

        private void button80_Click(object sender, EventArgs e)
        {
            textBox6.Text = "";
        }

        private void bt_Printer_Click(object sender, EventArgs e)
        {

        }

        // The PrintPage event is raised for each page to be printed.
        private void pd_PrintPage(object sender, PrintPageEventArgs ev)
        {
            /*
            Font printFont = new Font("AR ESSENCE", 12);
            float yPos = 150;
            float leftMargin = 0;
            string line = null;
            StreamReader streamToPrint = new StreamReader(supportFilesPath + "\\TxtTest.txt");
            StringFormat strFormat = new StringFormat();
            strFormat.Alignment = StringAlignment.Near;
            //strFormat.FormatFlags = StringFormatFlags.FitBlackBox;
            strFormat.LineAlignment = StringAlignment.Center;

            line = streamToPrint.ReadToEnd();
            {
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, strFormat);
            }

            ev.HasMorePages = false;
            streamToPrint.Close();
            */
            Font printFont = new Font("AR ESSENCE", 10);
            if(Environment.MachineName.Contains("DANIELOPC"))
            {
                printFont = new Font("AR ESSENCE", 11);
            }
            //float yPos = 150;
            float leftMargin = 0;
            string line = null;
            StreamReader streamToRead = new StreamReader(supportFilesPath + "\\TxtTest.txt");
            bool endOfFile = false;
            int nbOfLines = 0;
            while (!endOfFile)
            {
                string texto = streamToRead.ReadLine();
                nbOfLines++;
                endOfFile = streamToRead.EndOfStream;
            }
            streamToRead.Close();
            StreamReader streamToPrint = new StreamReader(supportFilesPath + "\\TxtTest.txt");
            float yPos = nbOfLines * 8;

            StringFormat strFormat = new StringFormat();
            strFormat.Alignment = StringAlignment.Near;
            //strFormat.FormatFlags = StringFormatFlags.FitBlackBox;
            strFormat.LineAlignment = StringAlignment.Center;

            line = streamToPrint.ReadToEnd();
            {
                ev.Graphics.DrawString(line, printFont, Brushes.Black,
                   leftMargin, yPos, strFormat);
            }

            ev.HasMorePages = false;
            streamToPrint.Close();
        }

        private string CenterString(string aCoregir)
        {
            int strlen = aCoregir.Length;
            string corregida = "";

            if (aCoregir.Length >= 24)
            {
                aCoregir = aCoregir.Substring(0, 24);
            }

            int nbOfSpacesEachSide = (24 - strlen) / 2;

            if (nbOfSpacesEachSide > 0)
            {
                for (int i = 0; i < nbOfSpacesEachSide; i++)
                {
                    corregida += ' ';
                }

                corregida += aCoregir;

                for (int i = 0; i < nbOfSpacesEachSide; i++)
                {
                    corregida += ' ';
                }
            }
            else
            {
                corregida = aCoregir;
            }
            return corregida;
        }

        private void dateTimePicker3_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                comboBox5.Items.Clear();
                buscarCategoriasParaComboBox5();
                poblarComboBoxColumna("categorias", "categoria", comboBox5, comboBox9.Text);
            }
        }

        private void buscarCategoriasParaComboBox5()
        {
            if (dateTimePicker3.Value.Year != DateTime.Now.Year)
            {
                if (comboBox9.Text != "" && comboBox4.Text != "")
                {
                    comboBox9_TextChanged(null, null);
                    string[] catArray = new string[30];
                    calcularCategoria(buscarIdEventoPorNombre(comboBox9.Text), dateTimePicker3.Value, comboBox4.Text, ref catArray);
                    comboBox5.Text = catArray[0];
                    foreach (string cat in catArray)
                    {
                        if (cat != null && cat != "")
                        {
                            comboBox5.Items.Insert(0, cat);
                            comboBox5.DroppedDown = true;
                        }
                    }
                }
            }
        }

        private void button82_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT MAX(team_id) FROM [finishers] WHERE id = '" + buscarIdEventoPorNombre(comboBox9.Text) + "'";
            sqlServerConnect(connection);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                int teamId = Convert.ToInt16(reader[0].ToString()) + 1;
                textBox28.Text = teamId.ToString();
            }
            connection.Close();
        }

        private void timer3_Tick(object sender, EventArgs e)
        {
            readGsm();
        }

        private void button86_Click(object sender, EventArgs e)
        {

        }

        private void button83_Click(object sender, EventArgs e)
        {
            int year = DateTime.Now.Year - 2000;
            int month = DateTime.Now.Month;
            int day = DateTime.Now.Day;
            int hora = DateTime.Now.Hour;
            int minuto = DateTime.Now.Minute;
            int segundo = DateTime.Now.Second;

            try
            {
                string setClockCmd = "!T" + (DateTime.Now.Year - 2000).ToString("D" + 2) + (DateTime.Now.Month).ToString("D" + 2) + (DateTime.Now.Date.Day).ToString("D" + 2) + (DateTime.Now.Hour).ToString("D" + 2) + (DateTime.Now.Minute).ToString("D" + 2) + (DateTime.Now.Second).ToString("D" + 2) + "OK#";
                // enviar comando
                serialPort5.Write(setClockCmd);
                System.Threading.Thread.Sleep(100);
                while (serialPort5.BytesToRead > 0)
                {
                    textBox6.AppendText(serialPort5.ReadExisting());
                }
            }
            catch{}
        }

        private void button88_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort5.Write("!S______________#");
                System.Threading.Thread.Sleep(5000);
                while (serialPort5.BytesToRead > 0)
                {
                    textBox6.AppendText(serialPort5.ReadExisting());
                }
            }
            catch{}
        }

        private void button84_Click(object sender, EventArgs e)
        {
            // enviar comando
            try
            {
                serialPort5.Write("!R______________#");
                System.Threading.Thread.Sleep(12000);
                while (serialPort5.BytesToRead > 0)
                {
                    textBox6.AppendText(serialPort5.ReadExisting());
                }
            }
            catch{}
        }

        private void groupBox13_Enter(object sender, EventArgs e)
        {

        }

        private void textBox29_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox29.Text != "")
                {
                    printticket(textBox29.Text);              
                }

                textBox29.Text = "";
            }
        }

        private void printticket(string consulta)
        {
            string team_id = "";
            string eventId = "";
            string nombre = "";
            string numero = "";
            string tiempo = "";
            string[] vueltas = new string[10];
            string categoria = "";
            string posCat = "";
            string posSexo = "";
            string posGral = "";
            string sexo = "";
            int laps = 0;
            int runnersCount = new int();
            int gralCount = new int();

            SqlConnection connectionDbMain = new SqlConnection(gConnectionString);
            SqlCommand cmd = connectionDbMain.CreateCommand();
            cmd.CommandType = CommandType.Text;
            SqlDataReader reader;

            // intentar con el team ID (o numero de corredor/equipo)
            cmd.CommandText = "SELECT * FROM [resultados] WHERE team_id = '" + consulta + "'";

            connectionDbMain.Open();
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                categoria = reader["categoria"].ToString();
                eventId = reader["id"].ToString();
                team_id = consulta;
            }
            connectionDbMain.Close();

            if(eventId == "")
            {
                // probar con DNI
                cmd.CommandText = "SELECT team_id FROM [finishers] WHERE dni = '" + consulta + "'";
                connectionDbMain.Open();
                reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    team_id = reader["team_id"].ToString();
                }

                connectionDbMain.Close();

                if (team_id != "")
                {
                    // intentar con el team ID (o numero de corredor/equipo)
                    cmd.CommandText = "SELECT * FROM [resultados] WHERE team_id = '" + team_id + "'";

                    connectionDbMain.Open(); ;
                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        categoria = reader["categoria"].ToString();
                        eventId = reader["id"].ToString();
                    }
                    connectionDbMain.Close();
                }
            }
            
            // refresh clasificacion por cada print si el checkbox lo indica
            // buscar nombre evento
            if (eventId != "" && checkBox18.CheckState == CheckState.Checked)
            {
                string eventNameAux = evName[evId.IndexOf(eventId)];
                comboBox30.Text = eventNameAux;
                comboBox29.Text = categoria;
                button36_Click_2(null, null);
            }

            cmd.CommandText = "SELECT * FROM [resultados] WHERE team_id = '" + team_id + "'";

            connectionDbMain.Open();
            reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                nombre = reader["nombre"].ToString();
                numero = reader["team_id"].ToString();
                tiempo = reader["tiempo"].ToString();
                tiempo = tiempo.Substring(0, 2) + "h" + tiempo.Substring(3, 2) + "'" + tiempo.Substring(6, 2) + "\"";
                sexo = reader["sexo"].ToString();
                posCat = reader["cat"].ToString();
                posGral = reader["gral"].ToString();
                posSexo = reader["gral_sex"].ToString();
                laps = Convert.ToInt16(reader["laps"].ToString());
                vueltas[0] = reader["v1"].ToString();
                vueltas[1] = reader["v2"].ToString();
                vueltas[2] = reader["v3"].ToString();
                vueltas[3] = reader["v4"].ToString();
                vueltas[4] = reader["v5"].ToString();
                vueltas[5] = reader["v6"].ToString();
                vueltas[6] = reader["v7"].ToString();
                vueltas[7] = reader["v8"].ToString();
                vueltas[8] = reader["v9"].ToString();
                vueltas[9] = reader["v10"].ToString();
            }

            connectionDbMain.Close();

            // cuenta de corredores categoria
            cmd.CommandText = "SELECT COUNT(categoria) FROM resultados WHERE categoria = '" + categoria +"' AND id = '"+ eventId +"'";
            connectionDbMain.Open();
            runnersCount = (int)cmd.ExecuteScalar();
            connectionDbMain.Close();

            // cuenta de corredores categoria
            cmd.CommandText = "SELECT COUNT(team_id) FROM resultados WHERE sexo = '" + sexo + "' AND id = '" + eventId + "'";
            connectionDbMain.Open();
            gralCount = (int)cmd.ExecuteScalar();
            connectionDbMain.Close();

            if (nombre != "")
            {
                // llenar labels de resultados en tab  print ticket
                label106.Text = evName[evId.IndexOf(eventId)];
                label107.Text = nombre;
                label108.Text = categoria;
                label109.Text = posCat;
                label110.Text = tiempo;
                label112.Text = numero;

                // Create a file to write to.
                // also create de directory just in case some mather fucker
                // deleted it while the program is running
                System.IO.Directory.CreateDirectory(supportFilesPath);
                using (StreamWriter sw = File.CreateText(supportFilesPath + "\\TxtTest.txt"))
                {
                    try
                    {
                        if (Environment.MachineName.Contains("DANIELOPC"))
                        {
                            sw.WriteLine(""); sw.WriteLine("");
                        }
                        string eventName = "";
                        if (evName[evId.IndexOf(eventId)].Length > 20)
                        {
                            int indexOfEspacio = evName[evId.IndexOf(eventId)].IndexOf(' ', 8);
                            if (indexOfEspacio != -1)
                            {
                                eventName = evName[evId.IndexOf(eventId)].Substring(0, indexOfEspacio) + '\n' + evName[evId.IndexOf(eventId)].Substring(indexOfEspacio + 1);
                            }
                        }
                        else
                        {
                            eventName = evName[evId.IndexOf(eventId)];
                        }

                        sw.WriteLine(eventName);
                        sw.WriteLine("");

                        if (nombre.Contains("/"))
                        {
                            sw.WriteLine("Equipo Nº: " + numero);
                            nombre = nombre.Replace("/", "\n");
                            if (nombre.Contains(" ("))
                            {
                                nombre = nombre.Remove(nombre.IndexOf(" ("));
                            }
                            sw.WriteLine(nombre);
                            sw.WriteLine("");
                        }
                        else
                        {
                            sw.WriteLine("Participante Nº: " + numero);
                            sw.WriteLine(nombre.ToUpper().Replace(", ", "\n"));
                            sw.WriteLine("");
                        }

                        sw.WriteLine("Categoria: ");
                        sw.WriteLine(categoria.ToUpper());
                        sw.WriteLine("");

                        if (laps > 1)
                        {
                            for (int i = 0; i < laps; i++)
                            {
                                string vta = vueltas[i].ToString().Substring(0, 2) + "h" + vueltas[i].ToString().Substring(3, 2) + "'" + vueltas[i].ToString().Substring(6, 2) + "\"";
                                sw.WriteLine("Vuelta " + (i + 1).ToString() + ": " + vta);
                            }
                        }
                        sw.WriteLine("Tiempo: " + tiempo);
                        //sw.WriteLine(tiempo);
                        sw.WriteLine("");
                        sw.WriteLine("Posición Categoría: "+ posCat + "º");
                        sw.WriteLine("Cantidad de Participantes: " + runnersCount.ToString());
                        sw.WriteLine("");

                        if (checkBox4.CheckState == CheckState.Checked)
                        {
                            sw.WriteLine("Posición general: "+ posSexo + "º");
                            sw.WriteLine("Cantidad de Participantes: " + gralCount.ToString());
                            sw.WriteLine("");
                        }

                        sw.WriteLine("**Resultados provisorios**");
                        sw.WriteLine("**sujeto a modificaciones**");
                        sw.WriteLine("-------------------------------");
                        sw.WriteLine("cronobottiming.com/resultados");
                        sw.Close();
                    }
                    catch (Exception expe)
                    {
                        MessageBox.Show(expe.Message);
                    }
                }

                try
                {
                    PrintDocument pd = new PrintDocument();
                    pd.DefaultPageSettings.Margins = new Margins(10, 0, 0, 0);
                    pd.DefaultPageSettings.Landscape = false;
                    pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                    // Print the document.
                    pd.Print();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
            else
            {
                label106.Text = "";
                label107.Text = "";
                label108.Text = "";
                label109.Text = "";
                label110.Text = "";
                label112.Text = "";
            }
        }

        private void textBox12_KeyDown(object sender, KeyEventArgs e)
        {
            textBox29_KeyDown(sender, e);
        }

        private void groupBox4_Enter(object sender, EventArgs e)
        {

        }

        private void tabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LlenarEventosRam()
        {
            // chequear si el evento esta en curso
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            if (sqlServerConnect(connection))
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT * FROM [eventos]";
                try
                {
                    SqlDataReader reader = cmd.ExecuteReader();
                    int idx = 0;
                    evId.Clear();
                    evStatus.Clear();
                    evType.Clear();
                    evName.Clear();
                    evStart.Clear();
                    evKey.Clear();
                    evVuelta.Clear();
                    while (reader.Read() && (idx < 10))
                    {
                        evId.Add(reader["id"].ToString());
                        evStatus.Add(reader["estado"].ToString());
                        evType.Add(reader["tipo"].ToString());
                        evName.Add(reader["nombre"].ToString());
                        evStart.Add(reader["fecha"].ToString());
                        evKey.Add(reader["key"].ToString());
                        evPrint.Add(reader["nombrePrint"].ToString());
                        evVuelta.Add(TimeSpan.Parse(reader["vuelta"].ToString()));
                        idx++;
                    }
                    connection.Close();

                    comboBox2.Items.Clear();
                    comboBox9.Items.Clear();
                    comboBox43.Items.Clear();
                    comboBox6.Items.Clear();
                    comboBox34.Items.Clear();
                    //cbImportEvent.Items.Clear();
                    comboBox11.Items.Clear();
                    //cbImportEvent.Items.Add("Listado General");
                    comboBox2.Text = "";
                    comboBox9.Text = "";
                    comboBox43.Text = "";
                    comboBox6.Text = "";
                    comboBox34.Text = "";
                    comboBox11.Text = "";

                    foreach (string name in evName)
                    {
                        comboBox2.Items.Add(name);
                        comboBox9.Items.Add(name);
                        comboBox43.Items.Add(name);
                        comboBox6.Items.Add(name);
                        comboBox34.Items.Add(name);
                        //comboBox41.Items.Add(name);
                        //cbImportEvent.Items.Add(name);
                        comboBox11.Items.Add(name);
                    }
                }
                catch (Exception ex){ MessageBox.Show(ex.Message); }
            }
        }

        private void textBox21_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox21_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                // read
                button37_Click(sender, e);

                // write
                button38_Click(sender, e);

                // read
                button37_Click(sender, e);

                try
                {
                    // check
                    string write_string = textBox21.Text;
                    string read_string = Convert.ToInt64(textBox20.Text, 16).ToString("X");

                    if (read_string != write_string)
                    {
                        MessageBox.Show("No se grabo el chip!");
                        textBox20.Text = "";
                    }
                    else
                    {
                        if (checkBox15.CheckState == CheckState.Checked) //auto increment
                        {
                            try
                            {
                                int numero = Convert.ToInt16(textBox21.Text);
                                numero++;
                                textBox21.Text = numero.ToString();
                            }
                            catch { }
                        }
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void comboBox33_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button96_Click(object sender, EventArgs e)
        {
            getPorts(comboBox33);
        }

        private void button97_Click(object sender, EventArgs e)
        {
            try
            {
                string dummy = "";
                serialPort6.PortName = comboBox33.Text;
                serialPort6.BaudRate = 9600;
                serialPort6.ReadTimeout = 1000;
                serialPort6.WriteTimeout = 1000;
                serialPort6.Open();

                dummy = "";
                // test modo AT
                serialPort6.Write("AT" + "\r\n");
                System.Threading.Thread.Sleep(500);
                while (serialPort6.BytesToRead > 0)
                {
                    dummy += serialPort6.ReadExisting();
                }

                if (dummy.Contains("OK"))
                {
                    dummy = "";
                    // configurar modo texto
                    serialPort6.Write("AT+CMGF=1" + "\r\n");
                    System.Threading.Thread.Sleep(100);
                    while (serialPort6.BytesToRead > 0)
                    {
                        dummy += serialPort6.ReadExisting();
                    }

                    if (dummy.Contains("OK"))
                    {
                        dummy = "";
                        // configurar que hacer con sms entrante
                        serialPort6.Write("AT+CNMI=0,0,0,0,0" + "\r\n");
                        System.Threading.Thread.Sleep(100);
                        while (serialPort6.BytesToRead > 0)
                        {
                            dummy += serialPort6.ReadExisting();
                        }

                        if (dummy.Contains("OK"))
                        {
                            textBox31.AppendText("Modulo GSM Local Conectado" + "\r\n");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error al conectar Modulo GSM: " + ex.Message);
                serialPort6.Close();
            }
        }

        private void button95_Click(object sender, EventArgs e)
        {
            textBox31.Text = "";
        }

        private void button98_Click(object sender, EventArgs e)
        {
            try
            {
                serialPort6.Close();
                timer3.Stop();
                button94.Text = "Start Timer";
            }
            catch
            {

            }
        }

        private void button93_Click(object sender, EventArgs e)
        {
            try
            {
                string dummy = "";
                // get signal quality
                serialPort6.Write("AT+CSQ\r\n");
                System.Threading.Thread.Sleep(500);
                while (serialPort6.BytesToRead > 0)
                {
                    dummy += serialPort6.ReadExisting();
                    textBox31.AppendText(dummy);
                }
            }
            catch{}
        }

        private void button90_Click(object sender, EventArgs e)
        {
            try
            {
                string dummy = "";
                // list unread sms
                serialPort6.Write("AT+CMGL=\"ALL\",1\r\n");
                System.Threading.Thread.Sleep(5000);
                while (serialPort6.BytesToRead > 0)
                {
                    dummy = serialPort6.ReadLine();
                    textBox31.AppendText(dummy + '\n');
                }
            }
            catch{}
        }

        private void button92_Click(object sender, EventArgs e)
        {
            try
            {
                string dummy = "";
                // list unread sms
                serialPort6.Write("AT+CMGL=\"REC UNREAD\",1\r\n");
                System.Threading.Thread.Sleep(2000);
                while (serialPort6.BytesToRead > 0)
                {
                    dummy = serialPort6.ReadLine();
                    textBox31.AppendText(dummy + '\n');
                }
            }
            catch {}
        }

        private void button89_Click(object sender, EventArgs e)
        {
            try
            {
                string dummy = "";
                // get signal quality
                serialPort6.Write("AT+CBC\r\n");
                System.Threading.Thread.Sleep(500);
                while (serialPort6.BytesToRead > 0)
                {
                    dummy += serialPort6.ReadExisting();
                    textBox31.AppendText(dummy);
                }
            }
            catch{}
        }

        private void button91_Click(object sender, EventArgs e)
        {
            try
            {
                string dummy = "";
                // list unread sms
                serialPort6.Write("AT+CMGD=65,1\r\n"); //65 max msg index
                System.Threading.Thread.Sleep(200);
                while (serialPort6.BytesToRead > 0)
                {
                    dummy = serialPort6.ReadLine();
                    textBox31.AppendText(dummy + '\n');
                }
            }
            catch{}
        }

        private void button94_Click(object sender, EventArgs e)
        {
            if (button94.Text == "Stop Timer")
            {
                timer3.Stop();
                button94.Text = "Start Timer";
            }
            else if (button94.Text == "Start Timer")
            {
                timer3.Start();
                button94.Text = "Stop Timer";
            }
        }

        private void textBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                button3_Click(sender, e);
            }
        }

        private void textBox29_TextChanged(object sender, EventArgs e)
        {

        }

        private void button100_Click(object sender, EventArgs e)
        {
            //tiempo maximo 59 min.
            if (dateTimePicker6.Value.Hour == 0 && dateTimePicker6.Value.Minute <= 59)
            {
                TimeSpan vueltaMinima = new TimeSpan(dateTimePicker6.Value.Hour, dateTimePicker6.Value.Minute, dateTimePicker6.Value.Second);
                for(int idx = 0; idx < evVuelta.Count; idx++)
                {
                    evVuelta[idx] = vueltaMinima;
                }

                label80.Text = "Actual: " + vueltaMinima.ToString();
            }
        }

        private void tabPage6_Click(object sender, EventArgs e)
        {

        }

        private void button101_Click(object sender, EventArgs e)
        {
            if (comboBox36.Text != "" &&
               numericUpDown4.Value > 1 &&
               comboBox38.Text != "" &&
               comboBox42.SelectedIndex >= 0 &&
               comboBox41.SelectedIndex >= 0) // data not null
            {
                if (!comboBox36.Items.Contains(comboBox36.Text)) // que no exista previamente un campeonato con ese nombre
                {
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    sqlServerConnect(connection);
                    cmd.CommandType = CommandType.Text;
                    Boolean regla1 = checkBox7.Checked;
                    cmd.CommandText = "INSERT INTO[campeonatos_info] VALUES('" + comboBox36.Text + "', '" + numericUpDown4.Value.ToString() + "', '" + checkBox7.Checked.ToString() + "','" + checkBox8.Checked.ToString() + "' , '" + comboBox38.Text + "', '" + comboBox42.Text + "', '" + comboBox41.Text + "')";
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    connection.Close();

                    poblarComboBoxColumna("campeonatos_info", "nombre", comboBox36, null);
                    poblarComboBoxColumna("campeonatos_info", "nombre", comboBox37, null);
                    displayTable("campeonatos_info", dataGridView7, true);
                }
                else
                {
                    MessageBox.Show("Ya existe campeonato");
                }
            }
        }

        private void label86_Click(object sender, EventArgs e)
        {

        }

        private void button105_Click(object sender, EventArgs e)
        {
            DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;

            if (result == DialogResult.OK) // Test result.
            {
                // conseguir set de categorias e info del campeonato
                string[] catNames = new string[40];
                cmd.CommandText = "SELECT * FROM [campeonatos_info] WHERE nombre = '" + comboBox37.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();

                string setCat = "";
                string campId = "";
                int nbFechas = 0;
                bool presentismoBool = false;
                bool finalObligatoriaBool = false;
                string tipo = "";
                string tipoId = "";

                if (reader.Read())
                {
                    setCat = reader["set_categorias"].ToString();
                    campId = reader["id"].ToString();
                    nbFechas = Convert.ToInt16(reader["fechas"].ToString());
                    presentismoBool = Convert.ToBoolean(reader["presentismo"].ToString());
                    finalObligatoriaBool = Convert.ToBoolean(reader["final_obligatoria"].ToString());
                    tipo = reader["tipo"].ToString();
                    tipoId = reader["tipoId"].ToString();
                }
                connection.Close();

                // chequear que la fecha que se quiere agregar
                // sea dentro de la cantidad de fechas del campeonato
                if (numericUpDown5.Value <= nbFechas)
                {
                    List<string> catList = new List<string>();
                    ////////////////////////////////////////////////////////////////////////////
                    cmd.CommandText = "SELECT categoria FROM [db_categorias] WHERE nombre = '" + setCat + "' ORDER BY inicio ASC";
                    sqlServerConnect(connection);
                    reader = cmd.ExecuteReader();
                    int catNb = 0;

                    if (tipo == "etapas")
                    {
                        catNames[catNb++] = "General";
                        catNames[catNb++] = "General_Varones";
                        catNames[catNb++] = "General_Mujeres";
                        catNames[catNb++] = "General_Mixto";
                    }

                    while (reader.Read())
                    {
                        catList.Add(reader[0].ToString());
                    }
                    connection.Close();

                    ///////////////////////////////////////////////////////////////////////////////
                    // leer fila por fila el .xls
                    // agregar
                    Workbook workbook = new Workbook();
                    workbook = Workbook.Load(openFileDialog1.FileName);
                    string numero = "";
                    string nombre = "";
                    string sexo = "";
                    string[] fechas = new string[10];
                    string[] presentismos = new string[10];
                    int columnaId = tipoId == "dni" ? 0 : 4;

                    // aqui el loop deberia ser por categorias...
                    /***nuevo loop ***/
                    for(int filaIdx = 1; filaIdx < 1000; filaIdx++)
                    {
                        string posicionStr = workbook.Worksheets[0].Cells[filaIdx, 1].ToString();
                        
                        //celda vacia, break!
                        if (posicionStr == "")
                        {
                            break;
                        }

                        numero = workbook.Worksheets[0].Cells[filaIdx, columnaId].ToString();
                        nombre = workbook.Worksheets[0].Cells[filaIdx, 2].ToString();
                        sexo = workbook.Worksheets[0].Cells[filaIdx, 19].ToString();
                        string cat = workbook.Worksheets[0].Cells[filaIdx, 3].ToString();

                        if (catList.Contains(cat))
                        {
                            // ver si el numero+id+categoria existe en la db.
                            // notese que la combinacion numero+id+categoria es valida para las ctagorias generales tambien
                            cmd.CommandText = "SELECT * FROM [campeonatos_data] WHERE id = '" + campId + "' AND numero = '" + numero + "' AND categoria = '" + cat + "'";
                            sqlServerConnect(connection);
                            reader = cmd.ExecuteReader();
                            bool nbExists = false;

                            if (reader.Read())
                            {
                                nbExists = true;
                                // puntos de cada carrera!
                                for (int i = 0; i < nbFechas; i++)
                                {
                                    fechas[i] = reader[4 + i * 2].ToString();
                                    presentismos[i] = reader[5 + i * 2].ToString();
                                }
                            }
                            connection.Close();

                            if (!nbExists)
                            {
                                // agregar
                                cmd.CommandText = "INSERT INTO [campeonatos_data] VALUES('" + campId + "','" + numero + "','" + nombre + "','" + cat + "','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','','" + sexo + "','','')";
                                sqlServerConnect(connection);
                                cmd.ExecuteNonQuery();
                                connection.Close();
                            }

                            // Y sigue...
                            // sumar puntos fechas + presentismos
                            int puntos = 0;
                            int puntosPresentismo = 0; // 5 puntos por presentismo
                            int puntosPosicion = 0;
                            int puntoPresentismoActual = 0;

                            TimeSpan tiempo = new TimeSpan();
                            TimeSpan tiempoTotal = new TimeSpan();

                            for (int i = 0; i < nbFechas; i++)
                            {
                                if (tipo == "etapas")
                                {
                                    if ((i + 1) == numericUpDown5.Value) // si es la fecha que estoy sumando
                                    {
                                        try // si el string no representa un tiempo (dnf, dns, dsq, -1LAP,etc), directamente se saltea
                                        {
                                            tiempo = TimeSpan.Parse(workbook.Worksheets[0].Cells[filaIdx, 8].ToString());
                                            tiempoTotal += tiempo;
                                            puntoPresentismoActual = 1;
                                            puntosPresentismo += 1;
                                        }
                                        catch { }
                                    }
                                    else
                                    {
                                        try // si el string no representa un tiempo (dnf, dns, dsq, -1LAP,etc), directamente se saltea
                                        {
                                            tiempoTotal += TimeSpan.Parse(fechas[i]);
                                            puntosPresentismo += Convert.ToInt16(presentismos[i]);
                                        }
                                        catch (Exception ex)
                                        {
                                            //MessageBox.Show(ex.Message);
                                        }
                                    }
                                }
                                else // TIPO Campeonato
                                {
                                    if ((i + 1) == numericUpDown5.Value) // si es la fecha que estoy sumando
                                    {
                                        try
                                        {
                                            puntosPosicion = getPointsFromPosition(Convert.ToInt16(posicionStr));
                                            puntos += puntosPosicion;
                                            if (presentismoBool)
                                            {
                                                puntos += 5;
                                                puntoPresentismoActual = 5;
                                            }
                                        }
                                        catch { }
                                    }
                                    else
                                    {
                                        puntos += Convert.ToInt16(fechas[i]) + Convert.ToInt16(presentismos[i]);
                                    }
                                }
                            }

                            string actual = "";
                            string final = "";
                            string total = "";

                            if (tipo == "etapas")
                            {
                                actual = tiempo.ToString();
                                final = tiempoTotal.ToString();
                                total = puntosPresentismo.ToString();
                            }
                            else // tipo ""campeonato"
                            {
                                actual = puntosPosicion.ToString().PadLeft(2, '0');
                                final = puntos.ToString().PadLeft(3, '0');
                                total = final;
                            }

                            // calculo puntos individual corredor
                            cmd.CommandText = "UPDATE [campeonatos_data] SET f" + numericUpDown5.Value.ToString() + " = '" + actual + "', p" + numericUpDown5.Value.ToString() + " = '" + puntoPresentismoActual.ToString() + "', total = '" + total + "', final = '" + final + "' WHERE id = '" + campId + "' AND categoria = '" + cat + "' AND numero = '" + numero + "'";
                            sqlServerConnect(connection);
                            cmd.ExecuteNonQuery();
                            connection.Close();
                        }
                        else
                        {
                            MessageBox.Show("Categoria " + cat + " no existe! DNI: "+ numero.ToString());
                        }
                    }          
                }
            }

            displayTable("campeonatos_data", dataGridView8, true);
        }

        private void button106_Click(object sender, EventArgs e)
        {
            if (comboBox24.Text != "")
            {
                DialogResult dialogResult = MessageBox.Show(comboBox24.Text, "Borrar set de categorias desde db?", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "DELETE FROM [db_categorias] WHERE nombre = '" + comboBox24.Text + "'";
                    sqlServerConnect(connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    connection.Close();
                    // refrescar y mostrar la tabla categorias para el evento sobre el cual se esta agregando la categoria
                    poblarComboBoxColumna("db_categorias", "nombre", comboBox24, null);
                    poblarComboBoxColumna("db_categorias", "nombre", comboBox38, null);
                }
            }
        }

        private void button107_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("ESTA A PUNTO DE HACER CAGADAS", "Borrar Campeonato?\r\n" + comboBox36.Text, MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                sqlServerConnect(connection);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "DELETE FROM [campeonatos_info] WHERE nombre = '" + comboBox36.Text + "'; DBCC CHECKIDENT(campeonatos_info, RESEED, 0)";

                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch { }

                connection.Close();
                poblarComboBoxColumna("campeonatos_info", "nombre", comboBox36, null);
                poblarComboBoxColumna("campeonatos_info", "nombre", comboBox37, null);
                displayTable("campeonatos_info", dataGridView7, false);
            }
        }

        private int getPointsFromPosition(int position)
        {
            int[] puntos = {20, 16, 13, 10, 8, 6, 4, 3, 2, 1};
            int retVal = 0;
            if (position > 0 && position <= 10)
            {
                retVal = puntos[position - 1];
            }
            else if(position > 10)
            {
                retVal = 1;
            }

            return retVal;
        }

        private void button108_Click(object sender, EventArgs e)
        {
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;

            // conseguir id campeonato
            string[] catNames = new string[30];
            cmd.CommandText = "SELECT id FROM [campeonatos_info] WHERE nombre = '" + comboBox37.Text + "'";
            sqlServerConnect(connection);
            SqlDataReader reader = cmd.ExecuteReader();

            string campId = "";

            if (reader.Read())
            {
                campId = reader["id"].ToString();
            }
            connection.Close();
            cmd.CommandText = "DELETE FROM [campeonatos_data] WHERE id = '" + campId + "'";
            sqlServerConnect(connection);
            cmd.ExecuteNonQuery();
            connection.Close();
            displayTable("campeonatos_data", dataGridView8, false);
        }

        private void button103_Click(object sender, EventArgs e)
        {
            if (comboBox37.Text != "")
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;

                // conseguir info del campeonato
                cmd.CommandText = "SELECT * FROM [campeonatos_info] WHERE nombre = '" + comboBox37.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();

                string campId = "";
                int nbFechas = 0;
                string tipo = "";
                bool finalObligatoriaBool = false;

                if (reader.Read())
                {
                    campId = reader["id"].ToString();
                    finalObligatoriaBool = Convert.ToBoolean(reader["final_obligatoria"].ToString());
                    nbFechas = Convert.ToInt16(reader["fechas"].ToString());
                    tipo = reader["tipo"].ToString();
                }
                connection.Close();

                ///////////////////////////////////////////////////////////////////////////
                int[] numeroInt = new int[3000];
                int numeroQty = 0;
                sqlServerConnect(connection);
                cmd.CommandText = "SELECT numero FROM [campeonatos_data] WHERE id = '" + campId + "'";

                reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    numeroInt[numeroQty++] = Convert.ToInt32(reader["numero"].ToString());
                }
                connection.Close();

                for (int idx = 0; idx < numeroQty; idx++)
                {
                    // levantar data de cada numero y calcular su campeonato
                    cmd.CommandText = "SELECT * FROM [campeonatos_data] WHERE id = '" + campId + "' AND numero = '" + numeroInt[idx].ToString() + "'";
                    sqlServerConnect(connection);
                    reader = cmd.ExecuteReader();
                    string[] fechas = new string[10];
                    string[] presentismos = new string[10];
                    int sumaPresentismos = 0;
                    int final = 0;
                    int total = 0;
                    TimeSpan tiempoTotal = new TimeSpan();
                    if (reader.Read())
                    {
                        // puntos de cada carrera!
                        for (int i = 0; i < nbFechas; i++)
                        {
                            fechas[i] = reader[4 + i * 2].ToString();
                            presentismos[i] = reader[5 + i * 2].ToString();

                            if (tipo == "campeonato")
                            {
                                sumaPresentismos += Convert.ToInt16(presentismos[i]);
                                total += Convert.ToInt16(fechas[i]) + Convert.ToInt16(presentismos[i]);
                            }
                            else
                            {
                                try
                                {
                                    sumaPresentismos += Convert.ToInt16(presentismos[i]);
                                    tiempoTotal += TimeSpan.Parse(fechas[i]);
                                }
                                catch { }
                            }
                        }
                    }
                    connection.Close();

                    if (tipo == "campeonato")
                    {
                        // calcular descartes.
                        // hay tantos valores de sumas con un descarte como numero de fechas tiene el campeonato
                        int[] descartes = new int[nbFechas];

                        if (checkBox9.CheckState == CheckState.Checked) // si hay que descartar una fecha
                        {
                            for (int j = 0; j < nbFechas; j++)
                            {
                                for (int k = 0; k < nbFechas; k++)
                                {
                                    if (k != j)
                                    {
                                        descartes[j] += Convert.ToInt16(fechas[k]);
                                    }
                                }
                            }

                            final = descartes.Max() + sumaPresentismos;
                        }
                        else
                        {
                            final = total;
                        }

                        // checkBox10.CheckState indica resolucion final
                        // evaluar fecha final obligatora
                        if ((checkBox10.CheckState == CheckState.Checked) && (fechas[nbFechas - 1] == "0"))
                        {
                            final = 0;
                        }

                        // check si hay al menos N fechas con puntos
                        if (checkBox5.CheckState == CheckState.Checked)
                        {
                            int nbFechasConPuntos = 0;
                            for (int i = 0; i < nbFechas; i++)
                            {
                                if (fechas[i] != "0")
                                {
                                    nbFechasConPuntos++;
                                }
                            }
                            //aplicar regla fechas minimas
                            if (nbFechasConPuntos < numericUpDown8.Value && numericUpDown8.Value <= nbFechas)
                            {
                                final = 0;
                            }
                        }

                        string finalStr = final.ToString().PadLeft(3, '0');
                        string totalStr = total.ToString().PadLeft(3, '0');
                        cmd.CommandText = "UPDATE [campeonatos_data] SET final = '" + finalStr + "', total = '" + totalStr + "' WHERE id = '" + campId + "' AND numero = '" + numeroInt[idx].ToString() + "'";
                    }
                    else // etapas
                    {
                        cmd.CommandText = "UPDATE [campeonatos_data] SET final = '" + tiempoTotal.ToString() + "', total = '" + sumaPresentismos.ToString() + "' WHERE id = '" + campId + "' AND numero = '" + numeroInt[idx].ToString() + "'";
                    }

                    sqlServerConnect(connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }

                comboBox39_SelectedIndexChanged(null, null);

                // chequear si hay un DNI en mas de 1 categoria
                // levantar data de cada numero y calcular su campeonato
                cmd.CommandText = "SELECT numero, categoria FROM [campeonatos_data] WHERE id = '" + campId + "' ORDER BY numero ASC";
                sqlServerConnect(connection);
                SqlDataReader checkDni = cmd.ExecuteReader();
                string prevNum = "";
                string prevCat = "";

                while (checkDni.Read())
                {
                    string actNum = checkDni["numero"].ToString();
                    string actCat = checkDni["categoria"].ToString();

                    if(actNum == prevNum)
                    {
                        MessageBox.Show("Dni repetido: " + actNum + ". Categorias: " + actCat + ", " + prevCat);
                    }
                    prevCat = actCat;
                    prevNum = actNum;
                }
                connection.Close();

                // calcular general
                cmd.CommandText = "SELECT numero, nombre, categoria, final, total  FROM [campeonatos_data] WHERE id = '" + campId + "' ORDER BY total DESC, final ASC, f" + nbFechas.ToString() + " ASC";
                sqlServerConnect(connection);
                SqlDataReader general = cmd.ExecuteReader();
                int posGral = 1;
                while(general.Read())
                {
                    SqlConnection connection2 = new SqlConnection(gConnectionString); SqlCommand cmd2 = connection2.CreateCommand();
                    cmd2.CommandType = CommandType.Text;
                    cmd2.CommandText = "UPDATE [campeonatos_data] SET gral = '" + posGral.ToString() + "' WHERE numero = '" + general["numero"] + "'";
                    sqlServerConnect(connection2);
                    cmd2.ExecuteNonQuery();
                    connection2.Close();
                    posGral++;
                }
                connection.Close();

                //MOSTRAR ORDEN GENERAL
                DataTable dta = new DataTable();
                SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                dataadapt.Fill(dta);
                dataGridView8.DataSource = dta;

                // mostrar numero de fila
                foreach (DataGridViewRow row in dataGridView8.Rows)
                {
                    row.HeaderCell.Value = (row.Index + 1).ToString();
                }

                dataGridView8.AutoResizeColumns();
                dataGridView8.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;

                // GENERAL SEXO FEMENINO
                cmd.CommandText = "SELECT numero, nombre, categoria, final, total  FROM [campeonatos_data] WHERE id = '" + campId + "' AND sexo = 'femenino' ORDER BY total DESC, final ASC, f" + nbFechas.ToString() + " ASC";
                sqlServerConnect(connection);
                SqlDataReader femenino = cmd.ExecuteReader();
                int posFem = 1;
                while (femenino.Read())
                {
                    SqlConnection connection2 = new SqlConnection(gConnectionString); SqlCommand cmd2 = connection2.CreateCommand();
                    cmd2.CommandType = CommandType.Text;
                    cmd2.CommandText = "UPDATE [campeonatos_data] SET gral_sexo = '" + posFem.ToString() + "' WHERE numero = '" + femenino["numero"] + "'";
                    sqlServerConnect(connection2);
                    cmd2.ExecuteNonQuery();
                    connection2.Close();
                    posFem++;
                }
                connection.Close();

                // GENERAL SEXO MASCULINO
                cmd.CommandText = "SELECT numero, nombre, categoria, final, total  FROM [campeonatos_data] WHERE id = '" + campId + "' AND sexo = 'masculino' ORDER BY total DESC, final ASC, f" + nbFechas.ToString() + " ASC";
                sqlServerConnect(connection);
                SqlDataReader masculino = cmd.ExecuteReader();
                int posMas = 1;
                while (masculino.Read())
                {
                    SqlConnection connection2 = new SqlConnection(gConnectionString); SqlCommand cmd2 = connection2.CreateCommand();
                    cmd2.CommandType = CommandType.Text;
                    cmd2.CommandText = "UPDATE [campeonatos_data] SET gral_sexo = '" + posMas.ToString() + "' WHERE numero = '" + masculino["numero"] + "'";
                    sqlServerConnect(connection2);
                    cmd2.ExecuteNonQuery();
                    connection2.Close();
                    posMas++;
                }
                connection.Close();

                // CATEGORIA
                cmd.CommandText = "SELECT numero, nombre, categoria, final, total  FROM [campeonatos_data] WHERE id = '" + campId + "' ORDER BY categoria ASC, total DESC, final ASC, f" + nbFechas.ToString() + " ASC";
                sqlServerConnect(connection);
                SqlDataReader cateReader = cmd.ExecuteReader();
                int posCat = 1;
                string prev = "";
                string actual = "";
                while (cateReader.Read())
                {
                    actual = cateReader["categoria"].ToString();
                    if(actual != prev) //hay cambio
                    {
                        posCat = 1;
                        prev = actual;
                    }

                    SqlConnection connection2 = new SqlConnection(gConnectionString); SqlCommand cmd2 = connection2.CreateCommand();
                    cmd2.CommandType = CommandType.Text;
                    cmd2.CommandText = "UPDATE [campeonatos_data] SET cat = '" + posCat.ToString() + "' WHERE numero = '" + cateReader["numero"] + "'";
                    sqlServerConnect(connection2);
                    cmd2.ExecuteNonQuery();
                    connection2.Close();
                    posCat++;
                }
                connection.Close();
            }
        }

        private void button104_Click(object sender, EventArgs e)
        {

        }

        private void comboBox37_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBox39.Items.Clear();
            // conseguir set de categorias e info del campeonato
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string setCat = "";
            string campId = "";
            string tipo = "";
            cmd.CommandText = "SELECT * FROM [campeonatos_info] WHERE nombre = '" + comboBox37.Text + "'";
            sqlServerConnect(connection);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                setCat = reader["set_categorias"].ToString();
                campId = reader["id"].ToString();
                tipo = reader["tipo"].ToString();
            }
            connection.Close();

            if (tipo == "etapas")
            {
                comboBox39.Items.Add("General");
                comboBox39.Items.Add("General_Varones");
                comboBox39.Items.Add("General_Mujeres");
                comboBox39.Items.Add("General_Mixto");
            }

            cmd.CommandText = "SELECT categoria FROM [db_categorias] WHERE nombre = '" + setCat + "' ORDER BY categoria ASC";
            sqlServerConnect(connection);
            reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                comboBox39.Items.Add(reader[0].ToString());
            }
            connection.Close();
        }

        private void comboBox39_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Buscar id campeonato cuyo nombre es combobox38.Text
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            int nbFechas = 0;
            string campId = "";
            string cmdFechas = "";
            string tipo = "";
            bool presentismo = false;
            cmd.CommandText = "SELECT * FROM [campeonatos_info] WHERE nombre = '" + comboBox37.Text + "'";
            sqlServerConnect(connection);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                nbFechas = Convert.ToInt16(reader["fechas"].ToString());
                campId = reader["id"].ToString();
                tipo = reader["tipo"].ToString();
                presentismo = Convert.ToBoolean(reader["presentismo"].ToString());
            }
            connection.Close();

            // Mostrar la sub tabla "campeonatos_data" para el evento cuyo nombre es "comboBox37.Text"
            // y categoria "comboBox39.Text"
            if (tipo == "campeonato")
            {
                for (int i = 0; i < nbFechas; i++)
                {
                    if (presentismo)
                    {
                        if (i == (nbFechas - 1))
                        {
                            cmdFechas += "f" + (i + 1).ToString() + "," + "p" + (i + 1).ToString();
                        }
                        else
                        {
                            cmdFechas += "f" + (i + 1).ToString() + "," + "p" + (i + 1).ToString() + ",";
                        }
                    }
                    else // no mostrar presentismo
                    {
                        if (i == (nbFechas - 1))
                        {
                            cmdFechas += "f" + (i + 1).ToString();
                        }
                        else
                        {
                            cmdFechas += "f" + (i + 1).ToString() + ",";
                        }
                    }
                }

                cmd.CommandText = "SELECT numero,nombre,categoria," + cmdFechas + ",total,final FROM [campeonatos_data] WHERE id = '" + campId + "' AND categoria = '" + comboBox39.Text + "' ORDER BY final DESC, f" + nbFechas.ToString() + " DESC";
            }
            else
            {
                for (int i = 0; i < nbFechas; i++)
                {
                    if (i == (nbFechas - 1))
                    {
                        cmdFechas += "f" + (i + 1).ToString();
                    }
                    else
                    {
                        cmdFechas += "f" + (i + 1).ToString() + ",";
                    }
                }

                switch(comboBox39.Text)
                {
                    case "General":
                        cmd.CommandText = "SELECT numero,nombre,categoria," + cmdFechas + ",total, final, gral FROM [campeonatos_data] WHERE id = '" + campId + "' ORDER BY total DESC, final ASC";
                        break;
                    case "General_Varones":
                        cmd.CommandText = "SELECT numero,nombre,categoria," + cmdFechas + ",total, final, gral_sexo FROM [campeonatos_data] WHERE id = '" + campId + "' AND sexo = 'masculino' ORDER BY total DESC, final ASC";
                        break;
                    case "General_Mujeres":
                        cmd.CommandText = "SELECT numero,nombre,categoria," + cmdFechas + ",total, final, gral_sexo FROM [campeonatos_data] WHERE id = '" + campId + "' AND sexo = 'femenino' ORDER BY total DESC, final ASC";
                        break;
                    default:
                        cmd.CommandText = "SELECT numero,nombre,categoria," + cmdFechas + ",final, cat FROM [campeonatos_data] WHERE id = '" + campId + "' AND categoria = '" + comboBox39.Text + "' ORDER BY total DESC, final ASC";
                        break;
                }
            }
            sqlServerConnect(connection);
            try
            {
                cmd.ExecuteNonQuery();
                DataTable dta = new DataTable();
                SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                dataadapt.Fill(dta);
                dataGridView8.DataSource = dta;

                // mostrar numero de fila
                foreach (DataGridViewRow row in dataGridView8.Rows)
                {
                    row.HeaderCell.Value = (row.Index + 1).ToString();
                }

                dataGridView8.AutoResizeColumns();
                dataGridView8.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }

        private void button109_Click(object sender, EventArgs e)
        {
            // Buscar id campeonato cuyo nombre es combobox37.Text
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            int nbFechas = 0;
            string campId = "";
            string tipo = "";
            bool presentismo = false;
            cmd.CommandText = "SELECT * FROM [campeonatos_info] WHERE nombre = '" + comboBox37.Text + "'";
            sqlServerConnect(connection);
            SqlDataReader reader = cmd.ExecuteReader();

            if (reader.Read())
            {
                nbFechas = Convert.ToInt16(reader["fechas"].ToString());
                campId = reader["id"].ToString();
                tipo = reader["tipo"].ToString();
                presentismo = Convert.ToBoolean(reader["presentismo"].ToString());
            }
            connection.Close();

            //nombre del excel a generar. TODO: seleccionar destino del archivo
            //string fileNameAndLoc = "C:\\Users\\hecto.DESKTOP-TEME2M5\\Documents\\0-Cronobot\\Campeonatos\\" + comboBox37.Text + ".xls";

            // carpeta destino es fija
            string fileNameAndLoc = "";
            string camp_dir = "C:\\cronobot\\campeonatos\\" + comboBox37.Text + "\\";
            System.IO.Directory.CreateDirectory(camp_dir);
            fileNameAndLoc = camp_dir + comboBox37.Text + ".xls";
            string fileCSV = camp_dir + comboBox37.Text + ".csv";

            // crear csv
            StreamWriter csv_file = new StreamWriter(new FileStream(fileCSV, FileMode.Create), Encoding.Unicode);
            //header
            csv_file.Write(lineForCsv("pechera", "pos cat", "gral abs", "gral sexo", "nombre", "localidad", "categoria", "tiempo", "vueltas", "vuelta 1", "vuelta 2", "vuelta 3", "vuelta 4", "vuelta 5", "vuelta 6", "vuelta 7", "evento", "modalidad", "dni", "sexo"));

            Workbook workbook = new Workbook();
            Worksheet worksheet = new Worksheet(comboBox37.Text);

            worksheet.Cells.ColumnWidth[0] = 1100; // Pos
            worksheet.Cells.ColumnWidth[1] = 2000; // Numero
            worksheet.Cells.ColumnWidth[2] = 7000; // Nombre

            ushort x = 3;
            ushort n = (tipo == "campeonato" && presentismo == true) ? (ushort)2 : (ushort)1;
            for (; x < (nbFechas * n) + 3; x++)
            {
                if (tipo == "campeonato")
                {
                    worksheet.Cells.ColumnWidth[x] = 1000; // Fechas
                }
                else
                {
                    worksheet.Cells.ColumnWidth[x] = 2500; // Fechas
                }
            }

            worksheet.Cells.ColumnWidth[x++] = 3000;
            worksheet.Cells.ColumnWidth[x] = 3000;
            //

            sqlServerConnect(connection);
            if(tipo == "etapas")
            {
                cmd.CommandText = "SELECT * FROM [campeonatos_data] WHERE id = '" + campId + "' ORDER BY categoria ASC, total DESC, final ASC, f" + nbFechas.ToString() + " ASC";
            }
            else
            {
                cmd.CommandText = "SELECT * FROM [campeonatos_data] WHERE id = '" + campId + "' ORDER BY categoria ASC, final DESC, f" + nbFechas.ToString() + " DESC";
            }
            reader = cmd.ExecuteReader();
            int posicion = 0;
            int filaCount = 0;
            string numero = "";
            string nombre = "";
            string categoria = "";
            string total = "";
            string final = "";
            string prevCat = "";
            string gral = "";
            string gral_sexo = "";
            string sexo = "";
            string[] posiciones = new string[10];
            string[] presentismos = new string[10];

            worksheet.Cells[filaCount, 0] = new Cell(comboBox37.Text);
            filaCount += 2;

            while (reader.Read())
            {
                numero = reader["numero"].ToString();
                nombre = reader["nombre"].ToString();
                categoria = reader["categoria"].ToString();
                total = reader["total"].ToString();
                final = reader["final"].ToString();
                sexo = reader["sexo"].ToString();
                gral = reader["gral"].ToString();
                gral_sexo = reader["gral_sexo"].ToString();

                for (int i = 0; i < nbFechas; i++)
                {
                    posiciones[i] = reader[4 + i * 2].ToString();
                    presentismos[i] = reader[5 + i * 2].ToString();
                }

                if (categoria != prevCat)
                {
                    filaCount++;
                    worksheet.Cells[filaCount, 0] = new Cell(categoria.ToUpper());
                    filaCount++;
                    posicion = 1;
                    worksheet.Cells[filaCount, 0] = new Cell("Pos");
                    worksheet.Cells[filaCount, 1] = new Cell("Numero");
                    worksheet.Cells[filaCount, 2] = new Cell("Nombre");
                    for (int i = 0; i < nbFechas; i++)
                    {
                        worksheet.Cells[filaCount, 3 + i * n] = new Cell((tipo == "campeonato"? "F":"E") + (i + 1).ToString());

                        if (tipo == "campeonato")
                        {
                            worksheet.Cells[filaCount, 4 + i * n] = new Cell("p" + (i + 1).ToString());
                        }
                    }
                    worksheet.Cells[filaCount, (nbFechas * n) + 3] = new Cell("Total");
                    //if (tipo == "campeonato")
                    {
                        worksheet.Cells[filaCount, (nbFechas * n) + 4] = new Cell("Final");
                    }
                    filaCount++;
                }

                worksheet.Cells[filaCount, 0] = new Cell(posicion);
                worksheet.Cells[filaCount, 1] = new Cell(numero);
                worksheet.Cells[filaCount, 2] = new Cell(nombre);

                for (int j = 0; j < nbFechas; j++)
                {
                    worksheet.Cells[filaCount, 3 + j * n] = new Cell(posiciones[j]);
                    if (tipo == "campeonato")
                    {
                        worksheet.Cells[filaCount, 4 + j * n] = new Cell(presentismos[j]);
                    }
                }
                //worksheet.Cells[filaCount, (nbFechas * n) + 3] = new Cell(total);

                if (tipo == "campeonato")
                {
                    worksheet.Cells[filaCount, (nbFechas * n) + 3] = new Cell(Convert.ToString(Convert.ToInt16(total)));
                    worksheet.Cells[filaCount, (nbFechas * n) + 4] = new Cell(Convert.ToString(Convert.ToInt16(final)));
                }
                else
                {
                    worksheet.Cells[filaCount, (nbFechas * n) + 3] = new Cell(total);
                    worksheet.Cells[filaCount, (nbFechas * n) + 4] = new Cell(final);
                }

                filaCount++;
                prevCat = categoria;
                // buscar localidad
                SqlConnection connection2 = new SqlConnection(gConnectionString); SqlCommand cmd2 = connection2.CreateCommand();
                cmd2.CommandType = CommandType.Text;
                cmd2.CommandText = "SELECT finishers.numero, finishers.dni, corredores.ciudad FROM finishers JOIN corredores ON finishers.dni = corredores.dni WHERE finishers.numero = '" + numero+"'";
                sqlServerConnect(connection2);
                SqlDataReader readerLocalidad = cmd2.ExecuteReader();
                string localidad = "";
                string dni = "";
                if (readerLocalidad.Read())
                {
                    localidad = readerLocalidad["ciudad"].ToString();
                    dni = readerLocalidad["dni"].ToString();
                }
                connection2.Close();
                csv_file.Write(lineForCsv(numero, posicion.ToString(), gral, gral_sexo, nombre, localidad, categoria, final, total, posiciones[0], posiciones[1], "", "", "", "", "", comboBox37.Text, "General Final", dni, sexo));
                posicion++;
            }

            connection.Close();
            csv_file.Close();

            // agregar info sistema de puntos
            if (tipo == "campeonato")
            {
                filaCount += 2;
                worksheet.Cells[filaCount++, 0] = new Cell("Sistema de puntaje");
                worksheet.Cells[filaCount, 0] = new Cell("Pos");
                worksheet.Cells[filaCount++, 1] = new Cell("Puntos");
                int[] puntos = { 35, 29, 24, 20, 17, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2, 1 };
                for (int position = 1; position <= 20; position++)
                {
                    worksheet.Cells[filaCount, 0] = new Cell(position.ToString() + "º");
                    worksheet.Cells[filaCount++, 1] = new Cell(puntos[position - 1].ToString());
                }
                // fin sistema de puntos
                // info reglas aplicadas
                filaCount += 2;
                worksheet.Cells[filaCount++, 0] = new Cell("Referencias:");
                worksheet.Cells[filaCount++, 0] = new Cell(" - f : fecha puntuable");
                worksheet.Cells[filaCount++, 0] = new Cell(" - Total: suma de puntos sin aplicar criterios reglamentarios");
                worksheet.Cells[filaCount++, 0] = new Cell(" - Final: suma de puntos luego de aplicar criterios reglamentarios");
                filaCount += 2;
                worksheet.Cells[filaCount++, 0] = new Cell("Criterios reglamentarios para resolucion final de las posiciones del campeonato:");
                if (checkBox9.CheckState == CheckState.Checked)
                {
                    worksheet.Cells[filaCount++, 0] = new Cell(" - Desacarte peor resultado");
                }
                if (checkBox5.CheckState == CheckState.Checked)
                {
                    worksheet.Cells[filaCount++, 0] = new Cell(" - Presente en al menos " + numericUpDown8.Value.ToString() + " fechas");
                }
                if (checkBox10.CheckState == CheckState.Checked)
                {
                    worksheet.Cells[filaCount++, 0] = new Cell(" - Fecha final Obligatoria");
                }
                worksheet.Cells[filaCount++, 0] = new Cell(" - Mayor cantidad de puntos sumados");
                worksheet.Cells[filaCount++, 0] = new Cell(" - Ante un empate, prevalece el que tenga mejor puntaje en la ultima fecha");
            }

            workbook.Worksheets.Add(worksheet);
            try
            {
                workbook.Save(fileNameAndLoc);
            }
            catch
            {

            }
        }

        private void button110_Click(object sender, EventArgs e)
        {

        }

        private void button99_Click(object sender, EventArgs e)
        {
            if (comboBox43.Text != "" && textBox3.Text != "")
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                string eventId = buscarIdEventoPorNombre(comboBox43.Text);
                string eventTipe = evType[evId.IndexOf(eventId)];

                cmd.CommandText = "INSERT INTO [eventos] VALUES ('" + textBox3.Text + "','','','a venir','" + eventTipe + "')";
                sqlServerConnect(connection);
                cmd.ExecuteNonQuery();
                connection.Close();
                LlenarEventosRam();
                displayTable("eventos", dataGridView1, true);

                //copiar categorias (y corredores?)
                string[] catName = new string[30];
                string[] catIn = new string[30];
                string[] catEnd = new string[30];
                string[] catSex = new string[30];
                string[] catLaps = new string[30];

                cmd.CommandText = "SELECT * FROM [categorias] WHERE id = '" + eventId + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();
                int catCount = 0;
                while (reader.Read())
                {
                    catName[catCount] = reader["categoria"].ToString();
                    catIn[catCount] = reader["inicio"].ToString();
                    catEnd[catCount] = reader["fin"].ToString();
                    catSex[catCount] = reader["sexo"].ToString();
                    catLaps[catCount] = reader["vueltas"].ToString();
                    catCount++;
                }
                connection.Close();

                // insertar categorias para el nuevo evento               
                for (int idx = 0; idx < catCount; idx++)
                {
                    cmd.CommandText = "INSERT INTO [categorias] VALUES ('" + buscarIdEventoPorNombre(textBox3.Text) + "','" + catName[idx] + "','" + catIn[idx] + "','" + catEnd[idx] + "','" + catSex[idx] + "','" + catLaps[idx] + "','')";
                    sqlServerConnect(connection);
                    cmd.ExecuteNonQuery();
                    connection.Close();
                }

                // insertar corredores (?)
                comboBox2.Items.Add(textBox3.Text);
                comboBox6.Items.Add(textBox3.Text);
                comboBox9.Items.Add(textBox3.Text);
                comboBox34.Items.Add(textBox3.Text);
                comboBox43.Items.Add(textBox3.Text);

                comboBox43.Text = "";
                textBox4.Text = "";
                comboBox31.Text = "";
                dateTimePicker2.Value = DateTime.Now;
                textBox3.Text = "";
            }
        }

        private void CrearDataBases()
        {
            // crear DB
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "CREATE DATABASE db_main ON PRIMARY" +
                "(NAME = db_main, " +
                "FILENAME = '" + supportFilesPath + "\\db_main.mdf" + "', " +
                "SIZE = 2MB, MAXSIZE = 10MB, FILEGROWTH = 10%) " +
                "LOG ON (NAME = db_main_log, " +
                "FILENAME = '" + supportFilesPath + "\\db_main_log.ldf" + "', " +
                "SIZE = 1MB, " +
                "MAXSIZE = 5MB, " +
                "FILEGROWTH = 10%)";
            //cmd.CommandText = "DROP DATABASE db_main";
            sqlServerConnect(connection);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            // crear tablas
            connection.Close();

            cmd.CommandText = "CREATE TABLE [dbo].[eventos] (" +
                                "[Id]     INT IDENTITY(1, 1) NOT NULL," +
                                "[nombre] VARCHAR(100) NOT NULL," +
                                "[fecha]  DATETIME NULL," +
                                "[lugar]  VARCHAR(20)  NULL," +
                                "[estado] VARCHAR(20)  NOT NULL," +
                                "[tipo]   VARCHAR(20)  NOT NULL," +
                                "PRIMARY KEY CLUSTERED([Id] ASC)," +
                                "CHECK([tipo]= 'vueltas' OR[tipo]= 'raid')," +
                                "CHECK([estado]= 'a venir' OR[estado]= 'en curso' OR[estado]= 'terminado'))";
            sqlServerConnect(connection);
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[campeonatos_data] (" +
                                "[id]        INT          NOT NULL," +
                                "[numero]    INT          NOT NULL," +
                                "[nombre]    VARCHAR (50) NULL," +
                                "[categoria] VARCHAR (50) NOT NULL," +
                                "[f1]        VARCHAR (50) NULL," +
                                "[p1]        VARCHAR (50) NULL," +
                                "[f2]        VARCHAR (50) NULL," +
                                "[p2]        VARCHAR (50) NULL," +
                                "[f3]        VARCHAR (50) NULL," +
                                "[p3]        VARCHAR (50) NULL," +
                                "[f4]        VARCHAR (50) NULL," +
                                "[p4]        VARCHAR (50) NULL," +
                                "[f5]        VARCHAR (50) NULL," +
                                "[p5]        VARCHAR (50) NULL," +
                                "[f6]        VARCHAR (50) NULL," +
                                "[p6]        VARCHAR (50) NULL," +
                                "[f7]        VARCHAR (50) NULL," +
                                "[p7]        VARCHAR (50) NULL," +
                                "[f8]        VARCHAR (50) NULL," +
                                "[p8]        VARCHAR (50) NULL," +
                                "[f9]        VARCHAR (50) NULL," +
                                "[p9]        VARCHAR (50) NULL," +
                                "[f10]       VARCHAR (50) NULL," +
                                "[p10]       VARCHAR (50) NULL," +
                                "[total]     VARCHAR (50) NULL," +
                                "[final]     VARCHAR (50) NULL);";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[campeonatos_info] (" +
                                "[id]                INT          IDENTITY (1, 1) NOT NULL," +
                                "[nombre]            VARCHAR (50) NULL," +
                                "[fechas]            INT          NOT NULL," +
                                "[presentismo]       BIT          NULL," +
                                "[final_obligatoria] BIT          NULL," +
                                "[set_categorias]    VARCHAR (50) NOT NULL," +
                                "[tipo]              VARCHAR (20) NULL," +
                                "CHECK ([tipo]='campeonato' OR [tipo]='etapas'));";

            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[categorias] (" +
                                "[id]        VARCHAR (50) NULL," +
                                "[categoria] VARCHAR (50) NOT NULL," +
                                "[inicio]    INT          NULL," +
                                "[fin]       INT          NULL," +
                                "[sexo]      VARCHAR (20) NULL," +
                                "[vueltas]   INT          NULL," +
                                "[largada]   DATETIME     NULL);";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[Corredores] ( " +
                                "[nombre]     VARCHAR (50) NULL," +
                                "[nacimiento] DATE         NULL," +
                                "[dni]        VARCHAR (20) NOT NULL," +
                                "[email]      VARCHAR (50) NULL," +
                                "[sexo]       VARCHAR (20) NULL," +
                                "[ciudad]     VARCHAR (50) NULL," +
                                "[team]       VARCHAR (20) NULL," +
                                "[numero]     VARCHAR (20) NULL," +
                                "[categoria]  VARCHAR (50) NULL," +
                                "[team_id]    INT          NULL," +
                                "[campo]      VARCHAR (50) NULL," +
                                "UNIQUE NONCLUSTERED ([dni] ASC));";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[db_categorias] (" +
                                "[categoria] VARCHAR (50) NOT NULL," +
                                "[inicio]    INT          NOT NULL," +
                                "[fin]       INT          NOT NULL," +
                                "[sexo]      VARCHAR (10) NOT NULL," +
                                "[nombre]    VARCHAR (50) NULL);";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[finishers] (" +
                                "[id]        INT           NOT NULL," +
                                "[nombre]    VARCHAR (50)  NULL," +
                                "[numero]    VARCHAR (20)  NOT NULL," +
                                "[categoria] VARCHAR (50)  NOT NULL," +
                                "[sexo]      VARCHAR (20)  NOT NULL," +
                                "[v1]        DATETIME2 (7) NULL," +
                                "[v2]        DATETIME2 (7) NULL," +
                                "[v3]        DATETIME2 (7) NULL," +
                                "[v4]        DATETIME2 (7) NULL," +
                                "[v5]        DATETIME2 (7) NULL," +
                                "[v6]        DATETIME2 (7) NULL," +
                                "[v7]        DATETIME2 (7) NULL," +
                                "[v8]        DATETIME2 (7) NULL," +
                                "[v9]        DATETIME2 (7) NULL," +
                                "[v10]       DATETIME2 (7) NULL," +
                                "[tiempo]    TIME (7)      NULL," +
                                "[lap]       INT           NULL," +
                                "[laps]      INT           NULL," +
                                "[estado]    VARCHAR (20)  NULL," +
                                "[largada]   DATETIME2 (7) NULL," +
                                "[dni]       VARCHAR (10)  NULL," +
                                "[ciudad]    VARCHAR (50)  NULL," +
                                "[team_id]   INT           NULL," +
                                "UNIQUE NONCLUSTERED ([numero] ASC)," +
                                "FOREIGN KEY ([id]) REFERENCES [dbo].[eventos] ([Id])," +
                                "CHECK ([estado]='1-finish' OR [estado]='4-dnf' OR [estado]='2-running' OR [estado]='6-dns' OR [estado]='7-to start' OR [estado]='5-dsq'));";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[log] (" +
                                "[time]   DATETIME     NOT NULL," +
                                "[id]     VARCHAR (10) NULL," +
                                "[source] VARCHAR (50) NULL," +
                                "[numero] VARCHAR (50) NULL," +
                                "[obs]    VARCHAR (50) NULL);";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            cmd.CommandText = "CREATE TABLE [dbo].[resultados] (" +
                                "[id]        INT           NOT NULL," +
                                "[nombre]    VARCHAR (200) NULL," +
                                "[numero]    VARCHAR (100) NOT NULL," +
                                "[team_id]   VARCHAR (20)  NULL," +
                                "[categoria] VARCHAR (50)  NOT NULL," +
                                "[v1]        VARCHAR (50)  NULL," +
                                "[v2]        VARCHAR (50)  NULL," +
                                "[v3]        VARCHAR (50)  NULL," +
                                "[v4]        VARCHAR (50)  NULL," +
                                "[v5]        VARCHAR (50)  NULL," +
                                "[v6]        VARCHAR (50)  NULL," +
                                "[v7]        VARCHAR (50)  NULL," +
                                "[v8]        VARCHAR (50)  NULL," +
                                "[v9]        VARCHAR (50)  NULL," +
                                "[v10]       VARCHAR (50)  NULL," +
                                "[tiempo]    VARCHAR (50)  NULL," +
                                "[estado]    VARCHAR (20)  NULL," +
                                "[largada]   TIME (7)      NULL," +
                                "[lap]       INT           NULL," +
                                "[laps]      INT           NULL," +
                                "[sexo]      VARCHAR (20)  NULL," +
                                "[cat]       INT           NULL," +
                                "[gral]      INT           NULL," +
                                "[gral_sex]  INT           NULL," +
                                "[ciudad]    VARCHAR (200) NULL," +
                                "UNIQUE NONCLUSTERED ([numero] ASC)," +
                                "FOREIGN KEY ([id]) REFERENCES [dbo].[eventos] ([Id])," +
                                "CHECK ([estado]='1-finish' OR [estado]='4-dnf' OR [estado]='2-running' OR [estado]='6-dns' OR [estado]='7-to start' OR [estado]='5-dsq'));";
            try
            {
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();

            MessageBox.Show(gConnectionString);
        }

        private void inicilizaciones()
        {
            LlenarEventosRam();
            load_cats();

            // inicializaciones TAB "Listado General"
            displayTable("Corredores", dataGridView_Corredores, false);

            // inicializaciones TAB "Configuracion Evento"
            displayTable("eventos", dataGridView1, true);

            //poblar combobox24 con nombres de los id de eventos disponibles en tabla categorias
            poblarComboBoxColumna("db_categorias", "nombre", comboBox24, null);

            //poblar combobox5 con nombres de las categorias para el evento
            comboBox5.Items.Clear();
            poblarComboBoxColumna("categorias", "categoria", comboBox5, comboBox2.Text);

            // mostrar en datagridview2 las categorias correspondientes al evento que aparece en comboBox2
            mostrarTablaChildEvento(comboBox2.Text, "categorias", dataGridView2, true);

            // mostrar en datagridview3 los finishers correspondientes al evento que aparece en comboBox2
            mostrarTablaInscriptos();

            //displayTable("resultados", dataGridView4, false);
            //poblarComboBoxEventosEnCurso(comboBox7);
            comboBox7.Items.Clear();
            poblarComboBoxColumna("eventos", "nombre", comboBox7, null);
            poblarComboBoxEventosEnCurso(comboBox30);

            // CAMPEONATOS
            poblarComboBoxColumna("campeonatos_info", "nombre", comboBox36, null);
            poblarComboBoxColumna("campeonatos_info", "nombre", comboBox37, null);
            poblarComboBoxColumna("db_categorias", "nombre", comboBox38, null); // campeonatos info
            displayTable("campeonatos_info", dataGridView7, true);
            displayTable("campeonatos_data", dataGridView8, true);
        }

        private void button25_Click(object sender, EventArgs e)
        {
            inicilizaciones();
        }

        private void button44_Click(object sender, EventArgs e)
        {
            if (button44.Text == "Start Viewer")
            {
                timer4.Start();
                button44.Text = "Stop Viewer";
            }
            else
            {
                timer4.Stop();
                button44.Text = "Start Viewer";
            }
        }

        public void timer4_Tick(object sender, EventArgs e)
        {
            if (gEvName != "")
            {
                CalcularTiempos(gEvName);
                Calcular_Pos_General_y_Categoria(gEvName);
                mostrarViewer();

                if (checkBox16.CheckState == CheckState.Checked)
                {
                    // ROLL Categorias
                    gCatActual++;
                    if (gCatActual == gCatsCount)
                    {
                        gCatActual = 0;
                    }
                }
            }
        }

        private void mostrarViewer()
        {
            label44.Text = gCats[gCatActual];
            string eventIdString = buscarIdEventoPorNombre(gEvName);
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            string vueltas = selectLapNumber(eventIdString, gCats[gCatActual], "resultados");
            switch (gCats[gCatActual])
            {
                case "General":
                    cmd.CommandText = "SELECT gral,nombre,numero,categoria,tiempo FROM [resultados] WHERE id = '" + eventIdString + "' AND NOT estado = '7-to start' ORDER BY gral ASC";
                    break;
                case "General Masculina":
                    cmd.CommandText = "SELECT gral_sex,nombre,numero,categoria,tiempo FROM [resultados] WHERE id = '" + eventIdString + "' AND sexo = 'masculino' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                    break;
                case "General Femenina":
                    cmd.CommandText = "SELECT gral_sex,nombre,numero,categoria,tiempo FROM [resultados] WHERE id = '" + eventIdString + "' AND sexo = 'femenino' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                    break;
                case "General Mixto":
                    cmd.CommandText = "SELECT gral_sex,nombre,numero,categoria,tiempo FROM [resultados] WHERE id = '" + eventIdString + "' AND sexo = 'mixto' AND NOT estado = '7-to start' ORDER BY gral_sex ASC";
                    break;
                default:
                    cmd.CommandText = "SELECT cat,nombre,numero,estado,tiempo FROM [resultados] WHERE id = '" + eventIdString + "' AND categoria = '" + gCats[gCatActual] + "' ORDER BY cat ASC";
                    break;
            }

            sqlServerConnect(connection);
            try
            {
                cmd.ExecuteNonQuery();
                DataTable dta = new DataTable();
                SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                dataadapt.Fill(dta);
                dataGridView9.DataSource = dta;

                //Change cell font
                foreach (DataGridViewColumn c in dataGridView9.Columns)
                {
                    c.DefaultCellStyle.Font = new Font("Arial", 20F, GraphicsUnit.Pixel);
                }

                dataGridView9.Columns[0].HeaderText = "posicion";

                dataGridView9.AutoResizeColumns();
                dataGridView9.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.AllCells;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            connection.Close();
        }

        private void comboBox11_SelectedIndexChanged(object sender, EventArgs e)
        {
            if(comboBox11.SelectedIndex != -1)
            {
                //clear cats array
                gCats = new string[30];
                gCatsCount = 0;
                gCatActual = 0;
                // Event ID
                gEvName = comboBox11.Text;
                
                if(evType[evName.IndexOf(gEvName)] == "raid")
                {
                    gCats[0] = "General";
                    gCats[1] = "General Masculina";
                    gCats[2] = "General Femenina";
                    gCats[3] = "General Mixto";
                    gCatsCount = 4;
                }

                // Categorias:
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT categoria FROM [categorias] WHERE id = '"+buscarIdEventoPorNombre(gEvName)+"'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();

                while(reader.Read())
                {
                    gCats[gCatsCount++] = reader["categoria"].ToString();
                }
                connection.Close();

                button44.Text = "Start Viewer";
                mostrarViewer();
            }
        }

        private void load_cats()
        {
            gCatNombres.Clear();
            gCatLargadas.Clear();
            //Event ID
            gEvName = comboBox11.Text;
            int evIdx = 0;

            foreach (string eventoId in evId)
            {
                int catIdx = 0;

                // Categorias:
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT categoria, largada FROM [categorias] WHERE id = '" + eventoId + "'";
                if (sqlServerConnect(connection))
                {
                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        // "codigo" para categorias es eventID-catName. 
                        // Ejemplo: 1-Elite
                        gCatNombres.Add(eventoId + "-" + reader["categoria"].ToString());
                        gCatLargadas.Add(Convert.ToDateTime(reader["largada"].ToString()));
                        catIdx++;
                    }
                    connection.Close();
                }

                evIdx++;
            }
        }

        private void button104_Click_1(object sender, EventArgs e)
        {
            if (gCatActual == 0)
            {
                gCatActual = gCatsCount - 1;
            }
            else
            {
                gCatActual--;
            }
            mostrarViewer();
        }

        private void button52_Click(object sender, EventArgs e)
        {
            gCatActual++;
            if(gCatActual == gCatsCount)
            {
                gCatActual = 0;
            }
            mostrarViewer();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LapeoAutomatico()
        {
            catLapeo = new string[30];
            vueltaActual = new int[30];
            tiempoVuelta = new TimeSpan[30];
            ultimoPaso = new DateTime[30];
            horaLapeo = new DateTime[30];
            catFinish = new string[30];
            catCountLapeo = 0;
            catCountFinish = 0;
          
            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;

            //buscar categorias ya finalizadas
            cmd.CommandText = "SELECT DISTINCT categoria FROM [finishers] WHERE estado = '1-finish' AND NOT lap = '0' ORDER BY categoria ASC";
            sqlServerConnect(connection);
            SqlDataReader reader = cmd.ExecuteReader();
            while (reader.Read())
            {
                catFinish[catCountFinish++] = reader["categoria"].ToString();
            }
            connection.Close();

            //buscar categorias
            cmd.CommandText = "SELECT DISTINCT categoria FROM [finishers] WHERE estado = '2-running' AND NOT lap = '0' ORDER BY categoria ASC";
            sqlServerConnect(connection);
            reader = cmd.ExecuteReader();

            while (reader.Read())
            {
                // chequear si la categoria no ha finalizado (no ha llegado el primero aun)
                int indx = Array.IndexOf(catFinish, reader["categoria"].ToString());
                if(indx == -1)
                {
                    catLapeo[catCountLapeo++] = reader["categoria"].ToString();
                }
            }
            connection.Close();

            // intento de encontrar primeros puestos en tabla finishers
            for(int j = 0; j < catCountLapeo; j++)
            {
                cmd.CommandText = "SELECT largada,v1,v2,v3,v4,v5,v6,v7,v8,v9,v10,lap FROM [finishers] WHERE categoria = '"+catLapeo[j]+"' AND estado = '2-running' AND NOT lap = '0' ORDER BY lap DESC, tiempo ASC";
                sqlServerConnect(connection);
                reader = cmd.ExecuteReader();
                if (reader.Read())
                {
                    vueltaActual[j] = Convert.ToInt16(reader["lap"].ToString());
                    ultimoPaso[j] = DateTime.Parse(reader[vueltaActual[j]].ToString());
                    DateTime pasoAnterior = DateTime.Parse(reader[vueltaActual[j] - 1].ToString());
                    tiempoVuelta[j] = ultimoPaso[j] - pasoAnterior;
                    horaLapeo[j] = ultimoPaso[j] + new TimeSpan((long)(tiempoVuelta[j].Ticks * 0.8));

                }
                connection.Close();
            }
        }

        private void timer5_Tick(object sender, EventArgs e)
        {
            LapeoAutomatico();
        }

        private void button110_Click_1(object sender, EventArgs e)
        {
            if(button110.Text == "Start Lapeo")
            {
                timer5.Start();
                button110.Text = "Stop Lapeo";
            }
            else
            {
                timer5.Stop();
                button110.Text = "Start Lapeo";
            }
        }

        private void label71_Click(object sender, EventArgs e)
        {

        }

        private void label53_Click(object sender, EventArgs e)
        {

        }

        private void label68_Click(object sender, EventArgs e)
        {

        }

        private void label72_Click(object sender, EventArgs e)
        {

        }

        private void label69_Click(object sender, EventArgs e)
        {

        }

        private void label73_Click(object sender, EventArgs e)
        {

        }

        private void label70_Click(object sender, EventArgs e)
        {

        }

        private void label74_Click(object sender, EventArgs e)
        {

        }

        private void label79_Click(object sender, EventArgs e)
        {

        }

        private void label12_Click(object sender, EventArgs e)
        {

        }

        private void button111_Click(object sender, EventArgs e)
        {
            DialogResult dialogResult = MessageBox.Show("ESTA A PUNTO DE HACER CAGADAS", "Ojo, va a reiniciar el evento" + comboBox6.Text, MessageBoxButtons.YesNo);

            if (dialogResult == DialogResult.Yes)
            {
                SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                string eventId = buscarIdEventoPorNombre(comboBox34.Text);
                if (eventId != "")
                {
                    // seterar vuelatas = 0; estado = 7-to start; reset vueltas; tiempo = 23:59:59;
                    cmd.CommandText = "UPDATE [finishers] SET lap = '0', estado = '7-to start', tiempo = '23:59:59', v0='', v1='', v2='', v3='', v4='', v5='', v6='', v7='', v8='', v9='', v10='' WHERE id = '" + eventId+"';";
                    cmd.CommandText += "UPDATE [eventos] SET estado = 'a venir', fecha = '' WHERE id = '" + eventId+"'";
                    cmd.CommandText += "UPDATE [categorias] SET largada = '' WHERE id = '" + eventId + "'";
                    // categorias hora largada
                    sqlServerConnect(connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch { }
                    connection.Close();
                    LlenarEventosRam();
                    load_cats();

                    //cargar archivo de texto "Manual" para el evento
                    if (File.Exists(supportFilesPath + "\\timestamps.txt"))
                    {
                        File.Delete(supportFilesPath + "\\timestamps.txt");
                    }

                    textBox30.Text = "";
                }
            }
        }

        private void button112_Click(object sender, EventArgs e)
        {

        }

        private void button112_Click_1(object sender, EventArgs e)
        {
            if (textBox18.Text != "")
            {
                string numero = "";
                int vueltas = new int();
                int new_vueltas = new int();
                string estado = "";
                string categoria = "";
                string resetvuelta = "";
                // leer datos completos del corredor del corredor

                SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.categoria,corredores.sexo,finishers.tiempo,finishers.estado, finishers.lap, categorias.vueltas FROM finishers JOIN corredores ON corredores.dni = finishers.dni JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.numero = '" + textBox18.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    numero = reader["numero"].ToString();
                    vueltas = Convert.ToInt16(reader["lap"].ToString());
                    estado = reader["estado"].ToString();
                    categoria = reader["categoria"].ToString();
                }
                connection.Close();

                DialogResult dialogResult = MessageBox.Show("Corredor " + numero + ", vueltas " + vueltas.ToString() + ", estado " + estado + ". Step back?", "Editor de Resultado", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    // bajarle una vuelta
                    if (vueltas > 0)
                    {
                        new_vueltas = vueltas - 1;
                        resetvuelta = ",v" + vueltas.ToString() + "=''";
                    }

                    // si es finisher, volver a running
                    if (estado == "1-finish")
                    {
                        estado = "2-running";
                    }
                    // si esta corriendo y no dio ninguna vuelta, volver a "to start"
                    else if (vueltas == 0 && estado == "2-running")
                    {
                        estado = "7-to start";
                    }

                    cmd.CommandText = "UPDATE [finishers] SET estado = '" + estado + "', lap = '" + new_vueltas.ToString() + "'" + resetvuelta + "   WHERE numero = '" + numero + "';";
                    sqlServerConnect(connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    connection.Close();
                    calcularClasificacion(comboBox7.Text, categoria, "finishers", dataGridView4);
                }
            }
        }

        private void button113_Click(object sender, EventArgs e)
        {
            // !PXX!!!!!!!!!!!!#

            try
            {
                string setpC = "!P" +numericUpDown6.Value.ToString().PadLeft(2,'0') +"%%%%%%%%%%%%#";
                // enviar comando
                serialPort5.Write(setpC);
                System.Threading.Thread.Sleep(100);
                while (serialPort5.BytesToRead > 0)
                {
                    textBox6.AppendText(serialPort5.ReadExisting());
                }
            }
            catch { }
        }

        private void button115_Click(object sender, EventArgs e)
        {
            try
            {
                // enviar comando
                serialPort5.Write("!X@@@@@@@@@@@@@@#");
                System.Threading.Thread.Sleep(5000);
                if (serialPort5.BytesToRead > 0)
                {
                    textBox6.AppendText(serialPort5.ReadExisting());
                }
            }
            catch { }
        }

        private void button114_Click(object sender, EventArgs e)
        {
            // !UXXX@@@@@@@@@@@# Dump Record Nro X
            try
            {
                // enviar comando
                for(int j = 0; j < 632; j++)
                {
                    serialPort5.Write("!U"+ j.ToString().PadLeft(3,'0') +"@@@@@@@@@@@#");
                    System.Threading.Thread.Sleep(60);
                    if (serialPort5.BytesToRead > 0)
                    {
                        textBox6.AppendText(serialPort5.ReadLine() + "\r\n");
                    }
                }
            }
            catch { }
        }

        private void button86_Click_1(object sender, EventArgs e)
        {
            try
            {
                // enviar comando
                serialPort5.Write("!Y%%%%%%%%%%%%%%#");
                System.Threading.Thread.Sleep(10);
                /*while (serialPort5.BytesToRead > 0)
                {
                    textBox6.Text += serialPort5.ReadExisting();
                }*/
            }
            catch { }
        }

        private void button116_Click(object sender, EventArgs e)
        {
            // Bajar solo hasta donde dice el indice
            try
            {
                serialPort5.Write("!S______________#");
                System.Threading.Thread.Sleep(1000);
                string dummy;
                while (serialPort5.BytesToRead > 0)
                {
                    dummy = serialPort5.ReadLine();
                    if (dummy.Contains("EEPROM Section"))
                    {
                        //dummy = serialPort5.ReadLine(); // fecha y hora
                        if (serialPort5.ReadLine().Contains("Pc:"))
                        {
                            string recordsInfo = serialPort5.ReadLine();
                            if (recordsInfo.Contains("Number of Records:"))
                            {
                                int nbOfRec = Convert.ToInt16(recordsInfo.Substring(recordsInfo.IndexOf(":") + 1));

                                // enviar comando
                                for (int j = 0; j < nbOfRec; j++)
                                {
                                    serialPort5.Write("!U" + j.ToString().PadLeft(3, '0') + "@@@@@@@@@@@#");
                                    System.Threading.Thread.Sleep(60);
                                    if (serialPort5.BytesToRead > 0)
                                    {
                                        textBox6.AppendText(serialPort5.ReadLine() + "\r\n");
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch { }
        }

        private void textBox12_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    // send custom command
                    serialPort6.Write(textBox12.Text + "\r\n");
                    System.Threading.Thread.Sleep(2000);
                    while (serialPort6.BytesToRead > 0)
                    {
                        textBox31.AppendText(serialPort6.ReadExisting());
                    }
                }
                catch { }
            }
        }

        private void textBox33_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                try
                {
                    //entrar en modo AT
                    string dummy;
                    serialPort5.Write("!A!!!!!!!!!!!!!!#");
                    System.Threading.Thread.Sleep(100);
                    if (serialPort5.BytesToRead > 0)
                    {
                        dummy = serialPort5.ReadExisting();

                        if(dummy.Contains("Enter AT OK"))
                        {
                            // send custom command
                            serialPort5.Write(textBox33.Text + "\r\n");
                            // esperar un maximo de 10 segundos
                            DateTime startWait = DateTime.Now;

                            while (serialPort5.BytesToRead < 0 && ((DateTime.Now.Ticks - startWait.Ticks) < 100000000)) ;
                            System.Threading.Thread.Sleep(100);
                            while (serialPort5.BytesToRead > 0)
                            {
                                textBox6.AppendText(serialPort5.ReadExisting());
                                textBox33.Text = "";
                            }

                            // exit AT mode
                            serialPort5.Write("!A!!!!!!!!!!!!!!#");
                            System.Threading.Thread.Sleep(100);
                            dummy = serialPort5.ReadExisting();
                        }
                    }
                }
                catch { }
            }
        }

        private void comboBox24_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void checkBox17_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void comboBox34_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button85_Click(object sender, EventArgs e)
        {

        }

        private void label94_Click(object sender, EventArgs e)
        {

        }

        private void textBox34_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //timer1.Stop();
                if (textBox34.Text != "")
                {
                    string idEvento = buscarIdEventoPorNombre(comboBox30.Text);
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT nombre,team_id,categoria,tiempo,estado,cat, gral, gral_sex FROM [resultados] WHERE team_id = '" + textBox34.Text + "'";

                    try
                    {
                        sqlServerConnect(connection);
                        SqlDataReader reader = cmd.ExecuteReader();

                        if (reader.Read())
                        {
                            connection.Close();
                            sqlServerConnect(connection);
                            cmd.ExecuteNonQuery();
                            DataTable dta = new DataTable();
                            SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                            dataadapt.Fill(dta);
                            dataGridView6.DataSource = dta;
                        }
                        else
                        {
                            MessageBox.Show("El corredor no esta en la lista de resultados");
                        }

                        connection.Close();

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }

                //timer1.Start();
            }
        }

        private void label97_Click(object sender, EventArgs e)
        {

        }

        private void label101_Click(object sender, EventArgs e)
        {

        }

        private void textBox7_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox7.Text != "")
                {
                    // buscar en base de datos principal
                    SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = "SELECT * FROM [corredores] WHERE CHARINDEX('" + textBox7.Text + "', nombre) > 0";
                    connection.Close();
                    sqlServerConnect(connection);
                    SqlDataReader dnisRead = cmd.ExecuteReader(); 
                    comboBox44.Items.Clear();
                    comboBox44.Text = "";

                    while (dnisRead.Read())
                    {
                        comboBox44.Items.Add(dnisRead["dni"].ToString());
                    }

                    comboBox44.DroppedDown = true;
                }
            }
        }

        private void comboBox44_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox44.Text != "")
            {
                poblarDatosCorredor();
                buscarCategoriasParaComboBox5();
            }
        }

        private void poblarDatosCorredor()
        {
            string dniStr = "";
            string nombreStr = "";
            DateTime nacimiento = DateTime.Now;
            string sexoStr = comboBox4.Text; ;
            string ciudad = "";
            string numero = "";
            string categoria = "";
            string teamId = "";
            string campo = "";
            string chip = "";
            string eventId = "";
            string ruuningTeam = "";

            SqlConnection connection = new SqlConnection(gConnectionString);SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;

            // buscar participante entre los inscriptos
            sqlServerConnect(connection);
            cmd.CommandText = "SELECT corredores.nacimiento, corredores.nombre, corredores.sexo, corredores.ciudad, corredores.campo, corredores.team, finishers.dni, finishers.numero, finishers.chip, finishers.team_id, finishers.categoria, finishers.id FROM corredores, finishers WHERE corredores.dni = finishers.dni AND finishers.dni = '" + comboBox44.Text + "'";
            SqlDataReader dniInscipto = cmd.ExecuteReader();

            if (dniInscipto.Read())
            {
                nombreStr = dniInscipto["nombre"].ToString();
                dniStr = dniInscipto["dni"].ToString();
                categoria = dniInscipto["categoria"].ToString();
                sexoStr = dniInscipto["sexo"].ToString().ToLower();
                ciudad = dniInscipto["ciudad"].ToString();
                numero = dniInscipto["numero"].ToString();
                teamId = dniInscipto["team_id"].ToString();
                campo = dniInscipto["campo"].ToString();
                chip = dniInscipto["chip"].ToString();
                nacimiento = Convert.ToDateTime(dniInscipto["nacimiento"].ToString());
                eventId = dniInscipto["id"].ToString();
                ruuningTeam = dniInscipto["team"].ToString();

                if (evId.Contains(eventId))
                {
                    comboBox9.Text = evName[evId.IndexOf(eventId)];
                }
            }
            else
            {
                MessageBox.Show("Participante no registrado en el evento!");
                connection.Close();
                // buscar datos personales en tabla corredores
                sqlServerConnect(connection);
                cmd.CommandText = "SELECT dni, nacimiento, nombre, sexo, ciudad, campo, team FROM corredores WHERE dni = '" + comboBox44.Text + "'";
                dniInscipto = cmd.ExecuteReader();
                if (dniInscipto.Read())
                {
                    nombreStr = dniInscipto["nombre"].ToString();
                    dniStr = dniInscipto["dni"].ToString();
                    sexoStr = dniInscipto["sexo"].ToString().ToLower();
                    ciudad = dniInscipto["ciudad"].ToString();
                    campo = dniInscipto["campo"].ToString();
                    nacimiento = Convert.ToDateTime(dniInscipto["nacimiento"].ToString());
                    ruuningTeam = dniInscipto["team"].ToString();
                }
                else
                {
                    MessageBox.Show("Participante no registrado en base de datos!");
                }
            }

            connection.Close();

            sqlServerConnect(connection);

            cmd.ExecuteNonQuery();
            DataTable dta = new DataTable();
            SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
            dataadapt.Fill(dta);
            dataGridView3.DataSource = dta;

            connection.Close();

            textBox7.Text = nombreStr;
            dateTimePicker3.Value = (nacimiento >= dateTimePicker1.MinDate && nacimiento <= dateTimePicker1.MaxDate) ? nacimiento : DateTime.Now;
            comboBox4.Text = sexoStr;
            textBox16.Text = ciudad;
            textBox32.Text = campo;
            textBox36.Text = ruuningTeam;
            comboBox5.Text = categoria;
            textBox13.Text = numero;
            textBox28.Text = teamId;

            textBox8.Text = chip;

            // tiene compañero????
            string dniCompa = "";
            cmd.CommandText = "SELECT dni, team_id FROM finishers WHERE team_id = '" + teamId + "' AND NOT dni = '" + dniStr + "'";
            sqlServerConnect(connection);
            SqlDataReader compa = cmd.ExecuteReader();
            if(compa.Read())
            {
                dniCompa = compa["dni"].ToString();
            }

            connection.Close();
            toggleView(false);
            textBox47.Text = "";
            //textBox44.Text = "";
            comboBox52.Text = "";
            textBox45.Text = "";
            textBox43.Text = "";
            dateTimePicker9.Value = DateTime.Now;
            textBox42.Text = "";
            textBox46.Text = "";
            textBox48.Text = "";
            textBox50.Text = "";
            textBox49.Text = "";
            comboBox50.Text = "";

            if (dniCompa!= "")
            {
                poblarDatosCompa(teamId, dniCompa, true);
                toggleView(true);
            }
        }

        private void toggleView(bool expand)
        {
            //mostrar groupbox datos compa
            //bajar datagridview
            if (expand)
            {
                groupBox28.Height = 149;
                dataGridView3.Height = 298;
                dataGridView3.Left = 632; //X coordinate
                dataGridView3.Top = 373; //Y coordinate
            }
            //ocultar groupbox datos compa
            //subir datagridview
            else
            {
                groupBox28.Height = 0;
                dataGridView3.Height = 456;
                dataGridView3.Left = 632; //X coordinate
                dataGridView3.Top = 215; //Y coordinate
            }

        }

        private bool poblarDatosCompa(string teamId, string dniStr, bool esCompa)
        {
            bool retval = false;
            SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
            cmd.CommandType = CommandType.Text;
            sqlServerConnect(connection);
            if(esCompa)
            {
                cmd.CommandText = "SELECT corredores.nacimiento, corredores.nombre, corredores.sexo, corredores.ciudad, corredores.campo, corredores.team, finishers.dni, finishers.numero, finishers.chip, finishers.team_id, finishers.categoria, finishers.id FROM corredores, finishers WHERE corredores.dni = finishers.dni AND finishers.dni = '" + dniStr + "'";
            }
            else
            {
                cmd.CommandText = "SELECT corredores.nacimiento, corredores.nombre, corredores.sexo, corredores.ciudad, corredores.campo, corredores.team, corredores.dni FROM corredores WHERE corredores.dni = '" + dniStr + "'";
            }
            SqlDataReader compa = cmd.ExecuteReader();

            if (compa.Read())
            {
                try
                {
                    textBox47.Text = compa["nombre"].ToString();
                    textBox44.Text = compa["dni"].ToString();
                    comboBox52.Text = compa["sexo"].ToString().ToLower();
                    textBox45.Text = compa["ciudad"].ToString();
                    textBox43.Text = compa["campo"].ToString();
                    dateTimePicker9.Value = Convert.ToDateTime(compa["nacimiento"].ToString());
                    textBox42.Text = compa["team"].ToString();
                    retval = true;
                    //-----------DATOS EVENTO
                    textBox46.Text = compa["numero"].ToString();
                    textBox48.Text = compa["chip"].ToString();
                    textBox49.Text = compa["team_id"].ToString();
                    string eventId = compa["id"].ToString();
                    if (evId.Contains(eventId))
                    {
                        textBox50.Text = evName[evId.IndexOf(eventId)];
                    }

                    comboBox50.Items.Clear();
                    poblarComboBoxColumna("categorias", "categoria", comboBox50, textBox50.Text);

                    comboBox50.Text = compa["categoria"].ToString();
                }
                catch { }
            }
            else
            {
                textBox47.Text = "";
                //textBox44.Text = "";
                comboBox52.Text = "";
                textBox45.Text = "";
                textBox43.Text = "";
                dateTimePicker9.Value = DateTime.Now;
                textBox42.Text = "";
                textBox46.Text = "";
                textBox48.Text = "";
                textBox50.Text = "";
                textBox49.Text = "";
                comboBox50.Text = "";
            }
            connection.Close();

            return retval;
        }

        private void comboBox44_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (comboBox44.Text != "")
                {
                    poblarDatosCorredor();
                    //buscarCategoriasParaComboBox5();
                }
            }
        }

        private void textBox17_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int potenciaPorcentual = Convert.ToInt16(textBox17.Text);
                if (ajustarPotencia(0, potenciaPorcentual))
                {
                    trackBar1.Value = Convert.ToInt16(textBox17.Text);
                }
            }
        }

        private void textBox25_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int potenciaPorcentual = Convert.ToInt16(textBox25.Text);
                if (ajustarPotencia(1, potenciaPorcentual))
                {
                    trackBar2.Value = Convert.ToInt16(textBox25.Text);
                }
            }
        }

        private void textBox26_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int potenciaPorcentual = Convert.ToInt16(textBox26.Text);
                if (ajustarPotencia(2, potenciaPorcentual))
                {
                    trackBar3.Value = Convert.ToInt16(textBox26.Text);
                }
            }
        }

        private void textBox27_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                int potenciaPorcentual = Convert.ToInt16(textBox27.Text);
                if (ajustarPotencia(3, potenciaPorcentual))
                {
                    trackBar4.Value = Convert.ToInt16(textBox27.Text);
                }
            }
        }

        private void label102_Click(object sender, EventArgs e)
        {

        }

        private void label105_Click(object sender, EventArgs e)
        {

        }

        private void textBox5_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox5.Text != "")
                {
                    printticket(textBox5.Text);
                }

                textBox5.Text = "";
            }
        }

        private void button87_Click(object sender, EventArgs e)
        {
            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";

            //DialogResult result = openFileDialog1.ShowDialog(); // Show the dialog.

            if (open.ShowDialog() == DialogResult.OK) // Test result.
            {
                pictureBox1.Image = new Bitmap(open.FileName);
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;
            }
        }

        private void trackBar1_TabIndexChanged(object sender, EventArgs e)
        {

        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            ajustarPotencia(0, trackBar1.Value);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            ajustarPotencia(1, trackBar2.Value);
        }

        private void trackBar3_Scroll(object sender, EventArgs e)
        {
            ajustarPotencia(2, trackBar3.Value);
        }

        private void trackBar4_Scroll(object sender, EventArgs e)
        {
            ajustarPotencia(3, trackBar4.Value);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            button123_Click(null, null);
            AutoConnectAntenas();
        }

        private void button118_Click(object sender, EventArgs e)
        {
            for (int idx = 0; idx < 4; idx++)
            {
                ClickBotonStop(idx);
            }
        }

        private void main_FormClosed(object sender, FormClosedEventArgs e)
        {
            for(int idx = 0; idx < 4; idx++)
            {
                ClickBotonStop(idx);
            }

            // abort threads!
            try
            {
                TriggerAntenna.Abort();
                AddChip.Abort();
                timer2.Stop();
            }
            catch { }
        }

        private void CheckThreads()
        {
            if (gAntActive[0] || gAntActive[1] || gAntActive[2] || gAntActive[3])
            {
                if (TriggerAntenna.ThreadState != System.Threading.ThreadState.Running)
                {
                    TriggerAntenna = new Thread(Thread1);
                    TriggerAntenna.Start();
                }
            }
            else
            {
                if (TriggerAntenna.ThreadState == System.Threading.ThreadState.Running)
                {
                    TriggerAntenna.Abort();
                }
            }
        }

        private void comboBox15_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private DateTime detectarFormatoFecha(string nac)
        {
            DateTime nacimiento = new DateTime();

            if (nac == "")
            {
                return nacimiento;
            }

            try
            {
                nacimiento = DateTime.FromOADate(Convert.ToDouble(nac));
            }
            catch
            {
                try
                {
                    nacimiento = DateTime.ParseExact(nac, "d/M/yy", new CultureInfo("en-EN"));
                }
                catch
                {
                    try
                    {
                        nacimiento = DateTime.ParseExact(nac, "dd/M/yy", new CultureInfo("en-EN"));
                    }
                    catch
                    {
                        try
                        {
                            nacimiento = DateTime.ParseExact(nac, "d/MM/yy", new CultureInfo("en-EN"));
                        }
                        catch
                        {
                            try
                            {
                                nacimiento = DateTime.ParseExact(nac, "dd/MM/yy", new CultureInfo("en-EN"));
                            }
                            catch
                            {
                                try
                                {
                                    nacimiento = DateTime.ParseExact(nac, "d/M/yyyy", new CultureInfo("en-EN"));
                                }
                                catch
                                {
                                    try
                                    {
                                        nacimiento = DateTime.ParseExact(nac, "dd/M/yyyy", new CultureInfo("en-EN"));
                                    }
                                    catch
                                    {
                                        try
                                        {
                                            nacimiento = DateTime.ParseExact(nac, "d/MM/yyyy", new CultureInfo("en-EN"));
                                        }
                                        catch
                                        {
                                            try
                                            {
                                                nacimiento = DateTime.ParseExact(nac, "dd/MM/yyyy", new CultureInfo("en-EN"));
                                            }
                                            catch
                                            {
                                                try
                                                {
                                                    nacimiento = DateTime.ParseExact(nac, "M/d/yy", new CultureInfo("en-EN"));
                                                }
                                                catch
                                                {
                                                    try
                                                    {
                                                        nacimiento = DateTime.ParseExact(nac, "M/dd/yy", new CultureInfo("en-EN"));
                                                    }
                                                    catch
                                                    {
                                                        try
                                                        {
                                                            nacimiento = DateTime.ParseExact(nac, "MM/d/yy", new CultureInfo("en-EN"));
                                                        }
                                                        catch
                                                        {
                                                            try
                                                            {
                                                                nacimiento = DateTime.ParseExact(nac, "MM/dd/yy", new CultureInfo("en-EN"));
                                                            }
                                                            catch
                                                            {
                                                                try
                                                                {
                                                                    nacimiento = DateTime.ParseExact(nac, "M/d/yyyy", new CultureInfo("en-EN"));
                                                                }
                                                                catch
                                                                {
                                                                    try
                                                                    {
                                                                        nacimiento = DateTime.ParseExact(nac, "M/dd/yyyy", new CultureInfo("en-EN"));
                                                                    }
                                                                    catch
                                                                    {
                                                                        try
                                                                        {
                                                                            nacimiento = DateTime.ParseExact(nac, "MM/d/yyyy", new CultureInfo("en-EN"));
                                                                        }
                                                                        catch
                                                                        {
                                                                            try
                                                                            {
                                                                                nacimiento = DateTime.ParseExact(nac, "MM/DD/yyyy", new CultureInfo("en-EN"));
                                                                            }
                                                                            catch
                                                                            {
                                                                                try
                                                                                {
                                                                                    nacimiento = DateTime.ParseExact(nac, "yyyy/MM/DD", new CultureInfo("en-EN"));
                                                                                }
                                                                                catch
                                                                                {
                                                                                    try
                                                                                    {
                                                                                        nacimiento = DateTime.ParseExact(nac, "yyyy-MM-DD", new CultureInfo("en-EN"));
                                                                                    }
                                                                                    catch
                                                                                    {
                                                                                        MessageBox.Show("yaaaaa wey");
                                                                                        try
                                                                                        {
                                                                                            nacimiento = DateTime.ParseExact(nac, "yyyy/MM/DD", CultureInfo.InvariantCulture);
                                                                                        }
                                                                                        catch
                                                                                        {
                                                                                            MessageBox.Show("yaaaaa wey");
                                                                                        }
                                                                                    }
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return nacimiento;
        }

        private string DetectarSexo(string sexo_crudo)
        {
            string sexo = "";

            if (sexo_crudo == "femenino" || sexo_crudo == "female" || sexo_crudo == "f" || sexo_crudo.Contains("fem") || sexo_crudo == "mujer")
            {
                sexo = "femenino";
            }
            else if (sexo_crudo == "masculino" || sexo_crudo == "hombre" || sexo_crudo == "male" || sexo_crudo == "h" || sexo_crudo == "v" || sexo_crudo == "m" || sexo_crudo.Contains("masc") || sexo_crudo == "varon" || sexo_crudo == "varón")
            {
                sexo = "masculino";
            }

            return sexo;
        }

        private void comboBox40_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void timer6_Tick(object sender, EventArgs e)
        {

        }

        private void button59_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "" && textBox23.Text != "")
            {              
                SqlConnection connectionDbMain = new SqlConnection(gConnectionString);
                SqlCommand cmd = connectionDbMain.CreateCommand();
                cmd.CommandType = CommandType.Text;
                SqlDataReader reader;

                int vuelta = -1; // vuelta a consultar
                try
                {
                    vuelta = Convert.ToInt16(textBox23.Text);
                }
                catch
                {

                }

                if (vuelta != -1 && vuelta <= 10)
                {
                    if (checkBox3.Checked)
                    {
                        cmd.CommandText = "SELECT finishers.v0, finishers.v1, finishers.v2, finishers.v3, finishers.v4, finishers.v5, finishers.v6, finishers.v7, finishers.v8, finishers.v9, finishers.v10 FROM finishers JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.numero = '" + textBox18.Text + "'";
                    }
                    else
                    {
                        cmd.CommandText = "SELECT categorias.largada, finishers.v1, finishers.v2, finishers.v3, finishers.v4, finishers.v5, finishers.v6, finishers.v7, finishers.v8, finishers.v9, finishers.v10 FROM finishers JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.numero = '" + textBox18.Text + "'";
                    }
                    connectionDbMain.Open();
                    reader = cmd.ExecuteReader();

                    if(reader.Read())
                    { 
                        DateTime paso = reader.GetDateTime(vuelta);
                        MessageBox.Show("Paso por llegada (vuelta "+ textBox23.Text + ") del corredor nro " + textBox18.Text + ": " + paso.ToString("dd/MM/yyyy HH:mm:ss"));
                        dateTimePicker4.Value = paso;
                    }

                    connectionDbMain.Close();
                }
            }
        }

        private void textBox34_TextChanged(object sender, EventArgs e)
        {

        }

        private void button75_Click(object sender, EventArgs e)
        {
            if (comboBox45.Text != "")
            {
                // check if server is available
                string connectionstring = @"server = " + comboBox45.Text + "; Database=db_red;integrated security=False;User ID=sa;Password=daniel; connect timeout = 10";

                if (comboBox45.Text == "DB Local")
                {
                    string appFileName = Environment.GetCommandLineArgs()[0];
                    string directory = Path.GetDirectoryName(appFileName);
                    connectionstring = @"Data Source = (LocalDB)\MSSQLLocalDB; AttachDbFilename=" + directory + @"\db_local.mdf;Integrated Security = True; Connect Timeout = 30";
                }

                SqlConnection newConnection = new SqlConnection(connectionstring);
                if (sqlServerCheckConnection(newConnection))
                {
                    gServerName = comboBox45.Text;
                    gConnectionString = connectionstring;

                    textBox35.Text = gServerName;
                    if (gServerName.Contains(Environment.MachineName))
                    {
                        textBox35.Text += " (THIS PC)";
                    }

                    // save new configuration
                    Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    configuration.AppSettings.Settings["server"].Value = comboBox45.Text;
                    configuration.Save(ConfigurationSaveMode.Full, true);
                    ConfigurationManager.RefreshSection("appSettings");

                    inicilizaciones();
                }
                else
                {
                    textBox35.Text = "NOT CONNECTED";
                }
            }
        }

        private void button67_Click(object sender, EventArgs e)
        {
            // string myServer = Environment.MachineName; // util para encontrar el nombre de la maquina!
            comboBox45.Items.Clear();
            DataTable servers = SqlDataSourceEnumerator.Instance.GetDataSources();

            for (int i = 0; i < servers.Rows.Count; i++)
            {
                if ((servers.Rows[i]["InstanceName"] as string) != null)
                {
                    comboBox45.Items.Add(servers.Rows[i]["ServerName"] + "\\" + servers.Rows[i]["InstanceName"]);
                }
                else
                {
                    comboBox45.Items.Add(servers.Rows[i]["ServerName"].ToString());
                }
            }

            comboBox45.Items.Add("DB Local");
        }

        private bool sqlServerConnect(SqlConnection sqlConnection)
        {
            bool connectionOk = false;

            try
            {
                sqlConnection.Open();
                connectionOk = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show(sqlConnection.ConnectionString.Substring(0, sqlConnection.ConnectionString.IndexOf(";")) + "\n\nNo está disponible, Cambiar Servidor!!\n\n" + ex.Message);
            }

            return connectionOk;
        }

        private bool sqlServerCheckConnection(SqlConnection sqlConnection)
        {
            bool connectionOk = false;
            if (sqlServerConnect(sqlConnection))
            {
                connectionOk = true;
                sqlConnection.Close();
            }

            return connectionOk;
        }

        private void comboBox21_SelectedIndexChanged_1(object sender, EventArgs e)
        {

        }

        private void checkBox19_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox19.CheckState ==  CheckState.Checked)
            {
                //inhabilitar 
                textBox8.Enabled = false;
            }
            else
            {
                textBox8.Enabled = true;
            }
        }

        private void comboBox7_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboBox7.Text != "")
            {
                string eventId = buscarIdEventoPorNombre(comboBox7.Text);
                comboBox8.Items.Clear();
                comboBox8.Items.Add("General");
                comboBox8.Items.Add("Masculino");
                comboBox8.Items.Add("Femenino");
                SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT categoria FROM [categorias] WHERE id = '" + eventId + "' ORDER BY sexo DESC, inicio ASC";
                sqlServerConnect(connection);
                SqlDataReader catReader = cmd.ExecuteReader();

                while (catReader.Read())
                {
                    comboBox8.Items.Add(catReader["categoria"].ToString());
                }

                connection.Close();
            }
            else
            {
                comboBox8.Text = "";
            }
        }

        private void comboBox8_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            calcularClasificacion(comboBox7.Text, comboBox8.Text, "finishers", dataGridView4);
        }

        private void button117_Click(object sender, EventArgs e)
        {
            if (textBox18.Text != "")
            {
                string numero = "";
                int vueltas = new int();
                int vueltas_totales = new int();
                int new_vueltas = new int();
                string estado = "";
                string categoria = "";
                string setvuelta = "";
                // leer datos completos del corredor del corredor

                SqlConnection connection = new SqlConnection(gConnectionString); SqlCommand cmd = connection.CreateCommand();
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "SELECT corredores.nombre,finishers.numero,finishers.categoria,corredores.sexo,finishers.tiempo,finishers.estado, finishers.lap, categorias.vueltas FROM finishers JOIN corredores ON corredores.dni = finishers.dni JOIN categorias ON categorias.categoria = finishers.categoria WHERE finishers.numero = '" + textBox18.Text + "'";
                sqlServerConnect(connection);
                SqlDataReader reader = cmd.ExecuteReader();

                if (reader.Read())
                {
                    numero = reader["numero"].ToString();
                    vueltas = Convert.ToInt16(reader["lap"].ToString());
                    estado = reader["estado"].ToString();
                    categoria = reader["categoria"].ToString();
                    vueltas_totales = Convert.ToInt16(reader["vueltas"].ToString());
                }
                connection.Close();

                DialogResult dialogResult = MessageBox.Show("Corredor " + numero + ", vueltas " + vueltas.ToString() + ", estado " + estado + ". Step forward?", "Editor de Resultado", MessageBoxButtons.YesNo);

                if (dialogResult == DialogResult.Yes)
                {
                    // subirle una vuelta
                    if (vueltas < vueltas_totales)
                    {
                        if (estado != "7-to start")
                        {
                            new_vueltas = vueltas + 1;
                            
                            // si es running, pasar a finisher en caso de tener todas las vueltas
                            if (estado == "2-running" && new_vueltas == vueltas_totales)
                            {
                                estado = "1-finish";
                            }
                        }
                        else
                        {
                           estado = "2-running";
                        }

                        string vuelta_actual = "";

                        cmd.CommandText = "SELECT categorias.largada,finishers.v1,finishers.v2,finishers.v3,finishers.v4,finishers.v5,finishers.v6,finishers.v7,finishers.v8,finishers.v9,finishers.v10 FROM finishers JOIN categorias ON categorias.categoria = finishers.categoria WHERE numero = '" + textBox18.Text + "'";
                        sqlServerConnect(connection);
                        reader = cmd.ExecuteReader();
                        if (reader.Read())
                        {
                            vuelta_actual = reader.GetDateTime(vueltas).ToString("MM-dd-yyyy HH:mm:ss.fffffff");
                        }
                        connection.Close();

                        // se debe copiar el paso de la vuelta previa la adicion, en la vuelta adicionada
                        if (new_vueltas > 0)
                        {
                            setvuelta = ",v" + new_vueltas.ToString() + "='" + vuelta_actual + "'"; 
                        }
                    }
                    else
                    {
                        new_vueltas = vueltas_totales;
                    }

                    cmd.CommandText = "UPDATE [finishers] SET estado = '" + estado + "', lap = '" + new_vueltas.ToString() + "'" + setvuelta + "   WHERE numero = '" + numero + "';";
                    sqlServerConnect(connection);
                    try
                    {
                        cmd.ExecuteNonQuery();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }

                    connection.Close();
                    calcularClasificacion(comboBox7.Text, categoria, "finishers", dataGridView4);
                }
            }
        }

        private void textBox37_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox37.Text != "")
                {
                    printticket(textBox37.Text);
                }

                textBox37.Text = "";
            }
        }

        private void button120_Click(object sender, EventArgs e)
        {
            // Sincronizar hora con internet
            Process process = new Process();
            ProcessStartInfo startInfo = new ProcessStartInfo();
            //ver la ventana del terminal ejecutando los comandos
            startInfo.WindowStyle = ProcessWindowStyle.Normal;
            // programa a ejecutar: command prompt
            startInfo.FileName = "cmd.exe";
            // comando(s) separados por &
            startInfo.Arguments = "/K tzutil /s \"Tocantins Standard Time\" & cd C:\\Windows\\System32 & net stop w32time & w32tm /unregister & w32tm /register & net start w32time & w32tm /resync";
            // rus as administrator
            startInfo.Verb = "runas";
            // arrancar!
            process.StartInfo = startInfo;
            process.Start();
        }

        private void button121_Click(object sender, EventArgs e)
        {
            textBox38.Text = "";
        }

        private void button122_Click(object sender, EventArgs e)
        {
            string antSet = "";
            bool prevThreadStatus = gThreadActive;
            gThreadActive = false;

            for (int idx = 0; idx < 4; idx++)
            {
                if(gAntConnected[idx] == true)
                {
                    if (SetSingleMultiTagModeAndFiltering(gAntAdrr[idx], gPortsConnected[idx]))
                    {
                        antSet += gAntAdrr[idx].ToString() + ", ";
                    }
                }
            }

            CheckThreads();
            string modeSt = comboBox40.Text == "0" ? "MultiTag Mode" : "Single Tag Mode";    
            MessageBox.Show("Antenas: " + antSet + "Set to " + modeSt + ", " + comboBox40.Text.ToString() +" [s] filtering");
            gThreadActive = prevThreadStatus;
        }

        private bool SetSingleMultiTagModeAndFiltering(byte antAdrss, SerialPort port)
        {
            byte workmodeGet = 0xff, readModeGet = 0xff, filterTimeGet = 0xff;
            if (ZkReader.GetWorkMode(antAdrss, ref workmodeGet, ref readModeGet, ref filterTimeGet, port))
            {
                //pasar a modo activo para que tenga efecto el cambio de modo
                byte readModeSet = 5; // single tag mode
                byte filterTimeSet = Convert.ToByte(comboBox40.Text);
                if (filterTimeSet == 0)
                {
                    readModeSet = 4; // multitag!
                    filterTimeSet = filterTimeGet; // no cambiar filter time en miltitag mode
                }

                if (ZkReader.SetWorkMode(antAdrss, (byte)1, readModeSet, filterTimeSet, port)) // set "active" Working Mode (1), (2), single tag (5), (0), (1), 1 seg filtering (1).
                {
                    // volver al modo en que estaba
                    if (ZkReader.SetWorkMode(antAdrss, workmodeGet, readModeSet, filterTimeSet, port)) // set "active" Working Mode (1), (2), single tag (5), (0), (1), 1 seg filtering (1).
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        private void button123_Click(object sender, EventArgs e)
        {
            gThreadActive = false;
            desconectarAntena(0);
            desconectarAntena(1);
            desconectarAntena(2);
            desconectarAntena(3);
            CheckThreads();
        }

        private void button124_Click(object sender, EventArgs e)
        {
            for (int idx = 0; idx < 4; idx++)
            {
                ClickBotonStart(idx);
            }
        }

        private void groupBox25_Enter(object sender, EventArgs e)
        {

        }

        private void trackBar5_Scroll(object sender, EventArgs e)
        {
            trackBar1.Value = trackBar5.Value;
            trackBar2.Value = trackBar5.Value;
            trackBar3.Value = trackBar5.Value;
            trackBar4.Value = trackBar5.Value;

            trackBar1_Scroll(null, null);
            trackBar2_Scroll(null, null);
            trackBar3_Scroll(null, null);
            trackBar4_Scroll(null, null);
        }

        private void tabPage5_Click(object sender, EventArgs e)
        {

        }

        private void label62_Click(object sender, EventArgs e)
        {

        }

        private void label121_Click(object sender, EventArgs e)
        {

        }

        private void label98_Click(object sender, EventArgs e)
        {

        }

        private void label31_Click(object sender, EventArgs e)
        {

        }

        private void label93_Click(object sender, EventArgs e)
        {

        }

        private void checkBox3_CheckStateChanged(object sender, EventArgs e)
        {
            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings["largadaInd"].Value = checkBox3.Checked.ToString();
            configuration.Save(ConfigurationSaveMode.Full, true);
            ConfigurationManager.RefreshSection("appSettings");
        }

        private void label27_Click(object sender, EventArgs e)
        {

        }


        static byte[] EncryptStringToBytes(string plainText, byte[] key, byte[] iv)
        {
            byte[] encrypted;

            // Create an Aes object with the specified key and IV.
            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                // Create a new MemoryStream object to contain the encrypted bytes.
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    // Create a CryptoStream object to perform the encryption.
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        // Encrypt the plaintext.
                        using (StreamWriter streamWriter = new StreamWriter(cryptoStream))
                        {
                            streamWriter.Write(plainText);
                        }

                        encrypted = memoryStream.ToArray();
                    }
                }
            }

            return encrypted;
        }

        static string DecryptStringFromBytes(byte[] cipherText, byte[] key, byte[] iv)
        {
            string decrypted;

            // Create an Aes object with the specified key and IV.
            using (Aes aes = Aes.Create())
            {
                aes.Key = key;
                aes.IV = iv;

                // Create a new MemoryStream object to contain the decrypted bytes.
                using (MemoryStream memoryStream = new MemoryStream(cipherText))
                {
                    // Create a CryptoStream object to perform the decryption.
                    using (CryptoStream cryptoStream = new CryptoStream(memoryStream, aes.CreateDecryptor(), CryptoStreamMode.Read))
                    {
                        // Decrypt the ciphertext.
                        using (StreamReader streamReader = new StreamReader(cryptoStream))
                        {
                            decrypted = streamReader.ReadToEnd();
                        }
                    }
                }
            }

            return decrypted;
        }

        private void textBox40_KeyDown(object sender, KeyEventArgs e)
        {

        }

        private void dateTimePicker7_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                DateTime newDateTime = dateTimePicker7.Value;
                string original = newDateTime.ToString("MM-dd-yyyy HH:mm:ss");
                byte[] encrypted;
                string decrypted;
                byte[] key = Convert.FromBase64String("iDT0QIhKMuOvuk/EasQhwJnPjlQIinsCNfK6UNH/bD0=");
                byte[] iv = Convert.FromBase64String("jfn4gSwOTUeJtnMcSlUQPQ==");

                using (Aes aes = Aes.Create())
                {
                    // set key and IV
                    aes.Key = key;
                    aes.IV = iv;

                    // Encrypt the string
                    encrypted = EncryptStringToBytes(original, aes.Key, aes.IV);
                    Console.WriteLine("key: {0}", Convert.ToBase64String(aes.Key));
                    Console.WriteLine("IV: {0}", Convert.ToBase64String(aes.IV));
                    Console.WriteLine("Encrypted: {0}", Convert.ToBase64String(encrypted));
                    textBox40.Text = Convert.ToBase64String(encrypted);
                    // Decrypt the bytes
                    decrypted = DecryptStringFromBytes(encrypted, aes.Key, aes.IV);
                    Console.WriteLine("Decrypted: " + decrypted);
                }
            }
        }

        private void checkLicencia()
        {
            bool licenciaOk = false;
            string keyStr = "";

            byte[] key = Convert.FromBase64String("iDT0QIhKMuOvuk/EasQhwJnPjlQIinsCNfK6UNH/bD0=");
            byte[] iv = Convert.FromBase64String("jfn4gSwOTUeJtnMcSlUQPQ==");

            //chequear licencia de las AppSetting
            string licencia = ConfigurationManager.AppSettings["key"];

            if (licencia != "") //licencia cargada
            {
                string decripted = DecryptStringFromBytes(Convert.FromBase64String(licencia), key, iv);
                DateTime deathTime = DateTime.ParseExact(decripted, "MM-dd-yyyy HH:mm:ss", new CultureInfo("en-EN"));
                if (DateTime.Now < deathTime) //valida
                {
                    licenciaOk = true;
                }
                else // licencia vencida. borrar licencia actual y cargarla como licencia vieja
                {
                    
                    Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    configuration.AppSettings.Settings["key"].Value = "";
                    configuration.AppSettings.Settings["key2"].Value = licencia; // key ya usada
                    configuration.Save(ConfigurationSaveMode.Full, true);
                    ConfigurationManager.RefreshSection("appSettings");
                }
            }
            else // cargar licencia
            {
                DialogResult result = openFileDialog1.ShowDialog(); // cargar archivo txt con la licencia

                if (result == DialogResult.OK)
                {
                    // leer clave
                    keyStr = File.ReadAllText(openFileDialog1.FileName); //leer la clave de un txt y validar
                    string licenciaVieja = ConfigurationManager.AppSettings["key2"];

                    if (keyStr != licenciaVieja)
                    {
                        string decripted = DecryptStringFromBytes(Convert.FromBase64String(keyStr), key, iv);

                        DateTime deathTime = DateTime.ParseExact(decripted, "MM-dd-yyyy HH:mm:ss", new CultureInfo("en-EN"));
                        if (DateTime.Now < deathTime)
                        {
                            // guardar licencia
                            Configuration configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                            configuration.AppSettings.Settings["key"].Value = keyStr;
                            configuration.Save(ConfigurationSaveMode.Full, true);
                            ConfigurationManager.RefreshSection("appSettings");

                            licenciaOk = true;
                        }
                    }
                }
            }

            if (licenciaOk)
            {
                // configuraciones
                tabControl1.TabPages.Remove(Check_Datos);
                //button112.Enabled = false;
                //button117.Enabled = false;
                //checkBox2.Enabled = false;
                button35.Enabled = false;
                //textBox8.Enabled = false;
                //button17.Enabled = false;
                checkBox3.Checked = false;
                checkBox3.Enabled = false;
                cbLargada.Enabled = false;
                groupBox27.Enabled = false;
                groupBox27.Visible = false;
                textBox23.Enabled = false;
                textBox23.Visible = false;
                cbLargada.Enabled = false;
                checkBox3.Enabled = false;
            }
            else
            {
                File.Delete(openFileDialog1.FileName); // borrar el archivo key
                // cerrar todo
                MessageBox.Show("LICENCIA INVALIDA");
                this.Close();
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void dateTimePicker3_ValueChanged(object sender, EventArgs e)
        {

        }

        private void checkBox2_CheckedChanged(object sender, EventArgs e)
        {
            if(checkBox2.Checked)
            {
                cbLargada.Checked = false;
                checkBox12.Checked = false;
            }
        }

        private void cbLargada_CheckedChanged(object sender, EventArgs e)
        {
            if (cbLargada.Checked)
            {
                checkBox2.Checked = false;
                checkBox12.Checked = false;
            }
        }

        private void checkBox12_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox12.Checked)
            {
                checkBox2.Checked = false;
                cbLargada.Checked = false;
            }
        }

        private void groupBox28_Enter(object sender, EventArgs e)
        {
 
        }

        private void button127_Click(object sender, EventArgs e)
        {
            textBox48.Text = "";

            try
            {
                UsbReader.ConnectAntenna();
                string usbread = UsbReader.AnswerModeData();
                if (usbread != "")
                {
                    textBox48.Text = Convert.ToInt64(usbread).ToString();
                }
                else
                {
                    textBox48.Text = "";
                }
                UsbReader.StopAntenna();
                //textBox8.Text = Convert.ToInt16(ZkReader.AnswerModeData(gAntAdrr[0], gPortsConnected[0])).ToString();
            }
            catch { }
        }

        private void textBox48_TextChanged(object sender, EventArgs e)
        {

        }

        private void label135_Click(object sender, EventArgs e)
        {

        }

        private void label130_Click(object sender, EventArgs e)
        {

        }

        private void textBox42_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox44_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox44.Text != "")
                {
                    if(!poblarDatosCompa(null, textBox44.Text, true))
                    {
                        poblarDatosCompa(null, textBox44.Text, false);
                    }
                    //buscarCategoriasParaComboBox5();
                }
            }
        }

        private void dateTimePicker9_ValueChanged(object sender, EventArgs e)
        {

        }

        private void textBox50_TextChanged(object sender, EventArgs e)
        {
            if (textBox50.Text != "")
            {
                //mostrarTablaInscriptos();

                // vaciar items de comboBox5 y re poblar
            }
        }

        private void dateTimePicker9_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                string[] catArray = new string[20];
                calcularCategoria(buscarIdEventoPorNombre(textBox50.Text), dateTimePicker9.Value, comboBox52.Text, ref catArray);
                comboBox50.Text = catArray[0];
                //poblarComboBoxColumna("categorias", "categoria", comboBox50, textBox50.Text);
            }
        }

        private void textBox46_TextChanged(object sender, EventArgs e)
        {
            //autogenerar team ID
            if (checkBox1.CheckState == CheckState.Checked)
            {
                string texto = "";

                foreach (char digito in textBox46.Text)
                {
                    if (digito >= '0' && digito <= '9')
                    {
                        texto += digito;
                    }
                }

                textBox49.Text = texto;
            }
        }

        private void button85_Click_1(object sender, EventArgs e)
        {
            if(button85.Text == "\\/")
            {
                toggleView(true);
                button85.Text = "/\\";
            }
            else
            {
                toggleView(false);
                button85.Text = "\\/";
                //ademas borrar los campos
                textBox47.Text = "";
                textBox44.Text = "";
                comboBox52.Text = "";
                textBox45.Text = "";
                textBox43.Text = "";
                dateTimePicker9.Value = DateTime.Now;
                textBox42.Text = "";
                textBox46.Text = "";
                textBox48.Text = "";
                textBox50.Text = "";
                textBox49.Text = "";
                comboBox50.Text = "";
            }
        }

        private void textBox51_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                if (textBox51.Text != "")
                {
                    string eventId = "";
                    string nombre = "";
                    int etapas = 0;
                    string tiempo = "";
                    string[] vueltas = new string[10];
                    string categoria = "";
                    string posCat = "";
                    string posSexo = "";
                    string posGral = "";
                    string sexo = "";
                    int runnersCount = new int();
                    int gralCount = new int();

                    SqlConnection connectionDbMain = new SqlConnection(gConnectionString);
                    SqlCommand cmd = connectionDbMain.CreateCommand();
                    cmd.CommandType = CommandType.Text;
                    SqlDataReader reader;

                    cmd.CommandText = "SELECT * FROM [campeonatos_data] WHERE numero = '" + textBox51.Text + "'";

                    connectionDbMain.Open();
                    reader = cmd.ExecuteReader();
                    if (reader.Read())
                    {
                        eventId = reader["id"].ToString();
                        categoria = reader["categoria"].ToString();
                        nombre = reader["nombre"].ToString();
                        etapas = Convert.ToInt16(reader["total"].ToString());
                        tiempo = reader["final"].ToString();
                        sexo = reader["sexo"].ToString();
                        posCat = reader["cat"].ToString();
                        posGral = reader["gral"].ToString();
                        posSexo = reader["gral_sexo"].ToString();
                        vueltas[0] = reader["f1"].ToString();
                        vueltas[1] = reader["f2"].ToString();
                        vueltas[2] = reader["f3"].ToString();
                        vueltas[3] = reader["f4"].ToString();
                        vueltas[4] = reader["f5"].ToString();
                        vueltas[5] = reader["f6"].ToString();
                        vueltas[6] = reader["f7"].ToString();
                        vueltas[7] = reader["f8"].ToString();
                        vueltas[8] = reader["f9"].ToString();
                        vueltas[9] = reader["f10"].ToString();
                    }

                    connectionDbMain.Close();

                    // cuenta de corredores categoria
                    cmd.CommandText = "SELECT COUNT(categoria) FROM campeonatos_data WHERE categoria = '" + categoria + "' AND id = '" + eventId + "'";
                    connectionDbMain.Open();
                    runnersCount = (int)cmd.ExecuteScalar();
                    connectionDbMain.Close();

                    // cuenta de corredores sexo
                    cmd.CommandText = "SELECT COUNT(numero) FROM campeonatos_data WHERE sexo = '" + sexo + "' AND id = '" + eventId + "'";
                    connectionDbMain.Open();
                    gralCount = (int)cmd.ExecuteScalar();
                    connectionDbMain.Close();

                    //nombre campeonato/etapas
                    string evento = "";
                    cmd.CommandText = "SELECT nombre FROM campeonatos_info WHERE id = '"+eventId+"'";
                    connectionDbMain.Open();
                    SqlDataReader readerCampeonato = cmd.ExecuteReader();
                    if(readerCampeonato.Read())
                    {
                        evento = readerCampeonato["nombre"].ToString();
                    }
                    connectionDbMain.Close();

                    if (evento != "")
                    {
                        // Create a file to write to.
                        // also create de directory just in case some mather fucker
                        // deleted it while the program is running
                        System.IO.Directory.CreateDirectory(supportFilesPath);
                        using (StreamWriter sw = File.CreateText(supportFilesPath + "\\TxtTest.txt"))
                        {
                            try
                            {
                                sw.WriteLine(evento);
                                sw.WriteLine("");

                                sw.WriteLine("Placa Nº: " + textBox51.Text);
                                sw.WriteLine(nombre.ToUpper());
                                sw.WriteLine("");

                                sw.WriteLine("Categoria: ");
                                sw.WriteLine(categoria.ToUpper());
                                sw.WriteLine("");

                                if (etapas > 1)
                                {
                                    for (int i = 0; i < etapas; i++)
                                    {
                                        string vta = vueltas[i].ToString().Substring(0, 2) + "h" + vueltas[i].ToString().Substring(3, 2) + "'" + vueltas[i].ToString().Substring(6, 2) + "\"";
                                        sw.WriteLine("Etapa " + (i + 1).ToString() + ": " + vta);
                                    }
                                }
                                sw.WriteLine("Tiempo: " + tiempo);
                                //sw.WriteLine(tiempo);
                                sw.WriteLine("");
                                sw.WriteLine("Posición Categoría: " + posCat + "º");
                                sw.WriteLine("Cantidad de Participantes: " + runnersCount.ToString());
                                sw.WriteLine("");

                                if (checkBox4.CheckState == CheckState.Checked)
                                {
                                    sw.WriteLine("Posición general: " + posSexo + "º");
                                    sw.WriteLine("Cantidad de Participantes: " + gralCount.ToString());
                                    sw.WriteLine("");
                                }

                                sw.WriteLine("**Resultados provisorios**");
                                sw.WriteLine("**sujeto a modificaciones**");
                                sw.WriteLine("-------------------------------");
                                sw.WriteLine("cronobottiming.com/resultados");
                                sw.Close();
                            }
                            catch (Exception expe)
                            {
                                MessageBox.Show(expe.Message);
                            }
                        }

                        try
                        {
                            PrintDocument pd = new PrintDocument();
                            pd.DefaultPageSettings.Margins = new Margins(10, 0, 0, 0);
                            pd.DefaultPageSettings.Landscape = false;
                            pd.PrintPage += new PrintPageEventHandler(pd_PrintPage);
                            // Print the document.
                            pd.Print();
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }
                }

                textBox51.Text = "";
            }
        }
    }
}