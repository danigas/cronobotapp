﻿namespace CronoBotTiming
{
    partial class resultViewer
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.comboBoxEventos = new System.Windows.Forms.ComboBox();
            this.comboBoxCategorias = new System.Windows.Forms.ComboBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBoxBuscarPorNumero = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(12, 61);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(995, 400);
            this.dataGridView1.TabIndex = 0;
            // 
            // comboBoxEventos
            // 
            this.comboBoxEventos.FormattingEnabled = true;
            this.comboBoxEventos.Location = new System.Drawing.Point(12, 34);
            this.comboBoxEventos.Name = "comboBoxEventos";
            this.comboBoxEventos.Size = new System.Drawing.Size(162, 21);
            this.comboBoxEventos.TabIndex = 1;
            this.comboBoxEventos.SelectedIndexChanged += new System.EventHandler(this.comboBoxEventos_SelectedIndexChanged);
            // 
            // comboBoxCategorias
            // 
            this.comboBoxCategorias.FormattingEnabled = true;
            this.comboBoxCategorias.Location = new System.Drawing.Point(190, 34);
            this.comboBoxCategorias.Name = "comboBoxCategorias";
            this.comboBoxCategorias.Size = new System.Drawing.Size(150, 21);
            this.comboBoxCategorias.TabIndex = 2;
            this.comboBoxCategorias.SelectedIndexChanged += new System.EventHandler(this.comboBoxCategorias_SelectedIndexChanged);
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(533, 31);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 3;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBoxBuscarPorNumero
            // 
            this.textBoxBuscarPorNumero.Location = new System.Drawing.Point(366, 35);
            this.textBoxBuscarPorNumero.Name = "textBoxBuscarPorNumero";
            this.textBoxBuscarPorNumero.Size = new System.Drawing.Size(100, 20);
            this.textBoxBuscarPorNumero.TabIndex = 4;
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(628, 31);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 5;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // resultViewer
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1019, 464);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBoxBuscarPorNumero);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.comboBoxCategorias);
            this.Controls.Add(this.comboBoxEventos);
            this.Controls.Add(this.dataGridView1);
            this.Name = "resultViewer";
            this.Text = "Resultados";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.ComboBox comboBoxEventos;
        private System.Windows.Forms.ComboBox comboBoxCategorias;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBoxBuscarPorNumero;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Timer timer1;
    }
}