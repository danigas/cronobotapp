﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace CronoBotTiming
{
    public partial class resultViewer : Form
    {
        SqlConnection gConnectionDbMain = new SqlConnection(@"server = DANIELOPC\SQLEXPRESS; Database=db_red;integrated security=False;User ID=sa;Password=daniel");

        public resultViewer()
        {
            InitializeComponent();
            poblarComboBoxColumna("eventos", "nombre", comboBoxEventos, null);
        }

        private void button1_Click(object sender, EventArgs e)
        {

        }

        private void comboBoxEventos_SelectedIndexChanged(object sender, EventArgs e)
        {
            comboBoxCategorias.Items.Clear();
            comboBoxCategorias.Items.Add("General");
            comboBoxCategorias.Items.Add("Masculino");
            comboBoxCategorias.Items.Add("Femenino");

            poblarComboBoxColumna("categorias", "categoria", comboBoxCategorias, comboBoxEventos.Text);
        }

        private void poblarComboBoxColumna(string tabla, string columna, ComboBox comboBox, string eventName)
        {
            dbConnectionSet(true);

            // poblar comboBox con nombres extraidos de "columna" de la tabla "tabla" 
            SqlCommand cmd = gConnectionDbMain.CreateCommand();
            cmd.CommandType = CommandType.Text;

            if (eventName == null)
            {
                cmd.CommandText = "SELECT " + columna + " FROM " + tabla + " ";
            }
            else
            {
                string eventId = buscarIdEventoPorNombre(eventName);
                cmd.CommandText = " SELECT " + columna + " FROM " + tabla + " WHERE id = '" + eventId + "' ";
                comboBox.Items.Add("");
                dbConnectionSet(true);
            }

            SqlDataReader columnaItems = cmd.ExecuteReader();


            while (columnaItems.Read())
            {
                comboBox.Items.Add(columnaItems[columna].ToString());
            }

            dbConnectionSet(false);

            dbConnectionSet(true);

            if (comboBox.Items.Count > 0)
            {
                // prentar en comboBox el primer valor hallado en la columna
                comboBox.Text = comboBox.Items[0].ToString();
            }

            dbConnectionSet(false);
        }

        private void dbConnectionSet(bool dbOpenClose)
        {
            // dbOpenClose =  true => open connection
            // dbOpenClose =  false => close connection
            if (dbOpenClose)
            {
                // open connection
                if (gConnectionDbMain.State == ConnectionState.Closed)
                {
                    gConnectionDbMain.Open();
                }
            }
            else
            {
                // close connection
                if (gConnectionDbMain.State == ConnectionState.Open)
                {
                    gConnectionDbMain.Close();
                }
            }
        }

        private string buscarIdEventoPorNombre(string nombreEvento)
        {
            if (gConnectionDbMain.State == ConnectionState.Open)
            {
                dbConnectionSet(false);
            }

            dbConnectionSet(true);

            // Buscar id evento cuyo nombre es "nombreEvento"
            SqlCommand cmd = gConnectionDbMain.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "SELECT id FROM eventos WHERE nombre = '" + nombreEvento + "'";
            SqlDataReader eventId = cmd.ExecuteReader();
            eventId.Read();
            string eventIdString = "";

            try
            {
                eventIdString = eventId["id"].ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            dbConnectionSet(false);

            return eventIdString;
        }

        private void comboBoxCategorias_SelectedIndexChanged(object sender, EventArgs e)
        {
            // Filtrar la clasificacion por evento y por categoria

            // Buscar nombre evento cuyo nombre es eventNameString
            string eventIdString = buscarIdEventoPorNombre(comboBoxEventos.Text);

            // Mostrar la sub tabla "finishers" para el evento cuyo nombre es "comboBox7.Text"
            // y categoria "comboBox8.Text"
            SqlCommand cmd = gConnectionDbMain.CreateCommand();
            cmd.CommandType = CommandType.Text;

            switch (comboBoxCategorias.Text)
            {
                case "General":
                    cmd.CommandText = "SELECT nombre,numero,categoria,sexo,tiempo,estado FROM [resultados] WHERE id = '" + eventIdString + "' ORDER BY estado DESC, lap DESC, laps DESC, tiempo ASC";
                    break;
                case "Masculino":
                    cmd.CommandText = "SELECT nombre,numero,categoria,sexo,tiempo,estado FROM [resultados] WHERE id = '" + eventIdString + "' AND sexo = 'masculino' ORDER BY estado DESC, lap DESC, laps DESC, tiempo ASC";
                    break;
                case "Femenino":
                    cmd.CommandText = "SELECT nombre,numero,categoria,tiempo,estado FROM [resultados] WHERE id = '" + eventIdString + "' AND sexo = 'femenino' ORDER BY estado DESC, lap DESC, laps DESC, tiempo ASC";
                    break;
                default:
                    cmd.CommandText = "SELECT nombre,numero,categoria,sexo,tiempo,estado FROM [resultados] WHERE id = '" + eventIdString + "' AND categoria = '" + comboBoxCategorias.Text + "' ORDER BY estado DESC, lap DESC, tiempo ASC";
                    break;
            }

            dbConnectionSet(true);

            try
            {
                cmd.ExecuteNonQuery();
                DataTable dta = new DataTable();
                SqlDataAdapter dataadapt = new SqlDataAdapter(cmd);
                dataadapt.Fill(dta);
                dataGridView1.DataSource = dta;

                // mostrar numero de fila
                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    row.HeaderCell.Value = (row.Index + 1).ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            dbConnectionSet(false);
        }
    }
}
