﻿using System;
using System.Diagnostics;
using System.IO.Ports;
using System.Text;

namespace Readers_UHF
{
    class UsbReader
    {
        Tools tools = new Tools();
        //POR AHORA SOLO SOPRTA 1 READER USB
        public bool ConnectAntenna()
        {
            bool retval = false;
            byte[] arrBuffer = new byte[512];
            if (RFID.SWHidApi.SWHid_GetUsbCount() > 0)
            {
                UInt16 INDEX = 0;
                if (RFID.SWHidApi.SWHid_OpenDevice(INDEX))
                {
                    if (RFID.SWHidApi.SWHid_GetDeviceSystemInfo(0xFF, arrBuffer) == true)
                    {
                        retval = true;
                    }
                }
            }

            return retval;
        }


        public void StopAntenna()
        {
            RFID.SWHidApi.SWHid_CloseDevice();
        }

        public string AnswerModeData()
        {
            string retval = "";
            int retries = 0;
            string justread = "";
            string lastread = "";
            while (retries < 4)
            {       
                //leer por usb:
                byte[] arrBuffer = new byte[64000];
                ushort tags_nb = 0;
                ushort totalLen = 0;
                justread = "";

                if (RFID.SWHidApi.SWHid_InventoryG2(0xFF, arrBuffer, out totalLen, out tags_nb) && tags_nb == 1)
                {  
                    for (int i = 3; i <= 14; i++)
                    {
                        justread += arrBuffer[i].ToString("X2");
                    }
                    
                    if(justread == lastread && justread != "")
                    {
                        retval = justread;
                        break;
                    }

                    RFID.SWHidApi.SWHid_ClearTagBuf();
                    lastread = justread;
                }
                tools.DelayBlock(1000);
                retries++;
            }

            RFID.SWHidApi.SWHid_ClearTagBuf();
            return retval;
        }

        public bool WriteCard_G2(string EPC_new)
        {
            byte[] epcNew = HexStringToByteArray(EPC_new);
            byte[] Password = new byte[4];
            return RFID.SWHidApi.SWHid_WriteEPCG2(0x01, Password, epcNew, 0x06);
        }

        private byte[] HexStringToByteArray(string s)
        {
            s = s.Replace(" ", "");
            byte[] buffer = new byte[s.Length / 2];
            for (int i = 0; i < s.Length; i += 2)
                buffer[i / 2] = (byte)Convert.ToByte(s.Substring(i, 2), 16);
            return buffer;
        }
    }
}
