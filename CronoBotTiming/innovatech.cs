﻿using System;
using System.Diagnostics;
using System.IO.Ports;

namespace Readers_UHF
{
    public class Innovatech
    {
        Tools tools = new Tools();
        /**************************************    OPEN COM PORT     ***********************************/
        public bool ConnectAntenna(SerialPort port, byte ComAddr)
        {
            return GetAntAddress(port, ComAddr);
        }

        /**************************************            ***********************************/
        // GET READER INFORMATION
        public bool GetReaderInformation(byte devAddr, ref int powerdBm, ref int scanTime, SerialPort port)
        {
            bool getInfo = false;
            byte[] command = {0x55, 0x01, 0x00, 0x01, 0x12, 0x12, 0x6c};
            ushort crc = tools.crc16_BUYPASS(command, 1, 4);
            command[6] = (byte)(crc & 0x00ff);
            command[5] = (byte)(crc >> 8);
            try
            {
                port.ReadExisting();

                port.Write(command, 0, command.Length);
                tools.WaitByte(port);

                byte[] bytesRead = new byte[300];
                int justRead = port.Read(bytesRead, 0, 20);
                if (justRead == 20 && tools.Check_CRC16_BUYPASS(bytesRead, 1, 19))
                {
                    // decode!
                    powerdBm = ((int)bytesRead[10]) * 100 / 30;
                    scanTime = (int)bytesRead[14];
                    getInfo = true;
                }
            }
            catch { }
            return getInfo;
        }

        /*************************************    SET POWER     **************************/
        public bool SetPower(byte devAdrr, int power, SerialPort port)
        {
            bool powerSet = false;
            byte enable = power == (int)0 ? (byte)0x00 : (byte)0x01;
            byte[] command = { 0x55, 0x03, 0x00, 0x0d, 0x11, enable, 0x00, 0x00, 0x00, (byte)(power * 30 / 100), 0x10, 0x10, 0x10, 0x01, 0x01, 0x01, 0x01, 0x67, 0x41 };
            ushort crc = tools.crc16_BUYPASS(command, 1, 16);
            command[18] = (byte)(crc & 0x00ff);
            command[17] = (byte)(crc >> 8);
            try
            {
                port.ReadExisting();

                port.Write(command, 0, command.Length);
                tools.WaitByte(port);

                byte[] bytesRead = new byte[300];
                int justRead = port.Read(bytesRead, 0, 8);
                if (justRead == 8 && tools.Check_CRC16_BUYPASS(bytesRead, 1, 7) && bytesRead[5] == 0x00)
                {
                    powerSet = true;
                }
            }
            catch { }
            return powerSet;
        }

        /*********************** GET WORK MODE PARAMS *******************/
        public bool GetWorkMode(byte devAddr, ref byte workMode, ref byte readMode, ref byte filterTime, SerialPort port)
        {
            bool getParams = false;
            return getParams;
        }

        /*********************** SET WORK MODE PARAMS *******************/
        public bool SetWorkMode(byte devAddr, byte workmode, byte readMode, byte filterTime, SerialPort port)
        {
            bool setParams = false;
            return setParams;
        }

        /*********************** READ SCAN MODE DATA *******************/
        public int ReadActiveModeData(ref string[] EPCs, ref string[] devAddr, SerialPort port)
        {
            byte[] readBuff = new byte[2100];
            int readBuffIdx = 0;
            int validDataIdx = 0;

            int nbOfEPCs = new int(); //hace de indice

            while (port.IsOpen && port.BytesToRead > 0)
            {
                //byte[] scanModeData = new byte[1800];
                int nbOfBytesRead = port.Read(readBuff, readBuffIdx, port.BytesToRead);
                tools.DelayBlock(20);// delay/bloqueo del thread sin salir del mismo

                if (nbOfBytesRead > 0)
                {
                    readBuffIdx += nbOfBytesRead;
                    Debug.WriteLine("pack read: " + nbOfBytesRead.ToString());

                    // puede ser 21 (o mas segun los eschape characters)
                    // 0x55 = 0x56 0x56; 
                    // 0x56 = 0x56 0x57;
                    while (readBuffIdx - validDataIdx >= 21)
                    {
                        //CHEQUEAR ESCAPE CHARACTERS
                        byte[] fullMsg = new byte[readBuffIdx - validDataIdx];
                        Array.ConstrainedCopy(readBuff, validDataIdx, fullMsg, 0, fullMsg.Length);
                        int escapedChars = 0;
                        byte[] finalMsg = tools.EscapeChars(fullMsg, ref escapedChars);
                        // TODO CRC 16 BUYPASS
                        if (finalMsg.Length == 21 && tools.Check_CRC16_BUYPASS(finalMsg, 1, finalMsg.Length - 1))
                        {
                            // EPC son 12 bytes 
                            byte[] epc = new byte[12];
                            Array.ConstrainedCopy(finalMsg, 7, epc, 0, epc.Length);
                            Array.Reverse(epc);
                            devAddr[nbOfEPCs] = finalMsg[2].ToString(); // 
                            EPCs[nbOfEPCs++] = BitConverter.ToUInt64(epc, 0).ToString("X");

                            // cumple CRC! incremento el indice de comienzo de data valida
                            // todo regio!
                            validDataIdx += 21 + escapedChars;
                        }
                        else
                        {
                            //si no comple CRC, moverme un byte mas adelante, esperar y recomenzar
                            validDataIdx++;
                            tools.DelayBlock(20);
                            Debug.WriteLine("resync and wait");
                        }
                    }
                    if (readBuffIdx - validDataIdx < 21 && readBuffIdx - validDataIdx != 0)
                    {
                        tools.DelayBlock(20);
                        Debug.WriteLine("wait for more bytes to complete");
                    }
                }
            }

            return nbOfEPCs;
        }


        /*********************** READ ANSWER MODE DATA *******************/
        public string AnswerModeData(byte devAddr, SerialPort port)
        {
            string retVal = "";
            return retVal;
        }

        /*********************** WRITE CARD *******************/
        public bool WriteCard_G2(byte devAddr, string EPC_original, string EPC_new, SerialPort port)
        {
            bool writeOk = false;
            return writeOk;
        }

        public bool StopAntenna(SerialPort serialPortx, byte antAdrr)
        {
            bool stopAntena = false;
            byte[] command = {0x55, 0x0b, antAdrr, 0x03, 0x20, 0x00, 0x01, 0xf6, 0x8f};
            ushort crc = tools.crc16_BUYPASS(command, 1, 6);
            command[8] = (byte)(crc & 0x00ff);
            command[7] = (byte)(crc >> 8);
            try
            {
                serialPortx.ReadExisting();

                serialPortx.Write(command, 0, command.Length);
                tools.WaitByte(serialPortx);

                byte[] bytesRead = new byte[300];
                int justRead = serialPortx.Read(bytesRead, 0, 8);
                if (justRead == 8 && tools.Check_CRC16_BUYPASS(bytesRead, 1, 7))
                {
                    stopAntena = true;
                    Debug.Print("stop Antenna INNOVATECH");
                }
                else
                {
                    Debug.Print("Innovatech: no se pudo detener la antena");
                }
            }
            catch { }
            return stopAntena;
        }

        public bool StartAntenna(SerialPort serialPortx, byte antAdrr)
        {
            bool startReadingOk = false;
            byte[] command = { 0x55, 0x0c, antAdrr, 0x04, 0x91, 0x00, 0x01, 0x00, 0x5f, 0x1a};
            ushort crc = tools.crc16_BUYPASS(command, 1, 7);
            command[9] = (byte)(crc & 0x00ff);
            command[8] = (byte)(crc >> 8);
            try
            {
                serialPortx.ReadExisting();

                serialPortx.Write(command, 0, command.Length);
                tools.WaitByte(serialPortx);

                byte[] bytesRead = new byte[300];
                int justRead = serialPortx.Read(bytesRead, 0, 9);
                if (justRead == 9 && tools.Check_CRC16_BUYPASS(bytesRead, 1, 8))
                {
                    startReadingOk = true;
                }
                else
                {
                    Debug.Print("Innovatech: no se pudo encender la antena");
                }
            }
            catch { }
            return startReadingOk;
        }

        private bool GetAntAddress(SerialPort serialPortx, byte antAdrr)
        {
            bool queryAddOk = false;

            byte[] command = {0x55, 0x09, 0x00, 0x02, 0x16, 0x00, 0x77, 0x90};
            ushort crc = tools.crc16_BUYPASS(command, 1, 5);
            command[7] = (byte)(crc & 0x00ff);
            command[6] = (byte)(crc >> 8);
            try
            {
                serialPortx.ReadExisting();

                serialPortx.Write(command, 0, command.Length);
                tools.WaitByte(serialPortx);

                byte[] bytesRead = new byte[9];
                int justRead = serialPortx.Read(bytesRead, 0, bytesRead.Length);
                if (justRead == 9 && tools.Check_CRC16_BUYPASS(bytesRead, 1, 8))
                {
                    queryAddOk = (antAdrr == bytesRead[6]); 
                }
            }
            catch { }
            return queryAddOk;
        }
    }
}
