﻿namespace CronoBotTiming
{
    partial class main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(main));
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tab1_Listado_General = new System.Windows.Forms.TabPage();
            this.button41 = new System.Windows.Forms.Button();
            this.label42 = new System.Windows.Forms.Label();
            this.textBox15 = new System.Windows.Forms.TextBox();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimePicker1 = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dataGridView_Corredores = new System.Windows.Forms.DataGridView();
            this.comboBox3 = new System.Windows.Forms.ComboBox();
            this.label10 = new System.Windows.Forms.Label();
            this.button2 = new System.Windows.Forms.Button();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.Config = new System.Windows.Forms.TabPage();
            this.groupBox28 = new System.Windows.Forms.GroupBox();
            this.textBox50 = new System.Windows.Forms.TextBox();
            this.comboBox50 = new System.Windows.Forms.ComboBox();
            this.label137 = new System.Windows.Forms.Label();
            this.label136 = new System.Windows.Forms.Label();
            this.label134 = new System.Windows.Forms.Label();
            this.label132 = new System.Windows.Forms.Label();
            this.label129 = new System.Windows.Forms.Label();
            this.textBox44 = new System.Windows.Forms.TextBox();
            this.textBox49 = new System.Windows.Forms.TextBox();
            this.textBox42 = new System.Windows.Forms.TextBox();
            this.label130 = new System.Windows.Forms.Label();
            this.textBox43 = new System.Windows.Forms.TextBox();
            this.label131 = new System.Windows.Forms.Label();
            this.textBox45 = new System.Windows.Forms.TextBox();
            this.label133 = new System.Windows.Forms.Label();
            this.button127 = new System.Windows.Forms.Button();
            this.textBox46 = new System.Windows.Forms.TextBox();
            this.label135 = new System.Windows.Forms.Label();
            this.dateTimePicker9 = new System.Windows.Forms.DateTimePicker();
            this.comboBox52 = new System.Windows.Forms.ComboBox();
            this.textBox47 = new System.Windows.Forms.TextBox();
            this.label140 = new System.Windows.Forms.Label();
            this.textBox48 = new System.Windows.Forms.TextBox();
            this.dataGridView3 = new System.Windows.Forms.DataGridView();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label127 = new System.Windows.Forms.Label();
            this.textBox41 = new System.Windows.Forms.TextBox();
            this.dateTimePicker8 = new System.Windows.Forms.DateTimePicker();
            this.label126 = new System.Windows.Forms.Label();
            this.label99 = new System.Windows.Forms.Label();
            this.textBox39 = new System.Windows.Forms.TextBox();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.comboBox43 = new System.Windows.Forms.ComboBox();
            this.button99 = new System.Windows.Forms.Button();
            this.comboBox31 = new System.Windows.Forms.ComboBox();
            this.label81 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.button8 = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label82 = new System.Windows.Forms.Label();
            this.textBox19 = new System.Windows.Forms.TextBox();
            this.label50 = new System.Windows.Forms.Label();
            this.numericUpDown3 = new System.Windows.Forms.NumericUpDown();
            this.label45 = new System.Windows.Forms.Label();
            this.button9 = new System.Windows.Forms.Button();
            this.numericUpDown2 = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.button106 = new System.Windows.Forms.Button();
            this.button51 = new System.Windows.Forms.Button();
            this.textBox24 = new System.Windows.Forms.TextBox();
            this.button50 = new System.Windows.Forms.Button();
            this.comboBox24 = new System.Windows.Forms.ComboBox();
            this.comboBox25 = new System.Windows.Forms.ComboBox();
            this.button48 = new System.Windows.Forms.Button();
            this.button49 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.button85 = new System.Windows.Forms.Button();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.label93 = new System.Windows.Forms.Label();
            this.textBox36 = new System.Windows.Forms.TextBox();
            this.label119 = new System.Windows.Forms.Label();
            this.checkBox19 = new System.Windows.Forms.CheckBox();
            this.comboBox44 = new System.Windows.Forms.ComboBox();
            this.checkBox17 = new System.Windows.Forms.CheckBox();
            this.textBox32 = new System.Windows.Forms.TextBox();
            this.label78 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button82 = new System.Windows.Forms.Button();
            this.label75 = new System.Windows.Forms.Label();
            this.textBox28 = new System.Windows.Forms.TextBox();
            this.button40 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.textBox16 = new System.Windows.Forms.TextBox();
            this.label43 = new System.Windows.Forms.Label();
            this.button26 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.comboBox9 = new System.Windows.Forms.ComboBox();
            this.label25 = new System.Windows.Forms.Label();
            this.comboBox5 = new System.Windows.Forms.ComboBox();
            this.label23 = new System.Windows.Forms.Label();
            this.textBox13 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.comboBox4 = new System.Windows.Forms.ComboBox();
            this.label19 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.en_curso = new System.Windows.Forms.TabPage();
            this.label120 = new System.Windows.Forms.Label();
            this.textBox37 = new System.Windows.Forms.TextBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.groupBox22 = new System.Windows.Forms.GroupBox();
            this.label71 = new System.Windows.Forms.Label();
            this.label53 = new System.Windows.Forms.Label();
            this.label68 = new System.Windows.Forms.Label();
            this.label69 = new System.Windows.Forms.Label();
            this.label70 = new System.Windows.Forms.Label();
            this.label72 = new System.Windows.Forms.Label();
            this.label74 = new System.Windows.Forms.Label();
            this.label73 = new System.Windows.Forms.Label();
            this.dataGridView10 = new System.Windows.Forms.DataGridView();
            this.Categoria = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Lapeo = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.button110 = new System.Windows.Forms.Button();
            this.label12 = new System.Windows.Forms.Label();
            this.textBox30 = new System.Windows.Forms.TextBox();
            this.label79 = new System.Windows.Forms.Label();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.label31 = new System.Windows.Forms.Label();
            this.dateTimePicker5 = new System.Windows.Forms.DateTimePicker();
            this.button42 = new System.Windows.Forms.Button();
            this.checkedListBox1 = new System.Windows.Forms.CheckedListBox();
            this.button15 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.comboBox6 = new System.Windows.Forms.ComboBox();
            this.button16 = new System.Windows.Forms.Button();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.button117 = new System.Windows.Forms.Button();
            this.button59 = new System.Windows.Forms.Button();
            this.button112 = new System.Windows.Forms.Button();
            this.button78 = new System.Windows.Forms.Button();
            this.button47 = new System.Windows.Forms.Button();
            this.textBox23 = new System.Windows.Forms.TextBox();
            this.label51 = new System.Windows.Forms.Label();
            this.button45 = new System.Windows.Forms.Button();
            this.button43 = new System.Windows.Forms.Button();
            this.button31 = new System.Windows.Forms.Button();
            this.button33 = new System.Windows.Forms.Button();
            this.button32 = new System.Windows.Forms.Button();
            this.dateTimePicker4 = new System.Windows.Forms.DateTimePicker();
            this.button30 = new System.Windows.Forms.Button();
            this.textBox18 = new System.Windows.Forms.TextBox();
            this.button46 = new System.Windows.Forms.Button();
            this.comboBox23 = new System.Windows.Forms.ComboBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.cbLargada = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.comboBox32 = new System.Windows.Forms.ComboBox();
            this.label30 = new System.Windows.Forms.Label();
            this.comboBox8 = new System.Windows.Forms.ComboBox();
            this.label27 = new System.Windows.Forms.Label();
            this.comboBox7 = new System.Windows.Forms.ComboBox();
            this.label24 = new System.Windows.Forms.Label();
            this.textBox14 = new System.Windows.Forms.TextBox();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.resultados = new System.Windows.Forms.TabPage();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox18 = new System.Windows.Forms.CheckBox();
            this.label96 = new System.Windows.Forms.Label();
            this.textBox34 = new System.Windows.Forms.TextBox();
            this.label95 = new System.Windows.Forms.Label();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.label94 = new System.Windows.Forms.Label();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.label77 = new System.Windows.Forms.Label();
            this.textBox29 = new System.Windows.Forms.TextBox();
            this.button39 = new System.Windows.Forms.Button();
            this.button36 = new System.Windows.Forms.Button();
            this.groupBox15 = new System.Windows.Forms.GroupBox();
            this.label76 = new System.Windows.Forms.Label();
            this.comboBox29 = new System.Windows.Forms.ComboBox();
            this.comboBox30 = new System.Windows.Forms.ComboBox();
            this.dataGridView6 = new System.Windows.Forms.DataGridView();
            this.tools = new System.Windows.Forms.TabPage();
            this.groupBox27 = new System.Windows.Forms.GroupBox();
            this.dateTimePicker7 = new System.Windows.Forms.DateTimePicker();
            this.textBox40 = new System.Windows.Forms.TextBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.button121 = new System.Windows.Forms.Button();
            this.textBox38 = new System.Windows.Forms.TextBox();
            this.button120 = new System.Windows.Forms.Button();
            this.groupBox26 = new System.Windows.Forms.GroupBox();
            this.textBox35 = new System.Windows.Forms.TextBox();
            this.label114 = new System.Windows.Forms.Label();
            this.button75 = new System.Windows.Forms.Button();
            this.button67 = new System.Windows.Forms.Button();
            this.comboBox45 = new System.Windows.Forms.ComboBox();
            this.groupBox25 = new System.Windows.Forms.GroupBox();
            this.comboBox40 = new System.Windows.Forms.ComboBox();
            this.label97 = new System.Windows.Forms.Label();
            this.trackBar5 = new System.Windows.Forms.TrackBar();
            this.button124 = new System.Windows.Forms.Button();
            this.button123 = new System.Windows.Forms.Button();
            this.button122 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button118 = new System.Windows.Forms.Button();
            this.label89 = new System.Windows.Forms.Label();
            this.button25 = new System.Windows.Forms.Button();
            this.button100 = new System.Windows.Forms.Button();
            this.groupBox18 = new System.Windows.Forms.GroupBox();
            this.label80 = new System.Windows.Forms.Label();
            this.dateTimePicker6 = new System.Windows.Forms.DateTimePicker();
            this.textBox31 = new System.Windows.Forms.TextBox();
            this.groupBox17 = new System.Windows.Forms.GroupBox();
            this.label90 = new System.Windows.Forms.Label();
            this.textBox12 = new System.Windows.Forms.TextBox();
            this.button94 = new System.Windows.Forms.Button();
            this.button91 = new System.Windows.Forms.Button();
            this.button89 = new System.Windows.Forms.Button();
            this.button90 = new System.Windows.Forms.Button();
            this.button92 = new System.Windows.Forms.Button();
            this.button93 = new System.Windows.Forms.Button();
            this.button95 = new System.Windows.Forms.Button();
            this.button96 = new System.Windows.Forms.Button();
            this.button97 = new System.Windows.Forms.Button();
            this.button98 = new System.Windows.Forms.Button();
            this.comboBox33 = new System.Windows.Forms.ComboBox();
            this.groupBox16 = new System.Windows.Forms.GroupBox();
            this.label91 = new System.Windows.Forms.Label();
            this.textBox33 = new System.Windows.Forms.TextBox();
            this.button116 = new System.Windows.Forms.Button();
            this.button86 = new System.Windows.Forms.Button();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.button115 = new System.Windows.Forms.Button();
            this.button114 = new System.Windows.Forms.Button();
            this.button113 = new System.Windows.Forms.Button();
            this.button88 = new System.Windows.Forms.Button();
            this.button84 = new System.Windows.Forms.Button();
            this.button83 = new System.Windows.Forms.Button();
            this.button80 = new System.Windows.Forms.Button();
            this.button77 = new System.Windows.Forms.Button();
            this.button79 = new System.Windows.Forms.Button();
            this.button81 = new System.Windows.Forms.Button();
            this.comboBox20 = new System.Windows.Forms.ComboBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.groupBox14 = new System.Windows.Forms.GroupBox();
            this.trackBar4 = new System.Windows.Forms.TrackBar();
            this.label64 = new System.Windows.Forms.Label();
            this.button69 = new System.Windows.Forms.Button();
            this.button70 = new System.Windows.Forms.Button();
            this.button71 = new System.Windows.Forms.Button();
            this.button72 = new System.Windows.Forms.Button();
            this.button73 = new System.Windows.Forms.Button();
            this.button74 = new System.Windows.Forms.Button();
            this.textBox27 = new System.Windows.Forms.TextBox();
            this.label67 = new System.Windows.Forms.Label();
            this.comboBox28 = new System.Windows.Forms.ComboBox();
            this.button76 = new System.Windows.Forms.Button();
            this.label66 = new System.Windows.Forms.Label();
            this.label65 = new System.Windows.Forms.Label();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.trackBar3 = new System.Windows.Forms.TrackBar();
            this.label60 = new System.Windows.Forms.Label();
            this.button61 = new System.Windows.Forms.Button();
            this.button62 = new System.Windows.Forms.Button();
            this.button63 = new System.Windows.Forms.Button();
            this.button64 = new System.Windows.Forms.Button();
            this.button65 = new System.Windows.Forms.Button();
            this.button66 = new System.Windows.Forms.Button();
            this.textBox26 = new System.Windows.Forms.TextBox();
            this.label62 = new System.Windows.Forms.Label();
            this.label63 = new System.Windows.Forms.Label();
            this.comboBox27 = new System.Windows.Forms.ComboBox();
            this.button68 = new System.Windows.Forms.Button();
            this.label61 = new System.Windows.Forms.Label();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.trackBar2 = new System.Windows.Forms.TrackBar();
            this.label47 = new System.Windows.Forms.Label();
            this.button53 = new System.Windows.Forms.Button();
            this.button54 = new System.Windows.Forms.Button();
            this.button55 = new System.Windows.Forms.Button();
            this.button56 = new System.Windows.Forms.Button();
            this.button57 = new System.Windows.Forms.Button();
            this.button58 = new System.Windows.Forms.Button();
            this.textBox25 = new System.Windows.Forms.TextBox();
            this.label49 = new System.Windows.Forms.Label();
            this.label59 = new System.Windows.Forms.Label();
            this.comboBox26 = new System.Windows.Forms.ComboBox();
            this.button60 = new System.Windows.Forms.Button();
            this.label48 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label118 = new System.Windows.Forms.Label();
            this.comboBox49 = new System.Windows.Forms.ComboBox();
            this.label117 = new System.Windows.Forms.Label();
            this.comboBox48 = new System.Windows.Forms.ComboBox();
            this.label116 = new System.Windows.Forms.Label();
            this.comboBox47 = new System.Windows.Forms.ComboBox();
            this.comboBox46 = new System.Windows.Forms.ComboBox();
            this.label115 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label58 = new System.Windows.Forms.Label();
            this.label52 = new System.Windows.Forms.Label();
            this.comboBox35 = new System.Windows.Forms.ComboBox();
            this.label57 = new System.Windows.Forms.Label();
            this.comboBox22 = new System.Windows.Forms.ComboBox();
            this.comboBox21 = new System.Windows.Forms.ComboBox();
            this.cbImportEvent = new System.Windows.Forms.ComboBox();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.comboBox18 = new System.Windows.Forms.ComboBox();
            this.label37 = new System.Windows.Forms.Label();
            this.comboBox19 = new System.Windows.Forms.ComboBox();
            this.label38 = new System.Windows.Forms.Label();
            this.comboBox16 = new System.Windows.Forms.ComboBox();
            this.label39 = new System.Windows.Forms.Label();
            this.comboBox17 = new System.Windows.Forms.ComboBox();
            this.comboBox14 = new System.Windows.Forms.ComboBox();
            this.comboBox15 = new System.Windows.Forms.ComboBox();
            this.comboBox13 = new System.Windows.Forms.ComboBox();
            this.button7 = new System.Windows.Forms.Button();
            this.label32 = new System.Windows.Forms.Label();
            this.comboBox12 = new System.Windows.Forms.ComboBox();
            this.button27 = new System.Windows.Forms.Button();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.label125 = new System.Windows.Forms.Label();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.label56 = new System.Windows.Forms.Label();
            this.textBox22 = new System.Windows.Forms.TextBox();
            this.label55 = new System.Windows.Forms.Label();
            this.label54 = new System.Windows.Forms.Label();
            this.textBox21 = new System.Windows.Forms.TextBox();
            this.textBox20 = new System.Windows.Forms.TextBox();
            this.button38 = new System.Windows.Forms.Button();
            this.button37 = new System.Windows.Forms.Button();
            this.button35 = new System.Windows.Forms.Button();
            this.dataGridView5 = new System.Windows.Forms.DataGridView();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.button111 = new System.Windows.Forms.Button();
            this.comboBox34 = new System.Windows.Forms.ComboBox();
            this.button34 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.button18 = new System.Windows.Forms.Button();
            this.button29 = new System.Windows.Forms.Button();
            this.label20 = new System.Windows.Forms.Label();
            this.button23 = new System.Windows.Forms.Button();
            this.button28 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.textBox17 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.comboBox10 = new System.Windows.Forms.ComboBox();
            this.button19 = new System.Windows.Forms.Button();
            this.label29 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.campeonatos = new System.Windows.Forms.TabPage();
            this.groupBox21 = new System.Windows.Forms.GroupBox();
            this.label100 = new System.Windows.Forms.Label();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.button109 = new System.Windows.Forms.Button();
            this.comboBox39 = new System.Windows.Forms.ComboBox();
            this.label88 = new System.Windows.Forms.Label();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.button103 = new System.Windows.Forms.Button();
            this.groupBox20 = new System.Windows.Forms.GroupBox();
            this.button108 = new System.Windows.Forms.Button();
            this.button105 = new System.Windows.Forms.Button();
            this.comboBox37 = new System.Windows.Forms.ComboBox();
            this.label84 = new System.Windows.Forms.Label();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.label86 = new System.Windows.Forms.Label();
            this.groupBox19 = new System.Windows.Forms.GroupBox();
            this.comboBox41 = new System.Windows.Forms.ComboBox();
            this.label138 = new System.Windows.Forms.Label();
            this.comboBox42 = new System.Windows.Forms.ComboBox();
            this.label92 = new System.Windows.Forms.Label();
            this.button107 = new System.Windows.Forms.Button();
            this.comboBox38 = new System.Windows.Forms.ComboBox();
            this.label87 = new System.Windows.Forms.Label();
            this.comboBox36 = new System.Windows.Forms.ComboBox();
            this.label83 = new System.Windows.Forms.Label();
            this.button102 = new System.Windows.Forms.Button();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.numericUpDown4 = new System.Windows.Forms.NumericUpDown();
            this.label85 = new System.Windows.Forms.Label();
            this.button101 = new System.Windows.Forms.Button();
            this.dataGridView8 = new System.Windows.Forms.DataGridView();
            this.dataGridView7 = new System.Windows.Forms.DataGridView();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.button104 = new System.Windows.Forms.Button();
            this.button52 = new System.Windows.Forms.Button();
            this.label44 = new System.Windows.Forms.Label();
            this.button44 = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.comboBox11 = new System.Windows.Forms.ComboBox();
            this.dataGridView9 = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button87 = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.groupBox24 = new System.Windows.Forms.GroupBox();
            this.label101 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.label113 = new System.Windows.Forms.Label();
            this.label102 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.Check_Datos = new System.Windows.Forms.TabPage();
            this.groupBox23 = new System.Windows.Forms.GroupBox();
            this.label123 = new System.Windows.Forms.Label();
            this.labelGenero = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.labelNumero = new System.Windows.Forms.Label();
            this.label121 = new System.Windows.Forms.Label();
            this.label122 = new System.Windows.Forms.Label();
            this.label124 = new System.Windows.Forms.Label();
            this.label128 = new System.Windows.Forms.Label();
            this.labelCategoria = new System.Windows.Forms.Label();
            this.labelEvento = new System.Windows.Forms.Label();
            this.labelNombre = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.serialPort1 = new System.IO.Ports.SerialPort(this.components);
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.timer2 = new System.Windows.Forms.Timer(this.components);
            this.serialPort2 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort3 = new System.IO.Ports.SerialPort(this.components);
            this.serialPort4 = new System.IO.Ports.SerialPort(this.components);
            this.db_mainDataSet = new CronoBotTiming.db_mainDataSet();
            this.categoriasTableAdapter = new CronoBotTiming.db_mainDataSetTableAdapters.categoriasTableAdapter();
            this.eventosBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.eventosTableAdapter = new CronoBotTiming.db_mainDataSet1TableAdapters.eventosTableAdapter();
            this.serialPort5 = new System.IO.Ports.SerialPort(this.components);
            this.timer3 = new System.Windows.Forms.Timer(this.components);
            this.serialPort6 = new System.IO.Ports.SerialPort(this.components);
            this.folderBrowserDialog1 = new System.Windows.Forms.FolderBrowserDialog();
            this.timer4 = new System.Windows.Forms.Timer(this.components);
            this.timer5 = new System.Windows.Forms.Timer(this.components);
            this.timer6 = new System.Windows.Forms.Timer(this.components);
            this.label139 = new System.Windows.Forms.Label();
            this.textBox51 = new System.Windows.Forms.TextBox();
            this.tabControl1.SuspendLayout();
            this.tab1_Listado_General.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Corredores)).BeginInit();
            this.Config.SuspendLayout();
            this.groupBox28.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.groupBox11.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.en_curso.SuspendLayout();
            this.groupBox22.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.groupBox8.SuspendLayout();
            this.groupBox4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.resultados.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            this.groupBox15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).BeginInit();
            this.tools.SuspendLayout();
            this.groupBox27.SuspendLayout();
            this.groupBox26.SuspendLayout();
            this.groupBox25.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).BeginInit();
            this.groupBox18.SuspendLayout();
            this.groupBox17.SuspendLayout();
            this.groupBox16.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            this.groupBox14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).BeginInit();
            this.groupBox13.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).BeginInit();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).BeginInit();
            this.groupBox5.SuspendLayout();
            this.groupBox10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            this.campeonatos.SuspendLayout();
            this.groupBox21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            this.groupBox20.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            this.groupBox19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).BeginInit();
            this.tabPage5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.groupBox24.SuspendLayout();
            this.Check_Datos.SuspendLayout();
            this.groupBox23.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.db_mainDataSet)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventosBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tab1_Listado_General);
            this.tabControl1.Controls.Add(this.Config);
            this.tabControl1.Controls.Add(this.en_curso);
            this.tabControl1.Controls.Add(this.resultados);
            this.tabControl1.Controls.Add(this.tools);
            this.tabControl1.Controls.Add(this.campeonatos);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.Check_Datos);
            this.tabControl1.Location = new System.Drawing.Point(1, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1366, 703);
            this.tabControl1.TabIndex = 0;
            this.tabControl1.SelectedIndexChanged += new System.EventHandler(this.tabControl1_SelectedIndexChanged);
            // 
            // tab1_Listado_General
            // 
            this.tab1_Listado_General.Controls.Add(this.button41);
            this.tab1_Listado_General.Controls.Add(this.label42);
            this.tab1_Listado_General.Controls.Add(this.textBox15);
            this.tab1_Listado_General.Controls.Add(this.button6);
            this.tab1_Listado_General.Controls.Add(this.button5);
            this.tab1_Listado_General.Controls.Add(this.textBox1);
            this.tab1_Listado_General.Controls.Add(this.button4);
            this.tab1_Listado_General.Controls.Add(this.button3);
            this.tab1_Listado_General.Controls.Add(this.dateTimePicker1);
            this.tab1_Listado_General.Controls.Add(this.label2);
            this.tab1_Listado_General.Controls.Add(this.textBox2);
            this.tab1_Listado_General.Controls.Add(this.label1);
            this.tab1_Listado_General.Controls.Add(this.dataGridView_Corredores);
            this.tab1_Listado_General.Controls.Add(this.comboBox3);
            this.tab1_Listado_General.Controls.Add(this.label10);
            this.tab1_Listado_General.Controls.Add(this.button2);
            this.tab1_Listado_General.Controls.Add(this.label13);
            this.tab1_Listado_General.Controls.Add(this.label14);
            this.tab1_Listado_General.Controls.Add(this.label15);
            this.tab1_Listado_General.Controls.Add(this.textBox9);
            this.tab1_Listado_General.Controls.Add(this.textBox10);
            this.tab1_Listado_General.Controls.Add(this.textBox11);
            this.tab1_Listado_General.Location = new System.Drawing.Point(4, 22);
            this.tab1_Listado_General.Name = "tab1_Listado_General";
            this.tab1_Listado_General.Padding = new System.Windows.Forms.Padding(3);
            this.tab1_Listado_General.Size = new System.Drawing.Size(1358, 677);
            this.tab1_Listado_General.TabIndex = 0;
            this.tab1_Listado_General.Text = "Listado General";
            this.tab1_Listado_General.UseVisualStyleBackColor = true;
            // 
            // button41
            // 
            this.button41.Location = new System.Drawing.Point(687, 66);
            this.button41.Name = "button41";
            this.button41.Size = new System.Drawing.Size(103, 21);
            this.button41.TabIndex = 94;
            this.button41.Text = "buscar x apellido";
            this.button41.UseVisualStyleBackColor = true;
            this.button41.Click += new System.EventHandler(this.button41_Click);
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Location = new System.Drawing.Point(452, 75);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(34, 13);
            this.label42.TabIndex = 93;
            this.label42.Text = "Team";
            // 
            // textBox15
            // 
            this.textBox15.Location = new System.Drawing.Point(512, 75);
            this.textBox15.Name = "textBox15";
            this.textBox15.Size = new System.Drawing.Size(128, 20);
            this.textBox15.TabIndex = 92;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(687, 94);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(210, 21);
            this.button6.TabIndex = 89;
            this.button6.Text = "Actualizar registro por dni";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(687, 120);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(210, 21);
            this.button5.TabIndex = 88;
            this.button5.Text = "Refresh Tabla";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(687, 40);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(103, 20);
            this.textBox1.TabIndex = 87;
            this.textBox1.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox1_KeyDown);
            // 
            // button4
            // 
            this.button4.Enabled = false;
            this.button4.Location = new System.Drawing.Point(796, 66);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(101, 21);
            this.button4.TabIndex = 86;
            this.button4.Text = "borrar x dni";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(796, 39);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(101, 21);
            this.button3.TabIndex = 85;
            this.button3.Text = "buscar x dni";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dateTimePicker1
            // 
            this.dateTimePicker1.Location = new System.Drawing.Point(301, 41);
            this.dateTimePicker1.Name = "dateTimePicker1";
            this.dateTimePicker1.Size = new System.Drawing.Size(128, 20);
            this.dateTimePicker1.TabIndex = 84;
            this.dateTimePicker1.ValueChanged += new System.EventHandler(this.dateTimePicker1_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(452, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(39, 13);
            this.label2.TabIndex = 83;
            this.label2.Text = "ciudad";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(512, 40);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(128, 20);
            this.textBox2.TabIndex = 82;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(233, 41);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 81;
            this.label1.Text = "nacimiento";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // dataGridView_Corredores
            // 
            this.dataGridView_Corredores.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Corredores.Location = new System.Drawing.Point(3, 150);
            this.dataGridView_Corredores.Name = "dataGridView_Corredores";
            this.dataGridView_Corredores.Size = new System.Drawing.Size(896, 524);
            this.dataGridView_Corredores.TabIndex = 79;
            this.dataGridView_Corredores.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // comboBox3
            // 
            this.comboBox3.FormattingEnabled = true;
            this.comboBox3.Items.AddRange(new object[] {
            "masculino",
            "femenino"});
            this.comboBox3.Location = new System.Drawing.Point(79, 109);
            this.comboBox3.Name = "comboBox3";
            this.comboBox3.Size = new System.Drawing.Size(128, 21);
            this.comboBox3.TabIndex = 76;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(11, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(31, 13);
            this.label10.TabIndex = 75;
            this.label10.Text = "Sexo";
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(301, 110);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(339, 21);
            this.button2.TabIndex = 70;
            this.button2.Text = "Agregar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(233, 76);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(31, 13);
            this.label13.TabIndex = 69;
            this.label13.Text = "email";
            this.label13.Click += new System.EventHandler(this.label13_Click);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(11, 76);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(26, 13);
            this.label14.TabIndex = 68;
            this.label14.Text = "DNI";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(11, 41);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(44, 13);
            this.label15.TabIndex = 67;
            this.label15.Text = "Nombre";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(301, 76);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(128, 20);
            this.textBox9.TabIndex = 65;
            this.textBox9.TextChanged += new System.EventHandler(this.textBox9_TextChanged);
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(79, 76);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(128, 20);
            this.textBox10.TabIndex = 64;
            this.textBox10.TextChanged += new System.EventHandler(this.textBox10_TextChanged);
            // 
            // textBox11
            // 
            this.textBox11.Location = new System.Drawing.Point(79, 41);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(128, 20);
            this.textBox11.TabIndex = 63;
            // 
            // Config
            // 
            this.Config.Controls.Add(this.groupBox28);
            this.Config.Controls.Add(this.dataGridView3);
            this.Config.Controls.Add(this.dataGridView2);
            this.Config.Controls.Add(this.dataGridView1);
            this.Config.Controls.Add(this.groupBox1);
            this.Config.Controls.Add(this.groupBox2);
            this.Config.Controls.Add(this.groupBox3);
            this.Config.Location = new System.Drawing.Point(4, 22);
            this.Config.Name = "Config";
            this.Config.Padding = new System.Windows.Forms.Padding(3);
            this.Config.Size = new System.Drawing.Size(1358, 677);
            this.Config.TabIndex = 1;
            this.Config.Text = "Configuracion Evento";
            this.Config.UseVisualStyleBackColor = true;
            this.Config.Click += new System.EventHandler(this.tabPage2_Click);
            // 
            // groupBox28
            // 
            this.groupBox28.Controls.Add(this.textBox50);
            this.groupBox28.Controls.Add(this.comboBox50);
            this.groupBox28.Controls.Add(this.label137);
            this.groupBox28.Controls.Add(this.label136);
            this.groupBox28.Controls.Add(this.label134);
            this.groupBox28.Controls.Add(this.label132);
            this.groupBox28.Controls.Add(this.label129);
            this.groupBox28.Controls.Add(this.textBox44);
            this.groupBox28.Controls.Add(this.textBox49);
            this.groupBox28.Controls.Add(this.textBox42);
            this.groupBox28.Controls.Add(this.label130);
            this.groupBox28.Controls.Add(this.textBox43);
            this.groupBox28.Controls.Add(this.label131);
            this.groupBox28.Controls.Add(this.textBox45);
            this.groupBox28.Controls.Add(this.label133);
            this.groupBox28.Controls.Add(this.button127);
            this.groupBox28.Controls.Add(this.textBox46);
            this.groupBox28.Controls.Add(this.label135);
            this.groupBox28.Controls.Add(this.dateTimePicker9);
            this.groupBox28.Controls.Add(this.comboBox52);
            this.groupBox28.Controls.Add(this.textBox47);
            this.groupBox28.Controls.Add(this.label140);
            this.groupBox28.Controls.Add(this.textBox48);
            this.groupBox28.Location = new System.Drawing.Point(632, 218);
            this.groupBox28.Name = "groupBox28";
            this.groupBox28.Size = new System.Drawing.Size(721, 149);
            this.groupBox28.TabIndex = 159;
            this.groupBox28.TabStop = false;
            this.groupBox28.Text = "Compañero";
            this.groupBox28.Enter += new System.EventHandler(this.groupBox28_Enter);
            // 
            // textBox50
            // 
            this.textBox50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox50.Location = new System.Drawing.Point(52, 20);
            this.textBox50.Name = "textBox50";
            this.textBox50.Size = new System.Drawing.Size(236, 26);
            this.textBox50.TabIndex = 163;
            this.textBox50.TextChanged += new System.EventHandler(this.textBox50_TextChanged);
            // 
            // comboBox50
            // 
            this.comboBox50.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox50.FormattingEnabled = true;
            this.comboBox50.Location = new System.Drawing.Point(354, 20);
            this.comboBox50.Name = "comboBox50";
            this.comboBox50.Size = new System.Drawing.Size(188, 28);
            this.comboBox50.TabIndex = 160;
            // 
            // label137
            // 
            this.label137.AutoSize = true;
            this.label137.Location = new System.Drawing.Point(326, 25);
            this.label137.Name = "label137";
            this.label137.Size = new System.Drawing.Size(22, 13);
            this.label137.TabIndex = 159;
            this.label137.Text = "cat";
            // 
            // label136
            // 
            this.label136.AutoSize = true;
            this.label136.Location = new System.Drawing.Point(5, 27);
            this.label136.Name = "label136";
            this.label136.Size = new System.Drawing.Size(41, 13);
            this.label136.TabIndex = 159;
            this.label136.Text = "Evento";
            // 
            // label134
            // 
            this.label134.AutoSize = true;
            this.label134.Location = new System.Drawing.Point(8, 88);
            this.label134.Name = "label134";
            this.label134.Size = new System.Drawing.Size(29, 13);
            this.label134.TabIndex = 162;
            this.label134.Text = "sexo";
            // 
            // label132
            // 
            this.label132.AutoSize = true;
            this.label132.Location = new System.Drawing.Point(327, 59);
            this.label132.Name = "label132";
            this.label132.Size = new System.Drawing.Size(21, 13);
            this.label132.TabIndex = 161;
            this.label132.Text = "dni";
            // 
            // label129
            // 
            this.label129.AutoSize = true;
            this.label129.Location = new System.Drawing.Point(516, 121);
            this.label129.Name = "label129";
            this.label129.Size = new System.Drawing.Size(44, 13);
            this.label129.TabIndex = 160;
            this.label129.Text = "team ID";
            // 
            // textBox44
            // 
            this.textBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox44.Location = new System.Drawing.Point(354, 53);
            this.textBox44.Name = "textBox44";
            this.textBox44.Size = new System.Drawing.Size(133, 26);
            this.textBox44.TabIndex = 153;
            this.textBox44.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox44_KeyDown);
            // 
            // textBox49
            // 
            this.textBox49.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox49.Location = new System.Drawing.Point(566, 115);
            this.textBox49.Name = "textBox49";
            this.textBox49.Size = new System.Drawing.Size(87, 26);
            this.textBox49.TabIndex = 159;
            // 
            // textBox42
            // 
            this.textBox42.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox42.Location = new System.Drawing.Point(355, 114);
            this.textBox42.Name = "textBox42";
            this.textBox42.Size = new System.Drawing.Size(133, 26);
            this.textBox42.TabIndex = 152;
            this.textBox42.TextChanged += new System.EventHandler(this.textBox42_TextChanged);
            // 
            // label130
            // 
            this.label130.AutoSize = true;
            this.label130.Location = new System.Drawing.Point(319, 122);
            this.label130.Name = "label130";
            this.label130.Size = new System.Drawing.Size(30, 13);
            this.label130.TabIndex = 151;
            this.label130.Text = "team";
            this.label130.Click += new System.EventHandler(this.label130_Click);
            // 
            // textBox43
            // 
            this.textBox43.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox43.Location = new System.Drawing.Point(355, 83);
            this.textBox43.Name = "textBox43";
            this.textBox43.Size = new System.Drawing.Size(133, 26);
            this.textBox43.TabIndex = 145;
            // 
            // label131
            // 
            this.label131.AutoSize = true;
            this.label131.Location = new System.Drawing.Point(310, 91);
            this.label131.Name = "label131";
            this.label131.Size = new System.Drawing.Size(39, 13);
            this.label131.TabIndex = 144;
            this.label131.Text = "campo";
            // 
            // textBox45
            // 
            this.textBox45.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox45.Location = new System.Drawing.Point(53, 111);
            this.textBox45.Name = "textBox45";
            this.textBox45.Size = new System.Drawing.Size(236, 26);
            this.textBox45.TabIndex = 137;
            // 
            // label133
            // 
            this.label133.AutoSize = true;
            this.label133.Location = new System.Drawing.Point(7, 119);
            this.label133.Name = "label133";
            this.label133.Size = new System.Drawing.Size(39, 13);
            this.label133.TabIndex = 136;
            this.label133.Text = "ciudad";
            // 
            // button127
            // 
            this.button127.Location = new System.Drawing.Point(521, 83);
            this.button127.Name = "button127";
            this.button127.Size = new System.Drawing.Size(39, 20);
            this.button127.TabIndex = 134;
            this.button127.Text = "Chip";
            this.button127.UseVisualStyleBackColor = true;
            this.button127.Click += new System.EventHandler(this.button127_Click);
            // 
            // textBox46
            // 
            this.textBox46.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox46.Location = new System.Drawing.Point(565, 51);
            this.textBox46.Name = "textBox46";
            this.textBox46.Size = new System.Drawing.Size(88, 26);
            this.textBox46.TabIndex = 125;
            this.textBox46.TextChanged += new System.EventHandler(this.textBox46_TextChanged);
            // 
            // label135
            // 
            this.label135.AutoSize = true;
            this.label135.Location = new System.Drawing.Point(518, 59);
            this.label135.Name = "label135";
            this.label135.Size = new System.Drawing.Size(42, 13);
            this.label135.TabIndex = 124;
            this.label135.Text = "numero";
            this.label135.Click += new System.EventHandler(this.label135_Click);
            // 
            // dateTimePicker9
            // 
            this.dateTimePicker9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker9.Location = new System.Drawing.Point(174, 81);
            this.dateTimePicker9.Name = "dateTimePicker9";
            this.dateTimePicker9.Size = new System.Drawing.Size(115, 26);
            this.dateTimePicker9.TabIndex = 123;
            this.dateTimePicker9.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePicker9_KeyDown);
            // 
            // comboBox52
            // 
            this.comboBox52.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox52.FormattingEnabled = true;
            this.comboBox52.Items.AddRange(new object[] {
            "masculino",
            "femenino"});
            this.comboBox52.Location = new System.Drawing.Point(53, 80);
            this.comboBox52.Name = "comboBox52";
            this.comboBox52.Size = new System.Drawing.Size(113, 28);
            this.comboBox52.TabIndex = 122;
            // 
            // textBox47
            // 
            this.textBox47.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox47.Location = new System.Drawing.Point(53, 51);
            this.textBox47.Name = "textBox47";
            this.textBox47.Size = new System.Drawing.Size(236, 26);
            this.textBox47.TabIndex = 118;
            // 
            // label140
            // 
            this.label140.AutoSize = true;
            this.label140.Location = new System.Drawing.Point(5, 57);
            this.label140.Name = "label140";
            this.label140.Size = new System.Drawing.Size(42, 13);
            this.label140.TabIndex = 117;
            this.label140.Text = "nombre";
            // 
            // textBox48
            // 
            this.textBox48.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox48.Location = new System.Drawing.Point(566, 83);
            this.textBox48.Name = "textBox48";
            this.textBox48.Size = new System.Drawing.Size(87, 26);
            this.textBox48.TabIndex = 127;
            this.textBox48.TextChanged += new System.EventHandler(this.textBox48_TextChanged);
            // 
            // dataGridView3
            // 
            this.dataGridView3.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView3.Location = new System.Drawing.Point(632, 373);
            this.dataGridView3.Name = "dataGridView3";
            this.dataGridView3.Size = new System.Drawing.Size(723, 298);
            this.dataGridView3.TabIndex = 110;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(230, 215);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.Size = new System.Drawing.Size(396, 455);
            this.dataGridView2.TabIndex = 93;
            this.dataGridView2.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellEndEdit);
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.Single;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(3, 283);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(221, 385);
            this.dataGridView1.TabIndex = 92;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label127);
            this.groupBox1.Controls.Add(this.textBox41);
            this.groupBox1.Controls.Add(this.dateTimePicker8);
            this.groupBox1.Controls.Add(this.label126);
            this.groupBox1.Controls.Add(this.label99);
            this.groupBox1.Controls.Add(this.textBox39);
            this.groupBox1.Controls.Add(this.textBox3);
            this.groupBox1.Controls.Add(this.comboBox43);
            this.groupBox1.Controls.Add(this.button99);
            this.groupBox1.Controls.Add(this.comboBox31);
            this.groupBox1.Controls.Add(this.label81);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.dateTimePicker2);
            this.groupBox1.Controls.Add(this.textBox4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.button8);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Location = new System.Drawing.Point(3, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(221, 271);
            this.groupBox1.TabIndex = 129;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Crear Evento";
            // 
            // label127
            // 
            this.label127.AutoSize = true;
            this.label127.Location = new System.Drawing.Point(6, 80);
            this.label127.Name = "label127";
            this.label127.Size = new System.Drawing.Size(28, 13);
            this.label127.TabIndex = 133;
            this.label127.Text = "Print";
            // 
            // textBox41
            // 
            this.textBox41.Location = new System.Drawing.Point(66, 77);
            this.textBox41.Name = "textBox41";
            this.textBox41.Size = new System.Drawing.Size(145, 20);
            this.textBox41.TabIndex = 132;
            // 
            // dateTimePicker8
            // 
            this.dateTimePicker8.Location = new System.Drawing.Point(85, 188);
            this.dateTimePicker8.Name = "dateTimePicker8";
            this.dateTimePicker8.Size = new System.Drawing.Size(126, 20);
            this.dateTimePicker8.TabIndex = 131;
            this.dateTimePicker8.Value = new System.DateTime(2024, 4, 24, 0, 10, 0, 0);
            // 
            // label126
            // 
            this.label126.AutoSize = true;
            this.label126.Location = new System.Drawing.Point(6, 190);
            this.label126.Name = "label126";
            this.label126.Size = new System.Drawing.Size(73, 13);
            this.label126.TabIndex = 130;
            this.label126.Text = "Vuelta Minima";
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Location = new System.Drawing.Point(6, 50);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(25, 13);
            this.label99.TabIndex = 129;
            this.label99.Text = "Key";
            // 
            // textBox39
            // 
            this.textBox39.Location = new System.Drawing.Point(66, 47);
            this.textBox39.Name = "textBox39";
            this.textBox39.Size = new System.Drawing.Size(145, 20);
            this.textBox39.TabIndex = 128;
            // 
            // textBox3
            // 
            this.textBox3.Enabled = false;
            this.textBox3.Location = new System.Drawing.Point(66, 244);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(89, 20);
            this.textBox3.TabIndex = 127;
            // 
            // comboBox43
            // 
            this.comboBox43.AllowDrop = true;
            this.comboBox43.FormattingEnabled = true;
            this.comboBox43.Items.AddRange(new object[] {
            "vueltas",
            "raid"});
            this.comboBox43.Location = new System.Drawing.Point(66, 20);
            this.comboBox43.Name = "comboBox43";
            this.comboBox43.Size = new System.Drawing.Size(145, 21);
            this.comboBox43.TabIndex = 126;
            // 
            // button99
            // 
            this.button99.Enabled = false;
            this.button99.Location = new System.Drawing.Point(161, 244);
            this.button99.Name = "button99";
            this.button99.Size = new System.Drawing.Size(50, 21);
            this.button99.TabIndex = 125;
            this.button99.Text = "Clonar Evento";
            this.button99.UseVisualStyleBackColor = true;
            this.button99.Click += new System.EventHandler(this.button99_Click);
            // 
            // comboBox31
            // 
            this.comboBox31.AllowDrop = true;
            this.comboBox31.FormattingEnabled = true;
            this.comboBox31.Items.AddRange(new object[] {
            "vueltas",
            "raid"});
            this.comboBox31.Location = new System.Drawing.Point(66, 158);
            this.comboBox31.Name = "comboBox31";
            this.comboBox31.Size = new System.Drawing.Size(145, 21);
            this.comboBox31.TabIndex = 124;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Location = new System.Drawing.Point(6, 160);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(28, 13);
            this.label81.TabIndex = 123;
            this.label81.Text = "Tipo";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 135);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 106;
            this.label6.Text = "Lugar";
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(66, 105);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(145, 20);
            this.dateTimePicker2.TabIndex = 91;
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(66, 132);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(145, 20);
            this.textBox4.TabIndex = 105;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 111);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(37, 13);
            this.label3.TabIndex = 90;
            this.label3.Text = "Fecha";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(66, 217);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(145, 21);
            this.button8.TabIndex = 87;
            this.button8.Text = "Crear Evento";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.button8_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 23);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(44, 13);
            this.label5.TabIndex = 86;
            this.label5.Text = "Nombre";
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label82);
            this.groupBox2.Controls.Add(this.textBox19);
            this.groupBox2.Controls.Add(this.label50);
            this.groupBox2.Controls.Add(this.numericUpDown3);
            this.groupBox2.Controls.Add(this.label45);
            this.groupBox2.Controls.Add(this.button9);
            this.groupBox2.Controls.Add(this.numericUpDown2);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.numericUpDown1);
            this.groupBox2.Controls.Add(this.comboBox1);
            this.groupBox2.Controls.Add(this.groupBox11);
            this.groupBox2.Controls.Add(this.comboBox2);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Location = new System.Drawing.Point(230, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(396, 207);
            this.groupBox2.TabIndex = 130;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Agregar Categorias a Evento";
            this.groupBox2.Enter += new System.EventHandler(this.groupBox2_Enter);
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Location = new System.Drawing.Point(11, 133);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(26, 13);
            this.label82.TabIndex = 142;
            this.label82.Text = "PCs";
            // 
            // textBox19
            // 
            this.textBox19.Location = new System.Drawing.Point(77, 44);
            this.textBox19.Name = "textBox19";
            this.textBox19.Size = new System.Drawing.Size(140, 20);
            this.textBox19.TabIndex = 120;
            this.textBox19.TextChanged += new System.EventHandler(this.textBox19_TextChanged);
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Location = new System.Drawing.Point(11, 44);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(60, 13);
            this.label50.TabIndex = 119;
            this.label50.Text = "nombre cat";
            this.label50.Click += new System.EventHandler(this.label50_Click);
            // 
            // numericUpDown3
            // 
            this.numericUpDown3.Location = new System.Drawing.Point(77, 118);
            this.numericUpDown3.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown3.Name = "numericUpDown3";
            this.numericUpDown3.Size = new System.Drawing.Size(140, 20);
            this.numericUpDown3.TabIndex = 111;
            this.numericUpDown3.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Location = new System.Drawing.Point(9, 120);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(49, 13);
            this.label45.TabIndex = 110;
            this.label45.Text = "vueltas /";
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(77, 144);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(140, 25);
            this.button9.TabIndex = 100;
            this.button9.Text = "+ Categoria";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // numericUpDown2
            // 
            this.numericUpDown2.Location = new System.Drawing.Point(177, 68);
            this.numericUpDown2.Name = "numericUpDown2";
            this.numericUpDown2.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown2.TabIndex = 109;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(11, 95);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 88;
            this.label4.Text = "Sexo";
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(77, 68);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(40, 20);
            this.numericUpDown1.TabIndex = 108;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "masculino",
            "femenino"});
            this.comboBox1.Location = new System.Drawing.Point(77, 93);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(140, 21);
            this.comboBox1.TabIndex = 89;
            // 
            // groupBox11
            // 
            this.groupBox11.Controls.Add(this.button106);
            this.groupBox11.Controls.Add(this.button51);
            this.groupBox11.Controls.Add(this.textBox24);
            this.groupBox11.Controls.Add(this.button50);
            this.groupBox11.Controls.Add(this.comboBox24);
            this.groupBox11.Controls.Add(this.comboBox25);
            this.groupBox11.Controls.Add(this.button48);
            this.groupBox11.Controls.Add(this.button49);
            this.groupBox11.Location = new System.Drawing.Point(233, 7);
            this.groupBox11.Name = "groupBox11";
            this.groupBox11.Size = new System.Drawing.Size(163, 162);
            this.groupBox11.TabIndex = 141;
            this.groupBox11.TabStop = false;
            this.groupBox11.Text = "Edicion";
            // 
            // button106
            // 
            this.button106.Enabled = false;
            this.button106.Location = new System.Drawing.Point(6, 72);
            this.button106.Name = "button106";
            this.button106.Size = new System.Drawing.Size(149, 21);
            this.button106.TabIndex = 141;
            this.button106.Text = "borrar en db categorias";
            this.button106.UseVisualStyleBackColor = true;
            this.button106.Click += new System.EventHandler(this.button106_Click);
            // 
            // button51
            // 
            this.button51.Location = new System.Drawing.Point(94, 136);
            this.button51.Name = "button51";
            this.button51.Size = new System.Drawing.Size(61, 20);
            this.button51.TabIndex = 140;
            this.button51.Text = "borrar";
            this.button51.UseVisualStyleBackColor = true;
            this.button51.Click += new System.EventHandler(this.button51_Click);
            // 
            // textBox24
            // 
            this.textBox24.Location = new System.Drawing.Point(6, 19);
            this.textBox24.Name = "textBox24";
            this.textBox24.Size = new System.Drawing.Size(86, 20);
            this.textBox24.TabIndex = 137;
            // 
            // button50
            // 
            this.button50.Location = new System.Drawing.Point(6, 135);
            this.button50.Name = "button50";
            this.button50.Size = new System.Drawing.Size(61, 20);
            this.button50.TabIndex = 139;
            this.button50.Text = "actualizar";
            this.button50.UseVisualStyleBackColor = true;
            this.button50.Click += new System.EventHandler(this.button50_Click);
            // 
            // comboBox24
            // 
            this.comboBox24.FormattingEnabled = true;
            this.comboBox24.Location = new System.Drawing.Point(6, 45);
            this.comboBox24.Name = "comboBox24";
            this.comboBox24.Size = new System.Drawing.Size(86, 21);
            this.comboBox24.TabIndex = 121;
            this.comboBox24.SelectedIndexChanged += new System.EventHandler(this.comboBox24_SelectedIndexChanged);
            // 
            // comboBox25
            // 
            this.comboBox25.FormattingEnabled = true;
            this.comboBox25.Location = new System.Drawing.Point(6, 109);
            this.comboBox25.Name = "comboBox25";
            this.comboBox25.Size = new System.Drawing.Size(149, 21);
            this.comboBox25.TabIndex = 138;
            this.comboBox25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox25_KeyDown);
            // 
            // button48
            // 
            this.button48.Location = new System.Drawing.Point(94, 45);
            this.button48.Name = "button48";
            this.button48.Size = new System.Drawing.Size(61, 21);
            this.button48.TabIndex = 135;
            this.button48.Text = "importar desde db categorias";
            this.button48.UseVisualStyleBackColor = true;
            this.button48.Click += new System.EventHandler(this.button48_Click);
            // 
            // button49
            // 
            this.button49.Location = new System.Drawing.Point(94, 19);
            this.button49.Name = "button49";
            this.button49.Size = new System.Drawing.Size(61, 21);
            this.button49.TabIndex = 136;
            this.button49.Text = "guargar en db ategorias";
            this.button49.UseVisualStyleBackColor = true;
            this.button49.Click += new System.EventHandler(this.button49_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Location = new System.Drawing.Point(77, 19);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(140, 21);
            this.comboBox2.TabIndex = 107;
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            this.comboBox2.TextChanged += new System.EventHandler(this.comboBox2_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(11, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 96;
            this.label7.Text = "Edad Inicio";
            this.label7.Click += new System.EventHandler(this.label7_Click);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(123, 70);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(49, 13);
            this.label8.TabIndex = 98;
            this.label8.Text = "Edad Fin";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(9, 22);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 13);
            this.label9.TabIndex = 103;
            this.label9.Text = "Evento";
            this.label9.Click += new System.EventHandler(this.label9_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.button85);
            this.groupBox3.Controls.Add(this.checkBox3);
            this.groupBox3.Controls.Add(this.label93);
            this.groupBox3.Controls.Add(this.textBox36);
            this.groupBox3.Controls.Add(this.label119);
            this.groupBox3.Controls.Add(this.checkBox19);
            this.groupBox3.Controls.Add(this.comboBox44);
            this.groupBox3.Controls.Add(this.checkBox17);
            this.groupBox3.Controls.Add(this.textBox32);
            this.groupBox3.Controls.Add(this.label78);
            this.groupBox3.Controls.Add(this.checkBox1);
            this.groupBox3.Controls.Add(this.button82);
            this.groupBox3.Controls.Add(this.label75);
            this.groupBox3.Controls.Add(this.textBox28);
            this.groupBox3.Controls.Add(this.button40);
            this.groupBox3.Controls.Add(this.button13);
            this.groupBox3.Controls.Add(this.textBox16);
            this.groupBox3.Controls.Add(this.label43);
            this.groupBox3.Controls.Add(this.button26);
            this.groupBox3.Controls.Add(this.button17);
            this.groupBox3.Controls.Add(this.comboBox9);
            this.groupBox3.Controls.Add(this.label25);
            this.groupBox3.Controls.Add(this.comboBox5);
            this.groupBox3.Controls.Add(this.label23);
            this.groupBox3.Controls.Add(this.textBox13);
            this.groupBox3.Controls.Add(this.label21);
            this.groupBox3.Controls.Add(this.button12);
            this.groupBox3.Controls.Add(this.dateTimePicker3);
            this.groupBox3.Controls.Add(this.comboBox4);
            this.groupBox3.Controls.Add(this.label19);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.label18);
            this.groupBox3.Controls.Add(this.textBox7);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.textBox8);
            this.groupBox3.Location = new System.Drawing.Point(632, 6);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(721, 208);
            this.groupBox3.TabIndex = 131;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Agregar Participantes a Evento";
            // 
            // button85
            // 
            this.button85.Location = new System.Drawing.Point(694, 179);
            this.button85.Name = "button85";
            this.button85.Size = new System.Drawing.Size(26, 26);
            this.button85.TabIndex = 159;
            this.button85.Text = "\\/";
            this.button85.UseVisualStyleBackColor = true;
            this.button85.Click += new System.EventHandler(this.button85_Click_1);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Location = new System.Drawing.Point(594, 91);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(108, 17);
            this.checkBox3.TabIndex = 158;
            this.checkBox3.Text = "largada individual";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckStateChanged += new System.EventHandler(this.checkBox3_CheckStateChanged);
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Location = new System.Drawing.Point(591, 116);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(75, 13);
            this.label93.TabIndex = 153;
            this.label93.Text = "ultimo numero:";
            this.label93.Click += new System.EventHandler(this.label93_Click);
            // 
            // textBox36
            // 
            this.textBox36.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox36.Location = new System.Drawing.Point(59, 151);
            this.textBox36.Name = "textBox36";
            this.textBox36.Size = new System.Drawing.Size(265, 26);
            this.textBox36.TabIndex = 152;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Location = new System.Drawing.Point(6, 157);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(30, 13);
            this.label119.TabIndex = 151;
            this.label119.Text = "team";
            // 
            // checkBox19
            // 
            this.checkBox19.AutoSize = true;
            this.checkBox19.Enabled = false;
            this.checkBox19.Location = new System.Drawing.Point(594, 44);
            this.checkBox19.Name = "checkBox19";
            this.checkBox19.Size = new System.Drawing.Size(70, 17);
            this.checkBox19.TabIndex = 150;
            this.checkBox19.Text = "auto chip";
            this.checkBox19.UseVisualStyleBackColor = true;
            this.checkBox19.CheckedChanged += new System.EventHandler(this.checkBox19_CheckedChanged);
            // 
            // comboBox44
            // 
            this.comboBox44.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox44.FormattingEnabled = true;
            this.comboBox44.Location = new System.Drawing.Point(380, 8);
            this.comboBox44.Name = "comboBox44";
            this.comboBox44.Size = new System.Drawing.Size(107, 28);
            this.comboBox44.TabIndex = 149;
            this.comboBox44.SelectedIndexChanged += new System.EventHandler(this.comboBox44_SelectedIndexChanged);
            this.comboBox44.KeyDown += new System.Windows.Forms.KeyEventHandler(this.comboBox44_KeyDown);
            // 
            // checkBox17
            // 
            this.checkBox17.AutoSize = true;
            this.checkBox17.Enabled = false;
            this.checkBox17.Location = new System.Drawing.Point(594, 68);
            this.checkBox17.Name = "checkBox17";
            this.checkBox17.Size = new System.Drawing.Size(120, 17);
            this.checkBox17.TabIndex = 148;
            this.checkBox17.Text = "categorias por edad";
            this.checkBox17.UseVisualStyleBackColor = true;
            this.checkBox17.CheckedChanged += new System.EventHandler(this.checkBox17_CheckedChanged);
            // 
            // textBox32
            // 
            this.textBox32.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox32.Location = new System.Drawing.Point(59, 180);
            this.textBox32.Name = "textBox32";
            this.textBox32.Size = new System.Drawing.Size(265, 26);
            this.textBox32.TabIndex = 145;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Location = new System.Drawing.Point(6, 186);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(39, 13);
            this.label78.TabIndex = 144;
            this.label78.Text = "campo";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Location = new System.Drawing.Point(594, 21);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(91, 17);
            this.checkBox1.TabIndex = 143;
            this.checkBox1.Text = "auto Team ID";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button82
            // 
            this.button82.Enabled = false;
            this.button82.Location = new System.Drawing.Point(503, 136);
            this.button82.Name = "button82";
            this.button82.Size = new System.Drawing.Size(39, 20);
            this.button82.TabIndex = 142;
            this.button82.Text = "Calc";
            this.button82.UseVisualStyleBackColor = true;
            this.button82.Visible = false;
            this.button82.Click += new System.EventHandler(this.button82_Click);
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Location = new System.Drawing.Point(331, 135);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(44, 13);
            this.label75.TabIndex = 141;
            this.label75.Text = "team ID";
            // 
            // textBox28
            // 
            this.textBox28.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox28.Location = new System.Drawing.Point(379, 130);
            this.textBox28.Name = "textBox28";
            this.textBox28.Size = new System.Drawing.Size(119, 26);
            this.textBox28.TabIndex = 140;
            // 
            // button40
            // 
            this.button40.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button40.Location = new System.Drawing.Point(495, 167);
            this.button40.Name = "button40";
            this.button40.Size = new System.Drawing.Size(64, 34);
            this.button40.TabIndex = 139;
            this.button40.Text = "Borrar";
            this.button40.UseVisualStyleBackColor = true;
            this.button40.Click += new System.EventHandler(this.button40_Click);
            // 
            // button13
            // 
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button13.Location = new System.Drawing.Point(421, 167);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(64, 34);
            this.button13.TabIndex = 138;
            this.button13.Text = "Actualizar";
            this.button13.UseVisualStyleBackColor = true;
            this.button13.Click += new System.EventHandler(this.button13_Click_1);
            // 
            // textBox16
            // 
            this.textBox16.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox16.Location = new System.Drawing.Point(59, 123);
            this.textBox16.Name = "textBox16";
            this.textBox16.Size = new System.Drawing.Size(265, 26);
            this.textBox16.TabIndex = 137;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Location = new System.Drawing.Point(6, 130);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(39, 13);
            this.label43.TabIndex = 136;
            this.label43.Text = "ciudad";
            // 
            // button26
            // 
            this.button26.Location = new System.Drawing.Point(594, 168);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(57, 35);
            this.button26.TabIndex = 135;
            this.button26.Text = "Exportar Listado";
            this.button26.UseVisualStyleBackColor = true;
            this.button26.Click += new System.EventHandler(this.button26_Click);
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(334, 102);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(39, 20);
            this.button17.TabIndex = 134;
            this.button17.Text = "Chip";
            this.button17.UseVisualStyleBackColor = true;
            this.button17.Click += new System.EventHandler(this.button17_Click);
            // 
            // comboBox9
            // 
            this.comboBox9.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox9.FormattingEnabled = true;
            this.comboBox9.Location = new System.Drawing.Point(59, 13);
            this.comboBox9.Name = "comboBox9";
            this.comboBox9.Size = new System.Drawing.Size(265, 28);
            this.comboBox9.TabIndex = 133;
            this.comboBox9.SelectedIndexChanged += new System.EventHandler(this.comboBox9_SelectedIndexChanged);
            this.comboBox9.TextChanged += new System.EventHandler(this.comboBox9_TextChanged);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(4, 19);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(41, 13);
            this.label25.TabIndex = 132;
            this.label25.Text = "Evento";
            this.label25.Click += new System.EventHandler(this.label25_Click);
            // 
            // comboBox5
            // 
            this.comboBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox5.FormattingEnabled = true;
            this.comboBox5.Location = new System.Drawing.Point(379, 39);
            this.comboBox5.Name = "comboBox5";
            this.comboBox5.Size = new System.Drawing.Size(202, 28);
            this.comboBox5.TabIndex = 133;
            this.comboBox5.SelectedIndexChanged += new System.EventHandler(this.comboBox5_SelectedIndexChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(351, 44);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(22, 13);
            this.label23.TabIndex = 132;
            this.label23.Text = "cat";
            // 
            // textBox13
            // 
            this.textBox13.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox13.Location = new System.Drawing.Point(379, 72);
            this.textBox13.Name = "textBox13";
            this.textBox13.Size = new System.Drawing.Size(119, 26);
            this.textBox13.TabIndex = 125;
            this.textBox13.TextChanged += new System.EventHandler(this.textBox13_TextChanged);
            this.textBox13.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox13_KeyDown);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(331, 75);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(42, 13);
            this.label21.TabIndex = 124;
            this.label21.Text = "numero";
            // 
            // button12
            // 
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Location = new System.Drawing.Point(357, 167);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(55, 34);
            this.button12.TabIndex = 111;
            this.button12.Text = "Agregar";
            this.button12.UseVisualStyleBackColor = true;
            this.button12.Click += new System.EventHandler(this.button12_Click);
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.dateTimePicker3.Location = new System.Drawing.Point(59, 96);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(115, 26);
            this.dateTimePicker3.TabIndex = 123;
            this.dateTimePicker3.ValueChanged += new System.EventHandler(this.dateTimePicker3_ValueChanged);
            this.dateTimePicker3.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePicker3_KeyDown);
            // 
            // comboBox4
            // 
            this.comboBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox4.FormattingEnabled = true;
            this.comboBox4.Items.AddRange(new object[] {
            "masculino",
            "femenino"});
            this.comboBox4.Location = new System.Drawing.Point(59, 67);
            this.comboBox4.Name = "comboBox4";
            this.comboBox4.Size = new System.Drawing.Size(115, 28);
            this.comboBox4.TabIndex = 122;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(4, 74);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(29, 13);
            this.label19.TabIndex = 121;
            this.label19.Text = "sexo";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(352, 13);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(21, 13);
            this.label11.TabIndex = 112;
            this.label11.Text = "dni";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(4, 102);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(52, 13);
            this.label18.TabIndex = 119;
            this.label18.Text = "f. de nac.";
            this.label18.Click += new System.EventHandler(this.label18_Click);
            // 
            // textBox7
            // 
            this.textBox7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.Location = new System.Drawing.Point(59, 41);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(265, 26);
            this.textBox7.TabIndex = 118;
            this.textBox7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox7_KeyDown);
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(4, 47);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(42, 13);
            this.label17.TabIndex = 117;
            this.label17.Text = "nombre";
            // 
            // textBox8
            // 
            this.textBox8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.Location = new System.Drawing.Point(379, 99);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(68, 26);
            this.textBox8.TabIndex = 127;
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            this.textBox8.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox8_KeyDown);
            // 
            // en_curso
            // 
            this.en_curso.Controls.Add(this.label120);
            this.en_curso.Controls.Add(this.textBox37);
            this.en_curso.Controls.Add(this.checkBox11);
            this.en_curso.Controls.Add(this.groupBox22);
            this.en_curso.Controls.Add(this.dataGridView10);
            this.en_curso.Controls.Add(this.button110);
            this.en_curso.Controls.Add(this.label12);
            this.en_curso.Controls.Add(this.textBox30);
            this.en_curso.Controls.Add(this.label79);
            this.en_curso.Controls.Add(this.groupBox9);
            this.en_curso.Controls.Add(this.groupBox8);
            this.en_curso.Controls.Add(this.groupBox4);
            this.en_curso.Controls.Add(this.dataGridView4);
            this.en_curso.Location = new System.Drawing.Point(4, 22);
            this.en_curso.Name = "en_curso";
            this.en_curso.Padding = new System.Windows.Forms.Padding(3);
            this.en_curso.Size = new System.Drawing.Size(1358, 677);
            this.en_curso.TabIndex = 2;
            this.en_curso.Text = "Eventos en Curso";
            this.en_curso.UseVisualStyleBackColor = true;
            this.en_curso.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // label120
            // 
            this.label120.AutoSize = true;
            this.label120.Location = new System.Drawing.Point(970, 125);
            this.label120.Name = "label120";
            this.label120.Size = new System.Drawing.Size(75, 13);
            this.label120.TabIndex = 149;
            this.label120.Text = "Imprimir Ticket";
            // 
            // textBox37
            // 
            this.textBox37.Location = new System.Drawing.Point(1044, 121);
            this.textBox37.Name = "textBox37";
            this.textBox37.Size = new System.Drawing.Size(54, 20);
            this.textBox37.TabIndex = 143;
            this.textBox37.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox37_KeyDown);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Enabled = false;
            this.checkBox11.Location = new System.Drawing.Point(879, 122);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Size = new System.Drawing.Size(47, 17);
            this.checkBox11.TabIndex = 148;
            this.checkBox11.Text = "auto";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.Visible = false;
            // 
            // groupBox22
            // 
            this.groupBox22.Controls.Add(this.label71);
            this.groupBox22.Controls.Add(this.label53);
            this.groupBox22.Controls.Add(this.label68);
            this.groupBox22.Controls.Add(this.label69);
            this.groupBox22.Controls.Add(this.label70);
            this.groupBox22.Controls.Add(this.label72);
            this.groupBox22.Controls.Add(this.label74);
            this.groupBox22.Controls.Add(this.label73);
            this.groupBox22.Location = new System.Drawing.Point(929, 6);
            this.groupBox22.Name = "groupBox22";
            this.groupBox22.Size = new System.Drawing.Size(169, 111);
            this.groupBox22.TabIndex = 155;
            this.groupBox22.TabStop = false;
            this.groupBox22.Text = "RIFD";
            // 
            // label71
            // 
            this.label71.AutoSize = true;
            this.label71.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label71.Location = new System.Drawing.Point(1, 13);
            this.label71.Name = "label71";
            this.label71.Size = new System.Drawing.Size(85, 24);
            this.label71.TabIndex = 138;
            this.label71.Text = "Antena 1";
            this.label71.Click += new System.EventHandler(this.label71_Click);
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(86, 13);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(85, 24);
            this.label53.TabIndex = 134;
            this.label53.Text = "Antena 1";
            this.label53.Click += new System.EventHandler(this.label53_Click);
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(1, 36);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(85, 24);
            this.label68.TabIndex = 139;
            this.label68.Text = "Antena 2";
            this.label68.Click += new System.EventHandler(this.label68_Click);
            // 
            // label69
            // 
            this.label69.AutoSize = true;
            this.label69.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label69.Location = new System.Drawing.Point(1, 60);
            this.label69.Name = "label69";
            this.label69.Size = new System.Drawing.Size(85, 24);
            this.label69.TabIndex = 140;
            this.label69.Text = "Antena 3";
            this.label69.Click += new System.EventHandler(this.label69_Click);
            // 
            // label70
            // 
            this.label70.AutoSize = true;
            this.label70.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label70.Location = new System.Drawing.Point(1, 84);
            this.label70.Name = "label70";
            this.label70.Size = new System.Drawing.Size(85, 24);
            this.label70.TabIndex = 141;
            this.label70.Text = "Antena 4";
            this.label70.Click += new System.EventHandler(this.label70_Click);
            // 
            // label72
            // 
            this.label72.AutoSize = true;
            this.label72.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label72.Location = new System.Drawing.Point(86, 37);
            this.label72.Name = "label72";
            this.label72.Size = new System.Drawing.Size(85, 24);
            this.label72.TabIndex = 142;
            this.label72.Text = "Antena 1";
            this.label72.Click += new System.EventHandler(this.label72_Click);
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(86, 84);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(85, 24);
            this.label74.TabIndex = 144;
            this.label74.Text = "Antena 1";
            this.label74.Click += new System.EventHandler(this.label74_Click);
            // 
            // label73
            // 
            this.label73.AutoSize = true;
            this.label73.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label73.Location = new System.Drawing.Point(86, 60);
            this.label73.Name = "label73";
            this.label73.Size = new System.Drawing.Size(85, 24);
            this.label73.TabIndex = 143;
            this.label73.Text = "Antena 1";
            this.label73.Click += new System.EventHandler(this.label73_Click);
            // 
            // dataGridView10
            // 
            this.dataGridView10.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView10.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Categoria,
            this.Lapeo});
            this.dataGridView10.Location = new System.Drawing.Point(1102, 5);
            this.dataGridView10.Name = "dataGridView10";
            this.dataGridView10.Size = new System.Drawing.Size(253, 136);
            this.dataGridView10.TabIndex = 154;
            this.dataGridView10.Visible = false;
            // 
            // Categoria
            // 
            this.Categoria.HeaderText = "Categoria";
            this.Categoria.Name = "Categoria";
            // 
            // Lapeo
            // 
            this.Lapeo.HeaderText = "Lapeo";
            this.Lapeo.Name = "Lapeo";
            // 
            // button110
            // 
            this.button110.Enabled = false;
            this.button110.Location = new System.Drawing.Point(791, 118);
            this.button110.Name = "button110";
            this.button110.Size = new System.Drawing.Size(81, 23);
            this.button110.TabIndex = 153;
            this.button110.Text = "Start Lapeo";
            this.button110.UseVisualStyleBackColor = true;
            this.button110.Visible = false;
            this.button110.Click += new System.EventHandler(this.button110_Click_1);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(1141, 3);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 24);
            this.label12.TabIndex = 146;
            this.label12.Text = "Antena 1";
            this.label12.Visible = false;
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // textBox30
            // 
            this.textBox30.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox30.Location = new System.Drawing.Point(1102, 6);
            this.textBox30.Multiline = true;
            this.textBox30.Name = "textBox30";
            this.textBox30.ReadOnly = true;
            this.textBox30.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox30.Size = new System.Drawing.Size(253, 668);
            this.textBox30.TabIndex = 149;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(1232, 1);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(52, 24);
            this.label79.TabIndex = 145;
            this.label79.Text = "GSM";
            this.label79.Visible = false;
            this.label79.Click += new System.EventHandler(this.label79_Click);
            // 
            // groupBox9
            // 
            this.groupBox9.Controls.Add(this.label31);
            this.groupBox9.Controls.Add(this.dateTimePicker5);
            this.groupBox9.Controls.Add(this.button42);
            this.groupBox9.Controls.Add(this.checkedListBox1);
            this.groupBox9.Controls.Add(this.button15);
            this.groupBox9.Controls.Add(this.label22);
            this.groupBox9.Controls.Add(this.comboBox6);
            this.groupBox9.Controls.Add(this.button16);
            this.groupBox9.Location = new System.Drawing.Point(6, 6);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(270, 136);
            this.groupBox9.TabIndex = 126;
            this.groupBox9.TabStop = false;
            this.groupBox9.Text = "Iniciar Evento";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(163, 94);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(98, 13);
            this.label31.TabIndex = 149;
            this.label31.Text = "Finalizar Categorias";
            this.label31.Click += new System.EventHandler(this.label31_Click);
            // 
            // dateTimePicker5
            // 
            this.dateTimePicker5.Location = new System.Drawing.Point(160, 40);
            this.dateTimePicker5.Name = "dateTimePicker5";
            this.dateTimePicker5.Size = new System.Drawing.Size(105, 20);
            this.dateTimePicker5.TabIndex = 144;
            // 
            // button42
            // 
            this.button42.Location = new System.Drawing.Point(160, 66);
            this.button42.Name = "button42";
            this.button42.Size = new System.Drawing.Size(105, 21);
            this.button42.TabIndex = 143;
            this.button42.Text = "Modif Hr Largada";
            this.button42.UseVisualStyleBackColor = true;
            this.button42.Click += new System.EventHandler(this.button42_Click);
            // 
            // checkedListBox1
            // 
            this.checkedListBox1.FormattingEnabled = true;
            this.checkedListBox1.Location = new System.Drawing.Point(6, 38);
            this.checkedListBox1.Name = "checkedListBox1";
            this.checkedListBox1.Size = new System.Drawing.Size(148, 94);
            this.checkedListBox1.TabIndex = 127;
            this.checkedListBox1.SelectedIndexChanged += new System.EventHandler(this.checkedListBox1_SelectedIndexChanged);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(160, 14);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(105, 21);
            this.button15.TabIndex = 110;
            this.button15.Text = "Iniciar Ahora";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.button15_Click);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(-70, 22);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(41, 13);
            this.label22.TabIndex = 108;
            this.label22.Text = "Evento";
            this.label22.Click += new System.EventHandler(this.label22_Click);
            // 
            // comboBox6
            // 
            this.comboBox6.FormattingEnabled = true;
            this.comboBox6.Location = new System.Drawing.Point(6, 14);
            this.comboBox6.Name = "comboBox6";
            this.comboBox6.Size = new System.Drawing.Size(148, 21);
            this.comboBox6.TabIndex = 109;
            this.comboBox6.SelectedIndexChanged += new System.EventHandler(this.comboBox6_SelectedIndexChanged);
            this.comboBox6.TextChanged += new System.EventHandler(this.comboBox6_TextChanged);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(160, 110);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(104, 21);
            this.button16.TabIndex = 115;
            this.button16.Text = "dnf";
            this.button16.UseVisualStyleBackColor = true;
            this.button16.Click += new System.EventHandler(this.button16_Click);
            // 
            // groupBox8
            // 
            this.groupBox8.Controls.Add(this.button117);
            this.groupBox8.Controls.Add(this.button59);
            this.groupBox8.Controls.Add(this.button112);
            this.groupBox8.Controls.Add(this.button78);
            this.groupBox8.Controls.Add(this.button47);
            this.groupBox8.Controls.Add(this.textBox23);
            this.groupBox8.Controls.Add(this.label51);
            this.groupBox8.Controls.Add(this.button45);
            this.groupBox8.Controls.Add(this.button43);
            this.groupBox8.Controls.Add(this.button31);
            this.groupBox8.Controls.Add(this.button33);
            this.groupBox8.Controls.Add(this.button32);
            this.groupBox8.Controls.Add(this.dateTimePicker4);
            this.groupBox8.Controls.Add(this.button30);
            this.groupBox8.Controls.Add(this.textBox18);
            this.groupBox8.Controls.Add(this.button46);
            this.groupBox8.Controls.Add(this.comboBox23);
            this.groupBox8.Location = new System.Drawing.Point(575, 6);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(351, 109);
            this.groupBox8.TabIndex = 125;
            this.groupBox8.TabStop = false;
            this.groupBox8.Text = "Edicion Manual";
            // 
            // button117
            // 
            this.button117.Location = new System.Drawing.Point(90, 15);
            this.button117.Name = "button117";
            this.button117.Size = new System.Drawing.Size(29, 25);
            this.button117.TabIndex = 142;
            this.button117.Text = "->";
            this.button117.UseVisualStyleBackColor = true;
            this.button117.Click += new System.EventHandler(this.button117_Click);
            // 
            // button59
            // 
            this.button59.Location = new System.Drawing.Point(255, 48);
            this.button59.Name = "button59";
            this.button59.Size = new System.Drawing.Size(90, 23);
            this.button59.TabIndex = 141;
            this.button59.Text = "ver Hr llegada";
            this.button59.UseVisualStyleBackColor = true;
            this.button59.Click += new System.EventHandler(this.button59_Click);
            // 
            // button112
            // 
            this.button112.Location = new System.Drawing.Point(61, 15);
            this.button112.Name = "button112";
            this.button112.Size = new System.Drawing.Size(30, 25);
            this.button112.TabIndex = 140;
            this.button112.Text = "<-";
            this.button112.UseVisualStyleBackColor = true;
            this.button112.Click += new System.EventHandler(this.button112_Click_1);
            // 
            // button78
            // 
            this.button78.Location = new System.Drawing.Point(304, 19);
            this.button78.Name = "button78";
            this.button78.Size = new System.Drawing.Size(41, 20);
            this.button78.TabIndex = 139;
            this.button78.Text = "DSQ";
            this.button78.UseVisualStyleBackColor = true;
            this.button78.Click += new System.EventHandler(this.button78_Click);
            // 
            // button47
            // 
            this.button47.Enabled = false;
            this.button47.Location = new System.Drawing.Point(270, 50);
            this.button47.Name = "button47";
            this.button47.Size = new System.Drawing.Size(75, 20);
            this.button47.TabIndex = 138;
            this.button47.Text = "Modif Lap";
            this.button47.UseVisualStyleBackColor = true;
            this.button47.Visible = false;
            this.button47.Click += new System.EventHandler(this.button47_Click);
            // 
            // textBox23
            // 
            this.textBox23.Location = new System.Drawing.Point(323, 76);
            this.textBox23.Name = "textBox23";
            this.textBox23.Size = new System.Drawing.Size(22, 20);
            this.textBox23.TabIndex = 137;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(86, 79);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(167, 24);
            this.label51.TabIndex = 127;
            this.label51.Text = "fecha y hora actual";
            // 
            // button45
            // 
            this.button45.Location = new System.Drawing.Point(163, 19);
            this.button45.Name = "button45";
            this.button45.Size = new System.Drawing.Size(41, 20);
            this.button45.TabIndex = 132;
            this.button45.Text = "DNS";
            this.button45.UseVisualStyleBackColor = true;
            this.button45.Click += new System.EventHandler(this.button45_Click);
            // 
            // button43
            // 
            this.button43.Enabled = false;
            this.button43.Location = new System.Drawing.Point(251, 19);
            this.button43.Name = "button43";
            this.button43.Size = new System.Drawing.Size(51, 20);
            this.button43.TabIndex = 130;
            this.button43.Text = "running";
            this.button43.UseVisualStyleBackColor = true;
            this.button43.Click += new System.EventHandler(this.button43_Click);
            // 
            // button31
            // 
            this.button31.Location = new System.Drawing.Point(207, 81);
            this.button31.Name = "button31";
            this.button31.Size = new System.Drawing.Size(65, 25);
            this.button31.TabIndex = 128;
            this.button31.Text = "Calc Clasif";
            this.button31.UseVisualStyleBackColor = true;
            this.button31.Visible = false;
            this.button31.Click += new System.EventHandler(this.button31_Click);
            // 
            // button33
            // 
            this.button33.Location = new System.Drawing.Point(207, 19);
            this.button33.Name = "button33";
            this.button33.Size = new System.Drawing.Size(42, 20);
            this.button33.TabIndex = 129;
            this.button33.Text = "Finisher";
            this.button33.UseVisualStyleBackColor = true;
            this.button33.Click += new System.EventHandler(this.button33_Click);
            // 
            // button32
            // 
            this.button32.Location = new System.Drawing.Point(120, 19);
            this.button32.Name = "button32";
            this.button32.Size = new System.Drawing.Size(41, 20);
            this.button32.TabIndex = 127;
            this.button32.Text = "DNF";
            this.button32.UseVisualStyleBackColor = true;
            this.button32.Click += new System.EventHandler(this.button32_Click);
            // 
            // dateTimePicker4
            // 
            this.dateTimePicker4.Location = new System.Drawing.Point(105, 50);
            this.dateTimePicker4.Name = "dateTimePicker4";
            this.dateTimePicker4.Size = new System.Drawing.Size(146, 20);
            this.dateTimePicker4.TabIndex = 126;
            // 
            // button30
            // 
            this.button30.Location = new System.Drawing.Point(4, 50);
            this.button30.Name = "button30";
            this.button30.Size = new System.Drawing.Size(99, 21);
            this.button30.TabIndex = 124;
            this.button30.Text = "Modif Hr Llegada";
            this.button30.UseVisualStyleBackColor = true;
            this.button30.Click += new System.EventHandler(this.button30_Click);
            // 
            // textBox18
            // 
            this.textBox18.Location = new System.Drawing.Point(4, 19);
            this.textBox18.Name = "textBox18";
            this.textBox18.Size = new System.Drawing.Size(54, 20);
            this.textBox18.TabIndex = 122;
            this.textBox18.TextChanged += new System.EventHandler(this.textBox18_TextChanged);
            this.textBox18.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox18_KeyDown);
            this.textBox18.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox18_KeyPress);
            // 
            // button46
            // 
            this.button46.Enabled = false;
            this.button46.Location = new System.Drawing.Point(4, 83);
            this.button46.Name = "button46";
            this.button46.Size = new System.Drawing.Size(80, 21);
            this.button46.TabIndex = 136;
            this.button46.Text = "Modif Categ";
            this.button46.UseVisualStyleBackColor = true;
            this.button46.Visible = false;
            this.button46.Click += new System.EventHandler(this.button46_Click);
            // 
            // comboBox23
            // 
            this.comboBox23.Enabled = false;
            this.comboBox23.FormattingEnabled = true;
            this.comboBox23.Location = new System.Drawing.Point(90, 84);
            this.comboBox23.Name = "comboBox23";
            this.comboBox23.Size = new System.Drawing.Size(111, 21);
            this.comboBox23.TabIndex = 135;
            this.comboBox23.Visible = false;
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.checkBox12);
            this.groupBox4.Controls.Add(this.cbLargada);
            this.groupBox4.Controls.Add(this.checkBox2);
            this.groupBox4.Controls.Add(this.comboBox32);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.comboBox8);
            this.groupBox4.Controls.Add(this.label27);
            this.groupBox4.Controls.Add(this.comboBox7);
            this.groupBox4.Controls.Add(this.label24);
            this.groupBox4.Controls.Add(this.textBox14);
            this.groupBox4.Location = new System.Drawing.Point(282, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(285, 127);
            this.groupBox4.TabIndex = 111;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Eventos en curso";
            this.groupBox4.Enter += new System.EventHandler(this.groupBox4_Enter);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Location = new System.Drawing.Point(138, 110);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Size = new System.Drawing.Size(48, 17);
            this.checkBox12.TabIndex = 149;
            this.checkBox12.Text = "Asist";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.checkBox12_CheckedChanged);
            // 
            // cbLargada
            // 
            this.cbLargada.AutoSize = true;
            this.cbLargada.Location = new System.Drawing.Point(70, 110);
            this.cbLargada.Name = "cbLargada";
            this.cbLargada.Size = new System.Drawing.Size(66, 17);
            this.cbLargada.TabIndex = 148;
            this.cbLargada.Text = "Start Ind";
            this.cbLargada.UseVisualStyleBackColor = true;
            this.cbLargada.CheckedChanged += new System.EventHandler(this.cbLargada_CheckedChanged);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Location = new System.Drawing.Point(2, 110);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(67, 17);
            this.checkBox2.TabIndex = 144;
            this.checkBox2.Text = "Team ID";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.checkBox2_CheckedChanged);
            // 
            // comboBox32
            // 
            this.comboBox32.FormattingEnabled = true;
            this.comboBox32.Items.AddRange(new object[] {
            "todos",
            "1-finish",
            "2-running",
            "3-lapped",
            "4-dnf",
            "5-dsq",
            "6-dns",
            "7-to start",
            "8-absent"});
            this.comboBox32.Location = new System.Drawing.Point(197, 104);
            this.comboBox32.Name = "comboBox32";
            this.comboBox32.Size = new System.Drawing.Size(87, 21);
            this.comboBox32.TabIndex = 147;
            this.comboBox32.TextChanged += new System.EventHandler(this.comboBox32_TextChanged);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(6, 39);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(52, 13);
            this.label30.TabIndex = 121;
            this.label30.Text = "Categoria";
            // 
            // comboBox8
            // 
            this.comboBox8.FormattingEnabled = true;
            this.comboBox8.Location = new System.Drawing.Point(7, 54);
            this.comboBox8.Name = "comboBox8";
            this.comboBox8.Size = new System.Drawing.Size(124, 21);
            this.comboBox8.TabIndex = 119;
            this.comboBox8.SelectedIndexChanged += new System.EventHandler(this.comboBox8_SelectedIndexChanged_1);
            this.comboBox8.TextChanged += new System.EventHandler(this.comboBox8_TextChanged);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 19F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(0, 78);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(99, 30);
            this.label27.TabIndex = 118;
            this.label27.Text = "Tiempo";
            this.label27.Click += new System.EventHandler(this.label27_Click);
            // 
            // comboBox7
            // 
            this.comboBox7.FormattingEnabled = true;
            this.comboBox7.Location = new System.Drawing.Point(7, 15);
            this.comboBox7.Name = "comboBox7";
            this.comboBox7.Size = new System.Drawing.Size(269, 21);
            this.comboBox7.TabIndex = 112;
            this.comboBox7.SelectedIndexChanged += new System.EventHandler(this.comboBox7_SelectedIndexChanged);
            this.comboBox7.TextChanged += new System.EventHandler(this.comboBox7_TextChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(194, 38);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(82, 13);
            this.label24.TabIndex = 116;
            this.label24.Text = "Numero Manual";
            // 
            // textBox14
            // 
            this.textBox14.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox14.Location = new System.Drawing.Point(137, 54);
            this.textBox14.Name = "textBox14";
            this.textBox14.Size = new System.Drawing.Size(148, 47);
            this.textBox14.TabIndex = 114;
            this.textBox14.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox14_KeyDown);
            // 
            // dataGridView4
            // 
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(8, 144);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.Size = new System.Drawing.Size(1090, 533);
            this.dataGridView4.TabIndex = 0;
            this.dataGridView4.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView4_CellContentClick);
            // 
            // resultados
            // 
            this.resultados.Controls.Add(this.checkBox14);
            this.resultados.Controls.Add(this.checkBox18);
            this.resultados.Controls.Add(this.label96);
            this.resultados.Controls.Add(this.textBox34);
            this.resultados.Controls.Add(this.label95);
            this.resultados.Controls.Add(this.numericUpDown7);
            this.resultados.Controls.Add(this.label94);
            this.resultados.Controls.Add(this.checkBox13);
            this.resultados.Controls.Add(this.checkBox4);
            this.resultados.Controls.Add(this.label77);
            this.resultados.Controls.Add(this.textBox29);
            this.resultados.Controls.Add(this.button39);
            this.resultados.Controls.Add(this.button36);
            this.resultados.Controls.Add(this.groupBox15);
            this.resultados.Controls.Add(this.dataGridView6);
            this.resultados.Location = new System.Drawing.Point(4, 22);
            this.resultados.Name = "resultados";
            this.resultados.Padding = new System.Windows.Forms.Padding(3);
            this.resultados.Size = new System.Drawing.Size(1358, 677);
            this.resultados.TabIndex = 3;
            this.resultados.Text = "Resultados";
            this.resultados.UseVisualStyleBackColor = true;
            this.resultados.Click += new System.EventHandler(this.tabPage4_Click);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Location = new System.Drawing.Point(425, 14);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Size = new System.Drawing.Size(67, 17);
            this.checkBox14.TabIndex = 156;
            this.checkBox14.Text = "presente";
            this.checkBox14.UseVisualStyleBackColor = true;
            // 
            // checkBox18
            // 
            this.checkBox18.AutoSize = true;
            this.checkBox18.Checked = true;
            this.checkBox18.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox18.Enabled = false;
            this.checkBox18.Location = new System.Drawing.Point(618, 21);
            this.checkBox18.Name = "checkBox18";
            this.checkBox18.Size = new System.Drawing.Size(58, 17);
            this.checkBox18.TabIndex = 155;
            this.checkBox18.Text = "refresh";
            this.checkBox18.UseVisualStyleBackColor = true;
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Location = new System.Drawing.Point(679, 3);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(82, 13);
            this.label96.TabIndex = 153;
            this.label96.Text = "Buscar corredor";
            // 
            // textBox34
            // 
            this.textBox34.Location = new System.Drawing.Point(682, 18);
            this.textBox34.Name = "textBox34";
            this.textBox34.Size = new System.Drawing.Size(100, 20);
            this.textBox34.TabIndex = 154;
            this.textBox34.TextChanged += new System.EventHandler(this.textBox34_TextChanged);
            this.textBox34.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox34_KeyDown);
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Location = new System.Drawing.Point(509, 60);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(109, 13);
            this.label95.TabIndex = 152;
            this.label95.Text = "por categoria (podios)";
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.Location = new System.Drawing.Point(512, 76);
            this.numericUpDown7.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.numericUpDown7.Minimum = new decimal(new int[] {
            3,
            0,
            0,
            0});
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(112, 20);
            this.numericUpDown7.TabIndex = 151;
            this.numericUpDown7.Value = new decimal(new int[] {
            5000,
            0,
            0,
            0});
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Location = new System.Drawing.Point(509, 47);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(115, 13);
            this.label94.TabIndex = 150;
            this.label94.Text = "Cantidad de resultados";
            this.label94.Click += new System.EventHandler(this.label94_Click);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Checked = true;
            this.checkBox13.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox13.Location = new System.Drawing.Point(382, 84);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Size = new System.Drawing.Size(75, 17);
            this.checkBox13.TabIndex = 148;
            this.checkBox13.Text = "categorias";
            this.checkBox13.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Location = new System.Drawing.Point(308, 83);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(72, 17);
            this.checkBox4.TabIndex = 146;
            this.checkBox4.Text = "generales";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Location = new System.Drawing.Point(509, 3);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(61, 13);
            this.label77.TabIndex = 123;
            this.label77.Text = "Print Ticket";
            // 
            // textBox29
            // 
            this.textBox29.Location = new System.Drawing.Point(512, 19);
            this.textBox29.Name = "textBox29";
            this.textBox29.Size = new System.Drawing.Size(100, 20);
            this.textBox29.TabIndex = 143;
            this.textBox29.TextChanged += new System.EventHandler(this.textBox29_TextChanged);
            this.textBox29.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox29_KeyDown);
            // 
            // button39
            // 
            this.button39.Location = new System.Drawing.Point(308, 54);
            this.button39.Name = "button39";
            this.button39.Size = new System.Drawing.Size(149, 23);
            this.button39.TabIndex = 132;
            this.button39.Text = "Exportar Resultados EXCEL";
            this.button39.UseVisualStyleBackColor = true;
            this.button39.Click += new System.EventHandler(this.button39_Click_1);
            // 
            // button36
            // 
            this.button36.Location = new System.Drawing.Point(308, 10);
            this.button36.Name = "button36";
            this.button36.Size = new System.Drawing.Size(111, 23);
            this.button36.TabIndex = 113;
            this.button36.Text = "refresh clasificacion";
            this.button36.UseVisualStyleBackColor = true;
            this.button36.Click += new System.EventHandler(this.button36_Click_2);
            // 
            // groupBox15
            // 
            this.groupBox15.Controls.Add(this.label76);
            this.groupBox15.Controls.Add(this.comboBox29);
            this.groupBox15.Controls.Add(this.comboBox30);
            this.groupBox15.Location = new System.Drawing.Point(8, 3);
            this.groupBox15.Name = "groupBox15";
            this.groupBox15.Size = new System.Drawing.Size(285, 88);
            this.groupBox15.TabIndex = 112;
            this.groupBox15.TabStop = false;
            this.groupBox15.Text = "Eventos en curso";
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Location = new System.Drawing.Point(6, 39);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(52, 13);
            this.label76.TabIndex = 121;
            this.label76.Text = "Categoria";
            // 
            // comboBox29
            // 
            this.comboBox29.FormattingEnabled = true;
            this.comboBox29.Location = new System.Drawing.Point(7, 54);
            this.comboBox29.Name = "comboBox29";
            this.comboBox29.Size = new System.Drawing.Size(269, 21);
            this.comboBox29.TabIndex = 119;
            this.comboBox29.TextChanged += new System.EventHandler(this.comboBox29_TextChanged);
            // 
            // comboBox30
            // 
            this.comboBox30.FormattingEnabled = true;
            this.comboBox30.Location = new System.Drawing.Point(7, 15);
            this.comboBox30.Name = "comboBox30";
            this.comboBox30.Size = new System.Drawing.Size(269, 21);
            this.comboBox30.TabIndex = 112;
            this.comboBox30.TextChanged += new System.EventHandler(this.comboBox30_TextChanged);
            // 
            // dataGridView6
            // 
            this.dataGridView6.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView6.Location = new System.Drawing.Point(8, 106);
            this.dataGridView6.Name = "dataGridView6";
            this.dataGridView6.Size = new System.Drawing.Size(1343, 562);
            this.dataGridView6.TabIndex = 1;
            this.dataGridView6.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView6_CellContentClick);
            // 
            // tools
            // 
            this.tools.Controls.Add(this.groupBox27);
            this.tools.Controls.Add(this.checkBox6);
            this.tools.Controls.Add(this.button121);
            this.tools.Controls.Add(this.textBox38);
            this.tools.Controls.Add(this.button120);
            this.tools.Controls.Add(this.groupBox26);
            this.tools.Controls.Add(this.groupBox25);
            this.tools.Controls.Add(this.label89);
            this.tools.Controls.Add(this.button25);
            this.tools.Controls.Add(this.button100);
            this.tools.Controls.Add(this.groupBox18);
            this.tools.Controls.Add(this.textBox31);
            this.tools.Controls.Add(this.groupBox17);
            this.tools.Controls.Add(this.groupBox16);
            this.tools.Controls.Add(this.textBox6);
            this.tools.Controls.Add(this.groupBox14);
            this.tools.Controls.Add(this.groupBox13);
            this.tools.Controls.Add(this.groupBox12);
            this.tools.Controls.Add(this.groupBox5);
            this.tools.Controls.Add(this.groupBox10);
            this.tools.Controls.Add(this.button35);
            this.tools.Controls.Add(this.dataGridView5);
            this.tools.Controls.Add(this.groupBox7);
            this.tools.Controls.Add(this.groupBox6);
            this.tools.Location = new System.Drawing.Point(4, 22);
            this.tools.Name = "tools";
            this.tools.Padding = new System.Windows.Forms.Padding(3);
            this.tools.Size = new System.Drawing.Size(1358, 677);
            this.tools.TabIndex = 5;
            this.tools.Text = "Tools";
            this.tools.UseVisualStyleBackColor = true;
            // 
            // groupBox27
            // 
            this.groupBox27.Controls.Add(this.dateTimePicker7);
            this.groupBox27.Controls.Add(this.textBox40);
            this.groupBox27.Location = new System.Drawing.Point(1063, 206);
            this.groupBox27.Name = "groupBox27";
            this.groupBox27.Size = new System.Drawing.Size(292, 70);
            this.groupBox27.TabIndex = 154;
            this.groupBox27.TabStop = false;
            this.groupBox27.Text = "LICENCIA";
            // 
            // dateTimePicker7
            // 
            this.dateTimePicker7.Location = new System.Drawing.Point(0, 16);
            this.dateTimePicker7.Name = "dateTimePicker7";
            this.dateTimePicker7.Size = new System.Drawing.Size(193, 20);
            this.dateTimePicker7.TabIndex = 156;
            this.dateTimePicker7.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dateTimePicker7_KeyDown);
            // 
            // textBox40
            // 
            this.textBox40.Location = new System.Drawing.Point(0, 42);
            this.textBox40.Name = "textBox40";
            this.textBox40.Size = new System.Drawing.Size(292, 20);
            this.textBox40.TabIndex = 138;
            this.textBox40.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox40_KeyDown);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Checked = true;
            this.checkBox6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox6.Location = new System.Drawing.Point(1063, 437);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Size = new System.Drawing.Size(110, 17);
            this.checkBox6.TabIndex = 141;
            this.checkBox6.Text = "actividad antenas";
            this.checkBox6.UseVisualStyleBackColor = true;
            // 
            // button121
            // 
            this.button121.Location = new System.Drawing.Point(1283, 433);
            this.button121.Name = "button121";
            this.button121.Size = new System.Drawing.Size(59, 23);
            this.button121.TabIndex = 145;
            this.button121.Text = "clear";
            this.button121.UseVisualStyleBackColor = true;
            this.button121.Click += new System.EventHandler(this.button121_Click);
            // 
            // textBox38
            // 
            this.textBox38.Location = new System.Drawing.Point(1062, 455);
            this.textBox38.Multiline = true;
            this.textBox38.Name = "textBox38";
            this.textBox38.ReadOnly = true;
            this.textBox38.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox38.Size = new System.Drawing.Size(280, 213);
            this.textBox38.TabIndex = 155;
            // 
            // button120
            // 
            this.button120.Location = new System.Drawing.Point(1157, 171);
            this.button120.Name = "button120";
            this.button120.Size = new System.Drawing.Size(69, 23);
            this.button120.TabIndex = 154;
            this.button120.Text = "sync time";
            this.button120.UseVisualStyleBackColor = true;
            this.button120.Click += new System.EventHandler(this.button120_Click);
            // 
            // groupBox26
            // 
            this.groupBox26.Controls.Add(this.textBox35);
            this.groupBox26.Controls.Add(this.label114);
            this.groupBox26.Controls.Add(this.button75);
            this.groupBox26.Controls.Add(this.button67);
            this.groupBox26.Controls.Add(this.comboBox45);
            this.groupBox26.Location = new System.Drawing.Point(1062, 282);
            this.groupBox26.Name = "groupBox26";
            this.groupBox26.Size = new System.Drawing.Size(226, 145);
            this.groupBox26.TabIndex = 153;
            this.groupBox26.TabStop = false;
            this.groupBox26.Text = "DB SERVERS";
            // 
            // textBox35
            // 
            this.textBox35.Enabled = false;
            this.textBox35.Location = new System.Drawing.Point(6, 122);
            this.textBox35.Name = "textBox35";
            this.textBox35.Size = new System.Drawing.Size(214, 20);
            this.textBox35.TabIndex = 138;
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Location = new System.Drawing.Point(92, 106);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(39, 13);
            this.label114.TabIndex = 136;
            this.label114.Text = "actual:";
            // 
            // button75
            // 
            this.button75.Location = new System.Drawing.Point(6, 79);
            this.button75.Name = "button75";
            this.button75.Size = new System.Drawing.Size(214, 23);
            this.button75.TabIndex = 139;
            this.button75.Text = "Connect Server";
            this.button75.UseVisualStyleBackColor = true;
            this.button75.Click += new System.EventHandler(this.button75_Click);
            // 
            // button67
            // 
            this.button67.Location = new System.Drawing.Point(6, 19);
            this.button67.Name = "button67";
            this.button67.Size = new System.Drawing.Size(214, 23);
            this.button67.TabIndex = 138;
            this.button67.Text = "Get Servers";
            this.button67.UseVisualStyleBackColor = true;
            this.button67.Click += new System.EventHandler(this.button67_Click);
            // 
            // comboBox45
            // 
            this.comboBox45.FormattingEnabled = true;
            this.comboBox45.Location = new System.Drawing.Point(6, 49);
            this.comboBox45.Name = "comboBox45";
            this.comboBox45.Size = new System.Drawing.Size(214, 21);
            this.comboBox45.TabIndex = 136;
            // 
            // groupBox25
            // 
            this.groupBox25.BackColor = System.Drawing.Color.LightCoral;
            this.groupBox25.Controls.Add(this.comboBox40);
            this.groupBox25.Controls.Add(this.label97);
            this.groupBox25.Controls.Add(this.trackBar5);
            this.groupBox25.Controls.Add(this.button124);
            this.groupBox25.Controls.Add(this.button123);
            this.groupBox25.Controls.Add(this.button122);
            this.groupBox25.Controls.Add(this.button20);
            this.groupBox25.Controls.Add(this.button118);
            this.groupBox25.Location = new System.Drawing.Point(1063, 6);
            this.groupBox25.Name = "groupBox25";
            this.groupBox25.Size = new System.Drawing.Size(163, 158);
            this.groupBox25.TabIndex = 152;
            this.groupBox25.TabStop = false;
            this.groupBox25.Text = "Master";
            this.groupBox25.Enter += new System.EventHandler(this.groupBox25_Enter);
            // 
            // comboBox40
            // 
            this.comboBox40.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.comboBox40.FormattingEnabled = true;
            this.comboBox40.Items.AddRange(new object[] {
            "0",
            "1",
            "2",
            "3",
            "4",
            "5"});
            this.comboBox40.Location = new System.Drawing.Point(72, 73);
            this.comboBox40.Name = "comboBox40";
            this.comboBox40.Size = new System.Drawing.Size(31, 21);
            this.comboBox40.TabIndex = 138;
            this.comboBox40.Text = "0";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Location = new System.Drawing.Point(118, 22);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(36, 13);
            this.label97.TabIndex = 136;
            this.label97.Text = "power";
            // 
            // trackBar5
            // 
            this.trackBar5.LargeChange = 20;
            this.trackBar5.Location = new System.Drawing.Point(113, 38);
            this.trackBar5.Maximum = 100;
            this.trackBar5.Name = "trackBar5";
            this.trackBar5.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBar5.Size = new System.Drawing.Size(45, 113);
            this.trackBar5.SmallChange = 10;
            this.trackBar5.TabIndex = 136;
            this.trackBar5.TickFrequency = 10;
            this.trackBar5.Scroll += new System.EventHandler(this.trackBar5_Scroll);
            // 
            // button124
            // 
            this.button124.Location = new System.Drawing.Point(4, 102);
            this.button124.Name = "button124";
            this.button124.Size = new System.Drawing.Size(103, 23);
            this.button124.TabIndex = 157;
            this.button124.Text = "Start Reading";
            this.button124.UseVisualStyleBackColor = true;
            this.button124.Click += new System.EventHandler(this.button124_Click);
            // 
            // button123
            // 
            this.button123.Location = new System.Drawing.Point(4, 44);
            this.button123.Name = "button123";
            this.button123.Size = new System.Drawing.Size(103, 23);
            this.button123.TabIndex = 137;
            this.button123.Text = "desconectar";
            this.button123.UseVisualStyleBackColor = true;
            this.button123.Click += new System.EventHandler(this.button123_Click);
            // 
            // button122
            // 
            this.button122.Location = new System.Drawing.Point(4, 72);
            this.button122.Name = "button122";
            this.button122.Size = new System.Drawing.Size(62, 23);
            this.button122.TabIndex = 135;
            this.button122.Text = "filter [s]";
            this.button122.UseVisualStyleBackColor = true;
            this.button122.Click += new System.EventHandler(this.button122_Click);
            // 
            // button20
            // 
            this.button20.Location = new System.Drawing.Point(4, 17);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(103, 23);
            this.button20.TabIndex = 136;
            this.button20.Text = "Auto Connect";
            this.button20.UseVisualStyleBackColor = true;
            this.button20.Click += new System.EventHandler(this.button20_Click);
            // 
            // button118
            // 
            this.button118.Location = new System.Drawing.Point(4, 131);
            this.button118.Name = "button118";
            this.button118.Size = new System.Drawing.Size(103, 23);
            this.button118.TabIndex = 130;
            this.button118.Text = "Stop Reading";
            this.button118.UseVisualStyleBackColor = true;
            this.button118.Click += new System.EventHandler(this.button118_Click);
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Location = new System.Drawing.Point(1, 601);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(41, 13);
            this.label89.TabIndex = 147;
            this.label89.Text = "label89";
            // 
            // button25
            // 
            this.button25.Location = new System.Drawing.Point(1063, 171);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(88, 23);
            this.button25.TabIndex = 110;
            this.button25.Text = "Refresh CBs";
            this.button25.UseVisualStyleBackColor = true;
            this.button25.Click += new System.EventHandler(this.button25_Click);
            // 
            // button100
            // 
            this.button100.Location = new System.Drawing.Point(364, 416);
            this.button100.Name = "button100";
            this.button100.Size = new System.Drawing.Size(33, 21);
            this.button100.TabIndex = 142;
            this.button100.Text = "ok";
            this.button100.UseVisualStyleBackColor = true;
            this.button100.Click += new System.EventHandler(this.button100_Click);
            // 
            // groupBox18
            // 
            this.groupBox18.Controls.Add(this.label80);
            this.groupBox18.Controls.Add(this.dateTimePicker6);
            this.groupBox18.Location = new System.Drawing.Point(227, 398);
            this.groupBox18.Name = "groupBox18";
            this.groupBox18.Size = new System.Drawing.Size(178, 64);
            this.groupBox18.TabIndex = 145;
            this.groupBox18.TabStop = false;
            this.groupBox18.Text = "Set tiempo vuelta minimo";
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Location = new System.Drawing.Point(6, 45);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(43, 13);
            this.label80.TabIndex = 134;
            this.label80.Text = "Actual: ";
            // 
            // dateTimePicker6
            // 
            this.dateTimePicker6.Location = new System.Drawing.Point(6, 19);
            this.dateTimePicker6.Name = "dateTimePicker6";
            this.dateTimePicker6.Size = new System.Drawing.Size(125, 20);
            this.dateTimePicker6.TabIndex = 144;
            this.dateTimePicker6.Value = new System.DateTime(2022, 4, 14, 0, 0, 10, 0);
            // 
            // textBox31
            // 
            this.textBox31.Location = new System.Drawing.Point(762, 455);
            this.textBox31.Multiline = true;
            this.textBox31.Name = "textBox31";
            this.textBox31.ReadOnly = true;
            this.textBox31.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox31.Size = new System.Drawing.Size(303, 213);
            this.textBox31.TabIndex = 143;
            // 
            // groupBox17
            // 
            this.groupBox17.Controls.Add(this.label90);
            this.groupBox17.Controls.Add(this.textBox12);
            this.groupBox17.Controls.Add(this.button94);
            this.groupBox17.Controls.Add(this.button91);
            this.groupBox17.Controls.Add(this.button89);
            this.groupBox17.Controls.Add(this.button90);
            this.groupBox17.Controls.Add(this.button92);
            this.groupBox17.Controls.Add(this.button93);
            this.groupBox17.Controls.Add(this.button95);
            this.groupBox17.Controls.Add(this.button96);
            this.groupBox17.Controls.Add(this.button97);
            this.groupBox17.Controls.Add(this.button98);
            this.groupBox17.Controls.Add(this.comboBox33);
            this.groupBox17.Location = new System.Drawing.Point(756, 325);
            this.groupBox17.Name = "groupBox17";
            this.groupBox17.Size = new System.Drawing.Size(303, 124);
            this.groupBox17.TabIndex = 142;
            this.groupBox17.TabStop = false;
            this.groupBox17.Text = "modulo GSM Local";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Location = new System.Drawing.Point(185, 61);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(58, 13);
            this.label90.TabIndex = 134;
            this.label90.Text = "Send cmd:";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(188, 74);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(115, 20);
            this.textBox12.TabIndex = 144;
            this.textBox12.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox12_KeyDown_1);
            // 
            // button94
            // 
            this.button94.Location = new System.Drawing.Point(100, 98);
            this.button94.Name = "button94";
            this.button94.Size = new System.Drawing.Size(82, 23);
            this.button94.TabIndex = 143;
            this.button94.Text = "Start Timer";
            this.button94.UseVisualStyleBackColor = true;
            this.button94.Click += new System.EventHandler(this.button94_Click);
            // 
            // button91
            // 
            this.button91.Location = new System.Drawing.Point(6, 98);
            this.button91.Name = "button91";
            this.button91.Size = new System.Drawing.Size(88, 23);
            this.button91.TabIndex = 142;
            this.button91.Text = "Clear READ";
            this.button91.UseVisualStyleBackColor = true;
            this.button91.Click += new System.EventHandler(this.button91_Click);
            // 
            // button89
            // 
            this.button89.Location = new System.Drawing.Point(100, 47);
            this.button89.Name = "button89";
            this.button89.Size = new System.Drawing.Size(82, 23);
            this.button89.TabIndex = 141;
            this.button89.Text = "Battery";
            this.button89.UseVisualStyleBackColor = true;
            this.button89.Click += new System.EventHandler(this.button89_Click);
            // 
            // button90
            // 
            this.button90.Location = new System.Drawing.Point(100, 72);
            this.button90.Name = "button90";
            this.button90.Size = new System.Drawing.Size(82, 23);
            this.button90.TabIndex = 140;
            this.button90.Text = "List ALL SMS";
            this.button90.UseVisualStyleBackColor = true;
            this.button90.Click += new System.EventHandler(this.button90_Click);
            // 
            // button92
            // 
            this.button92.Location = new System.Drawing.Point(6, 72);
            this.button92.Name = "button92";
            this.button92.Size = new System.Drawing.Size(88, 23);
            this.button92.TabIndex = 138;
            this.button92.Text = "List UNREAD";
            this.button92.UseVisualStyleBackColor = true;
            this.button92.Click += new System.EventHandler(this.button92_Click);
            // 
            // button93
            // 
            this.button93.Location = new System.Drawing.Point(6, 47);
            this.button93.Name = "button93";
            this.button93.Size = new System.Drawing.Size(88, 23);
            this.button93.TabIndex = 137;
            this.button93.Text = "Signal Qlty";
            this.button93.UseVisualStyleBackColor = true;
            this.button93.Click += new System.EventHandler(this.button93_Click);
            // 
            // button95
            // 
            this.button95.Location = new System.Drawing.Point(244, 99);
            this.button95.Name = "button95";
            this.button95.Size = new System.Drawing.Size(59, 23);
            this.button95.TabIndex = 135;
            this.button95.Text = "clear";
            this.button95.UseVisualStyleBackColor = true;
            this.button95.Click += new System.EventHandler(this.button95_Click);
            // 
            // button96
            // 
            this.button96.Location = new System.Drawing.Point(6, 19);
            this.button96.Name = "button96";
            this.button96.Size = new System.Drawing.Size(73, 23);
            this.button96.TabIndex = 124;
            this.button96.Text = "Get Ports";
            this.button96.UseVisualStyleBackColor = true;
            this.button96.Click += new System.EventHandler(this.button96_Click);
            // 
            // button97
            // 
            this.button97.Location = new System.Drawing.Point(164, 18);
            this.button97.Name = "button97";
            this.button97.Size = new System.Drawing.Size(59, 23);
            this.button97.TabIndex = 117;
            this.button97.Text = "Conectar";
            this.button97.UseVisualStyleBackColor = true;
            this.button97.Click += new System.EventHandler(this.button97_Click);
            // 
            // button98
            // 
            this.button98.Location = new System.Drawing.Point(227, 18);
            this.button98.Name = "button98";
            this.button98.Size = new System.Drawing.Size(76, 23);
            this.button98.TabIndex = 118;
            this.button98.Text = "Desconectar";
            this.button98.UseVisualStyleBackColor = true;
            this.button98.Click += new System.EventHandler(this.button98_Click);
            // 
            // comboBox33
            // 
            this.comboBox33.FormattingEnabled = true;
            this.comboBox33.Location = new System.Drawing.Point(85, 20);
            this.comboBox33.Name = "comboBox33";
            this.comboBox33.Size = new System.Drawing.Size(73, 21);
            this.comboBox33.TabIndex = 125;
            this.comboBox33.SelectedIndexChanged += new System.EventHandler(this.comboBox33_SelectedIndexChanged);
            // 
            // groupBox16
            // 
            this.groupBox16.Controls.Add(this.label91);
            this.groupBox16.Controls.Add(this.textBox33);
            this.groupBox16.Controls.Add(this.button116);
            this.groupBox16.Controls.Add(this.button86);
            this.groupBox16.Controls.Add(this.numericUpDown6);
            this.groupBox16.Controls.Add(this.button115);
            this.groupBox16.Controls.Add(this.button114);
            this.groupBox16.Controls.Add(this.button113);
            this.groupBox16.Controls.Add(this.button88);
            this.groupBox16.Controls.Add(this.button84);
            this.groupBox16.Controls.Add(this.button83);
            this.groupBox16.Controls.Add(this.button80);
            this.groupBox16.Controls.Add(this.button77);
            this.groupBox16.Controls.Add(this.button79);
            this.groupBox16.Controls.Add(this.button81);
            this.groupBox16.Controls.Add(this.comboBox20);
            this.groupBox16.Location = new System.Drawing.Point(447, 325);
            this.groupBox16.Name = "groupBox16";
            this.groupBox16.Size = new System.Drawing.Size(303, 124);
            this.groupBox16.TabIndex = 137;
            this.groupBox16.TabStop = false;
            this.groupBox16.Text = "modulo Lector Remoto (RFID/EEPROM)";
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Location = new System.Drawing.Point(3, 83);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(58, 13);
            this.label91.TabIndex = 145;
            this.label91.Text = "Send cmd:";
            // 
            // textBox33
            // 
            this.textBox33.Location = new System.Drawing.Point(0, 99);
            this.textBox33.Name = "textBox33";
            this.textBox33.Size = new System.Drawing.Size(83, 20);
            this.textBox33.TabIndex = 145;
            this.textBox33.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox33_KeyDown);
            // 
            // button116
            // 
            this.button116.Location = new System.Drawing.Point(89, 72);
            this.button116.Name = "button116";
            this.button116.Size = new System.Drawing.Size(73, 23);
            this.button116.TabIndex = 147;
            this.button116.Text = "Dump Rec";
            this.button116.UseVisualStyleBackColor = true;
            this.button116.Click += new System.EventHandler(this.button116_Click);
            // 
            // button86
            // 
            this.button86.Location = new System.Drawing.Point(164, 71);
            this.button86.Name = "button86";
            this.button86.Size = new System.Drawing.Size(34, 23);
            this.button86.TabIndex = 146;
            this.button86.Text = "Write Rec";
            this.button86.UseVisualStyleBackColor = true;
            this.button86.Click += new System.EventHandler(this.button86_Click_1);
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.Location = new System.Drawing.Point(166, 49);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            0});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(32, 20);
            this.numericUpDown6.TabIndex = 145;
            // 
            // button115
            // 
            this.button115.Location = new System.Drawing.Point(89, 97);
            this.button115.Name = "button115";
            this.button115.Size = new System.Drawing.Size(73, 23);
            this.button115.TabIndex = 144;
            this.button115.Text = "Erase Mem";
            this.button115.UseVisualStyleBackColor = true;
            this.button115.Click += new System.EventHandler(this.button115_Click);
            // 
            // button114
            // 
            this.button114.Location = new System.Drawing.Point(165, 98);
            this.button114.Name = "button114";
            this.button114.Size = new System.Drawing.Size(69, 23);
            this.button114.TabIndex = 143;
            this.button114.Text = "Full Dump";
            this.button114.UseVisualStyleBackColor = true;
            this.button114.Click += new System.EventHandler(this.button114_Click);
            // 
            // button113
            // 
            this.button113.Location = new System.Drawing.Point(90, 47);
            this.button113.Name = "button113";
            this.button113.Size = new System.Drawing.Size(73, 23);
            this.button113.TabIndex = 142;
            this.button113.Text = "Set Pc Nb";
            this.button113.UseVisualStyleBackColor = true;
            this.button113.Click += new System.EventHandler(this.button113_Click);
            // 
            // button88
            // 
            this.button88.Location = new System.Drawing.Point(230, 73);
            this.button88.Name = "button88";
            this.button88.Size = new System.Drawing.Size(73, 23);
            this.button88.TabIndex = 139;
            this.button88.Text = "Get Status";
            this.button88.UseVisualStyleBackColor = true;
            this.button88.Click += new System.EventHandler(this.button88_Click);
            // 
            // button84
            // 
            this.button84.Location = new System.Drawing.Point(5, 47);
            this.button84.Name = "button84";
            this.button84.Size = new System.Drawing.Size(74, 23);
            this.button84.TabIndex = 137;
            this.button84.Text = "Reset GSM";
            this.button84.UseVisualStyleBackColor = true;
            this.button84.Click += new System.EventHandler(this.button84_Click);
            // 
            // button83
            // 
            this.button83.Location = new System.Drawing.Point(230, 46);
            this.button83.Name = "button83";
            this.button83.Size = new System.Drawing.Size(73, 23);
            this.button83.TabIndex = 136;
            this.button83.Text = "Set Clock";
            this.button83.UseVisualStyleBackColor = true;
            this.button83.Click += new System.EventHandler(this.button83_Click);
            // 
            // button80
            // 
            this.button80.Location = new System.Drawing.Point(249, 98);
            this.button80.Name = "button80";
            this.button80.Size = new System.Drawing.Size(54, 23);
            this.button80.TabIndex = 135;
            this.button80.Text = "clear";
            this.button80.UseVisualStyleBackColor = true;
            this.button80.Click += new System.EventHandler(this.button80_Click);
            // 
            // button77
            // 
            this.button77.Location = new System.Drawing.Point(6, 19);
            this.button77.Name = "button77";
            this.button77.Size = new System.Drawing.Size(73, 23);
            this.button77.TabIndex = 124;
            this.button77.Text = "Get Ports";
            this.button77.UseVisualStyleBackColor = true;
            this.button77.Click += new System.EventHandler(this.button77_Click);
            // 
            // button79
            // 
            this.button79.Location = new System.Drawing.Point(164, 18);
            this.button79.Name = "button79";
            this.button79.Size = new System.Drawing.Size(59, 23);
            this.button79.TabIndex = 117;
            this.button79.Text = "Conectar";
            this.button79.UseVisualStyleBackColor = true;
            this.button79.Click += new System.EventHandler(this.button79_Click);
            // 
            // button81
            // 
            this.button81.Location = new System.Drawing.Point(227, 18);
            this.button81.Name = "button81";
            this.button81.Size = new System.Drawing.Size(76, 23);
            this.button81.TabIndex = 118;
            this.button81.Text = "Desconectar";
            this.button81.UseVisualStyleBackColor = true;
            this.button81.Click += new System.EventHandler(this.button81_Click);
            // 
            // comboBox20
            // 
            this.comboBox20.FormattingEnabled = true;
            this.comboBox20.Location = new System.Drawing.Point(83, 20);
            this.comboBox20.Name = "comboBox20";
            this.comboBox20.Size = new System.Drawing.Size(73, 21);
            this.comboBox20.TabIndex = 125;
            // 
            // textBox6
            // 
            this.textBox6.Location = new System.Drawing.Point(447, 455);
            this.textBox6.Multiline = true;
            this.textBox6.Name = "textBox6";
            this.textBox6.ReadOnly = true;
            this.textBox6.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox6.Size = new System.Drawing.Size(303, 213);
            this.textBox6.TabIndex = 140;
            // 
            // groupBox14
            // 
            this.groupBox14.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox14.Controls.Add(this.trackBar4);
            this.groupBox14.Controls.Add(this.label64);
            this.groupBox14.Controls.Add(this.button69);
            this.groupBox14.Controls.Add(this.button70);
            this.groupBox14.Controls.Add(this.button71);
            this.groupBox14.Controls.Add(this.button72);
            this.groupBox14.Controls.Add(this.button73);
            this.groupBox14.Controls.Add(this.button74);
            this.groupBox14.Controls.Add(this.textBox27);
            this.groupBox14.Controls.Add(this.label67);
            this.groupBox14.Controls.Add(this.comboBox28);
            this.groupBox14.Controls.Add(this.button76);
            this.groupBox14.Controls.Add(this.label66);
            this.groupBox14.Controls.Add(this.label65);
            this.groupBox14.Location = new System.Drawing.Point(756, 166);
            this.groupBox14.Name = "groupBox14";
            this.groupBox14.Size = new System.Drawing.Size(303, 158);
            this.groupBox14.TabIndex = 137;
            this.groupBox14.TabStop = false;
            this.groupBox14.Text = "Antena 4";
            // 
            // trackBar4
            // 
            this.trackBar4.LargeChange = 10;
            this.trackBar4.Location = new System.Drawing.Point(194, 73);
            this.trackBar4.Maximum = 100;
            this.trackBar4.Name = "trackBar4";
            this.trackBar4.Size = new System.Drawing.Size(103, 45);
            this.trackBar4.SmallChange = 10;
            this.trackBar4.TabIndex = 137;
            this.trackBar4.Scroll += new System.EventHandler(this.trackBar4_Scroll);
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Location = new System.Drawing.Point(6, 129);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(36, 13);
            this.label64.TabIndex = 133;
            this.label64.Text = "on/off";
            // 
            // button69
            // 
            this.button69.Location = new System.Drawing.Point(6, 19);
            this.button69.Name = "button69";
            this.button69.Size = new System.Drawing.Size(88, 23);
            this.button69.TabIndex = 124;
            this.button69.Text = "Get Ports";
            this.button69.UseVisualStyleBackColor = true;
            this.button69.Click += new System.EventHandler(this.button69_Click);
            // 
            // button70
            // 
            this.button70.Location = new System.Drawing.Point(101, 129);
            this.button70.Name = "button70";
            this.button70.Size = new System.Drawing.Size(88, 23);
            this.button70.TabIndex = 132;
            this.button70.Text = "Reset Antena";
            this.button70.UseVisualStyleBackColor = true;
            this.button70.Click += new System.EventHandler(this.button70_Click);
            // 
            // button71
            // 
            this.button71.Location = new System.Drawing.Point(194, 17);
            this.button71.Name = "button71";
            this.button71.Size = new System.Drawing.Size(103, 23);
            this.button71.TabIndex = 117;
            this.button71.Text = "Conectar Antena";
            this.button71.UseVisualStyleBackColor = true;
            this.button71.Click += new System.EventHandler(this.button71_Click);
            // 
            // button72
            // 
            this.button72.Location = new System.Drawing.Point(101, 101);
            this.button72.Name = "button72";
            this.button72.Size = new System.Drawing.Size(88, 23);
            this.button72.TabIndex = 131;
            this.button72.Text = "Start Reading";
            this.button72.UseVisualStyleBackColor = true;
            this.button72.Click += new System.EventHandler(this.button72_Click);
            // 
            // button73
            // 
            this.button73.Location = new System.Drawing.Point(194, 46);
            this.button73.Name = "button73";
            this.button73.Size = new System.Drawing.Size(103, 23);
            this.button73.TabIndex = 118;
            this.button73.Text = "Desconectar";
            this.button73.UseVisualStyleBackColor = true;
            this.button73.Click += new System.EventHandler(this.button73_Click);
            // 
            // button74
            // 
            this.button74.Location = new System.Drawing.Point(6, 102);
            this.button74.Name = "button74";
            this.button74.Size = new System.Drawing.Size(88, 23);
            this.button74.TabIndex = 130;
            this.button74.Text = "Stop Reading";
            this.button74.UseVisualStyleBackColor = true;
            this.button74.Click += new System.EventHandler(this.button74_Click);
            // 
            // textBox27
            // 
            this.textBox27.Location = new System.Drawing.Point(101, 75);
            this.textBox27.Name = "textBox27";
            this.textBox27.Size = new System.Drawing.Size(66, 20);
            this.textBox27.TabIndex = 129;
            this.textBox27.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox27_KeyDown);
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Location = new System.Drawing.Point(173, 78);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(15, 13);
            this.label67.TabIndex = 128;
            this.label67.Text = "%";
            // 
            // comboBox28
            // 
            this.comboBox28.FormattingEnabled = true;
            this.comboBox28.Location = new System.Drawing.Point(101, 19);
            this.comboBox28.Name = "comboBox28";
            this.comboBox28.Size = new System.Drawing.Size(87, 21);
            this.comboBox28.TabIndex = 125;
            // 
            // button76
            // 
            this.button76.Location = new System.Drawing.Point(6, 73);
            this.button76.Name = "button76";
            this.button76.Size = new System.Drawing.Size(88, 23);
            this.button76.TabIndex = 126;
            this.button76.Text = "Leer Potencia";
            this.button76.UseVisualStyleBackColor = true;
            this.button76.Click += new System.EventHandler(this.button76_Click);
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Location = new System.Drawing.Point(7, 51);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(39, 13);
            this.label66.TabIndex = 120;
            this.label66.Text = "estado";
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Location = new System.Drawing.Point(3, 51);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(43, 13);
            this.label65.TabIndex = 119;
            this.label65.Text = "Estado:";
            this.label65.UseWaitCursor = true;
            this.label65.Visible = false;
            // 
            // groupBox13
            // 
            this.groupBox13.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox13.Controls.Add(this.trackBar3);
            this.groupBox13.Controls.Add(this.label60);
            this.groupBox13.Controls.Add(this.button61);
            this.groupBox13.Controls.Add(this.button62);
            this.groupBox13.Controls.Add(this.button63);
            this.groupBox13.Controls.Add(this.button64);
            this.groupBox13.Controls.Add(this.button65);
            this.groupBox13.Controls.Add(this.button66);
            this.groupBox13.Controls.Add(this.textBox26);
            this.groupBox13.Controls.Add(this.label62);
            this.groupBox13.Controls.Add(this.label63);
            this.groupBox13.Controls.Add(this.comboBox27);
            this.groupBox13.Controls.Add(this.button68);
            this.groupBox13.Controls.Add(this.label61);
            this.groupBox13.Location = new System.Drawing.Point(447, 166);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(303, 158);
            this.groupBox13.TabIndex = 136;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Antena 3";
            this.groupBox13.Enter += new System.EventHandler(this.groupBox13_Enter);
            // 
            // trackBar3
            // 
            this.trackBar3.LargeChange = 10;
            this.trackBar3.Location = new System.Drawing.Point(194, 73);
            this.trackBar3.Maximum = 100;
            this.trackBar3.Name = "trackBar3";
            this.trackBar3.Size = new System.Drawing.Size(103, 45);
            this.trackBar3.SmallChange = 10;
            this.trackBar3.TabIndex = 136;
            this.trackBar3.Scroll += new System.EventHandler(this.trackBar3_Scroll);
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Location = new System.Drawing.Point(6, 129);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(36, 13);
            this.label60.TabIndex = 133;
            this.label60.Text = "on/off";
            // 
            // button61
            // 
            this.button61.Location = new System.Drawing.Point(6, 19);
            this.button61.Name = "button61";
            this.button61.Size = new System.Drawing.Size(88, 23);
            this.button61.TabIndex = 124;
            this.button61.Text = "Get Ports";
            this.button61.UseVisualStyleBackColor = true;
            this.button61.Click += new System.EventHandler(this.button61_Click);
            // 
            // button62
            // 
            this.button62.Location = new System.Drawing.Point(101, 129);
            this.button62.Name = "button62";
            this.button62.Size = new System.Drawing.Size(86, 23);
            this.button62.TabIndex = 132;
            this.button62.Text = "Reset Antena";
            this.button62.UseVisualStyleBackColor = true;
            this.button62.Click += new System.EventHandler(this.button62_Click);
            // 
            // button63
            // 
            this.button63.Location = new System.Drawing.Point(194, 17);
            this.button63.Name = "button63";
            this.button63.Size = new System.Drawing.Size(103, 23);
            this.button63.TabIndex = 117;
            this.button63.Text = "Conectar Antena";
            this.button63.UseVisualStyleBackColor = true;
            this.button63.Click += new System.EventHandler(this.button63_Click);
            // 
            // button64
            // 
            this.button64.Location = new System.Drawing.Point(101, 101);
            this.button64.Name = "button64";
            this.button64.Size = new System.Drawing.Size(88, 23);
            this.button64.TabIndex = 131;
            this.button64.Text = "Start Reading";
            this.button64.UseVisualStyleBackColor = true;
            this.button64.Click += new System.EventHandler(this.button64_Click);
            // 
            // button65
            // 
            this.button65.Location = new System.Drawing.Point(194, 46);
            this.button65.Name = "button65";
            this.button65.Size = new System.Drawing.Size(103, 23);
            this.button65.TabIndex = 118;
            this.button65.Text = "Desconectar";
            this.button65.UseVisualStyleBackColor = true;
            this.button65.Click += new System.EventHandler(this.button65_Click);
            // 
            // button66
            // 
            this.button66.Location = new System.Drawing.Point(6, 102);
            this.button66.Name = "button66";
            this.button66.Size = new System.Drawing.Size(88, 23);
            this.button66.TabIndex = 130;
            this.button66.Text = "Stop Reading";
            this.button66.UseVisualStyleBackColor = true;
            this.button66.Click += new System.EventHandler(this.button66_Click);
            // 
            // textBox26
            // 
            this.textBox26.Location = new System.Drawing.Point(101, 75);
            this.textBox26.Name = "textBox26";
            this.textBox26.Size = new System.Drawing.Size(66, 20);
            this.textBox26.TabIndex = 129;
            this.textBox26.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox26_KeyDown);
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Location = new System.Drawing.Point(6, 52);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(39, 13);
            this.label62.TabIndex = 120;
            this.label62.Text = "estado";
            this.label62.Click += new System.EventHandler(this.label62_Click);
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Location = new System.Drawing.Point(173, 78);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(15, 13);
            this.label63.TabIndex = 128;
            this.label63.Text = "%";
            // 
            // comboBox27
            // 
            this.comboBox27.FormattingEnabled = true;
            this.comboBox27.Location = new System.Drawing.Point(101, 19);
            this.comboBox27.Name = "comboBox27";
            this.comboBox27.Size = new System.Drawing.Size(87, 21);
            this.comboBox27.TabIndex = 125;
            // 
            // button68
            // 
            this.button68.Location = new System.Drawing.Point(6, 73);
            this.button68.Name = "button68";
            this.button68.Size = new System.Drawing.Size(88, 23);
            this.button68.TabIndex = 126;
            this.button68.Text = "Leer Potencia";
            this.button68.UseVisualStyleBackColor = true;
            this.button68.Click += new System.EventHandler(this.button68_Click);
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Location = new System.Drawing.Point(6, 52);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(43, 13);
            this.label61.TabIndex = 119;
            this.label61.Text = "Estado:";
            this.label61.Visible = false;
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox12.Controls.Add(this.trackBar2);
            this.groupBox12.Controls.Add(this.label47);
            this.groupBox12.Controls.Add(this.button53);
            this.groupBox12.Controls.Add(this.button54);
            this.groupBox12.Controls.Add(this.button55);
            this.groupBox12.Controls.Add(this.button56);
            this.groupBox12.Controls.Add(this.button57);
            this.groupBox12.Controls.Add(this.button58);
            this.groupBox12.Controls.Add(this.textBox25);
            this.groupBox12.Controls.Add(this.label49);
            this.groupBox12.Controls.Add(this.label59);
            this.groupBox12.Controls.Add(this.comboBox26);
            this.groupBox12.Controls.Add(this.button60);
            this.groupBox12.Controls.Add(this.label48);
            this.groupBox12.Location = new System.Drawing.Point(756, 6);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(303, 158);
            this.groupBox12.TabIndex = 135;
            this.groupBox12.TabStop = false;
            this.groupBox12.Text = "Antena 2";
            // 
            // trackBar2
            // 
            this.trackBar2.LargeChange = 10;
            this.trackBar2.Location = new System.Drawing.Point(194, 73);
            this.trackBar2.Maximum = 100;
            this.trackBar2.Name = "trackBar2";
            this.trackBar2.Size = new System.Drawing.Size(103, 45);
            this.trackBar2.SmallChange = 10;
            this.trackBar2.TabIndex = 135;
            this.trackBar2.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Location = new System.Drawing.Point(6, 129);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(36, 13);
            this.label47.TabIndex = 133;
            this.label47.Text = "on/off";
            // 
            // button53
            // 
            this.button53.Location = new System.Drawing.Point(6, 19);
            this.button53.Name = "button53";
            this.button53.Size = new System.Drawing.Size(88, 23);
            this.button53.TabIndex = 124;
            this.button53.Text = "Get Ports";
            this.button53.UseVisualStyleBackColor = true;
            this.button53.Click += new System.EventHandler(this.button53_Click);
            // 
            // button54
            // 
            this.button54.Location = new System.Drawing.Point(101, 128);
            this.button54.Name = "button54";
            this.button54.Size = new System.Drawing.Size(88, 23);
            this.button54.TabIndex = 132;
            this.button54.Text = "Reset Antena";
            this.button54.UseVisualStyleBackColor = true;
            this.button54.Click += new System.EventHandler(this.button54_Click);
            // 
            // button55
            // 
            this.button55.Location = new System.Drawing.Point(194, 17);
            this.button55.Name = "button55";
            this.button55.Size = new System.Drawing.Size(103, 23);
            this.button55.TabIndex = 117;
            this.button55.Text = "Conectar Antena";
            this.button55.UseVisualStyleBackColor = true;
            this.button55.Click += new System.EventHandler(this.button55_Click);
            // 
            // button56
            // 
            this.button56.Location = new System.Drawing.Point(101, 101);
            this.button56.Name = "button56";
            this.button56.Size = new System.Drawing.Size(88, 23);
            this.button56.TabIndex = 131;
            this.button56.Text = "Start Reading";
            this.button56.UseVisualStyleBackColor = true;
            this.button56.Click += new System.EventHandler(this.button56_Click);
            // 
            // button57
            // 
            this.button57.Location = new System.Drawing.Point(194, 46);
            this.button57.Name = "button57";
            this.button57.Size = new System.Drawing.Size(103, 23);
            this.button57.TabIndex = 118;
            this.button57.Text = "Desconectar";
            this.button57.UseVisualStyleBackColor = true;
            this.button57.Click += new System.EventHandler(this.button57_Click);
            // 
            // button58
            // 
            this.button58.Location = new System.Drawing.Point(6, 102);
            this.button58.Name = "button58";
            this.button58.Size = new System.Drawing.Size(88, 23);
            this.button58.TabIndex = 130;
            this.button58.Text = "Stop Reading";
            this.button58.UseVisualStyleBackColor = true;
            this.button58.Click += new System.EventHandler(this.button58_Click);
            // 
            // textBox25
            // 
            this.textBox25.Location = new System.Drawing.Point(101, 75);
            this.textBox25.Name = "textBox25";
            this.textBox25.Size = new System.Drawing.Size(66, 20);
            this.textBox25.TabIndex = 129;
            this.textBox25.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox25_KeyDown);
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Location = new System.Drawing.Point(7, 51);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(39, 13);
            this.label49.TabIndex = 120;
            this.label49.Text = "estado";
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Location = new System.Drawing.Point(173, 78);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(15, 13);
            this.label59.TabIndex = 128;
            this.label59.Text = "%";
            // 
            // comboBox26
            // 
            this.comboBox26.FormattingEnabled = true;
            this.comboBox26.Location = new System.Drawing.Point(101, 19);
            this.comboBox26.Name = "comboBox26";
            this.comboBox26.Size = new System.Drawing.Size(87, 21);
            this.comboBox26.TabIndex = 125;
            // 
            // button60
            // 
            this.button60.Location = new System.Drawing.Point(6, 73);
            this.button60.Name = "button60";
            this.button60.Size = new System.Drawing.Size(88, 23);
            this.button60.TabIndex = 126;
            this.button60.Text = "Leer Potencia";
            this.button60.UseVisualStyleBackColor = true;
            this.button60.Click += new System.EventHandler(this.button60_Click);
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Location = new System.Drawing.Point(3, 51);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(43, 13);
            this.label48.TabIndex = 119;
            this.label48.Text = "Estado:";
            this.label48.Visible = false;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.label118);
            this.groupBox5.Controls.Add(this.comboBox49);
            this.groupBox5.Controls.Add(this.label117);
            this.groupBox5.Controls.Add(this.comboBox48);
            this.groupBox5.Controls.Add(this.label116);
            this.groupBox5.Controls.Add(this.comboBox47);
            this.groupBox5.Controls.Add(this.comboBox46);
            this.groupBox5.Controls.Add(this.label115);
            this.groupBox5.Controls.Add(this.label46);
            this.groupBox5.Controls.Add(this.label58);
            this.groupBox5.Controls.Add(this.label52);
            this.groupBox5.Controls.Add(this.comboBox35);
            this.groupBox5.Controls.Add(this.label57);
            this.groupBox5.Controls.Add(this.comboBox22);
            this.groupBox5.Controls.Add(this.comboBox21);
            this.groupBox5.Controls.Add(this.cbImportEvent);
            this.groupBox5.Controls.Add(this.label41);
            this.groupBox5.Controls.Add(this.label40);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.label33);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.comboBox18);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Controls.Add(this.comboBox19);
            this.groupBox5.Controls.Add(this.label38);
            this.groupBox5.Controls.Add(this.comboBox16);
            this.groupBox5.Controls.Add(this.label39);
            this.groupBox5.Controls.Add(this.comboBox17);
            this.groupBox5.Controls.Add(this.comboBox14);
            this.groupBox5.Controls.Add(this.comboBox15);
            this.groupBox5.Controls.Add(this.comboBox13);
            this.groupBox5.Controls.Add(this.button7);
            this.groupBox5.Controls.Add(this.label32);
            this.groupBox5.Controls.Add(this.comboBox12);
            this.groupBox5.Controls.Add(this.button27);
            this.groupBox5.Location = new System.Drawing.Point(3, 6);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(214, 553);
            this.groupBox5.TabIndex = 139;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Importar Tabla";
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Location = new System.Drawing.Point(13, 398);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(27, 13);
            this.label118.TabIndex = 155;
            this.label118.Text = "chip";
            // 
            // comboBox49
            // 
            this.comboBox49.FormattingEnabled = true;
            this.comboBox49.Location = new System.Drawing.Point(100, 395);
            this.comboBox49.Name = "comboBox49";
            this.comboBox49.Size = new System.Drawing.Size(100, 21);
            this.comboBox49.TabIndex = 154;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Location = new System.Drawing.Point(13, 131);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(44, 13);
            this.label117.TabIndex = 114;
            this.label117.Text = "Apellido";
            // 
            // comboBox48
            // 
            this.comboBox48.FormattingEnabled = true;
            this.comboBox48.Location = new System.Drawing.Point(100, 128);
            this.comboBox48.Name = "comboBox48";
            this.comboBox48.Size = new System.Drawing.Size(100, 21);
            this.comboBox48.TabIndex = 115;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Location = new System.Drawing.Point(13, 475);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(51, 13);
            this.label116.TabIndex = 113;
            this.label116.Text = "id evento";
            // 
            // comboBox47
            // 
            this.comboBox47.FormattingEnabled = true;
            this.comboBox47.Location = new System.Drawing.Point(100, 472);
            this.comboBox47.Name = "comboBox47";
            this.comboBox47.Size = new System.Drawing.Size(100, 21);
            this.comboBox47.TabIndex = 112;
            // 
            // comboBox46
            // 
            this.comboBox46.FormattingEnabled = true;
            this.comboBox46.Location = new System.Drawing.Point(100, 445);
            this.comboBox46.Name = "comboBox46";
            this.comboBox46.Size = new System.Drawing.Size(100, 21);
            this.comboBox46.TabIndex = 111;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Location = new System.Drawing.Point(13, 446);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(59, 13);
            this.label115.TabIndex = 110;
            this.label115.Text = "confirmado";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Location = new System.Drawing.Point(13, 502);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(65, 13);
            this.label46.TabIndex = 109;
            this.label46.Text = "campo extra";
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Location = new System.Drawing.Point(13, 421);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(52, 13);
            this.label58.TabIndex = 107;
            this.label58.Text = "Categoria";
            this.label58.Click += new System.EventHandler(this.label58_Click);
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Location = new System.Drawing.Point(73, 45);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(73, 13);
            this.label52.TabIndex = 105;
            this.label52.Text = "Tabla Destino";
            // 
            // comboBox35
            // 
            this.comboBox35.FormattingEnabled = true;
            this.comboBox35.Location = new System.Drawing.Point(100, 499);
            this.comboBox35.Name = "comboBox35";
            this.comboBox35.Size = new System.Drawing.Size(100, 21);
            this.comboBox35.TabIndex = 108;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Location = new System.Drawing.Point(13, 347);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(44, 13);
            this.label57.TabIndex = 104;
            this.label57.Text = "Numero";
            // 
            // comboBox22
            // 
            this.comboBox22.FormattingEnabled = true;
            this.comboBox22.Location = new System.Drawing.Point(100, 418);
            this.comboBox22.Name = "comboBox22";
            this.comboBox22.Size = new System.Drawing.Size(100, 21);
            this.comboBox22.TabIndex = 106;
            this.comboBox22.SelectedIndexChanged += new System.EventHandler(this.comboBox22_SelectedIndexChanged);
            // 
            // comboBox21
            // 
            this.comboBox21.FormattingEnabled = true;
            this.comboBox21.Location = new System.Drawing.Point(100, 344);
            this.comboBox21.Name = "comboBox21";
            this.comboBox21.Size = new System.Drawing.Size(100, 21);
            this.comboBox21.TabIndex = 103;
            this.comboBox21.SelectedIndexChanged += new System.EventHandler(this.comboBox21_SelectedIndexChanged_1);
            // 
            // cbImportEvent
            // 
            this.cbImportEvent.FormattingEnabled = true;
            this.cbImportEvent.Items.AddRange(new object[] {
            "Listado General",
            "Finishers"});
            this.cbImportEvent.Location = new System.Drawing.Point(16, 59);
            this.cbImportEvent.Name = "cbImportEvent";
            this.cbImportEvent.Size = new System.Drawing.Size(184, 21);
            this.cbImportEvent.TabIndex = 102;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(13, 96);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(71, 13);
            this.label41.TabIndex = 101;
            this.label41.Text = "Columnas DB";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(13, 320);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(34, 13);
            this.label40.TabIndex = 100;
            this.label40.Text = "Team";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(13, 293);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(31, 13);
            this.label36.TabIndex = 96;
            this.label36.Text = "Sexo";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(13, 239);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(58, 13);
            this.label35.TabIndex = 97;
            this.label35.Text = "nacimiento";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(13, 266);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(66, 13);
            this.label33.TabIndex = 92;
            this.label33.Text = "procedencia";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(13, 374);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(44, 13);
            this.label34.TabIndex = 93;
            this.label34.Text = "team_id";
            // 
            // comboBox18
            // 
            this.comboBox18.FormattingEnabled = true;
            this.comboBox18.Location = new System.Drawing.Point(100, 317);
            this.comboBox18.Name = "comboBox18";
            this.comboBox18.Size = new System.Drawing.Size(100, 21);
            this.comboBox18.TabIndex = 99;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(13, 212);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(31, 13);
            this.label37.TabIndex = 95;
            this.label37.Text = "email";
            // 
            // comboBox19
            // 
            this.comboBox19.FormattingEnabled = true;
            this.comboBox19.Location = new System.Drawing.Point(100, 290);
            this.comboBox19.Name = "comboBox19";
            this.comboBox19.Size = new System.Drawing.Size(100, 21);
            this.comboBox19.TabIndex = 98;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(13, 185);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(26, 13);
            this.label38.TabIndex = 94;
            this.label38.Text = "DNI";
            // 
            // comboBox16
            // 
            this.comboBox16.FormattingEnabled = true;
            this.comboBox16.Location = new System.Drawing.Point(100, 263);
            this.comboBox16.Name = "comboBox16";
            this.comboBox16.Size = new System.Drawing.Size(100, 21);
            this.comboBox16.TabIndex = 97;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(13, 158);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(44, 13);
            this.label39.TabIndex = 93;
            this.label39.Text = "Nombre";
            // 
            // comboBox17
            // 
            this.comboBox17.FormattingEnabled = true;
            this.comboBox17.Location = new System.Drawing.Point(100, 236);
            this.comboBox17.Name = "comboBox17";
            this.comboBox17.Size = new System.Drawing.Size(100, 21);
            this.comboBox17.TabIndex = 96;
            // 
            // comboBox14
            // 
            this.comboBox14.FormattingEnabled = true;
            this.comboBox14.Location = new System.Drawing.Point(100, 209);
            this.comboBox14.Name = "comboBox14";
            this.comboBox14.Size = new System.Drawing.Size(100, 21);
            this.comboBox14.TabIndex = 95;
            // 
            // comboBox15
            // 
            this.comboBox15.FormattingEnabled = true;
            this.comboBox15.Location = new System.Drawing.Point(100, 182);
            this.comboBox15.Name = "comboBox15";
            this.comboBox15.Size = new System.Drawing.Size(100, 21);
            this.comboBox15.TabIndex = 94;
            this.comboBox15.SelectedIndexChanged += new System.EventHandler(this.comboBox15_SelectedIndexChanged);
            // 
            // comboBox13
            // 
            this.comboBox13.FormattingEnabled = true;
            this.comboBox13.Location = new System.Drawing.Point(100, 155);
            this.comboBox13.Name = "comboBox13";
            this.comboBox13.Size = new System.Drawing.Size(100, 21);
            this.comboBox13.TabIndex = 93;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(16, 524);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(184, 23);
            this.button7.TabIndex = 90;
            this.button7.Text = "Importar Tabla";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(97, 96);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(103, 13);
            this.label32.TabIndex = 92;
            this.label32.Text = "Columnas a Importar";
            // 
            // comboBox12
            // 
            this.comboBox12.FormattingEnabled = true;
            this.comboBox12.Location = new System.Drawing.Point(100, 371);
            this.comboBox12.Name = "comboBox12";
            this.comboBox12.Size = new System.Drawing.Size(100, 21);
            this.comboBox12.TabIndex = 92;
            // 
            // button27
            // 
            this.button27.Location = new System.Drawing.Point(16, 19);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(184, 25);
            this.button27.TabIndex = 91;
            this.button27.Text = "Abrir Tabla (xls)";
            this.button27.UseVisualStyleBackColor = true;
            this.button27.Click += new System.EventHandler(this.button27_Click);
            // 
            // groupBox10
            // 
            this.groupBox10.Controls.Add(this.label125);
            this.groupBox10.Controls.Add(this.checkBox15);
            this.groupBox10.Controls.Add(this.label56);
            this.groupBox10.Controls.Add(this.textBox22);
            this.groupBox10.Controls.Add(this.label55);
            this.groupBox10.Controls.Add(this.label54);
            this.groupBox10.Controls.Add(this.textBox21);
            this.groupBox10.Controls.Add(this.textBox20);
            this.groupBox10.Controls.Add(this.button38);
            this.groupBox10.Controls.Add(this.button37);
            this.groupBox10.Enabled = false;
            this.groupBox10.Location = new System.Drawing.Point(227, 6);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Size = new System.Drawing.Size(214, 158);
            this.groupBox10.TabIndex = 138;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Editor de ID (Antena 1)";
            // 
            // label125
            // 
            this.label125.AutoSize = true;
            this.label125.Location = new System.Drawing.Point(126, 25);
            this.label125.Name = "label125";
            this.label125.Size = new System.Drawing.Size(52, 13);
            this.label125.TabIndex = 141;
            this.label125.Text = "READER";
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Location = new System.Drawing.Point(113, 135);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Size = new System.Drawing.Size(96, 17);
            this.checkBox15.TabIndex = 140;
            this.checkBox15.Text = "auto increment";
            this.checkBox15.UseVisualStyleBackColor = true;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Location = new System.Drawing.Point(6, 81);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(59, 13);
            this.label56.TabIndex = 139;
            this.label56.Text = "read (DEC)";
            // 
            // textBox22
            // 
            this.textBox22.Enabled = false;
            this.textBox22.Location = new System.Drawing.Point(6, 96);
            this.textBox22.Name = "textBox22";
            this.textBox22.Size = new System.Drawing.Size(202, 20);
            this.textBox22.TabIndex = 138;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Location = new System.Drawing.Point(6, 119);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(101, 13);
            this.label55.TabIndex = 137;
            this.label55.Text = "value to write (HEX)";
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Location = new System.Drawing.Point(6, 44);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(88, 13);
            this.label54.TabIndex = 134;
            this.label54.Text = "read value (HEX)";
            // 
            // textBox21
            // 
            this.textBox21.Location = new System.Drawing.Point(6, 134);
            this.textBox21.Name = "textBox21";
            this.textBox21.Size = new System.Drawing.Size(101, 20);
            this.textBox21.TabIndex = 136;
            this.textBox21.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox21_KeyDown);
            // 
            // textBox20
            // 
            this.textBox20.Enabled = false;
            this.textBox20.Location = new System.Drawing.Point(6, 58);
            this.textBox20.Name = "textBox20";
            this.textBox20.Size = new System.Drawing.Size(202, 20);
            this.textBox20.TabIndex = 134;
            // 
            // button38
            // 
            this.button38.Location = new System.Drawing.Point(56, 19);
            this.button38.Name = "button38";
            this.button38.Size = new System.Drawing.Size(51, 23);
            this.button38.TabIndex = 2;
            this.button38.Text = "Write Tag";
            this.button38.UseVisualStyleBackColor = true;
            this.button38.Click += new System.EventHandler(this.button38_Click);
            // 
            // button37
            // 
            this.button37.Location = new System.Drawing.Point(6, 19);
            this.button37.Name = "button37";
            this.button37.Size = new System.Drawing.Size(44, 23);
            this.button37.TabIndex = 1;
            this.button37.Text = "Read Tag";
            this.button37.UseVisualStyleBackColor = true;
            this.button37.Click += new System.EventHandler(this.button37_Click);
            // 
            // button35
            // 
            this.button35.Location = new System.Drawing.Point(411, 170);
            this.button35.Name = "button35";
            this.button35.Size = new System.Drawing.Size(17, 114);
            this.button35.TabIndex = 137;
            this.button35.Text = "viewer";
            this.button35.UseVisualStyleBackColor = true;
            this.button35.Click += new System.EventHandler(this.button35_Click);
            // 
            // dataGridView5
            // 
            this.dataGridView5.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView5.Location = new System.Drawing.Point(3, 617);
            this.dataGridView5.Name = "dataGridView5";
            this.dataGridView5.Size = new System.Drawing.Size(402, 51);
            this.dataGridView5.TabIndex = 136;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.button111);
            this.groupBox7.Controls.Add(this.comboBox34);
            this.groupBox7.Controls.Add(this.button34);
            this.groupBox7.Controls.Add(this.button1);
            this.groupBox7.Controls.Add(this.button10);
            this.groupBox7.Controls.Add(this.button22);
            this.groupBox7.Controls.Add(this.button11);
            this.groupBox7.Controls.Add(this.button14);
            this.groupBox7.Location = new System.Drawing.Point(227, 166);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(178, 225);
            this.groupBox7.TabIndex = 135;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Database";
            // 
            // button111
            // 
            this.button111.Location = new System.Drawing.Point(6, 197);
            this.button111.Name = "button111";
            this.button111.Size = new System.Drawing.Size(164, 23);
            this.button111.TabIndex = 118;
            this.button111.Text = "reset evento";
            this.button111.UseVisualStyleBackColor = true;
            this.button111.Click += new System.EventHandler(this.button111_Click);
            // 
            // comboBox34
            // 
            this.comboBox34.FormattingEnabled = true;
            this.comboBox34.Location = new System.Drawing.Point(6, 42);
            this.comboBox34.Name = "comboBox34";
            this.comboBox34.Size = new System.Drawing.Size(164, 21);
            this.comboBox34.TabIndex = 117;
            this.comboBox34.SelectedIndexChanged += new System.EventHandler(this.comboBox34_SelectedIndexChanged);
            // 
            // button34
            // 
            this.button34.Location = new System.Drawing.Point(6, 145);
            this.button34.Name = "button34";
            this.button34.Size = new System.Drawing.Size(164, 23);
            this.button34.TabIndex = 116;
            this.button34.Text = "vaciar log";
            this.button34.UseVisualStyleBackColor = true;
            this.button34.Click += new System.EventHandler(this.button34_Click);
            // 
            // button1
            // 
            this.button1.Enabled = false;
            this.button1.Location = new System.Drawing.Point(6, 16);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(164, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "vaciar tabla corredores";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(6, 171);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(164, 23);
            this.button10.TabIndex = 1;
            this.button10.Text = "vaciar tabla eventos";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // button22
            // 
            this.button22.Location = new System.Drawing.Point(6, 119);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(164, 23);
            this.button22.TabIndex = 115;
            this.button22.Text = "vaciar tabla resultados";
            this.button22.UseVisualStyleBackColor = true;
            this.button22.Click += new System.EventHandler(this.button22_Click);
            // 
            // button11
            // 
            this.button11.Location = new System.Drawing.Point(6, 67);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(164, 23);
            this.button11.TabIndex = 2;
            this.button11.Text = "vaciar tabla categorias";
            this.button11.UseVisualStyleBackColor = true;
            this.button11.Click += new System.EventHandler(this.button11_Click);
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(6, 93);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(164, 23);
            this.button14.TabIndex = 3;
            this.button14.Text = "vaciar tabla finishers";
            this.button14.UseVisualStyleBackColor = true;
            this.button14.Click += new System.EventHandler(this.button14_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.groupBox6.Controls.Add(this.trackBar1);
            this.groupBox6.Controls.Add(this.button18);
            this.groupBox6.Controls.Add(this.button29);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.button23);
            this.groupBox6.Controls.Add(this.button28);
            this.groupBox6.Controls.Add(this.button24);
            this.groupBox6.Controls.Add(this.button21);
            this.groupBox6.Controls.Add(this.textBox17);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.comboBox10);
            this.groupBox6.Controls.Add(this.button19);
            this.groupBox6.Controls.Add(this.label29);
            this.groupBox6.Controls.Add(this.label28);
            this.groupBox6.Location = new System.Drawing.Point(447, 6);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(303, 158);
            this.groupBox6.TabIndex = 134;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Antena 1";
            this.groupBox6.Enter += new System.EventHandler(this.groupBox6_Enter);
            // 
            // trackBar1
            // 
            this.trackBar1.LargeChange = 10;
            this.trackBar1.Location = new System.Drawing.Point(194, 75);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.Size = new System.Drawing.Size(103, 45);
            this.trackBar1.SmallChange = 10;
            this.trackBar1.TabIndex = 134;
            this.trackBar1.TickFrequency = 10;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            this.trackBar1.TabIndexChanged += new System.EventHandler(this.trackBar1_TabIndexChanged);
            // 
            // button18
            // 
            this.button18.Location = new System.Drawing.Point(6, 19);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(88, 23);
            this.button18.TabIndex = 124;
            this.button18.Text = "Get Ports";
            this.button18.UseVisualStyleBackColor = true;
            this.button18.Click += new System.EventHandler(this.button18_Click_2);
            // 
            // button29
            // 
            this.button29.Location = new System.Drawing.Point(100, 128);
            this.button29.Name = "button29";
            this.button29.Size = new System.Drawing.Size(87, 23);
            this.button29.TabIndex = 132;
            this.button29.Text = "Reset Antena";
            this.button29.UseVisualStyleBackColor = true;
            this.button29.Click += new System.EventHandler(this.button29_Click_1);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 131);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(36, 13);
            this.label20.TabIndex = 133;
            this.label20.Text = "on/off";
            // 
            // button23
            // 
            this.button23.Location = new System.Drawing.Point(194, 17);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(103, 23);
            this.button23.TabIndex = 117;
            this.button23.Text = "Conectar Antena";
            this.button23.UseVisualStyleBackColor = true;
            this.button23.Click += new System.EventHandler(this.button23_Click_1);
            // 
            // button28
            // 
            this.button28.Location = new System.Drawing.Point(100, 101);
            this.button28.Name = "button28";
            this.button28.Size = new System.Drawing.Size(88, 23);
            this.button28.TabIndex = 131;
            this.button28.Text = "Start Reading";
            this.button28.UseVisualStyleBackColor = true;
            this.button28.Click += new System.EventHandler(this.button28_Click_1);
            // 
            // button24
            // 
            this.button24.Location = new System.Drawing.Point(194, 46);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(103, 23);
            this.button24.TabIndex = 118;
            this.button24.Text = "Desconectar";
            this.button24.UseVisualStyleBackColor = true;
            this.button24.Click += new System.EventHandler(this.button24_Click_1);
            // 
            // button21
            // 
            this.button21.Location = new System.Drawing.Point(5, 102);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(88, 23);
            this.button21.TabIndex = 130;
            this.button21.Text = "Stop Reading";
            this.button21.UseVisualStyleBackColor = true;
            this.button21.Click += new System.EventHandler(this.button21_Click_1);
            // 
            // textBox17
            // 
            this.textBox17.Location = new System.Drawing.Point(100, 75);
            this.textBox17.Name = "textBox17";
            this.textBox17.Size = new System.Drawing.Size(66, 20);
            this.textBox17.TabIndex = 129;
            this.textBox17.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox17_KeyDown);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(172, 78);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(15, 13);
            this.label26.TabIndex = 128;
            this.label26.Text = "%";
            // 
            // comboBox10
            // 
            this.comboBox10.FormattingEnabled = true;
            this.comboBox10.Location = new System.Drawing.Point(101, 19);
            this.comboBox10.Name = "comboBox10";
            this.comboBox10.Size = new System.Drawing.Size(87, 21);
            this.comboBox10.TabIndex = 125;
            this.comboBox10.SelectedIndexChanged += new System.EventHandler(this.comboBox10_SelectedIndexChanged_1);
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(5, 73);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(88, 23);
            this.button19.TabIndex = 126;
            this.button19.Text = "Leer Potencia";
            this.button19.UseVisualStyleBackColor = true;
            this.button19.Click += new System.EventHandler(this.button19_Click);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(6, 52);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(39, 13);
            this.label29.TabIndex = 120;
            this.label29.Text = "estado";
            this.label29.TextChanged += new System.EventHandler(this.label29_TextChanged);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(3, 51);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(43, 13);
            this.label28.TabIndex = 119;
            this.label28.Text = "Estado:";
            this.label28.Visible = false;
            // 
            // campeonatos
            // 
            this.campeonatos.Controls.Add(this.label139);
            this.campeonatos.Controls.Add(this.textBox51);
            this.campeonatos.Controls.Add(this.groupBox21);
            this.campeonatos.Controls.Add(this.groupBox20);
            this.campeonatos.Controls.Add(this.groupBox19);
            this.campeonatos.Controls.Add(this.dataGridView8);
            this.campeonatos.Controls.Add(this.dataGridView7);
            this.campeonatos.Location = new System.Drawing.Point(4, 22);
            this.campeonatos.Name = "campeonatos";
            this.campeonatos.Padding = new System.Windows.Forms.Padding(3);
            this.campeonatos.Size = new System.Drawing.Size(1358, 677);
            this.campeonatos.TabIndex = 6;
            this.campeonatos.Text = "Campeonatos";
            this.campeonatos.UseVisualStyleBackColor = true;
            this.campeonatos.Click += new System.EventHandler(this.tabPage6_Click);
            // 
            // groupBox21
            // 
            this.groupBox21.Controls.Add(this.label100);
            this.groupBox21.Controls.Add(this.numericUpDown8);
            this.groupBox21.Controls.Add(this.checkBox5);
            this.groupBox21.Controls.Add(this.button109);
            this.groupBox21.Controls.Add(this.comboBox39);
            this.groupBox21.Controls.Add(this.label88);
            this.groupBox21.Controls.Add(this.checkBox10);
            this.groupBox21.Controls.Add(this.checkBox9);
            this.groupBox21.Controls.Add(this.button103);
            this.groupBox21.Location = new System.Drawing.Point(7, 80);
            this.groupBox21.Name = "groupBox21";
            this.groupBox21.Size = new System.Drawing.Size(396, 94);
            this.groupBox21.TabIndex = 162;
            this.groupBox21.TabStop = false;
            this.groupBox21.Text = "Resolusion Final Campeonato";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Location = new System.Drawing.Point(361, 20);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(34, 13);
            this.label100.TabIndex = 160;
            this.label100.Text = "fecha";
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.Location = new System.Drawing.Point(325, 18);
            this.numericUpDown8.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(35, 20);
            this.numericUpDown8.TabIndex = 160;
            this.numericUpDown8.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Location = new System.Drawing.Point(263, 19);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Size = new System.Drawing.Size(68, 17);
            this.checkBox5.TabIndex = 164;
            this.checkBox5.Text = "al menos";
            this.checkBox5.UseVisualStyleBackColor = true;
            // 
            // button109
            // 
            this.button109.Location = new System.Drawing.Point(265, 67);
            this.button109.Name = "button109";
            this.button109.Size = new System.Drawing.Size(125, 21);
            this.button109.TabIndex = 163;
            this.button109.Text = "Exportar";
            this.button109.UseVisualStyleBackColor = true;
            this.button109.Click += new System.EventHandler(this.button109_Click);
            // 
            // comboBox39
            // 
            this.comboBox39.FormattingEnabled = true;
            this.comboBox39.Location = new System.Drawing.Point(80, 67);
            this.comboBox39.Name = "comboBox39";
            this.comboBox39.Size = new System.Drawing.Size(172, 21);
            this.comboBox39.TabIndex = 161;
            this.comboBox39.SelectedIndexChanged += new System.EventHandler(this.comboBox39_SelectedIndexChanged);
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Location = new System.Drawing.Point(3, 72);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(52, 13);
            this.label88.TabIndex = 160;
            this.label88.Text = "Categoria";
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Location = new System.Drawing.Point(165, 19);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Size = new System.Drawing.Size(101, 17);
            this.checkBox10.TabIndex = 159;
            this.checkBox10.Text = "Final Obligatoria";
            this.checkBox10.UseVisualStyleBackColor = true;
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Location = new System.Drawing.Point(4, 19);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Size = new System.Drawing.Size(163, 17);
            this.checkBox9.TabIndex = 158;
            this.checkBox9.Text = "Descartar Peor Resultado (1)";
            this.checkBox9.UseVisualStyleBackColor = true;
            // 
            // button103
            // 
            this.button103.Location = new System.Drawing.Point(6, 42);
            this.button103.Name = "button103";
            this.button103.Size = new System.Drawing.Size(246, 21);
            this.button103.TabIndex = 155;
            this.button103.Text = "Calcular Campeonato / Actualizar";
            this.button103.UseVisualStyleBackColor = true;
            this.button103.Click += new System.EventHandler(this.button103_Click);
            // 
            // groupBox20
            // 
            this.groupBox20.Controls.Add(this.button108);
            this.groupBox20.Controls.Add(this.button105);
            this.groupBox20.Controls.Add(this.comboBox37);
            this.groupBox20.Controls.Add(this.label84);
            this.groupBox20.Controls.Add(this.numericUpDown5);
            this.groupBox20.Controls.Add(this.label86);
            this.groupBox20.Location = new System.Drawing.Point(7, 3);
            this.groupBox20.Name = "groupBox20";
            this.groupBox20.Size = new System.Drawing.Size(396, 71);
            this.groupBox20.TabIndex = 155;
            this.groupBox20.TabStop = false;
            this.groupBox20.Text = "Carga de Fechas";
            // 
            // button108
            // 
            this.button108.Location = new System.Drawing.Point(265, 45);
            this.button108.Name = "button108";
            this.button108.Size = new System.Drawing.Size(125, 21);
            this.button108.TabIndex = 159;
            this.button108.Text = "borrar";
            this.button108.UseVisualStyleBackColor = true;
            this.button108.Click += new System.EventHandler(this.button108_Click);
            // 
            // button105
            // 
            this.button105.Location = new System.Drawing.Point(141, 45);
            this.button105.Name = "button105";
            this.button105.Size = new System.Drawing.Size(118, 21);
            this.button105.TabIndex = 158;
            this.button105.Text = "Abrir .xls";
            this.button105.UseVisualStyleBackColor = true;
            this.button105.Click += new System.EventHandler(this.button105_Click);
            // 
            // comboBox37
            // 
            this.comboBox37.FormattingEnabled = true;
            this.comboBox37.Location = new System.Drawing.Point(72, 19);
            this.comboBox37.Name = "comboBox37";
            this.comboBox37.Size = new System.Drawing.Size(318, 21);
            this.comboBox37.TabIndex = 154;
            this.comboBox37.SelectedIndexChanged += new System.EventHandler(this.comboBox37_SelectedIndexChanged);
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Location = new System.Drawing.Point(6, 22);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(44, 13);
            this.label84.TabIndex = 153;
            this.label84.Text = "Nombre";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(72, 45);
            this.numericUpDown5.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(63, 20);
            this.numericUpDown5.TabIndex = 141;
            this.numericUpDown5.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Location = new System.Drawing.Point(6, 47);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(47, 13);
            this.label86.TabIndex = 140;
            this.label86.Text = "# Fecha";
            this.label86.Click += new System.EventHandler(this.label86_Click);
            // 
            // groupBox19
            // 
            this.groupBox19.Controls.Add(this.comboBox41);
            this.groupBox19.Controls.Add(this.label138);
            this.groupBox19.Controls.Add(this.comboBox42);
            this.groupBox19.Controls.Add(this.label92);
            this.groupBox19.Controls.Add(this.button107);
            this.groupBox19.Controls.Add(this.comboBox38);
            this.groupBox19.Controls.Add(this.label87);
            this.groupBox19.Controls.Add(this.comboBox36);
            this.groupBox19.Controls.Add(this.label83);
            this.groupBox19.Controls.Add(this.button102);
            this.groupBox19.Controls.Add(this.checkBox8);
            this.groupBox19.Controls.Add(this.checkBox7);
            this.groupBox19.Controls.Add(this.numericUpDown4);
            this.groupBox19.Controls.Add(this.label85);
            this.groupBox19.Controls.Add(this.button101);
            this.groupBox19.Location = new System.Drawing.Point(635, 3);
            this.groupBox19.Name = "groupBox19";
            this.groupBox19.Size = new System.Drawing.Size(221, 171);
            this.groupBox19.TabIndex = 139;
            this.groupBox19.TabStop = false;
            this.groupBox19.Text = "Crear Campeonato";
            // 
            // comboBox41
            // 
            this.comboBox41.FormattingEnabled = true;
            this.comboBox41.Items.AddRange(new object[] {
            "dni",
            "placa"});
            this.comboBox41.Location = new System.Drawing.Point(151, 41);
            this.comboBox41.Name = "comboBox41";
            this.comboBox41.Size = new System.Drawing.Size(64, 21);
            this.comboBox41.TabIndex = 161;
            // 
            // label138
            // 
            this.label138.AutoSize = true;
            this.label138.Location = new System.Drawing.Point(103, 43);
            this.label138.Name = "label138";
            this.label138.Size = new System.Drawing.Size(42, 13);
            this.label138.TabIndex = 160;
            this.label138.Text = "Tipo ID";
            // 
            // comboBox42
            // 
            this.comboBox42.FormattingEnabled = true;
            this.comboBox42.Items.AddRange(new object[] {
            "campeonato",
            "etapas"});
            this.comboBox42.Location = new System.Drawing.Point(66, 66);
            this.comboBox42.Name = "comboBox42";
            this.comboBox42.Size = new System.Drawing.Size(149, 21);
            this.comboBox42.TabIndex = 159;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Location = new System.Drawing.Point(6, 69);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(28, 13);
            this.label92.TabIndex = 158;
            this.label92.Text = "Tipo";
            // 
            // button107
            // 
            this.button107.Location = new System.Drawing.Point(152, 139);
            this.button107.Name = "button107";
            this.button107.Size = new System.Drawing.Size(63, 21);
            this.button107.TabIndex = 157;
            this.button107.Text = "Borrar";
            this.button107.UseVisualStyleBackColor = true;
            this.button107.Click += new System.EventHandler(this.button107_Click);
            // 
            // comboBox38
            // 
            this.comboBox38.FormattingEnabled = true;
            this.comboBox38.Location = new System.Drawing.Point(98, 115);
            this.comboBox38.Name = "comboBox38";
            this.comboBox38.Size = new System.Drawing.Size(117, 21);
            this.comboBox38.TabIndex = 156;
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Location = new System.Drawing.Point(6, 118);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(91, 13);
            this.label87.TabIndex = 155;
            this.label87.Text = "Set de Categorias";
            // 
            // comboBox36
            // 
            this.comboBox36.FormattingEnabled = true;
            this.comboBox36.Location = new System.Drawing.Point(66, 16);
            this.comboBox36.Name = "comboBox36";
            this.comboBox36.Size = new System.Drawing.Size(149, 21);
            this.comboBox36.TabIndex = 154;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Location = new System.Drawing.Point(6, 19);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(44, 13);
            this.label83.TabIndex = 153;
            this.label83.Text = "Nombre";
            // 
            // button102
            // 
            this.button102.Location = new System.Drawing.Point(81, 139);
            this.button102.Name = "button102";
            this.button102.Size = new System.Drawing.Size(63, 21);
            this.button102.TabIndex = 152;
            this.button102.Text = "Actualizar";
            this.button102.UseVisualStyleBackColor = true;
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Location = new System.Drawing.Point(105, 92);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Size = new System.Drawing.Size(101, 17);
            this.checkBox8.TabIndex = 149;
            this.checkBox8.Text = "Final Obligatoria";
            this.checkBox8.UseVisualStyleBackColor = true;
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Location = new System.Drawing.Point(9, 92);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Size = new System.Drawing.Size(83, 17);
            this.checkBox7.TabIndex = 148;
            this.checkBox7.Text = "Presentismo";
            this.checkBox7.UseVisualStyleBackColor = true;
            // 
            // numericUpDown4
            // 
            this.numericUpDown4.Location = new System.Drawing.Point(66, 41);
            this.numericUpDown4.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
            this.numericUpDown4.Name = "numericUpDown4";
            this.numericUpDown4.Size = new System.Drawing.Size(31, 20);
            this.numericUpDown4.TabIndex = 141;
            this.numericUpDown4.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Location = new System.Drawing.Point(6, 43);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(42, 13);
            this.label85.TabIndex = 140;
            this.label85.Text = "Fechas";
            // 
            // button101
            // 
            this.button101.Location = new System.Drawing.Point(9, 139);
            this.button101.Name = "button101";
            this.button101.Size = new System.Drawing.Size(63, 21);
            this.button101.TabIndex = 87;
            this.button101.Text = "Crear";
            this.button101.UseVisualStyleBackColor = true;
            this.button101.Click += new System.EventHandler(this.button101_Click);
            // 
            // dataGridView8
            // 
            this.dataGridView8.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView8.Location = new System.Drawing.Point(7, 180);
            this.dataGridView8.Name = "dataGridView8";
            this.dataGridView8.Size = new System.Drawing.Size(1345, 488);
            this.dataGridView8.TabIndex = 138;
            // 
            // dataGridView7
            // 
            this.dataGridView7.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView7.Location = new System.Drawing.Point(862, 6);
            this.dataGridView7.Name = "dataGridView7";
            this.dataGridView7.Size = new System.Drawing.Size(490, 168);
            this.dataGridView7.TabIndex = 137;
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.checkBox16);
            this.tabPage5.Controls.Add(this.button104);
            this.tabPage5.Controls.Add(this.button52);
            this.tabPage5.Controls.Add(this.label44);
            this.tabPage5.Controls.Add(this.button44);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Controls.Add(this.comboBox11);
            this.tabPage5.Controls.Add(this.dataGridView9);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage5.Size = new System.Drawing.Size(1358, 677);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Viewer";
            this.tabPage5.UseVisualStyleBackColor = true;
            this.tabPage5.Click += new System.EventHandler(this.tabPage5_Click);
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Location = new System.Drawing.Point(1290, 9);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(39, 17);
            this.checkBox16.TabIndex = 168;
            this.checkBox16.Text = "roll";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // button104
            // 
            this.button104.Location = new System.Drawing.Point(1332, 30);
            this.button104.Name = "button104";
            this.button104.Size = new System.Drawing.Size(23, 23);
            this.button104.TabIndex = 167;
            this.button104.Text = "-";
            this.button104.UseVisualStyleBackColor = true;
            this.button104.Click += new System.EventHandler(this.button104_Click_1);
            // 
            // button52
            // 
            this.button52.Location = new System.Drawing.Point(1332, 6);
            this.button52.Name = "button52";
            this.button52.Size = new System.Drawing.Size(23, 23);
            this.button52.TabIndex = 166;
            this.button52.Text = "+";
            this.button52.UseVisualStyleBackColor = true;
            this.button52.Click += new System.EventHandler(this.button52_Click);
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Comic Sans MS", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(3, 8);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(162, 45);
            this.label44.TabIndex = 165;
            this.label44.Text = "Categoria";
            // 
            // button44
            // 
            this.button44.BackColor = System.Drawing.Color.Transparent;
            this.button44.FlatAppearance.BorderColor = System.Drawing.Color.Red;
            this.button44.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.button44.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.button44.Location = new System.Drawing.Point(1202, 6);
            this.button44.Name = "button44";
            this.button44.Size = new System.Drawing.Size(76, 23);
            this.button44.TabIndex = 164;
            this.button44.Text = "Start Viewer";
            this.button44.UseVisualStyleBackColor = false;
            this.button44.Click += new System.EventHandler(this.button44_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(1135, 11);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(41, 13);
            this.label16.TabIndex = 163;
            this.label16.Text = "Evento";
            // 
            // comboBox11
            // 
            this.comboBox11.FormattingEnabled = true;
            this.comboBox11.Location = new System.Drawing.Point(1138, 31);
            this.comboBox11.Name = "comboBox11";
            this.comboBox11.Size = new System.Drawing.Size(191, 21);
            this.comboBox11.TabIndex = 162;
            this.comboBox11.SelectedIndexChanged += new System.EventHandler(this.comboBox11_SelectedIndexChanged);
            // 
            // dataGridView9
            // 
            this.dataGridView9.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView9.Location = new System.Drawing.Point(3, 55);
            this.dataGridView9.Name = "dataGridView9";
            this.dataGridView9.Size = new System.Drawing.Size(1352, 613);
            this.dataGridView9.TabIndex = 158;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button87);
            this.tabPage1.Controls.Add(this.pictureBox1);
            this.tabPage1.Controls.Add(this.groupBox24);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1358, 677);
            this.tabPage1.TabIndex = 7;
            this.tabPage1.Text = "print ticket";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // button87
            // 
            this.button87.Location = new System.Drawing.Point(1042, 469);
            this.button87.Name = "button87";
            this.button87.Size = new System.Drawing.Size(75, 23);
            this.button87.TabIndex = 16;
            this.button87.Text = "imagen";
            this.button87.UseVisualStyleBackColor = true;
            this.button87.Click += new System.EventHandler(this.button87_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(465, 26);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(652, 437);
            this.pictureBox1.TabIndex = 15;
            this.pictureBox1.TabStop = false;
            // 
            // groupBox24
            // 
            this.groupBox24.Controls.Add(this.label101);
            this.groupBox24.Controls.Add(this.label112);
            this.groupBox24.Controls.Add(this.textBox5);
            this.groupBox24.Controls.Add(this.label113);
            this.groupBox24.Controls.Add(this.label102);
            this.groupBox24.Controls.Add(this.label110);
            this.groupBox24.Controls.Add(this.label103);
            this.groupBox24.Controls.Add(this.label111);
            this.groupBox24.Controls.Add(this.label104);
            this.groupBox24.Controls.Add(this.label109);
            this.groupBox24.Controls.Add(this.label105);
            this.groupBox24.Controls.Add(this.label108);
            this.groupBox24.Controls.Add(this.label106);
            this.groupBox24.Controls.Add(this.label107);
            this.groupBox24.Location = new System.Drawing.Point(19, 26);
            this.groupBox24.Name = "groupBox24";
            this.groupBox24.Size = new System.Drawing.Size(401, 437);
            this.groupBox24.TabIndex = 14;
            this.groupBox24.TabStop = false;
            this.groupBox24.Text = "info resultados";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(25, 37);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(341, 25);
            this.label101.TabIndex = 1;
            this.label101.Text = "Ingrese Numero de corredor o DNI";
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(143, 212);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(81, 25);
            this.label112.TabIndex = 13;
            this.label112.Text = "Numero";
            // 
            // textBox5
            // 
            this.textBox5.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.Location = new System.Drawing.Point(29, 71);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(301, 53);
            this.textBox5.TabIndex = 0;
            this.textBox5.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox5_KeyDown_1);
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(24, 212);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(87, 25);
            this.label113.TabIndex = 12;
            this.label113.Text = "Numero:";
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(24, 140);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(79, 25);
            this.label102.TabIndex = 2;
            this.label102.Text = "Evento:";
            this.label102.Click += new System.EventHandler(this.label102_Click);
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(143, 279);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(70, 25);
            this.label110.TabIndex = 11;
            this.label110.Text = "tiempo";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(24, 179);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(87, 25);
            this.label103.TabIndex = 3;
            this.label103.Text = "Nombre:";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(24, 279);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(84, 25);
            this.label111.TabIndex = 10;
            this.label111.Text = "Tiempo:";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(24, 312);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(92, 25);
            this.label104.TabIndex = 4;
            this.label104.Text = "Posicion:";
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(143, 312);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(86, 25);
            this.label109.TabIndex = 9;
            this.label109.Text = "Posicion";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(24, 246);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(103, 25);
            this.label105.TabIndex = 5;
            this.label105.Text = "Categoria:";
            this.label105.Click += new System.EventHandler(this.label105_Click);
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(143, 246);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(97, 25);
            this.label108.TabIndex = 8;
            this.label108.Text = "Categoria";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(143, 140);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(73, 25);
            this.label106.TabIndex = 6;
            this.label106.Text = "Evento";
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(143, 179);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(81, 25);
            this.label107.TabIndex = 7;
            this.label107.Text = "Nombre";
            // 
            // Check_Datos
            // 
            this.Check_Datos.BackColor = System.Drawing.Color.Transparent;
            this.Check_Datos.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.Check_Datos.Controls.Add(this.groupBox23);
            this.Check_Datos.Location = new System.Drawing.Point(4, 22);
            this.Check_Datos.Name = "Check_Datos";
            this.Check_Datos.Size = new System.Drawing.Size(1358, 677);
            this.Check_Datos.TabIndex = 8;
            this.Check_Datos.Text = "Check_Datos";
            // 
            // groupBox23
            // 
            this.groupBox23.Controls.Add(this.label123);
            this.groupBox23.Controls.Add(this.labelGenero);
            this.groupBox23.Controls.Add(this.label98);
            this.groupBox23.Controls.Add(this.labelNumero);
            this.groupBox23.Controls.Add(this.label121);
            this.groupBox23.Controls.Add(this.label122);
            this.groupBox23.Controls.Add(this.label124);
            this.groupBox23.Controls.Add(this.label128);
            this.groupBox23.Controls.Add(this.labelCategoria);
            this.groupBox23.Controls.Add(this.labelEvento);
            this.groupBox23.Controls.Add(this.labelNombre);
            this.groupBox23.Location = new System.Drawing.Point(24, 13);
            this.groupBox23.Name = "groupBox23";
            this.groupBox23.Size = new System.Drawing.Size(900, 465);
            this.groupBox23.TabIndex = 15;
            this.groupBox23.TabStop = false;
            this.groupBox23.Text = "info corredores";
            // 
            // label123
            // 
            this.label123.AutoSize = true;
            this.label123.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label123.Location = new System.Drawing.Point(17, 402);
            this.label123.Name = "label123";
            this.label123.Size = new System.Drawing.Size(164, 46);
            this.label123.TabIndex = 14;
            this.label123.Text = "Genero:";
            // 
            // labelGenero
            // 
            this.labelGenero.AutoSize = true;
            this.labelGenero.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelGenero.Location = new System.Drawing.Point(223, 402);
            this.labelGenero.Name = "labelGenero";
            this.labelGenero.Size = new System.Drawing.Size(153, 46);
            this.labelGenero.TabIndex = 15;
            this.labelGenero.Text = "Genero";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.label98.Location = new System.Drawing.Point(25, 37);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(835, 25);
            this.label98.TabIndex = 1;
            this.label98.Text = "ACERQUE EL NUMERO AL LECTOR (NO DEBES QUITAR EL CHIP DEL NUMERO!)";
            this.label98.Click += new System.EventHandler(this.label98_Click);
            // 
            // labelNumero
            // 
            this.labelNumero.AutoSize = true;
            this.labelNumero.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNumero.Location = new System.Drawing.Point(226, 241);
            this.labelNumero.Name = "labelNumero";
            this.labelNumero.Size = new System.Drawing.Size(162, 46);
            this.labelNumero.TabIndex = 13;
            this.labelNumero.Text = "Numero";
            // 
            // label121
            // 
            this.label121.AutoSize = true;
            this.label121.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label121.Location = new System.Drawing.Point(20, 241);
            this.label121.Name = "label121";
            this.label121.Size = new System.Drawing.Size(173, 46);
            this.label121.TabIndex = 12;
            this.label121.Text = "Numero:";
            this.label121.Click += new System.EventHandler(this.label121_Click);
            // 
            // label122
            // 
            this.label122.AutoSize = true;
            this.label122.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label122.Location = new System.Drawing.Point(20, 81);
            this.label122.Name = "label122";
            this.label122.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.label122.Size = new System.Drawing.Size(196, 46);
            this.label122.TabIndex = 2;
            this.label122.Text = ":Distancia";
            // 
            // label124
            // 
            this.label124.AutoSize = true;
            this.label124.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label124.Location = new System.Drawing.Point(20, 163);
            this.label124.Name = "label124";
            this.label124.Size = new System.Drawing.Size(173, 46);
            this.label124.TabIndex = 3;
            this.label124.Text = "Nombre:";
            // 
            // label128
            // 
            this.label128.AutoSize = true;
            this.label128.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label128.Location = new System.Drawing.Point(17, 318);
            this.label128.Name = "label128";
            this.label128.Size = new System.Drawing.Size(204, 46);
            this.label128.TabIndex = 5;
            this.label128.Text = "Categoria:";
            // 
            // labelCategoria
            // 
            this.labelCategoria.AutoSize = true;
            this.labelCategoria.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelCategoria.Location = new System.Drawing.Point(223, 318);
            this.labelCategoria.Name = "labelCategoria";
            this.labelCategoria.Size = new System.Drawing.Size(193, 46);
            this.labelCategoria.TabIndex = 8;
            this.labelCategoria.Text = "Categoria";
            // 
            // labelEvento
            // 
            this.labelEvento.AutoSize = true;
            this.labelEvento.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelEvento.Location = new System.Drawing.Point(231, 81);
            this.labelEvento.Name = "labelEvento";
            this.labelEvento.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.labelEvento.Size = new System.Drawing.Size(144, 46);
            this.labelEvento.TabIndex = 6;
            this.labelEvento.Text = "Evento";
            // 
            // labelNombre
            // 
            this.labelNombre.AutoSize = true;
            this.labelNombre.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.labelNombre.Location = new System.Drawing.Point(226, 163);
            this.labelNombre.Name = "labelNombre";
            this.labelNombre.Size = new System.Drawing.Size(162, 46);
            this.labelNombre.TabIndex = 7;
            this.labelNombre.Text = "Nombre";
            // 
            // timer1
            // 
            this.timer1.Interval = 1000;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // timer2
            // 
            this.timer2.Enabled = true;
            this.timer2.Interval = 50;
            this.timer2.Tick += new System.EventHandler(this.timer2_Tick);
            // 
            // serialPort2
            // 
            this.serialPort2.PortName = "COM2";
            // 
            // serialPort3
            // 
            this.serialPort3.PortName = "COM3";
            // 
            // serialPort4
            // 
            this.serialPort4.PortName = "COM4";
            // 
            // db_mainDataSet
            // 
            this.db_mainDataSet.DataSetName = "db_mainDataSet";
            this.db_mainDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // categoriasTableAdapter
            // 
            this.categoriasTableAdapter.ClearBeforeFill = true;
            // 
            // eventosTableAdapter
            // 
            this.eventosTableAdapter.ClearBeforeFill = true;
            // 
            // serialPort5
            // 
            this.serialPort5.PortName = "COM4";
            // 
            // timer3
            // 
            this.timer3.Interval = 10000;
            this.timer3.Tick += new System.EventHandler(this.timer3_Tick);
            // 
            // serialPort6
            // 
            this.serialPort6.PortName = "COM4";
            // 
            // timer4
            // 
            this.timer4.Interval = 10000;
            this.timer4.Tick += new System.EventHandler(this.timer4_Tick);
            // 
            // timer5
            // 
            this.timer5.Interval = 10000;
            this.timer5.Tick += new System.EventHandler(this.timer5_Tick);
            // 
            // timer6
            // 
            this.timer6.Interval = 10000;
            this.timer6.Tick += new System.EventHandler(this.timer6_Tick);
            // 
            // label139
            // 
            this.label139.AutoSize = true;
            this.label139.Location = new System.Drawing.Point(466, 106);
            this.label139.Name = "label139";
            this.label139.Size = new System.Drawing.Size(75, 13);
            this.label139.TabIndex = 164;
            this.label139.Text = "Imprimir Ticket";
            // 
            // textBox51
            // 
            this.textBox51.Location = new System.Drawing.Point(547, 103);
            this.textBox51.Name = "textBox51";
            this.textBox51.Size = new System.Drawing.Size(64, 20);
            this.textBox51.TabIndex = 163;
            this.textBox51.KeyDown += new System.Windows.Forms.KeyEventHandler(this.textBox51_KeyDown);
            // 
            // main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MidnightBlue;
            this.ClientSize = new System.Drawing.Size(1370, 703);
            this.Controls.Add(this.tabControl1);
            this.ForeColor = System.Drawing.SystemColors.InfoText;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "main";
            this.Text = "CronoBot";
            this.TransparencyKey = System.Drawing.Color.Lavender;
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.main_FormClosed);
            this.Load += new System.EventHandler(this.main_Load);
            this.tabControl1.ResumeLayout(false);
            this.tab1_Listado_General.ResumeLayout(false);
            this.tab1_Listado_General.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Corredores)).EndInit();
            this.Config.ResumeLayout(false);
            this.groupBox28.ResumeLayout(false);
            this.groupBox28.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.groupBox11.ResumeLayout(false);
            this.groupBox11.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.en_curso.ResumeLayout(false);
            this.en_curso.PerformLayout();
            this.groupBox22.ResumeLayout(false);
            this.groupBox22.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView10)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.groupBox9.PerformLayout();
            this.groupBox8.ResumeLayout(false);
            this.groupBox8.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.resultados.ResumeLayout(false);
            this.resultados.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            this.groupBox15.ResumeLayout(false);
            this.groupBox15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView6)).EndInit();
            this.tools.ResumeLayout(false);
            this.tools.PerformLayout();
            this.groupBox27.ResumeLayout(false);
            this.groupBox27.PerformLayout();
            this.groupBox26.ResumeLayout(false);
            this.groupBox26.PerformLayout();
            this.groupBox25.ResumeLayout(false);
            this.groupBox25.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar5)).EndInit();
            this.groupBox18.ResumeLayout(false);
            this.groupBox18.PerformLayout();
            this.groupBox17.ResumeLayout(false);
            this.groupBox17.PerformLayout();
            this.groupBox16.ResumeLayout(false);
            this.groupBox16.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            this.groupBox14.ResumeLayout(false);
            this.groupBox14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar4)).EndInit();
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar3)).EndInit();
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar2)).EndInit();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.groupBox10.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView5)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            this.campeonatos.ResumeLayout(false);
            this.campeonatos.PerformLayout();
            this.groupBox21.ResumeLayout(false);
            this.groupBox21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            this.groupBox20.ResumeLayout(false);
            this.groupBox20.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            this.groupBox19.ResumeLayout(false);
            this.groupBox19.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView8)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView7)).EndInit();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView9)).EndInit();
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.groupBox24.ResumeLayout(false);
            this.groupBox24.PerformLayout();
            this.Check_Datos.ResumeLayout(false);
            this.groupBox23.ResumeLayout(false);
            this.groupBox23.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.db_mainDataSet)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.eventosBindingSource)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tab1_Listado_General;
        private System.Windows.Forms.TabPage Config;
        private System.Windows.Forms.DataGridView dataGridView_Corredores;
        private System.Windows.Forms.ComboBox comboBox3;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.TabPage en_curso;
        private System.Windows.Forms.TabPage resultados;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.DateTimePicker dateTimePicker1;
        private System.Windows.Forms.TabPage tools;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.NumericUpDown numericUpDown2;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.Button button11;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DataGridView dataGridView3;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.ComboBox comboBox4;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.TextBox textBox13;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ComboBox comboBox5;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.ComboBox comboBox6;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.ComboBox comboBox7;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.TextBox textBox14;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.ComboBox comboBox9;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label label27;
        private System.IO.Ports.SerialPort serialPort1;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.TabPage campeonatos;
        private System.Windows.Forms.Button button22;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.ComboBox comboBox8;
        private System.Windows.Forms.Button button26;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Label label42;
        private System.Windows.Forms.TextBox textBox15;
        private System.Windows.Forms.TextBox textBox16;
        private System.Windows.Forms.Label label43;
        private System.Windows.Forms.Timer timer2;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button18;
        private System.Windows.Forms.Button button29;
        private System.Windows.Forms.Button button23;
        private System.Windows.Forms.Button button28;
        private System.Windows.Forms.Button button24;
        private System.Windows.Forms.Button button21;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox textBox17;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.ComboBox comboBox10;
        private System.Windows.Forms.Button button19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Button button30;
        private System.Windows.Forms.TextBox textBox18;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.DateTimePicker dateTimePicker4;
        private System.Windows.Forms.Button button32;
        private System.Windows.Forms.Button button31;
        private System.Windows.Forms.Button button33;
        private System.Windows.Forms.DataGridView dataGridView5;
        private System.Windows.Forms.Button button34;
        private System.Windows.Forms.Button button35;
        private System.Windows.Forms.NumericUpDown numericUpDown3;
        private System.Windows.Forms.Label label45;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TextBox textBox19;
        private System.Windows.Forms.Label label50;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.CheckedListBox checkedListBox1;
        private System.Windows.Forms.Label label51;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.TextBox textBox20;
        private System.Windows.Forms.Button button38;
        private System.Windows.Forms.Button button37;
        private System.Windows.Forms.Label label55;
        private System.Windows.Forms.Label label54;
        private System.Windows.Forms.TextBox textBox21;
        private System.Windows.Forms.Label label56;
        private System.Windows.Forms.TextBox textBox22;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.ComboBox comboBox18;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.ComboBox comboBox19;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.ComboBox comboBox16;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.ComboBox comboBox17;
        private System.Windows.Forms.ComboBox comboBox14;
        private System.Windows.Forms.ComboBox comboBox15;
        private System.Windows.Forms.ComboBox comboBox13;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.ComboBox comboBox12;
        private System.Windows.Forms.Button button27;
        private System.Windows.Forms.ComboBox cbImportEvent;
        private System.Windows.Forms.Label label57;
        private System.Windows.Forms.ComboBox comboBox21;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Button button40;
        private System.Windows.Forms.Label label52;
        private System.Windows.Forms.Label label53;
        private System.Windows.Forms.Label label58;
        private System.Windows.Forms.ComboBox comboBox22;
        private System.Windows.Forms.Button button41;
        private System.Windows.Forms.DateTimePicker dateTimePicker5;
        private System.Windows.Forms.Button button42;
        private System.Windows.Forms.Button button43;
        private System.Windows.Forms.Button button47;
        private System.Windows.Forms.TextBox textBox23;
        private System.Windows.Forms.Button button46;
        private System.Windows.Forms.ComboBox comboBox23;
        private System.Windows.Forms.Button button45;
        private System.Windows.Forms.ComboBox comboBox24;
        private System.Windows.Forms.Button button48;
        private System.Windows.Forms.TextBox textBox24;
        private System.Windows.Forms.Button button49;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.Button button51;
        private System.Windows.Forms.Button button50;
        private System.Windows.Forms.ComboBox comboBox25;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Label label47;
        private System.Windows.Forms.Button button53;
        private System.Windows.Forms.Button button54;
        private System.Windows.Forms.Button button55;
        private System.Windows.Forms.Button button56;
        private System.Windows.Forms.Button button57;
        private System.Windows.Forms.Button button58;
        private System.Windows.Forms.Label label48;
        private System.Windows.Forms.TextBox textBox25;
        private System.Windows.Forms.Label label49;
        private System.Windows.Forms.Label label59;
        private System.Windows.Forms.ComboBox comboBox26;
        private System.Windows.Forms.Button button60;
        private System.IO.Ports.SerialPort serialPort2;
        private System.IO.Ports.SerialPort serialPort3;
        private System.IO.Ports.SerialPort serialPort4;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Label label60;
        private System.Windows.Forms.Button button61;
        private System.Windows.Forms.Button button62;
        private System.Windows.Forms.Button button63;
        private System.Windows.Forms.Button button64;
        private System.Windows.Forms.Button button65;
        private System.Windows.Forms.Button button66;
        private System.Windows.Forms.Label label61;
        private System.Windows.Forms.TextBox textBox26;
        private System.Windows.Forms.Label label62;
        private System.Windows.Forms.Label label63;
        private System.Windows.Forms.ComboBox comboBox27;
        private System.Windows.Forms.Button button68;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.Label label64;
        private System.Windows.Forms.Button button69;
        private System.Windows.Forms.Button button70;
        private System.Windows.Forms.Button button71;
        private System.Windows.Forms.Button button72;
        private System.Windows.Forms.Button button73;
        private System.Windows.Forms.Button button74;
        private System.Windows.Forms.Label label65;
        private System.Windows.Forms.TextBox textBox27;
        private System.Windows.Forms.Label label66;
        private System.Windows.Forms.Label label67;
        private System.Windows.Forms.ComboBox comboBox28;
        private System.Windows.Forms.Button button76;
        private System.Windows.Forms.Label label71;
        private System.Windows.Forms.Label label74;
        private System.Windows.Forms.Label label73;
        private System.Windows.Forms.Label label72;
        private System.Windows.Forms.Label label70;
        private System.Windows.Forms.Label label69;
        private System.Windows.Forms.Label label68;
        private System.Windows.Forms.Label label75;
        private System.Windows.Forms.TextBox textBox28;
        private System.Windows.Forms.DataGridView dataGridView6;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.Label label76;
        private System.Windows.Forms.ComboBox comboBox29;
        private System.Windows.Forms.ComboBox comboBox30;
        private db_mainDataSet db_mainDataSet;
        private db_mainDataSetTableAdapters.categoriasTableAdapter categoriasTableAdapter;
        private System.Windows.Forms.BindingSource eventosBindingSource;
        private db_mainDataSet1TableAdapters.eventosTableAdapter eventosTableAdapter;
        private System.Windows.Forms.Button button36;
        private System.Windows.Forms.Button button39;
        private System.Windows.Forms.TextBox textBox6;
        private System.IO.Ports.SerialPort serialPort5;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.Button button77;
        private System.Windows.Forms.Button button79;
        private System.Windows.Forms.Button button81;
        private System.Windows.Forms.ComboBox comboBox20;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label79;
        private System.Windows.Forms.ComboBox comboBox31;
        private System.Windows.Forms.Label label81;
        private System.Windows.Forms.Label label82;
        private System.Windows.Forms.Button button78;
        private System.Windows.Forms.ComboBox comboBox32;
        private System.Windows.Forms.Button button80;
        private System.Windows.Forms.TextBox textBox29;
        private System.Windows.Forms.Button button82;
        private System.Windows.Forms.Timer timer3;
        private System.Windows.Forms.Button button84;
        private System.Windows.Forms.Button button83;
        private System.Windows.Forms.Button button88;
        private System.Windows.Forms.Label label77;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.TextBox textBox30;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.Button button95;
        private System.Windows.Forms.Button button96;
        private System.Windows.Forms.Button button97;
        private System.Windows.Forms.Button button98;
        private System.Windows.Forms.ComboBox comboBox33;
        private System.IO.Ports.SerialPort serialPort6;
        private System.Windows.Forms.TextBox textBox31;
        private System.Windows.Forms.Button button89;
        private System.Windows.Forms.Button button90;
        private System.Windows.Forms.Button button92;
        private System.Windows.Forms.Button button93;
        private System.Windows.Forms.Button button91;
        private System.Windows.Forms.Button button94;
        private System.Windows.Forms.ComboBox comboBox34;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.Label label46;
        private System.Windows.Forms.ComboBox comboBox35;
        private System.Windows.Forms.TextBox textBox32;
        private System.Windows.Forms.Label label78;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.DateTimePicker dateTimePicker6;
        private System.Windows.Forms.Button button100;
        private System.Windows.Forms.Label label80;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.Button button101;
        private System.Windows.Forms.DataGridView dataGridView8;
        private System.Windows.Forms.DataGridView dataGridView7;
        private System.Windows.Forms.NumericUpDown numericUpDown4;
        private System.Windows.Forms.Label label85;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.Button button102;
        private System.Windows.Forms.ComboBox comboBox36;
        private System.Windows.Forms.Label label83;
        private System.Windows.Forms.Button button103;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.ComboBox comboBox37;
        private System.Windows.Forms.Label label84;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.Label label86;
        private System.Windows.Forms.Button button105;
        private System.Windows.Forms.Button button106;
        private System.Windows.Forms.Button button107;
        private System.Windows.Forms.ComboBox comboBox38;
        private System.Windows.Forms.Label label87;
        private System.Windows.Forms.Button button108;
        private System.Windows.Forms.ComboBox comboBox39;
        private System.Windows.Forms.Label label88;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.Button button109;
        private System.Windows.Forms.DataGridView dataGridView9;
        private System.Windows.Forms.ComboBox comboBox42;
        private System.Windows.Forms.Label label92;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.Button button99;
        private System.Windows.Forms.ComboBox comboBox43;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog1;
        private System.Windows.Forms.Button button25;
        private System.Windows.Forms.ComboBox comboBox11;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button44;
        private System.Windows.Forms.Timer timer4;
        private System.Windows.Forms.Label label44;
        private System.Windows.Forms.Button button104;
        private System.Windows.Forms.Button button52;
        private System.Windows.Forms.Timer timer5;
        private System.Windows.Forms.Button button110;
        private System.Windows.Forms.DataGridView dataGridView10;
        private System.Windows.Forms.DataGridViewTextBoxColumn Categoria;
        private System.Windows.Forms.DataGridViewTextBoxColumn Lapeo;
        private System.Windows.Forms.GroupBox groupBox22;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox cbLargada;
        private System.Windows.Forms.Button button111;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button112;
        private System.Windows.Forms.CheckBox checkBox16;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Button button115;
        private System.Windows.Forms.Button button114;
        private System.Windows.Forms.Button button113;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.Button button86;
        private System.Windows.Forms.Button button116;
        private System.Windows.Forms.TextBox textBox12;
        private System.Windows.Forms.Label label90;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.TextBox textBox33;
        private System.Windows.Forms.CheckBox checkBox17;
        private System.Windows.Forms.Label label94;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.TextBox textBox34;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.Label label100;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.ComboBox comboBox44;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Label label103;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.Button button87;
        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.TrackBar trackBar2;
        private System.Windows.Forms.TrackBar trackBar3;
        private System.Windows.Forms.TrackBar trackBar4;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.Button button118;
        private System.Windows.Forms.Button button20;
        private System.Windows.Forms.Timer timer6;
        private System.Windows.Forms.Button button59;
        private System.Windows.Forms.CheckBox checkBox18;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.Button button75;
        private System.Windows.Forms.Button button67;
        private System.Windows.Forms.ComboBox comboBox45;
        private System.Windows.Forms.TextBox textBox35;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.ComboBox comboBox47;
        private System.Windows.Forms.ComboBox comboBox46;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.ComboBox comboBox48;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.ComboBox comboBox49;
        private System.Windows.Forms.CheckBox checkBox19;
        private System.Windows.Forms.TextBox textBox36;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Button button117;
        private System.Windows.Forms.TextBox textBox37;
        private System.Windows.Forms.Label label120;
        private System.Windows.Forms.Button button120;
        private System.Windows.Forms.TextBox textBox38;
        private System.Windows.Forms.Button button121;
        private System.Windows.Forms.Button button122;
        private System.Windows.Forms.Button button123;
        private System.Windows.Forms.Button button124;
        private System.Windows.Forms.TrackBar trackBar5;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.ComboBox comboBox40;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.Label label93;
        private System.Windows.Forms.TabPage Check_Datos;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.Label label123;
        private System.Windows.Forms.Label labelGenero;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label labelNumero;
        private System.Windows.Forms.Label label121;
        private System.Windows.Forms.Label label122;
        private System.Windows.Forms.Label label124;
        private System.Windows.Forms.Label label128;
        private System.Windows.Forms.Label labelCategoria;
        private System.Windows.Forms.Label labelEvento;
        private System.Windows.Forms.Label labelNombre;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.TextBox textBox39;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.TextBox textBox40;
        private System.Windows.Forms.DateTimePicker dateTimePicker7;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.Label label125;
        private System.Windows.Forms.DateTimePicker dateTimePicker8;
        private System.Windows.Forms.Label label126;
        private System.Windows.Forms.Label label127;
        private System.Windows.Forms.TextBox textBox41;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.GroupBox groupBox28;
        private System.Windows.Forms.TextBox textBox42;
        private System.Windows.Forms.Label label130;
        private System.Windows.Forms.TextBox textBox43;
        private System.Windows.Forms.Label label131;
        private System.Windows.Forms.TextBox textBox45;
        private System.Windows.Forms.Label label133;
        private System.Windows.Forms.Button button127;
        private System.Windows.Forms.TextBox textBox46;
        private System.Windows.Forms.Label label135;
        private System.Windows.Forms.DateTimePicker dateTimePicker9;
        private System.Windows.Forms.ComboBox comboBox52;
        private System.Windows.Forms.TextBox textBox47;
        private System.Windows.Forms.Label label140;
        private System.Windows.Forms.TextBox textBox48;
        private System.Windows.Forms.TextBox textBox44;
        private System.Windows.Forms.Label label129;
        private System.Windows.Forms.TextBox textBox49;
        private System.Windows.Forms.Label label134;
        private System.Windows.Forms.Label label132;
        private System.Windows.Forms.ComboBox comboBox50;
        private System.Windows.Forms.Label label137;
        private System.Windows.Forms.Label label136;
        private System.Windows.Forms.TextBox textBox50;
        private System.Windows.Forms.Button button85;
        private System.Windows.Forms.ComboBox comboBox41;
        private System.Windows.Forms.Label label138;
        private System.Windows.Forms.Label label139;
        private System.Windows.Forms.TextBox textBox51;
    }
}

